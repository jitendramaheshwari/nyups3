package com.aptecllc.oim.matching.scheduledtasks;

//~--- non-JDK imports --------------------------------------------------------

//~--- JDK imports ------------------------------------------------------------
import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import java.util.logging.Logger;

import javax.sql.DataSource;

import oracle.iam.platform.Platform;
import oracle.iam.platform.utils.logging.SuperLogger;
import oracle.iam.reconciliation.api.EventMgmtService;
import oracle.iam.reconciliation.vo.EventConstants;
import oracle.iam.reconciliation.vo.ReconEvent;
import oracle.iam.reconciliation.vo.ReconEventData;
import oracle.iam.reconciliation.vo.ReconSearchCriteria;
import oracle.iam.reconciliation.vo.ReconTargetAttribute;
import oracle.iam.scheduler.vo.TaskSupport;

import com.aptecllc.oim.matching.api.MatchResult;
import com.aptecllc.oim.matching.api.MatchingProvider;
import com.aptecllc.oim.matching.impl.MatchingProviderImpl;
import com.aptecllc.oim.utils.ContextUtil;
import com.google.gson.Gson;

/**
 * Class description
 * 
 * 
 * @version 1.0, 14/07/01
 * @author Eric A. Fisher, APTEC LLC
 * 
 */
public class EvaluateMatches extends TaskSupport {
	private final static Logger LOGGER = SuperLogger
			.getLogger(EvaluateMatches.class.getName());

	/**
	 * Method description
	 * 
	 * 
	 * @param attributes
	 * 
	 * @throws Exception
	 */
	public void execute(HashMap attributes) throws Exception {
		String methodName = "execute";

		LOGGER.entering(getClass().getName(), methodName);
		System.out.println("Inside Fuzzy Match Schedule job");
		String resourceName = (String) attributes.get("Resource Object Name");
		String configFile = (String) attributes.get("Configuration File");
		String oimDataSource = (String) attributes.get("OIM DataSource");
		DataSource dataSource = (DataSource) ContextUtil.getInitialContext()
				.lookup(oimDataSource);
		processFuzzyMatches(resourceName, configFile, dataSource);
	}

	/**
	 * Method description
	 * 
	 * 
	 * @param resourceName
	 * @param configFile
	 * @param dataSource
	 */
	public void processFuzzyMatches(String resourceName, String configFile,
			DataSource dataSource) {
		String methodName = "processFuzzyMatches";
		System.out.println("Inside processFuzzyMatches");

		EventMgmtService eventService = Platform
				.getService(EventMgmtService.class);
		ReconSearchCriteria criteria1 = new ReconSearchCriteria();

		Vector order = new Vector();

		order.add(EventConstants.RECON_EVENT_CREATETIMESTMP);

		boolean ascOrderFlag = true;

		// criteria1.addExpression(EventConstants.RECON_EVENT_KEY, 273,
		// ReconSearchCriteria.Operator.EQUAL);
		criteria1.addExpression(EventConstants.RECON_EVENT_RSRC_NAME,
				resourceName, ReconSearchCriteria.Operator.EQUAL);
		criteria1.addExpression(EventConstants.RECON_EVENT_STATUS,
				EventConstants.STATUS_NOUSR_MATCH,
				ReconSearchCriteria.Operator.EQUAL);
		criteria1.setConjunctionOp(ReconSearchCriteria.Operator.AND);
		System.out.println("searchCriteria:" + criteria1.toString());
		long totalEvents = eventService.getSearchCount(criteria1);

		System.out.println("Number of events to process:" + totalEvents);
		System.out.println("Number of events to process:" + totalEvents);
		List<ReconEvent> bulkReconEvents = eventService.search(criteria1,
				order, ascOrderFlag, 0, (int) totalEvents);

		System.out.println("Config File: " + configFile);
		System.out.println("Number of user to provision: "
				+ bulkReconEvents.size());

		reconEventsProcessor(bulkReconEvents, configFile, dataSource,
				resourceName);

	}

	private void reconEventsProcessor(List<ReconEvent> bulkReconEvents,
			String matchConfigPath, DataSource dataSource, String resourceName) {
		try {
			EventMgmtService eventService = Platform
					.getService(EventMgmtService.class);
			System.out.println("Before Calling MatchingProvider:");
			/*
			 * DataSource dataSource = (DataSource) ContextUtil
			 * .getInitialContext().lookup(dataSource2);
			 */
			MatchingProvider matchingProvider = new MatchingProviderImpl(
					matchConfigPath, dataSource, resourceName);
			System.out.println("After Calling MatchingProvider:");
			for (ReconEvent event : bulkReconEvents) {
				Object rekey = event.getReKey();
				System.out.println("Processing Reconciliation Event: "
						+ event.getReKey());
				try {
					ReconEventData eventData = eventService
							.getReconEventData(event);
					List<ReconTargetAttribute> reconAttributes = eventData
							.getSingleValuedAttrs();
					HashMap<String, Object> sourceDataSet = new HashMap<String, Object>();

					for (ReconTargetAttribute reconAttribute : reconAttributes) {
						String key = reconAttribute.getTadName();

						System.out
								.println("SourceFieldName: "
										+ key
										+ " SourceFieldName: "
										+ reconAttribute
												.getOimMappedFieldDescription());
						System.out.println("Type: "
								+ reconAttribute.getTadDatatype());

						Object val = null;

						if (reconAttribute.getTadDatatype().equals("Date")) {
							val = reconAttribute.getDateVal();
						} else {
							val = reconAttribute.getStringVal();
						}

						System.out.println("value: " + val);
						sourceDataSet.put(key, val);
					}
					Gson gson = new Gson();
					System.out.println("ReconEventData json:"
							+ gson.toJson(eventData));
					sourceDataSet.put("Source", event.getResourceName());
					sourceDataSet.put("ReconEventKey", rekey);
					sourceDataSet.put("ReconEventData", eventData);
					System.out.println("Source: " + event.getResourceName()
							+ " REKEY " + rekey);

					if (("No User Match Found".equals(event.getReStatus()))) {
						MatchResult matchresult = matchingProvider.match(
								(Long) rekey, sourceDataSet, resourceName);

						System.out.println("MatchResult is :" + matchresult);
					}
				} catch (Exception ex) {
					System.out.println("Event Processing Exception: " + rekey);
					System.out.println(ex.getMessage());
					ex.printStackTrace();
				}
			}
		} catch (Exception ex) {
			System.out.println("MatchThreadException");
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		}
	}

	@Override
	public HashMap getAttributes() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setAttributes() {
		// TODO Auto-generated method stub

	}
}
