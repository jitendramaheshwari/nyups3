
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;

import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.web.client.RestTemplate;

import com.aptecllc.http.client.TokenAuthHeaderRequestInterceptor;
import com.aptecllc.iam.selfsvc.OIMRestClient;
import com.aptecllc.iam.selfsvc.vo.Notification;
import com.aptecllc.iam.selfsvc.vo.UserAccount;
import com.aptecllc.iam.selfsvc.vo.UserRegistration;
public class Test1 {
    public static void main(String args[]) throws ParseException {
        // String geturl =
        // "http://boc22oimdws.fau.edu/OIMRESTServices/api/v1/user/{userName}";
        String geturl = "http://oialcdcdvm001.nyumc.org:14000/OIMRESTServices";
        String pApiKey = "SelfService";
        String pSharedSecret = "nyuL4ng0n3";
        String pOperationUsername = "xelsysadm";

		ClientHttpRequestInterceptor tokenAuthInterceptor = new TokenAuthHeaderRequestInterceptor(
				pApiKey, pSharedSecret, pOperationUsername);

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setInterceptors(Collections
				.singletonList(tokenAuthInterceptor));
		UserRegistration userRegistration = new UserRegistration();
		userRegistration.setFirstName("Abbey");
		userRegistration.setLastName("Segal");
		userRegistration.setDobDay("8");
		userRegistration.setDobMonth("01");
		userRegistration.setDobYear("1959");
		String dob = userRegistration.getDobYear()
				+ userRegistration.getDobMonth() + userRegistration.getDobDay();
		SimpleDateFormat dobFormat = new SimpleDateFormat("yyyyMMd");
		SimpleDateFormat format = new SimpleDateFormat("MMM dd,yyyy");
		Date dobDate = dobFormat.parse(dob);
		/*
		 * System.out.println("format.format(dobDate):" +
		 * format.format(dobDate)); Date dob2 =
		 * format.parse(format.format(dobDate)); System.out.println("dob2 :" +
		 * dob2);
		 */
		userRegistration.setDOB(dobDate);
		
		//Notification notification=new Notification();
		//notification.setRecipient("12014525576");
		//notification.setMessage("test");
		String query = userRegistration.buildSearchQuery();
		System.out.println("query:" + query);
		restTemplate.postForLocation(geturl + OIMRestClient.ServiceURLs.ENABLE_USER.getUrl(),
                "MKZ100");
		//restTemplate.postForObject(geturl + OIMRestClient.ServiceURLs.SEND_SMS,notification, Notification.class);
		/*UserAccount oimUserArray = restTemplate.getForObject(geturl
				+ OIMRestClient.ServiceURLs.SEND_SMS,
				UserAccount.class);

		System.out.println("completed successfully:" + oimUserArray);*/

		System.out.println("Successfully updated");
	}
}
