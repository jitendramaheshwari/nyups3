package com.aptecllc.iam.selfsvc.services;

//~--- non-JDK imports --------------------------------------------------------

import com.aptecllc.iam.selfsvc.OIMRestClient;
import com.aptecllc.iam.selfsvc.exception.UserCreateException;
import com.aptecllc.iam.selfsvc.exception.UserNotFoundException;
import com.aptecllc.iam.selfsvc.exception.UserUpdateException;
import com.aptecllc.iam.selfsvc.utils.*;
import com.aptecllc.iam.selfsvc.vo.Notification;
import com.aptecllc.iam.selfsvc.vo.OTP;
import com.aptecllc.iam.selfsvc.vo.SecurityQuestions;
import com.aptecllc.iam.selfsvc.vo.SponsoredAccount;
import com.aptecllc.iam.selfsvc.vo.UserAccount;
import com.aptecllc.iam.selfsvc.vo.UserRegistration;
import com.aptecllc.iam.selfsvc.vo.UserRequest;
import com.aptecllc.iam.selfsvc.vo.UserSelfService;

//import oracle.iam.identity.usermgmt.vo.User;
//import oracle.iam.request.vo.RequestEntityAttribute;

import org.apache.log4j.Logger;
import org.springframework.binding.message.MessageBuilder;
import org.springframework.binding.message.MessageContext;
/*import org.springframework.binding.message.MessageResolver;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
*/
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.webflow.execution.RequestContext;

//~--- JDK imports ------------------------------------------------------------


//import org.springframework.ws.mime.MimeMessage;















import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;




import java.util.Set;

/*import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
*/

/**
 * Class description
 *
 *
 * @version        1.1, 13/10/18
 * @author         Eric A. Fisher, APTEC LLC
 */
public class OIMUserServicesClient {
    private final static Logger      logger = Logger.getLogger(OIMUserServicesClient.class);
    private OIMRestClient            oimAdminClient;
    private String                   serverUrl;
    private String                   apiKey;
    private String                   sharedSecret;
    private String                   adminUserLogin;
    private BaseSelfServiceUtilities selfServiceUtilities;


    /**
     * Constructs ...
     *
     */
    public OIMUserServicesClient() {}

    /**
     * Constructs ...
     *
     *
     * @param pServerUrl
     * @param pApiKey
     * @param pSharedSecret
     * @param pAdminLogin
     * @param selfUtilities
     */
    public OIMUserServicesClient(String pServerUrl, String pApiKey, String pSharedSecret, String pAdminLogin,
                                 BaseSelfServiceUtilities selfUtilities) {
        serverUrl            = pServerUrl;
        apiKey               = pApiKey;
        sharedSecret         = pSharedSecret;
        adminUserLogin       = pAdminLogin;
        selfServiceUtilities = selfUtilities;

        //System.out.println("***URL" + serverUrl + " KEY " + pApiKey + " Secret " + pSharedSecret + " login " +  pAdminLogin);
        System.out.println("Inside OIMUserServicesClient Constructor");
        oimAdminClient = new OIMRestClient(serverUrl, apiKey, sharedSecret, adminUserLogin);
        
        //tcLookupOperationsIntf lookupOpsIntf = Platform.getService(tcLookupOperationsIntf.class);
        
        
    }
    

    /**
     * Method description
     *
     *
     * @param pUserID
     *
     * @return
     * @throws UserNotFoundException
     */
    public UserSelfService getUserSelfService(String pUserID) throws UserNotFoundException {
        logger.trace("getUserSelfService" + pUserID);
        System.out.println("Inside OIMUserServicesClient getUserSelfService()"+pUserID); 
        UserSelfService uss = new UserSelfService();
        
        

        uss.setUserLogin(pUserID);
        logger.trace(serverUrl + OIMRestClient.ServiceURLs.GET_USER_CHALLENGES.getUrl());

        ArrayList<String> challenges = null;

        try {
            challenges = oimAdminClient.getRestTemplate().getForObject(serverUrl+ OIMRestClient.ServiceURLs.GET_USER_CHALLENGES.getUrl(), ArrayList.class, pUserID);
        } catch (HttpClientErrorException e) {
            logger.trace("HCEE  ERROR " + e.getResponseBodyAsString() + e.getStatusText());

            MessageBuilder builder = new MessageBuilder();

            // context.getMessageContext().addMessage(builder.error().code("Error."
            // + e.getResponseBodyAsString().replaceAll(" ", "_")).defaultText(e.getResponseBodyAsString()).build());
            throw new UserNotFoundException("User Not Found");
        } catch (Exception e) {
            logger.trace(e.getMessage());
        }

        logger.trace(challenges);

       /* HashMap questionMap = new HashMap();

        for (String question : challenges) {
            questionMap.put(question, "");
        }

        uss.setQuestions(questionMap);
        logger.trace(uss.toString());*/

        return uss;
    }

    /**
     * Method description
     *
     *
     * @param pUSS
     * @param questions
     * @param context
     *
     * @return
     */
    public String setUserChallenges(UserSelfService pUSS, SecurityQuestions questions, RequestContext context) {
        pUSS.setQuestions(questions.getQuestionMap());
        logger.trace(pUSS.getQuestions());

        OIMRestClient oimClient = getUserOIMRestClient(pUSS.getUserLogin());

        try {
            logger.trace("setUserChallenges: Attempting Set");
            oimClient.getRestTemplate().put(serverUrl + OIMRestClient.ServiceURLs.SET_USER_CHALLENGES.getUrl(), pUSS,
                                            pUSS.getUserLogin());
        } catch (HttpClientErrorException e) {
            logger.trace("HCEE PUT ERROR " + e.getResponseBodyAsString() + e.getStatusText());

            MessageBuilder builder = new MessageBuilder();

            context.getMessageContext().addMessage(builder.error().code("Error."
                    + e.getResponseBodyAsString().replaceAll(" ",
                        "_")).defaultText(e.getResponseBodyAsString()).build());

            return "QuestionSetFailed";
        } catch (Exception e) {
            logger.trace("PUT ERROR " + e.getMessage());

            return "Error";
        }

        return "QuestionsSet";
    }

    /**
     * Method description
     *
     *
     * @param pUSS
     * @param context
     *
     * @return
     */
    public String setUserPassword(UserSelfService pUSS, RequestContext context) {
        OIMRestClient oimClient = getUserOIMRestClient(pUSS.getUserLogin());

        pUSS.setQuestions(null);

        try {
            logger.trace("setUserPassword: Attempting Set");
            oimClient.getRestTemplate().put(serverUrl + OIMRestClient.ServiceURLs.CHANGE_USER_PASSWORD.getUrl(), pUSS,
                                            pUSS.getUserLogin());
        } catch (HttpClientErrorException e) {
            logger.trace("HCEE PUT ERROR " + e.getResponseBodyAsString() + e.getStatusText());

            MessageBuilder builder = new MessageBuilder();

            context.getMessageContext().addMessage(builder.error().code("Error."
                    + e.getResponseBodyAsString().replaceAll(" ",
                        "_")).defaultText(e.getResponseBodyAsString()).build());

            return "PasswordSetFailed";
        } catch (Exception e) {
            logger.trace("PUT ERROR " + e.getMessage());

            return "Error";
        }

        return "PasswordSet";
    }

    /**
     * Method description
     *
     *
     * @param pUSS
     * @param context
     *
     * @return
     */
    public String resetUserPassword(UserSelfService pUSS, RequestContext context) {
        try {
            logger.trace("resetUserPassword: Attempting Reset");
            oimAdminClient.getRestTemplate().put(serverUrl + OIMRestClient.ServiceURLs.CHANGE_USER_PASSWORD.getUrl(),
                    pUSS, pUSS.getUserLogin());
        } catch (HttpClientErrorException e) {
            logger.trace("PUT ERROR " + e.getResponseBodyAsString());

            MessageBuilder builder = new MessageBuilder();

            context.getMessageContext().addMessage(builder.error().code("Error."
                    + e.getResponseBodyAsString().replaceAll(" ",
                        "_")).defaultText(e.getResponseBodyAsString()).build());

            return e.getResponseBodyAsString();
        } catch (Exception e) {
            logger.trace("PUT ERROR " + e.getMessage());

            return "Error";
        }

        logger.trace("resetUserPassword: Success");

        return "Success";
    }

    /**
     * Method description
     *
     *
     *
     * @param searchQuery
     * @param context
     *
     * @return
     *
     * @throws Exception
     * @throws UserNotFoundException
     */

//  public String normalizeAddress(UserRegistration userReg, RequestContext context) {
//      Address outAddress = null;
//
//      logger.trace("Normalize Address" + userReg.getAddress().getAddressLine1());
//
//      try {
//          outAddress = AddressServiceDelegate.validateAddress(userReg.getAddress());
//      } catch (Exception e) {
//          logger.trace(e.getMessage());
//      }
//
//      logger.trace("Format Status" + outAddress.getFormatStatusCode());
//
//      if ("U".equals(outAddress.getFormatStatusCode())) {
//
//          // Unable to Validate
//          return "UnableToNormalize";
//      }
//
//      userReg.getAddress().setFormatAddress(outAddress.getFormatAddress());
//      userReg.getAddress().setZipCode5(outAddress.getZipCode5());
//      userReg.getAddress().setZipCode4(outAddress.getZipCode4());
//
//      return "Normalized";
//  }
    public UserAccount searchForOIMUser(String searchQuery, RequestContext context)
            throws UserNotFoundException, Exception {
        UserAccount oimUser = null;

        try {
            logger.trace("Searching for : " + searchQuery);
            System.out.println("SEARCHING FOR-->"+searchQuery);

            UserAccount[] oimUserArray = oimAdminClient.getRestTemplate().getForObject(serverUrl
                                             + OIMRestClient.ServiceURLs.SEARCH_USERS.getUrl(), UserAccount[].class,
                                                 searchQuery);
            List<UserAccount> oimUsers = Arrays.asList(oimUserArray);

            if (oimUsers.isEmpty()) {
                MessageBuilder builder = new MessageBuilder();

                context.getMessageContext().addMessage(
                    builder.error().code("Error.UserNotFound").defaultText("User Not Found").build());

                throw new UserNotFoundException("User Not Found");
            } else if (oimUsers.size() > 1) {
                throw new Exception("Multiple Users Found");
            } else {
                oimUser = (UserAccount) oimUsers.get(0);
            }
        } catch (HttpClientErrorException e) {
            logger.trace("HCEE ERROR " + e.getResponseBodyAsString() + e.getStatusText());

            MessageBuilder builder = new MessageBuilder();

            // context.getMessageContext().addMessage(builder.error().code("Error." + e.getResponseBodyAsString().replaceAll(" ", "_")).build());
            throw new UserNotFoundException(e.getStatusText());
        } catch (UserNotFoundException e) {
            throw e;
        } catch (Exception e) {
            logger.trace("REST ERROR " + e.getMessage());

            throw new Exception(e.getMessage());
        }

        logger.trace(oimUser);
        System.out.println("oimUser is----------->"+oimUser);
        return oimUser;
    }

    public String searchForOIMUserforAccountClaiming(UserAccount oimUser, RequestContext context)
            throws UserNotFoundException, Exception {
        //UserAccount oimUser = null;
        String searchQuery="";
        MessageContext messages = context.getMessageContext();
        try {
            System.out.println("inside searchForOIMUserforAccountClaiming");
            System.out.println("terms inside searchForOIMUserforAccountClaiming is "+oimUser.getTerms());
            System.out.println("DOB inside searchForOIMUserforAccountClaiming is "+oimUser.getDOB());

            
            StringBuilder    query = new StringBuilder();
            SimpleDateFormat sdf   = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");

            query.append("userName==").append(oimUser.getUserName()).append(";dateOfBirth==").append(sdf.format(new Date(oimUser.getDOB())));
           
            
            //searchQuery.concat(("userName=="+oimUser.getUserName()+";dateOfBirth=="+sdf.format(new Date(oimUser.getDOB()))));
            searchQuery=query.toString();
            System.out.println("SEARCHING FOR-->"+searchQuery);
            System.out.println("1");
            UserAccount[] oimUserArray = oimAdminClient.getRestTemplate().getForObject(serverUrl
                                             + OIMRestClient.ServiceURLs.SEARCH_USERS.getUrl(), UserAccount[].class,
                                                 searchQuery);
            System.out.println("2");
            List<UserAccount> oimUsers = Arrays.asList(oimUserArray);

            if (oimUsers.isEmpty()) {
            	oimUser.setTerms("terms"); 
            	 messages.addMessage(
     	                new MessageBuilder().error().source("terms").defaultText("Please enter the correct date of birth to continue").build());

                throw new UserNotFoundException("User Not Found");
            } else if (oimUsers.size() > 1) {
                throw new Exception("Multiple Users Found");
            } else {
                oimUser = (UserAccount) oimUsers.get(0);
            }
        } catch (HttpClientErrorException e) {
            logger.trace("HCEE ERROR " + e.getResponseBodyAsString() + e.getStatusText());

            

            // context.getMessageContext().addMessage(builder.error().code("Error." + e.getResponseBodyAsString().replaceAll(" ", "_")).build());
            throw new UserNotFoundException(e.getStatusText());
        } catch (UserNotFoundException e) {
            throw e;
        } catch (Exception e) {
            logger.trace("REST ERROR " + e.getMessage());

            throw new Exception(e.getMessage());
        }

        logger.trace(oimUser);
        System.out.println("oimUser is----------->"+oimUser);
        return "User Found";
    }

    /**
     * Method description
     *
     *
     *
     * @param identifier
     *
     * @return
     *
     * @throws Exception
     * @throws UserNotFoundException
     */
    public UserAccount findOIMUserByID(String identifier) throws UserNotFoundException, Exception {
        UserAccount oimUser = null;

        try {
            logger.trace("Searching for : " + identifier);
            System.out.println("Inside findOIMUserByID"+identifier);
            oimUser = oimAdminClient.getRestTemplate().getForObject(serverUrl
                    + OIMRestClient.ServiceURLs.GET_USER_BY_ID.getUrl(), UserAccount.class, identifier);
        } catch (HttpClientErrorException e) {
            logger.trace("HCEE ERROR " + e.getResponseBodyAsString() + e.getStatusText());

            MessageBuilder builder = new MessageBuilder();

            // context.getMessageContext().addMessage(builder.error().code("Error." + e.getResponseBodyAsString().replaceAll(" ", "_")).build());
            throw new UserNotFoundException(e.getStatusText());
        } catch (Exception e) {
            logger.trace("ERROR " + e.getMessage());

            throw new Exception(e.getMessage());
        }

        logger.trace(oimUser);

        return oimUser;
    }
    
/***************Sponsored Individual Tool Additions******************
 * 	----------------->Dan Donnelly, Sprint 7-8
 * 	
 * 	1. 	checkUserUpdate(login, first name, last name)
 * 		Check function to decide what action to proceed with in request 
 * 		user flow.xml. Check based on first and last name as well as 
 * 		the posited former Kerberos ID, will return a Create, Modify,
 * 		or a Create because of failed name match with the login
 * 
 * 	2.	updateEnableOIMUser(SponsoredAccount)
 * 		Update function that takes all the attributes garnered from 
 * 		the request user views and maps them back to an former user.
 * 
 */    
    
    
    /**
     *Method that checks to see if a Sponsored account is to be created or updated
     *
     *----->Dan Donnelly
     *
     *@param userlogin
     *@param fname
     *@param lname
     *
     *@return status of next action
     *
     */
    public String checkUserUpdate(String userlogin, String fname, String lname){
    	System.out.println("\n\n----------------->Inside Boolean check for Sponsored Individual, USR: "+userlogin+"\n\n");
    	//First case, no former ID entered, create new user
    	if(userlogin.isEmpty() || userlogin.equals(null)){
    			
    			return "CreateNewUser";
    			
    	}else{
    		//Search OIM for user by login
    		UserAccount retacct = null;
    		System.out.println("\n\n----------------->Searching for Sponsored Individual, USR: "+userlogin+"\n\n");
    		try{
    			retacct = oimAdminClient.getRestTemplate().getForObject(serverUrl
    					+ OIMRestClient.ServiceURLs.GET_USER_BY_LOGIN.getUrl(), UserAccount.class, userlogin);
    		}catch(Exception e){
    			e.printStackTrace();
    		}
    		
    		//If account found, check names. If names match, update, otherwise, create
    		if(retacct!=null){
    			System.out.println("\n\n----------------->Search is not null, USR: "+userlogin+"\n\n");
    			
    			if(retacct.getFirstName().equalsIgnoreCase(fname) 
        			&& retacct.getLastName().equalsIgnoreCase(lname)){
    				return "UpdateUser";
    			}else{
    				return "CreateUserInvalidUserName";
    			}
    		}
    		//Unreachable, but at worst they'll get a new account
    		return "CreateNewUser";
    	}
    } 
    
    /**
     * Method that updates and enables a former user with their new information
     * gotten from the request user flow
     * 
     * ----->Dan Donnelly
     * 
     * @param SponsoredAccount with new user information in it
     * 
     * @return SUCCESS code
     * 
     */
    public String updateEnableOIMUser(SponsoredAccount user){
    	System.out.println("\n\n--------------->Inside of update/enable Sponsored Account\n\n");
    	
    	
    	//Retrieve the UserAccount for the username received from SponsorNetId field
    	UserAccount toUpdate = null;
    	String login = user.getSponsorNetID();
    	System.out.println("************updateEnableOIMUser**********"+login);
    	
    	try{
    		toUpdate= getOIMUser(login);
    		System.out.println("\n\nUser to string: \n"+user.toString()+"\n\n");
    	} catch(UserNotFoundException e1){
    		e1.printStackTrace();
    	}catch (Exception e){
    		e.printStackTrace();
    	}
    	
    	//Get the attributes from that account, remove and store status since it can't 
    	//be altered until the dates are properly entered in OIM, set the dates
    	
    	HashMap<String,Object> attrs = toUpdate.getAttributes();
    	String disable = (String) attrs.remove("status");
    	
    	attrs.put("endDate", user.getEndDate());
    	attrs.put("startDate", user.getStartDate()); 				
    	attrs.put("dateOfBirth", user.getDOB());
    	attrs.put("provisioningDate", (Date)null);
    	//added on 02/17 to nullify Termination Date in OIM when a sponsored user is rehired
    	//attrs.put("terminationDate", (Date)null);
    	
    	System.out.println("\n\n-------------->Attributes for first update:\n"+attrs.toString()+"\n\n");
    	
    	//Set the new account's somewhat updated attributes and login
    	toUpdate = new UserAccount();
    	toUpdate.setAttributes(attrs);
    	toUpdate.setUserName(login);
    	
    	//Update the user
    	String updateresponse = null;
    	try {
			updateresponse = updateOIMUser(toUpdate);
		} catch (UserUpdateException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    	//Enable the user with REST function
    	String enableresponse = null;
    	if(disable.equalsIgnoreCase("Disabled")){
    		enableresponse = oimAdminClient.getRestTemplate().getForObject(serverUrl + 
    				OIMRestClient.ServiceURLs.ENABLE_USER.getUrl(),String.class , login);
    	}
    	else{
    		System.out.println("\n\n--------------->Account "+login+" already enabled \n\n");
    	}
    	
    	//Set the rest of the newly entered attributes
    	System.out.println("Setting rest of newly enetered attributes");
    	toUpdate.setAttribute("personalEmail", user.getPersonalEmail());
    	toUpdate.setAttribute("phoneNumber",user.getTelephone());
    	toUpdate.setAttribute("sponsor", user.getSponsor());
    	//added on 06/07/2017
    	System.out.println("Before updating company to null ---------------------------------------------------------------------");
    	toUpdate.setAttribute("company", null);
    	toUpdate.setAttribute("company", "");
    	//added on 06/07/2017 
    	
    	// Duo Bug Fix 11/29/2016
    	        
        UserAccount sponsoraccount = null;
        try{
        	sponsoraccount=getOIMUser((String) user.getSponsor());
        	System.out.println("While rehire a user as sponsored individual, admin department of sponsor is:"+sponsoraccount.getAttribute("AdminDept"));
        	toUpdate.setAttribute("AdminDept", sponsoraccount.getAttribute("AdminDept"));
        	
        	
            }catch(Exception e)
        {e.printStackTrace();}
    	// EOF Duo Bug Fix 11/29/2016
    	
    	toUpdate.setAttribute("vendor",user.getVendor());
    	toUpdate.setAttribute("externalaffiliation", user.getExternalaffiliation());
    	toUpdate.setAttribute("emailRequired", user.getEmailRequired());
    	//toUpdate.setAttribute("terminationDate", null);
    	//toUpdate.setAttribute("olduserstatus", "Disabled");
    	System.out.println("Before updating the user type in OIM");
    	toUpdate.setAttribute("role", "Contractor");
    	System.out.println("After updating the user type in OIM");
    	
    	System.out.println("After Nullifying terminationdate");
    	
    	
    	//Update the user for the final time
    	HashMap<String, Object> attrMap = toUpdate.getAttributes();
    	System.out.println("\n\n---------------->Attributes for Second update:\n"
    			+attrMap.toString()+'\n');
    	try {
			updateOIMUser(toUpdate);
		} catch (UserUpdateException e) {
			System.out.println("\n\n--------------->User Update Exception in UserAccount\n\n");
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("\n\n--------------->Rando Update Exception in UserAccount\n\n");
			e.printStackTrace();
		}
  	
    	return "SUCCESS";
    }
    
/*END Dan Donnelly-------------------------------------------------------------------------------------------*/
    
    /**
     * Method description
     *
     *
     * @param userLogin
     *
     * @return
     *
     * @throws Exception
     * @throws UserNotFoundException
     */
    public UserAccount getOIMUser(String userLogin) throws UserNotFoundException, Exception {
        UserAccount oimUser = null;
        System.out.println("INSIDE getOIMUser method to authenticate User");
        try {
            logger.trace("Searching for : " + userLogin);
            System.out.println("Searching for : " + userLogin);
            oimUser = oimAdminClient.getRestTemplate().getForObject(serverUrl
                    + OIMRestClient.ServiceURLs.GET_USER_BY_LOGIN.getUrl(), UserAccount.class, userLogin);
        } catch (HttpClientErrorException e) {
            logger.trace("HCEE ERROR " + e.getResponseBodyAsString() + e.getStatusText());

            MessageBuilder builder = new MessageBuilder();

            // context.getMessageContext().addMessage(builder.error().code("Error." + e.getResponseBodyAsString().replaceAll(" ", "_")).build());
            throw new UserNotFoundException(e.getStatusText());
        } catch (Exception e) {
            logger.trace("ERROR " + e.getMessage());

            throw new Exception(e.getMessage());
        }

        logger.trace(oimUser);
        System.out.println("Oimuser"+oimUser);
        System.out.println("Role of this user is:"+oimUser.getAccountId());
        return oimUser;
    }
    
    public Map getLookupValues(String lookupName) throws UserNotFoundException, Exception {
        UserAccount oimUser = null;
        String updatedUsr = null;
        String returnCode = "";
        String Status = "SUCCESS";
        Map<String,String> lookupPairs= new HashMap<String,String>();
        
        try {
            System.out.println("Lookup name passed is : " + lookupName);
            lookupPairs = oimAdminClient.getRestTemplate().getForObject(serverUrl
                    + OIMRestClient.ServiceURLs.LOOKUP.getUrl(), Map.class, lookupName);
         
        }  catch (Exception e) {
        	System.out.println("ERROR " + e.getMessage());
            returnCode="Failure";
            throw new Exception(e.getMessage());
        }

        System.out.println("Final return size from getLookupValues is "+lookupPairs.size());

        return lookupPairs;
        
    }
    public String checkOIMUserExistandActivate(String userLogin) throws UserNotFoundException, Exception {
        UserAccount oimUser = null;
        String updatedUsr = null;
        String returnCode = "";
        String Status = "SUCCESS";
        
        try {
            System.out.println("Searching for user in checkOIMUserExistandActivate : " + userLogin);
            oimUser = oimAdminClient.getRestTemplate().getForObject(serverUrl
                    + OIMRestClient.ServiceURLs.GET_USER_BY_LOGIN.getUrl(), UserAccount.class, userLogin);
            if(oimUser!=null){
            	Status=oimUser.getStatus();
            	if(Status.equalsIgnoreCase("Disabled")){
            		//oimUser.setAttribute("Status", "Active");
            		//updateOIMUser(selfServiceUtilities.getAccountStatusUpdate(oimUser,"Active"));
            		System.out.println("user is disabled URL"+serverUrl+OIMRestClient.ServiceURLs.ENABLE_USER.getUrl());
            		
            		updatedUsr = oimAdminClient.getRestTemplate().getForObject(serverUrl + OIMRestClient.ServiceURLs.ENABLE_USER.getUrl(),String.class , userLogin);
            		System.out.println("After user is enabled");
            		//updateOIMUser(oimUser);
            	 returnCode=updatedUsr;
            	}
            }
        }  catch (Exception e) {
        	System.out.println("ERROR " + e.getMessage());
            returnCode="Failure";
            throw new Exception(e.getMessage());
        }

        System.out.println("Final return code from checkOIMUserExistandActivate"+returnCode);

        return returnCode;
    }
    
    

    /**
     * Method description
     *
     *
     * @param pUSS
     * @param context
     *
     * @return
     */
    public String validatePassword(UserSelfService pUSS, RequestContext context) {
        try {
            logger.trace("validatePassword: Attempting Validate");
            System.out.println("Inside validatePassword method upon entering the password"+serverUrl+"getUrl"+OIMRestClient.ServiceURLs.VALIDATE_USER_PASSWORD.getUrl()+"pUSS"+pUSS+"user login"+pUSS.getUserLogin());
            oimAdminClient.getRestTemplate().put(serverUrl + OIMRestClient.ServiceURLs.VALIDATE_USER_PASSWORD.getUrl(),
                    pUSS, pUSS.getUserLogin());
            System.out.println("after getRestTemplate"+pUSS.getNewPassword()+"confirm new password"+pUSS.getConfirmPassword());
        } catch (HttpClientErrorException e) {
            logger.trace("HCEE PUT ERROR " + e.getResponseBodyAsString());
            System.out.println("HCEE PUT ERROR " + e.getResponseBodyAsString());

            MessageBuilder builder       = new MessageBuilder();
            String[]       violatedRules = e.getResponseBodyAsString().split("\\|");

            for (String rule : violatedRules) {
                context.getMessageContext().addMessage(builder.error().source("newPassword").defaultText(rule).build());
            }

            return "PasswordNotValid";
        } catch (Exception e) {
            logger.trace("PUT ERROR " + e.getMessage());

            return "Error";
        }

        return "PasswordValid";
    }

    /**
     * Method description
     *
     *
     * @param oimUser
     *
     * @return
     */
    public UserSelfService getNewUserSelfService(UserAccount oimUser) {
        logger.trace("Setting USS" + oimUser.getUserName());
        System.out.println("Inside getNewUserSelfService()");
        return new UserSelfService(oimUser.getUserName());
    }

    /**
     * Method description
     *
     *
     * @return
     *
     * @throws Exception
     */
    public List getSystemChallenges() throws Exception {
        List challenges = null;

        try {
            logger.trace("getSystemChallenges");
            challenges = oimAdminClient.getRestTemplate().getForObject(serverUrl
                    + OIMRestClient.ServiceURLs.GET_SYSTEM_CHALLENGES.getUrl(), List.class);
        } catch (HttpClientErrorException e) {
            logger.trace("HCEE  ERROR " + e.getResponseBodyAsString());

            throw new Exception(e.getMessage());
        } catch (Exception e) {
            logger.trace("PUT ERROR " + e.getMessage());

            throw new Exception(e.getMessage());
        }

        logger.trace(challenges);

        return challenges;
    }

    private OIMRestClient getUserOIMRestClient(String userLogin) {
    	System.out.println("Inside getUserOIMRestClient()");
        return new OIMRestClient(serverUrl, apiKey, sharedSecret, userLogin);
    }

    /**
     * Method description
     *
     *
     * @param userReg
     *
     * @return
     *
     * @throws Exception
     */
    public UserAccount createNewOIMUser(UserRegistration userReg) throws Exception {
        UserAccount oimUser = null;
        UserAccount newUser = selfServiceUtilities.getNewAccountFromRegistration(userReg);

        try {
            logger.trace("createNewOIMUser: Attempting Post");

            URI objUri = oimAdminClient.getRestTemplate().postForLocation(serverUrl
                             + OIMRestClient.ServiceURLs.CREATE_USER.getUrl(), newUser);

            logger.trace("createNewOIMUser: " + objUri.toString());
            oimUser = oimAdminClient.getRestTemplate().getForObject(objUri, UserAccount.class);
            logger.trace(oimUser);
        } catch (HttpClientErrorException e) {
            logger.trace("HCEE PUT ERROR " + e.getResponseBodyAsString());

            throw new UserCreateException(e.getStatusText());
        } catch (Exception e) {
            logger.trace("PUT ERROR " + e.getMessage());

            throw new Exception(e.getMessage());
        }

        return oimUser;
    }

    /**
     * Method description
     *
     *
     * @param sponsoredAcct
     * @param requestingUser
     *
     * @return
     *
     * @throws Exception
     */
    public String requestNewOIMUser(SponsoredAccount sponsoredAcct, String requestingUser) throws Exception {
        String        reqId     = null;
        UserAccount   newUser   = selfServiceUtilities.getNewAccountFromSponsoredAccount(sponsoredAcct);
        OIMRestClient oimClient = getUserOIMRestClient(requestingUser);

        try {
            logger.trace("requestNewOIMUser: Attempting Post");
            System.out.println("requestNewOIMUser: Attempting PostNYU");
            System.out.println("kerberos ID or NetID entered by Helpdesk team is:"+newUser.getSponsorNetID());
        /*   if(newUser.getSponsorNetID()!=null){
            String   retMsg = checkOIMUserExistandActivate(newUser.getSponsorNetID());
            System.out.println("Return message from checkOIMUserExistandActivate is:"+retMsg);
		            if(retMsg.equalsIgnoreCase("SUCCESS")){
		            	System.out.println("User found and activated");
		            	return "SUCCESS";
		            
		            }
		            else{
		            	System.out.println("There is no such user or the user is Active :"+newUser.getSponsorNetID());
		            }
		            return "SUCCESS";
            }*/
           
            UserRequest req = new UserRequest();
            System.out.println("1");
            req.setRequestObject(newUser);
            System.out.println("2");
            req.setRequestingUser(requestingUser);
            System.out.println("3");

            URI objUri = oimClient.getRestTemplate().postForLocation(this.serverUrl
                             + OIMRestClient.ServiceURLs.REQUEST_USER.getUrl(), req, new Object[0]);

            logger.trace("requestNewOIMUser: " + objUri.toString());
            System.out.println("requestNewOIMUser: " + objUri.toString());
            String reqUrl = objUri.toString();

            reqId = "/identity/faces/home?tf=request_details&requestId="
                    + reqUrl.substring(reqUrl.lastIndexOf("/") + 1);
        } catch (HttpClientErrorException e) {
            logger.trace("HCEE PUT ERROR " + e.getResponseBodyAsString());

            throw new UserCreateException(e.getStatusText());
        } catch (Exception e) {
            logger.trace("PUT ERROR " + e.getMessage());

            throw new Exception(e.getMessage());
        }
        
        return reqId;
    }
    
    
    /*
     *Need to be able to pass the value from jsp to OIMRestClient and back again 
     * 
     * 
     * 
     
    public Boolean checkUpdate(SponsoredAccount sponsoredAcct, String requestingUser) throws Exception {
        String        reqId     = null;
        UserAccount   newUser   = selfServiceUtilities.getNewAccountFromSponsoredAccount(sponsoredAcct);
        OIMRestClient oimClient = getUserOIMRestClient(requestingUser);
        
        
        
    	return false;
    }*/
    /**
     * Method description
     *
     *
     *
     * @param account
     *
     * @return
     *
     * @throws Exception
     * @throws UserUpdateException
     */
    public String updateOIMUser(UserAccount account) throws UserUpdateException, Exception {
        try {
            logger.trace("updateOIMUser: Attempting Put");
            System.out.println("Inside updateOIMUser in OIMUserServicesClient");
            oimAdminClient.getRestTemplate().put(serverUrl + OIMRestClient.ServiceURLs.UPDATE_USER_BY_LOGIN.getUrl(),
                    account, account.getUserName());
        } catch (HttpClientErrorException e) {
            logger.trace("HCEE PUT ERROR " + e.getResponseBodyAsString());
            System.out.println("HCEE PUT ERROR " + e.getResponseBodyAsString());
            throw new UserUpdateException(e.getStatusText());
        } catch (Exception e) {
            logger.trace("PUT ERROR " + e.getMessage());
            System.out.println("PUT ERROR " + e.getMessage());
            throw new Exception(e.getMessage());
        }

        return "Update Successful";
    }

    
    //adeed on Dec 8th 2017
    
    public String updateOIMUserNYU(HashMap nyuaccount) throws UserUpdateException, Exception {
    	String prehiretype="";
    	String usertype="";
        try {
            logger.trace("updateOIMUserNYU: Attempting Put");
            System.out.println("Inside updateOIMUserNYU in OIMUserServicesClient");
            Iterator i = nyuaccount.keySet().iterator();
            UserAccount key = null;
            String value = null;
            //String prehiretype="";
            while(i.hasNext())
            {
                 key = (UserAccount) i.next();  
                 value = (String) nyuaccount.get(key);
                System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"+key + " <<<<<<<<  >>>>>>>" + value);
            }
            if(value!=null){
            String[] parts = value.split("\\+");
    		System.out.println("size of key"+parts.length);
    		
    		if(parts.length>=3){
    		System.out.println("-------------1---------->>>>>>>"+parts[0]);
    		System.out.println("-------------2---------->>>>>>>"+parts[1]);
    		System.out.println("-------------3---------->>>>>>>"+parts[2]);
    		prehiretype=parts[1];
    		usertype=parts[2];
    		}
            }
            /*String silkroadintg = account.getSilkRoadIntegration();
            String usertype = account.getRole();
            
            System.out.println("*****Inside getAccountforStatusUpdateNYU****silkroadintg************"+silkroadintg+"$$$$$$$$$$$$$$$"+usertype);
            */
            oimAdminClient.getRestTemplate().put(serverUrl + OIMRestClient.ServiceURLs.UPDATE_USER_BY_LOGIN.getUrl(),
                    key, key.getUserName());
        } catch (HttpClientErrorException e) {
            logger.trace("HCEE PUT ERROR " + e.getResponseBodyAsString());
            System.out.println("HCEE PUT ERROR " + e.getResponseBodyAsString());
            throw new UserUpdateException(e.getStatusText());
        } catch (Exception e) {
            logger.trace("PUT ERROR " + e.getMessage());
            System.out.println("PUT ERROR " + e.getMessage());
            throw new Exception(e.getMessage());
        }
        System.out.println("-------------------"+prehiretype);
       // if(prehiretype.isEmpty()){
        	//return "registrationComplete";
       // }
        
        if(prehiretype.equalsIgnoreCase("OBNTV") && usertype.equalsIgnoreCase("PreHire")){
        	return "registrationCompleteNTV";
        }
        
        if(prehiretype.equalsIgnoreCase("OBNYU") && usertype.equalsIgnoreCase("PreHire")){
        	return "registrationCompleteNYU";
        }
        
        if(prehiretype.equalsIgnoreCase("OBFHC") && usertype.equalsIgnoreCase("PreHire")){
        	return "registrationCompleteFHC";
        }
        
        if(prehiretype.equalsIgnoreCase("OBBLKN") && usertype.equalsIgnoreCase("PreHire")){
        	return "registrationCompleteBKLN";
        }
        
        if(prehiretype.equalsIgnoreCase("OBFGPA") && usertype.equalsIgnoreCase("PreHire")){
        	return "registrationCompleteFGPA";
        }
        
        else return "registrationComplete";
    }
    
    //ended on dec 8th 2017
    
    
    /**
     * Method description
     *
     *
     * @param account
     * @param otp
     *
     * @return
     *
     * @throws Exception
     */
    public String sendSMS(UserAccount account, OTP otp) throws Exception {
        Notification notification = new Notification();

        notification.setRecipient(otp.getSmsNumber());
        notification.setMessage(otp.getMessage());

        try {
            logger.trace("sendSMS: Attempting Put"+otp.getSmsNumber()+":"+otp.getMessage());
            System.out.println("sendSMS: Attempting Put"+otp.getSmsNumber()+":"+otp.getMessage()+":notification"+notification);
            oimAdminClient.getRestTemplate().postForLocation(serverUrl + OIMRestClient.ServiceURLs.SEND_SMS.getUrl(),notification);
            System.out.println("After sending SMS+serverUrl");
        } catch (HttpClientErrorException e) {
            logger.trace("HCEE PUT ERROR " + e.getResponseBodyAsString());
            System.out.println("HCEE PUT ERROR " + e.getResponseBodyAsString());

            throw new Exception(e.getStatusText());
        } catch (Exception e) {
            logger.trace("PUT ERROR " + e.getMessage());
            System.out.println("PUT ERROR " + e.getMessage());

            throw new Exception(e.getMessage());
        }
        System.out.println("Message Sent");
        return "Message Sent";
    }
    
    /**
     * Method description
     *
     *
     * @param account
     * @param otp
     *
     * @return
     *
     * @throws Exception
     */
    public String sendEmail(UserAccount account, OTP otp) throws Exception {
       /*Notification notification = new Notification();

        notification.setRecipient(otp.getSmsNumber());
        notification.setMessage(otp.getMessage());

     try {
			System.out.println("Inside NYUSendCustomizedEmail: sendEmail()"+otp.getpersonalEmail()+":"+otp.getMessage().toString());
			Properties props = System.getProperties();
			props.put("mail.smtp.host", "Smtp.nyumc.org");
			props.put("mail.smtp.port", "25");
			Session session = Session.getInstance(props, null);
			MimeMessage msg = new MimeMessage(session);
			// set message headers
			msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
			msg.addHeader("format", "flowed");
			msg.addHeader("Content-Transfer-Encoding", "8bit");

			msg.setFrom(new InternetAddress("oimadmin@nyumc.org",
					"NYUMC - OTP Code"));

			msg.setReplyTo(InternetAddress.parse(otp.getpersonalEmail(), false));

			msg.setSubject("NYUMC OTP Code", "UTF-8");

			msg.setText(otp.getMessage(), "UTF-8");

			// msg.setSentDate(new Date());

			msg.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(otp.getpersonalEmail(), false));
			System.out.println("Message is ready");
			Transport.send(msg);

			System.out.println("EMail Sent Successfully!!");
		} catch (Exception e) {
			e.printStackTrace();
			return "Message Not Sent";
		}*/
		return "Email Message Sent";
    }
    
    public String getLookupValue(String strLookup)
    {
    	//com.aptecllc.oim.services.OIMRESTServices oimrestservices = new com.aptecllc.oim.services.OIMRESTServices();
    	return "";
    }
    
}
