package com.aptecllc.iam.selfsvc.validator;

import org.springframework.binding.message.MessageBuilder;
import org.springframework.binding.message.MessageContext;
import org.springframework.binding.validation.ValidationContext;
import org.springframework.stereotype.Component;

import com.aptecllc.iam.selfsvc.vo.UserSelfService;

@Component
public class UserSelfServiceValidator {

	public UserSelfServiceValidator() {
		// TODO Auto-generated constructor stub
	}
	
	/*public void validateEnterTerms(UserSelfService uss, ValidationContext context) {
        MessageContext messages = context.getMessageContext();
        System.out.println("Inside validate terms");
        System.out.println("uss.getTerms"+uss.getTerms());
       
        if(uss.getTerms()==null){
        //if (uss.getTerms()!=null || uss.getTerms().isEmpty()) {
            messages.addMessage(new MessageBuilder().error().source("terms").
                defaultText("Please check the terms and conditions to continue.").build());
       
    }
       // }
	}
	*/
	public void validateEnterNewPassword(UserSelfService uss, ValidationContext context) {
        MessageContext messages = context.getMessageContext();
        System.out.println("Inside validate password"); 
        if (uss.getNewPassword().isEmpty()) {
            messages.addMessage(new MessageBuilder().error().source("newPassword").
                defaultText("Enter your new password.").build());
        } else if (!uss.getNewPassword().equals(uss.getConfirmPassword())) {
            messages.addMessage(new MessageBuilder().error().source("confirmPassword").
                defaultText("Entered passwords must match.").build());
        }
    }
	
	
}
