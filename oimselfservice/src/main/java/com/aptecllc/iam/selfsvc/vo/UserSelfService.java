package com.aptecllc.iam.selfsvc.vo;

//~--- non-JDK imports --------------------------------------------------------

import org.codehaus.jackson.annotate.JsonIgnore;

//~--- JDK imports ------------------------------------------------------------

import java.io.Serializable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Class description
 *
 *
 * @version        1.0, 14/05/06
 * @author         Eric A. Fisher, APTEC LLC    
 */
public class UserSelfService implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String            userLogin;
    private String            oldPassword;
    private String            newPassword;
    private String            confirmPassword;
    private HashMap           questions;
    private String            terms;

    /**
     * Constructs ...
     *
     */
    public UserSelfService() {}

    /**
     * Constructs ...
     *
     *
     * @param pUserLogin
     */
    public UserSelfService(String pUserLogin) {
        this.userLogin = pUserLogin;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getUserLogin() {
        return userLogin;
    }

    /**
     * Method description
     *
     *
     * @param userLogin
     */
    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getOldPassword() {
        return oldPassword;
    }

    /**
     * Method description
     *
     *
     * @param oldPassword
     */
    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getNewPassword() {
        return newPassword;
    }

    /**
     * Method description
     *
     *
     * @param newPassword
     */
    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
//
    
    /**
     * Method description
     *
     *
     * @return
     */
   /* public String getTerms() {
        return terms;
    }*/

    /**
     * Method description
     *
     *
     * @param terms
     */
   /* public void setTerms(String terms) {
    	System.out.println("Inside setTerms"+terms);
        this.terms = terms;
    }*/
    
//    
    /**
     * Method description
     *
     *
     * @return
     */
    public String getConfirmPassword() {
        return confirmPassword;
    }

    /**
     * Method description
     *
     *
     * @param confirmPassword
     */
    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public HashMap getQuestions() {
        return questions;
    }

    /**
     * Method description
     *
     *
     * @param questions
     */
    public void setQuestions(HashMap questions) {
        this.questions = questions;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    @Override
    public String toString() {
        return "UserSelfService [userLogin=" + userLogin + ", oldPassword=****" + ", newPassword=****" + ", questions="
               + questions + "]";
    }
}
