package com.aptecllc.iam.selfsvc;

//~--- non-JDK imports --------------------------------------------------------

import java.util.Collections;

import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.web.client.RestTemplate;
import com.aptecllc.http.client.TokenAuthHeaderRequestInterceptor;

/**
 * Class description
 *
 *
 * @version        1.0, 13/10/09
 * @author         Eric A. Fisher, APTEC LLC    
 */
public class OIMRestClient {
    private RestTemplate restTemplate;
    private String       operationUsername;
    private String 		 serverUrl;

    /**
     * Constructs ...
     *
     */
    public OIMRestClient() {
        restTemplate = new RestTemplate();
    }

    /**
     * Constructs ...
     *
     *
     * @param pApiKey
     * @param pSharedSecret
     * @param pOperationUsername
     */
    public OIMRestClient(String pServerUrl, String pApiKey, String pSharedSecret, String pOperationUsername) {
    	restTemplate = new RestTemplate();
        operationUsername = pOperationUsername;
        serverUrl = pServerUrl;

        ClientHttpRequestInterceptor tokenAuthInterceptor = new TokenAuthHeaderRequestInterceptor(pApiKey,
                                                                pSharedSecret, pOperationUsername);
      
        restTemplate.setInterceptors(Collections.singletonList(tokenAuthInterceptor));
        
    }

    public enum ServiceURLs {
        GET_USER_BY_LOGIN("/api/v1/user/{userName}"), GET_USER_BY_ID("/api/v1/user/acctid/{accountId}"),
        GET_USER_BY_KEY("/api/v1/user/key/{userKey}"), CREATE_USER("/api/v1/user"),
                UPDATE_USER_BY_LOGIN("/api/v1/user/{userName}"), UPDATE_USER_BY_ID("/api/v1/user/acctid/{accountId}"),
        GET_USER_CHALLENGES("/api/v1/user/{userName}/challenges"),
        SET_USER_CHALLENGES("/api/v1/user/{userName}/challenges"),
        CHANGE_USER_PASSWORD("/api/v1/user/{userName}/password"), GET_SYSTEM_CHALLENGES("/api/v1/challenges"),
        VALIDATE_USER_PASSWORD("/api/v1/user/{userName}/validatepassword"), SEARCH_USERS("/api/v1/users?query={query}"),
        REQUEST_USER("/api/v1/request/user"),SEND_SMS("/notify/v1/sendsms"),
        ENABLE_USER("/api/v1/user/{userName}/enable"), LOOKUP("/api/v1/user/{lookupName}/getlookup");

        private String id;
        private String url;

        private ServiceURLs(String id) {
            this.id   = id;
            this.url = id;
        }

        private ServiceURLs(String id, String url) {
            this.id   = id;
            this.url = url;
        }

        /**
         * Method description
         *
         *
         * @return
         */
        public String getId() {
            return this.id;
        }

        /**
         * Method description
         *
         *
         * @return
         */
        public String getUrl() {
            return this.url;
        }
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public RestTemplate getRestTemplate() {
        return this.restTemplate;
    }
}
