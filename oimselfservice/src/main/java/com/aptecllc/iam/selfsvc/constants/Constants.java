package com.aptecllc.iam.selfsvc.constants;

//~--- non-JDK imports --------------------------------------------------------

import com.google.common.collect.ImmutableMap;

//~--- JDK imports ------------------------------------------------------------

import java.util.Map;

/**
 * Class description
 *
 *
 * @version        1.0, 13/10/14
 * @author         Eric A. Fisher, APTEC LLC
 */
public class Constants {
    public final static Map<String, String> STATE_MAP = ImmutableMap.<String, String>builder().put(
                                                            "AA", "Americas").put("AE", "Armed Forces Middle East").put(
                                                            "AK", "Alaska").put("AL", "Alabama").put(
                                                            "AP", "Armed Forces Pacific").put("AR", "Arkansas").put(
                                                            "AS", "American Samoa").put("AZ", "Arizona").put(
                                                            "CA", "California").put("CO", "Colorado").put(
                                                            "CT", "Connecticut").put("DC", "Dst Of Columbia").put(
                                                            "DE", "Delaware").put("FL", "Florida").put(
                                                            "FM", "Fed St. Micronesia").put("GA", "Georgia").put(
                                                            "GU", "Guam").put("HI", "Hawaii").put("IA", "Iowa").put(
                                                            "ID", "Idaho").put("IL", "Illinois").put(
                                                            "IN", "Indiana").put("KS", "Kansas").put(
                                                            "KY", "Kentucky").put("LA", "Louisiana").put(
                                                            "MA", "Massachusetts").put("MD", "Maryland").put(
                                                            "ME", "Maine").put("MH", "Marshall Islands").put(
                                                            "MI", "Michigan").put("MN", "Minnesota").put(
                                                            "MO", "Missouri").put("MP", "Northern Mariana Isl").put(
                                                            "MS", "Mississippi").put("MT", "Montana").put(
                                                            "NC", "North Carolina").put("ND", "North Dakota").put(
                                                            "NE", "Nebraska").put("NH", "New Hampshire").put(
                                                            "NJ", "New Jersey").put("NM", "New Mexico").put(
                                                            "NV", "Nevada").put("NY", "New York").put("OH", "Ohio").put(
                                                            "OK", "Oklahoma").put("OR", "Oregon").put(
                                                            "PA", "Pennsylvania").put("PR", "Puerto Rico").put(
                                                            "PW", "Palua Island").put("RI", "Rhode Island").put(
                                                            "SC", "South Carolina").put("SD", "South Dakota").put(
                                                            "TN", "Tennessee").put("TT", "Trust Terr Pac Isles").put(
                                                            "TX", "Texas").put("UT", "Utah").put("VA", "Virginia").put(
                                                            "VI", "Virgin Islands").put("VT", "Vermont").put(
                                                            "WA", "Washington").put("WI", "Wisconsin").put(
                                                            "WV", "West Virginia").put("WY", "Wyoming").build();
    public static final Map<String, String> COUNTRY_MAP = ImmutableMap.<String, String>builder().put(
                                                              "US", " United States").put("AF", "Afghanistan").put(
                                                              "AL", "Albania").put("AG", "Algeria").put(
                                                              "AN", "Andorra").put("AO", "Angola").put(
                                                              "AA", "Anguilla").put("AY", "Antarctica").put(
                                                              "AC", "Antigua").put("AR", "Argentina").put(
                                                              "AM", "Armenia").put("AV", "Aruba").put(
                                                              "AE", "Ascension").put(
                                                              "AT", "Ashmore & Cartier Islands").put(
                                                              "AS", "Australia").put("AU", "Austria").put(
                                                              "AB", "Azerbaijan").put("BC", "Bahamas").put(
                                                              "BA", "Bahrain").put("BD", "Bangladesh").put(
                                                              "BB", "Barbados").put("BJ", "Barbuda").put(
                                                              "BZ", "Belarus").put("BG", "Belgium").put(
                                                              "BI", "Belize").put("BE", "Benin").put(
                                                              "BF", "Bermuda").put("BT", "Bhutan").put(
                                                              "BL", "Bolivia").put("BN", "Bonaire").put(
                                                              "BS", "Bosnia-herzegovina").put("BO", "Botswana").put(
                                                              "BV", "Bouvet Island").put("BR", "Brazil").put(
                                                              "VB", "British Virgin Islands").put("BX", "Brunei").put(
                                                              "BU", "Bulgaria").put(
                                                              "UV", "Burkina Faso(upper Volta)").put("BM", "Burma").put(
                                                              "BY", "Burundi").put("CB", "Cambodia").put(
                                                              "CM", "Cameroon").put("CA", "Canada").put(
                                                              "CC", "Canary Islands").put("CV", "Cape Verde").put(
                                                              "CJ", "Cayman Islands").put(
                                                              "CT", "Central African Republic").put("CD", "Chad").put(
                                                              "CI", "Chile").put("CP", "China, Peoples Republic").put(
                                                              "CK", "Cocos & Keeling Islands").put(
                                                              "CO", "Colombia").put("CN", "Comoros").put(
                                                              "CF", "Congo").put("CG", "Congo, Kinshasa").put(
                                                              "CW", "Cook Islands").put("CX", "Costa Rica").put(
                                                              "CQ", "Croatia").put("CU", "Cuba").put(
                                                              "CR", "Curacao").put("CY", "Cyprus").put(
                                                              "CZ", "Czech Republic").put("DM", "Dahomey").put(
                                                              "DA", "Denmark").put("DB", "Djibouti").put(
                                                              "DO", "Dominica").put("DR", "Dominican Republic").put(
                                                              "EC", "Ecuador").put("EG", "Egypt").put(
                                                              "ES", "El Salvador").put(
                                                              "EQ", "Enderbury/canton Islands").put(
                                                              "EN", "England").put("EK", "Equatorial Guinea").put(
                                                              "EI", "Estonia").put("ET", "Ethiopia").put(
                                                              "FA", "Falkland Islands").put("FO", "Faroe Islands").put(
                                                              "FJ", "Fiji").put("FI", "Finland").put(
                                                              "FS", "Fr.so. & Antarctic Is").put("FR", "France").put(
                                                              "FG", "French Guinea").put("FP", "French Polynesia").put(
                                                              "GB", "Gabon").put("GA", "Gambia").put(
                                                              "GZ", "Gaza Strip").put("GG", "Georgia").put(
                                                              "GE", "Germany").put("GH", "Ghana").put(
                                                              "GI", "Gibraltar").put("GR", "Greece").put(
                                                              "GL", "Greenland").put("GJ", "Grenada").put(
                                                              "GP", "Guadeloupe").put("GT", "Guatemala").put(
                                                              "GV", "Guinea").put("GX", "Guinea-bissau").put(
                                                              "GY", "Guyana").put("HA", "Haiti").put(
                                                              "HM", "Heard & Mcdonald Islands").put(
                                                              "HO", "Honduras").put("HK", "Hong Kong").put(
                                                              "HU", "Hungary").put("IC", "Iceland").put(
                                                              "IN", "India").put("IO", "Indian Ocean Terr.").put(
                                                              "ID", "Indonesia").put("IR", "Iran").put(
                                                              "IZ", "Iraq").put("IY", "Iraq-saudi Arabi N. Zone").put(
                                                              "IE", "Ireland, Eire").put("IL", "Ireland, Northern").put(
                                                              "IS", "Israel").put("IU", "Israel-syria Demil.zone").put(
                                                              "IT", "Italy").put("IV", "Ivory Coast").put(
                                                              "JM", "Jamaica").put("JN", "Jan Mayen").put(
                                                              "JA", "Japan").put("JQ", "Johnston Atoll").put(
                                                              "JO", "Jordan").put("KZ", "Kazakhstan").put(
                                                              "KE", "Kenya").put("KB", "Kiribati").put(
                                                              "KN", "Korea, Dem. Peoples Repl.").put(
                                                              "KS", "Korea, Republic Of").put("OS", "Kosovo").put(
                                                              "KU", "Kuwait").put("KY", "Kyrgyzstan").put(
                                                              "LA", "Laos").put("LZ", "Latvia").put(
                                                              "LE", "Lebanon").put("LW", "Leeward Islands").put(
                                                              "LT", "Lesotho").put("LI", "Liberia").put(
                                                              "LY", "Libya").put("LS", "Liechtenstein").put(
                                                              "LH", "Lithuania").put("LU", "Luxembourg").put(
                                                              "MC", "Macau").put("MK", "Macedonia").put(
                                                              "MA", "Madagascar").put("MI", "Malawi").put(
                                                              "MY", "Malaysia").put("MV", "Maldives").put(
                                                              "ML", "Mali").put("MT", "Malta").put(
                                                              "ME", "Marshall Islands").put("MD", "Martinique").put(
                                                              "MR", "Mauritania").put("MP", "Mauritius").put(
                                                              "MX", "Mexico").put("MQ", "Midway Islands").put(
                                                              "MJ", "Moldova").put("MN", "Monaco").put(
                                                              "MG", "Mongolia").put("MS", "Montenegro").put(
                                                              "MH", "Montserrat").put("MO", "Morocco").put(
                                                              "MZ", "Mozambique").put("NB", "Namibia").put(
                                                              "NR", "Nauru").put("NP", "Nepal").put(
                                                              "NL", "Netherlands").put(
                                                              "NA", "Netherlands Antilles").put("NV", "Nevis").put(
                                                              "NC", "New Caledonia").put("NZ", "New Zealand").put(
                                                              "NU", "Nicaragua").put("NG", "Niger").put(
                                                              "NI", "Nigeria").put("NE", "Niue").put(
                                                              "NF", "Norfolk Islands").put("NO", "Norway").put(
                                                              "OM", "Oman").put("PK", "Pakistan").put(
                                                              "PN", "Panama").put("PP", "Papua-new Guinea").put(
                                                              "PA", "Paraguay").put("PE", "Peru").put(
                                                              "PH", "Philippines").put("PC", "Pitcairn Islands").put(
                                                              "PL", "Poland").put("PO", "Portugal").put(
                                                              "PT", "Portuguese Timor").put("QA", "Qatar").put(
                                                              "RD", "Redonda").put("RO", "Romania").put(
                                                              "RA", "Russia").put("RW", "Rwanda").put(
                                                              "YQ", "Ryukyu Islands, Southern").put("RT", "Saba").put(
                                                              "SM", "San Marino").put("SA", "Saudi Arabia").put(
                                                              "SD", "Scotland").put("SE", "Senegal").put(
                                                              "ER", "Serbia").put("SF", "Seychelles").put(
                                                              "SL", "Sierre Leone").put("SK", "Sikkim").put(
                                                              "SG", "Singapore").put("LV", "Slovakia").put(
                                                              "LO", "Slovenia").put("RS", "Soloman Islands").put(
                                                              "SO", "Somalia").put("SN", "South Africa, Republic").put(
                                                              "SP", "Spain").put("SS", "Spanish Sahara").put(
                                                              "SJ", "Sri Lanka").put("SC", "St. Christopher/kitts").put(
                                                              "RU", "St. Eustatius").put("SH", "St. Helena").put(
                                                              "ST", "St. Lucia").put("SI", "St. Martin").put(
                                                              "SB", "St. Pierre & Miquelon").put(
                                                              "TP", "St. Thomas & Principe").put(
                                                              "VC", "St. Vincent & Grenadines").put("SU", "Sudan").put(
                                                              "SR", "Suriname").put("SV", "Svalbard").put(
                                                              "SX", "Swaziland").put("SW", "Sweden").put(
                                                              "SZ", "Switzerland").put("SY", "Syria").put(
                                                              "TW", "Taiwan").put("TA", "Tajikistan").put(
                                                              "TZ", "Tanzania").put("TH", "Thailand").put(
                                                              "TB", "Tibet").put("TO", "Togo").put(
                                                              "TL", "Tokelau Islands").put("TN", "Tonga").put(
                                                              "TT", "Tortola").put("TD", "Trinidad & Tobago").put(
                                                              "TC", "Trucial States").put("TS", "Tunisia").put(
                                                              "TU", "Turkey").put("TM", "Turkmenistan").put(
                                                              "TK", "Turks & Caicos Islands").put("TV", "Tuvalu").put(
                                                              "UG", "Uganda").put("UE", "Ukraine").put(
                                                              "UA", "United Arab Emirates").put("UY", "Uruguay").put(
                                                              "UZ", "Uzbekistan").put("VA", "Vanuatu").put(
                                                              "VT", "Vatican City State").put("VE", "Venezuela").put(
                                                              "VN", "Vietnam").put("VI", "Virgin Islands").put(
                                                              "VO", "Vojvodina").put("WQ", "Wake Island").put(
                                                              "WL", "Wales").put("WF", "Wallis & Fortuna Islands").put(
                                                              "WB", "West Berlin").put("WS", "Western Samoa").put(
                                                              "WI", "Windward Islands").put(
                                                              "YM", "Yemen Arab Republic").put(
                                                              "YE", "Yemen, Peoples Dem. Repl.").put(
                                                              "YS", "Yemen, Southern").put("YO", "Yugoslavia").put(
                                                              "ZR", "Zaire").put("ZA", "Zambia").put(
                                                              "ZB", "Zimbabwe").build();
    public static final Map<String, String> SUFFIX_MAP = ImmutableMap.<String, String>builder().put("Jr",
                                                             "Jr.").put("Sr", "Sr.").put("II", "II").put("III",
                                                                 "III").put("IV", "IV").build();
    public static final Map<String, String> MONTH_MAP = ImmutableMap.<String, String>builder().put("01",
                                                            "JAN").put("02", "FEB").put("03", "MAR").put("04",
                                                                "APR").put("05", "MAY").put("06", "JUN").put("07",
                                                                    "JUL").put("08", "AUG").put("09", "SEP").put("10",
                                                                        "OCT").put("11", "NOV").put("12",
                                                                            "DEC").build();

    /**
     * Constructs ...
     *
     */
    public Constants() {}
}
