package com.aptecllc.iam.selfsvc.utils;

//~--- non-JDK imports --------------------------------------------------------

import com.aptecllc.iam.selfsvc.vo.SecurityQuestions;
import com.aptecllc.iam.selfsvc.vo.SponsoredAccount;
import com.aptecllc.iam.selfsvc.vo.UserAccount;
import com.aptecllc.iam.selfsvc.vo.UserRegistration;
import com.aptecllc.iam.selfsvc.vo.UserSelfService;

//~--- JDK imports ------------------------------------------------------------



import java.util.HashMap;
import java.util.Random;

/**
 * Class description
 *
 *
 * @version        1.0, 14/05/12
 * @author         Eric A. Fisher, APTEC LLC
 */
public abstract class BaseSelfServiceUtilities {
    private Random rand = new Random();

    /**
     * Constructs ...
     *
     */
    public BaseSelfServiceUtilities() {}

    /**
     * Method description
     *
     *
     * @param pUSS
     *
     * @return
     */
    public SecurityQuestions getSecurityQuestionsFromUSS(UserSelfService pUSS) {
        SecurityQuestions questions     = new SecurityQuestions();
        Object[]          userQuestions = pUSS.getQuestions().keySet().toArray();

        if (userQuestions.length >= 3) {
            questions.setQuestion3((String) userQuestions[2]);
        }

        if (userQuestions.length >= 2) {
            questions.setQuestion2((String) userQuestions[1]);
        }

        if (userQuestions.length >= 1) {
            questions.setQuestion1((String) userQuestions[0]);
        }

        return questions;
    }

    /**
     * Method description
     *
     *
     * @param pUSS
     * @param questions
     */
    public void setAnswersInUSS(UserSelfService pUSS, SecurityQuestions questions) {
        if (questions.getQuestion1() != null) {
            pUSS.getQuestions().put(questions.getQuestion1(), questions.getAnswer1());
        }

        if (questions.getQuestion2() != null) {
            pUSS.getQuestions().put(questions.getQuestion2(), questions.getAnswer2());
        }

        if (questions.getQuestion3() != null) {
            pUSS.getQuestions().put(questions.getQuestion3(), questions.getAnswer3());
        }
    }

    /**
     * Method description
     *
     *
     * @param acct
     *
     * @return
     */
    public UserAccount getNewAccountFromSponsoredAccount(SponsoredAccount acct) {
        UserAccount user = new UserAccount();

        user.setFirstName(acct.getFirstName());
        user.setLastName(acct.getLastName());
        user.setOrganizationName(acct.getOrgName());
        user.setAttribute("dateOfBirth", acct.getDOB());
        user.setAttribute("title", acct.getTitle());
        user.setAttribute("phoneNumber", acct.getTelephone());
        user.setAttribute("startDate", acct.getStartDate());
        user.setAttribute("endDate", acct.getEndDate());
        user.setAttribute("departmentNumber", acct.getDepartmentNumber());
        user.setAttribute("personalEmail", acct.getPersonalEmail());
        user.setAttribute("externalaffiliation", acct.getExternalaffiliation());
        user.setAttribute("sponsorAlternateEmail", acct.getSponsorAlternateEmail());
        user.setAttribute("sponsorAlternatePhone", acct.getSponsorAlternatePhone());
        user.setAttribute("SSN", acct.getSSN());
        user.setAttribute("sponsorNetID",acct.getSponsorNetID());
        user.setAttribute("emailRequired",acct.getEmailRequired());
        
        user.setAttribute("sponsor",acct.getSponsor());
        user.setAttribute("vendor",acct.getVendor());
        System.out.println("Inside BaseSefServiceUtility"+user);
        

        return user;
    }

    /**
     * Method description
     *
     *
     * @param userReg
     *
     * @return
     */
    
    
    public UserAccount getNewAccountFromRegistration(UserRegistration userReg) {
        UserAccount user = new UserAccount();

        user.setFirstName(userReg.getFirstName());
        user.setLastName(userReg.getLastName());

        return user;
    }

    /**
     * Method description
     *
     *
     * @param oimUser
     *
     * @return
     */
    public String determineRegistrationStatus(UserAccount oimUser) {
        String result = "Unknown Status";

        System.out.println("determineRegStatus: " + oimUser);
        
        if(oimUser.getUserName().equalsIgnoreCase("THOMAS22")){
        	System.out.println("Inside wrong flow");
        	result="Wrong Flow";
        }
        else{

        if ("Disabled".equalsIgnoreCase(oimUser.getStatus())) {
            result = "Security Disabled";
        } else if ("Y".equals(oimUser.getAcctClaimed())) {
            result = "Account Registered";
        } else if ("R".equals(oimUser.getAcctClaimed())) {
            result = "ReRegister Account";
        } else {
            if (oimUser.getPhoneNumber() == null) {
                result = "Register Account";
            } else if (oimUser.getPhoneNumber() != null) {
                if (!oimUser.getPhoneNumber().isEmpty()) {
                    result = "Register Account";
                } else {
                    result = "Register Account";
                }
            }
        }
        }
        System.out.println("determineRegStatus: " + result);

        return result;
    }

    /**
     * Method description
     *
     *
     * @param user
     * @param regStatus
     *
     * @return
     */
    public UserAccount getAccountforStatusUpdate(UserAccount user, String regStatus) {
        UserAccount updAccount = new UserAccount();
        System.out.println("Inside getAccountForStatusUpdate");
        System.out.println("Inside getAccountForStatusUpdate"+user.getUserName());
        updAccount.setUserName(user.getUserName());

        if (!regStatus.isEmpty()) {
            updAccount.setAcctClaimed(regStatus);
           
        }

        System.out.println(updAccount);

        return updAccount;
    }
    
 
    // added on Dec 8th 2017
    
    public HashMap getAccountforStatusUpdateNYU(UserAccount user, String regStatus) {
      UserAccount updAccount = new UserAccount();
        System.out.println("Inside getAccountForStatusUpdate");
        System.out.println("Inside getAccountForStatusUpdate"+user.getUserName());
        HashMap nyuUser = new HashMap();
      String silkroadintg = user.getSilkRoadIntegration()+"+"+(String) user.getRole();
        String usertype = (String) user.getRole();
        System.out.println("usertype from getAccountforStatusUpdateNYU is"+usertype);
        System.out.println("*****Inside getAccountforStatusUpdateNYU****silkroadintg************"+silkroadintg);
        updAccount.setUserName(user.getUserName());

        if (!regStatus.isEmpty()) {
            updAccount.setAcctClaimed(regStatus);
            
           
        }

        System.out.println(updAccount);
        nyuUser.put(updAccount, silkroadintg);
        return nyuUser;
    }
    
    // ended on Dec 8th 2017
    
    
    /**
     * Method description
     *
     *
     * @param user
     * @param regStatus
     *
     * @return
     */
    public UserAccount getAccountStatusUpdate(UserAccount user, String pStatus) {
        UserAccount updAccount = new UserAccount();
        System.out.println("Inside getAccountForIdentityStatusUpdate");
        System.out.println("Inside getAccountForIdentityStatusUpdate"+user.getUserName());
        updAccount.setUserName(user.getUserName());

        if (!pStatus.isEmpty()) {
            updAccount.setAcctClaimed(pStatus);
        	//updAccount.setStatus(pStatus);
           
        }

        System.out.println(updAccount);

        return updAccount;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getRandomCode() {
        String randomCode = "";
        int    randomNum  = rand.nextInt((9999999 - 1000000) + 1) + 1000000;

        randomCode = Integer.toString(randomNum);

        return randomCode;
    }
}
