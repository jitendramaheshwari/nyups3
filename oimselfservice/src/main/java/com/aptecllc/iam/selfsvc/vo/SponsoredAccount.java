package com.aptecllc.iam.selfsvc.vo;

//~--- non-JDK imports --------------------------------------------------------

import org.springframework.binding.message.MessageBuilder;
import org.springframework.binding.message.MessageContext;
import org.springframework.binding.validation.ValidationContext;
import org.springframework.format.annotation.DateTimeFormat;

//~--- JDK imports ------------------------------------------------------------




import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class description
 *
 *
 * @version        1.0, 14/04/06
 * @author         Eric A. Fisher, APTEC LLC
 */
public class SponsoredAccount implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    private Date              DOB;
    private String            dobMonth;
    private String            dobDay;
    private String            dobYear;
    private String            firstName;
    private String            middleName;
    private String            lastName;
    private String            office;
    private String            departmentNumber;
    private String            telephone;
    private String            personalEmail;
    private String            orgName;
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    private Date              startDate;
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    private Date              endDate;
    private String            title;
    private String            externalaffiliation;
    private String            sponsorAlternateEmail;
    private String            sponsorAlternatePhone;
    private String            SSN;
    private String            sponsorNetID;
    private String            emailRequired;
    private String            status;
    private String            vendor;
    private String            sponsor;
    private Date              terminationDate;

    /**
     * Constructs ...
     *
     */
    public SponsoredAccount() {
        orgName   = "Xellerate Users";
        //startDate = new Date();
    }

    /**
     * Method description
     *
     *
     * @return
     */
   /* public String getStatus() {
        return status;
    }*/

    /**
     * Method description
     *
     *
     * @param status
     */
   /* public void setStatus(String status) {
        this.status = status;
    }*/

    /**
     * Method description
     *
     *
     * @return
     */
    public String getTitle() {
        return title;
    }

    /**
     * Method description
     *
     *
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }
    /**
     * Method description
     *
     *
     * @return
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * Method description
     *
     *
     * @param startDate
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * Method description
     *
     *
     * @param endDate
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    
    
    //
    
   /* public Date getTerminationDate() {
        return terminationDate;
    }

    /**
     * Method description
     *
     *
     * @param endDate
     */
   /* public void setTerminationDate(Date terminationDate) {
        this.terminationDate = terminationDate;
    }
    */
    //
    

    /**
     * Method description
     *
     *
     * @return
     */
    public String getOrgName() {
        return orgName;
    }

    /**
     * Method description
     *
     *
     * @param orgName
     */
    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getOffice() {
        return office;
    }

   

    /**
     * Method description
     *
     *
     * @param office
     */
    public void setOffice(String office) {
        this.office = office;
    }
    /**
     * Method description
     *
     *
     * @param externalaffiliation
     */
    public void setExternalaffiliation(String externalaffiliation) {
        this.externalaffiliation = externalaffiliation;
    }
    
    public String getExternalaffiliation() {
        return externalaffiliation;
    }
    
    /**
     * Method description
     *
     *
     * @param sponsorExternalEmail
     */
    public void setSponsorAlternateEmail(String sponsorAlternateEmail) {
        this.sponsorAlternateEmail = sponsorAlternateEmail;
    }
    
    public String getSponsorAlternateEmail() {
        return sponsorAlternateEmail;
    }
    
    public void setSponsorNetID(String sponsorNetID) {
        this.sponsorNetID = sponsorNetID;
    }
    
    public String getSponsorNetID() {
        return sponsorNetID;
    }
    
    public void setEmailRequired(String emailRequired) {
        this.emailRequired = emailRequired;
    }
    
    public String getEmailRequired() {
        return emailRequired;
    }
    
    /**
     * Method description
     *
     *
     * @param sponsorExternalEmail
     */
    public void setSponsorAlternatePhone(String sponsorAlternatePhone) {
        this.sponsorAlternatePhone = sponsorAlternatePhone;
    }
    
    public String getSponsorAlternatePhone() {
        return sponsorAlternatePhone;
    }
    
    public void setVendor(String vendor) {
        this.vendor = vendor;
    }
    
    public String getVendor() {
        return vendor;
    }
    
    public void setSponsor(String sponsor) {
        this.sponsor = sponsor;
    }
    
    public String getSponsor() {
        return sponsor;
    }
    
    public void setSSN(String SSN) {
        this.SSN = SSN;
    }
    
    public String getSSN() {
        return SSN;
    }
    
    /**
     * Method description
     *
     *
     * @return
     */
    public String getDepartmentNumber() {
        return departmentNumber;
    }

    /**
     * Method description
     *
     *
     * @param departmentNumber
     */
    public void setDepartmentNumber(String departmentNumber) {
        this.departmentNumber = departmentNumber;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * Method description
     *
     *
     * @param telephone
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getPersonalEmail() {
        return personalEmail;
    }

    /**
     * Method description
     *
     *
     * @param personalEmail
     */
    public void setPersonalEmail(String personalEmail) {
        this.personalEmail = personalEmail;
    }

    /**
     * Method description
     *
     *
     * @param context
     */
    public void validateEnterRequestInfo(ValidationContext context) {
        MessageContext messages = context.getMessageContext();

        checkDateOfBirth(messages);
        checkAlternateEmail(messages);
        checkTelephoneNumber(messages);
        
        //checkSSN(messages);
        if(this.getFirstName().isEmpty()){
        	
        	messages.addMessage(
                    new MessageBuilder().error().source("firstName").defaultText("Enter your First Name.").build());
                
        }
        if(this.getLastName().isEmpty()){
     
        	messages.addMessage(
                    new MessageBuilder().error().source("lastName").defaultText("Enter your Last Name.").build());
                
        }
    }

    /**
     * Method description
     *
     *
     * @param context
     */
    public void validateEnterSponsorshipInfo(ValidationContext context) {
        MessageContext    messages = context.getMessageContext();
        
       
        checkStartEndDate(messages);
        checkExternalAffiliation(messages);		//<<------------------------JUST ADDED BY ME 
        checkSponsor(messages);
    }

    private void checkSponsor(MessageContext messages){
    	if(this.getSponsor().isEmpty()){
    		messages.addMessage(
    				new MessageBuilder().error().source("sponsor").defaultText("Sponsor ID is required").build());
    	}
    }
    
    /*--> Check cases for start and end date
     * 		- Cant be null
     * 		- Start date has to be today or later (but not more than a year)
     * 		- End date has to be later than start date
     * */
    private void checkStartEndDate(MessageContext messages){
    	System.out.println("Inside check start/end date");
    	
    	GregorianCalendar cal      = new GregorianCalendar();
        GregorianCalendar cal1      = new GregorianCalendar();
        
        cal.add(Calendar.YEAR, 1);
        cal1.add(Calendar.DAY_OF_MONTH,-1);
        
        //<--------------Start Date Tests (null, before current time, 1 year in the future, respectively)
        //<--------------End Date Tests (null, over a year in the future, before start date, respectively)
        
        
        if (this.getStartDate() == null) {
            messages.addMessage(
                new MessageBuilder().error().source("StartDate").defaultText("Start Date is required.").build());
        } 
        else if (this.getEndDate() == null) {
            messages.addMessage(
                new MessageBuilder().error().source("EndDate").defaultText("End Date is required.").build());
        }
        else if (this.getStartDate().before(cal1.getTime())) {
            messages.addMessage(
                    new MessageBuilder().error().source("StartDate").defaultText(
                        "Start Date can not be in the past.").build());
        }
        else if(this.getEndDate().before(cal1.getTime())){
        	messages.addMessage(
                    new MessageBuilder().error().source("EndDate").defaultText(
                        "End Date can not be in the past.").build());
        }
        else if (this.getStartDate().after(cal.getTime())){
        	messages.addMessage(
                    new MessageBuilder().error().source("StartDate").defaultText(
                        "Start Date can not be more than one year in the future.").build());
        }
        else if (this.getEndDate().after(cal.getTime())) {
            messages.addMessage(
                new MessageBuilder().error().source("EndDate").defaultText(
                    "End date can not be more than one year in the future.").build());
        } 
               
        else if (this.getEndDate().before(this.getStartDate())){
        	messages.addMessage(
                    new MessageBuilder().error().source("EndDate").defaultText(
                        "End date can not be before start date.").build());
        }
        else{
        	System.out.println("Valid start/end dates");
        }
           	
    }
    
    private void checkAlternateEmail(MessageContext messages){
    	System.out.println("Inside Check AlternateEmail");
    	String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
    	CharSequence inputStr = this.getPersonalEmail();
    	System.out.println("Inside Check AlternateEmail"+inputStr);
    	Pattern pattern = Pattern.compile(expression,Pattern.CASE_INSENSITIVE);
    	Matcher matcher = pattern.matcher(inputStr);  
    	if(matcher.matches()){  
    		System.out.println("Valid email");
    	}  
    	else{
    		messages.addMessage(
                    new MessageBuilder().error().source("personalEmail").defaultText("Please enter a valid email address.").build());
    	}
    }
    
    /* 3 Cases: Empty, too big (fails), or valid
     * If fails, send error message out
     * 
     * */
    private void checkExternalAffiliation(MessageContext messages){
    	System.out.println("Checking External Affiliations");
    	if (this.getExternalaffiliation().length()>65){
    		messages.addMessage(
    				new MessageBuilder().error().source("externalaffiliation").defaultText("External affiliation must be less than 65 characters.").build());
    	}
    	else if(this.getExternalaffiliation().isEmpty()){
    		System.out.println("No external affiliation");
    	}
    	else{
    		System.out.println("Valid External affiliation");
    	}
    	
    }
    private void checkTelephoneNumber(MessageContext messages){
    	System.out.println("Inside Check TelephoneNumber");
		String telephone = this.getTelephone();;
		System.out.println("Inside Check TelephoneNumber"+telephone);
		String newph = telephone.replaceAll( "[^\\d]", "" );
		System.out.println("Inside Check TelephoneNumber Replaced Non NUmeric Chars"+newph);
		//started on Nov 2 2017
		if(this.telephone.isEmpty())
		{
			
				  messages.addMessage(new MessageBuilder().error().source("telephone").defaultText("Please enter a valid Telephone Number. If you do not provide a valid telephone number sposnored individual will not be able to login remotely via VPN").build());
			 
		}
		//ended on Nov 2 2017
		if(newph.length()<10 && !newph.isEmpty())
		{
			messages.addMessage(new MessageBuilder().error().source("telephone").defaultText("Please enter a valid Telephone Number.").build());
		}
		else
		{
			if(newph.length()==10)
			{
			  if(newph.charAt(3)=='1')
			  {
				  messages.addMessage(new MessageBuilder().error().source("telephone").defaultText("Please enter a valid Telephone Number.").build());
			  }
			  else
			  {
				  System.out.println("Valid telephone number for SMS");
			  }
			}
			else
			{
				if(newph.length()==11)
				{
				  if(newph.startsWith("1") && newph.charAt(4)=='1')
				  {
					messages.addMessage(new MessageBuilder().error().source("telephone").defaultText("Please enter a valid Telephone Number.").build());
				  }
				  else
				  {
					  System.out.println("Valid telephone number for SMS");
				  }
					
				}
				else
				{
					if(telephone.contains("#")||telephone.contains("x")||telephone.contains("X")||telephone.startsWith("+") || telephone.isEmpty())
					{
						System.out.println("Valid telephone number for SMS");
					}
					else
					{
						messages.addMessage(new MessageBuilder().error().source("telephone").defaultText("Please enter a valid Telephone Number.").build());
					}
				}
			}
		}
    }
    
    
    
    
   /* private void checkSSN(MessageContext messages){
    	System.out.println("Inside Check SSN");
    	String expression = "^\\d{3}[- ]?\\d{2}[- ]?\\d{4}$";
    	CharSequence inputStr = this.getSSN();
    	System.out.println("Inside Check SSN"+inputStr);
    	Pattern pattern = Pattern.compile(expression,Pattern.CASE_INSENSITIVE);
    	Matcher matcher = pattern.matcher(inputStr);  
    	if(matcher.matches()){  
    		System.out.println("Valid SSN ");
    	}  
    	else{
    		messages.addMessage(
                    new MessageBuilder().error().source("telephone").defaultText("Please enter a valid SSN in the format XXXXXXXXX.").build());
    	}
    }*/
    
    private void checkDateOfBirth(MessageContext messages) {
        boolean          validateDate = true;
        SimpleDateFormat dobFormat    = new SimpleDateFormat("yyyyMMd");

        if (this.getDobMonth().isEmpty() || this.getDobMonth().equals("Month")) {
            messages.addMessage(
                new MessageBuilder().error().source("dobMonth").defaultText("Select your birth month.").build());
            validateDate = false;
        }

        if (this.getDobDay().isEmpty()) {
            messages.addMessage(
                new MessageBuilder().error().source("dobDay").defaultText("Enter your birth day.").build());
            validateDate = false;
        }

        if (this.getDobYear().isEmpty()) {
            messages.addMessage(
                new MessageBuilder().error().source("dobYear").defaultText("Enter your birth year.").build());
            validateDate = false;
        }

        if (validateDate) {
            String dob = this.getDobYear() + this.getDobMonth() + this.getDobDay();

            // System.out.println(dob);
            try {
            	System.out.println("INSIDE checkDateofBirth--dob is -->"+dob);
                Date dobDate = dobFormat.parse(dob);
                System.out.println("INSIDE checkDateofBirth--dobDate is -->"+dobDate);	
                this.setDOB(dobDate);

                // System.out.println(dobDate);
            } catch (ParseException e) {
                messages.addMessage(
                    new MessageBuilder().error().source("DOB").defaultText("Date of Birth is Invalid.").build());
            }
        }
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public Date getDOB() {
        return DOB;
    }

    /**
     * Method description
     *
     *
     * @param dOB
     */
    public void setDOB(Date dOB) {
    	System.out.println("Setting date in SponsoredAccount and the value is->"+dOB);
        DOB = dOB;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getDobMonth() {
        return dobMonth;
    }

    /**
     * Method description
     *
     *
     * @param dobMonth
     */
    public void setDobMonth(String dobMonth) {
        this.dobMonth = dobMonth;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getDobDay() {
        return dobDay;
    }

    /**
     * Method description
     *
     *
     * @param dobDay
     */
    public void setDobDay(String dobDay) {
        this.dobDay = dobDay;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getDobYear() {
        return dobYear;
    }

    /**
     * Method description
     *
     *
     * @param dobYear
     */
    public void setDobYear(String dobYear) {
        this.dobYear = dobYear;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Method description
     *
     *
     * @param firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Method description
     *
     *
     * @param middleName
     */
    public void setMiddleName(String middleName) {
    	System.out.println("Middle Name in sponsored Account.java is:"+middleName);
        this.middleName = middleName;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Method description
     *
     *
     * @param lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
