package com.aptecllc.iam.controller;

import java.io.Serializable;

import org.springframework.webflow.context.ExternalContext;

import java.lang.reflect.Member;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.binding.message.MessageBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.servlet.ModelAndView;

import com.aptecllc.iam.selfsvc.OIMRestClient;
import com.aptecllc.iam.selfsvc.exception.UserNotFoundException;
import com.aptecllc.iam.selfsvc.services.OIMUserServicesClient;
import com.aptecllc.iam.selfsvc.utils.BaseSelfServiceUtilities;
import com.aptecllc.iam.selfsvc.vo.UserAccount;
import com.aptecllc.iam.selfsvc.vo.UserSelfService;
//import com.aptecllc.oim.services.OIMClient;
//import com.aptecllc.iam.selfsvc.services.OIMClient;

/**
 * 
 * @author thomas22
 */
public class RequestUtil implements Serializable {
	OIMRestClient oimAdminClient;
	static BigInteger p = new BigInteger(
			"186903683994784883974286803981340343409");
	static BigInteger q = new BigInteger(
			"238365206090827088390448048852555109291");
	static BigInteger n = p.multiply(q);
	static BigInteger phiN = (p.subtract(BigInteger.ONE)).multiply((q
			.subtract(BigInteger.ONE)));
	static BigInteger e = new BigInteger("449");
	static BigInteger d = new BigInteger(
			"17364106129279623304364236508747275879893306787892962132847318388210189166049");
	
	

	public String getRequestParameter(String name, ExternalContext context) {

		try {
			//String paramValue = context.getRequestParameterMap().get(name);
			
			String paramValue = context.getRequestParameterMap().get(name);
			System.out
					.println("--------------------------------------------------"+ paramValue);
			String output = recoverTextFromBigInteger(decrypt(new BigInteger(paramValue)));
			System.out.println("-------------decrypted value is------------------------------"+ output);
			return output;
		} catch (Exception e) {
			e.printStackTrace();
			return "thomas22";
		}
	}
   
    
	 public static BigInteger encodeMessageAsBigInteger(String plaintext) {
		 return new BigInteger(plaintext.getBytes());
		 }
		     
		     public static String recoverTextFromBigInteger(BigInteger plaintext) {
		 return new String(plaintext.toByteArray());
		 }
		     
		     private static BigInteger encrypt(String input){
		     
		     BigInteger m = encodeMessageAsBigInteger(input);
		     
		     BigInteger c = m.modPow(e, n);
		     return c;
		     }
		     
		  private static BigInteger encryptInt(int input){
		     
		 String aString = Integer.toString(input);
		  
		     BigInteger m = encodeMessageAsBigInteger(aString);
		     
		     BigInteger c = m.modPow(e, n);
		     return c;
		     }
		     
		     private static BigInteger decrypt(BigInteger c){
		 return c.modPow(d,n);
		 }
		    

}
