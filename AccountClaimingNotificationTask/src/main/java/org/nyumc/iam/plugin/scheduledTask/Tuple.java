package org.nyumc.iam.plugin.scheduledTask;

/**
 * 
 * @author Dan Melando
 *
 * @param <T1>
 * @param <T2>
 * 
 * This data structure allows classes to return two different objects. In AccountClaimingNotificationTask: the HashMap userHashMap
 * and the String managerKey are the two things that are loaded into the Tuple and passed down through different methods. 
 * This is a very simplistic way to get around Java's inability to return more than one object at a time. 
 */


public class Tuple<T1, T2> {

	public final T1 t1;
	public final T2 t2; 
	
	public Tuple(T1 t1, T2 t2){
		this.t1 = t1; 
		this.t2 = t2;
	}
}
