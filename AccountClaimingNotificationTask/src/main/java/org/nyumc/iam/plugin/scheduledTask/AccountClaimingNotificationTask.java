package org.nyumc.iam.plugin.scheduledTask;

//~--- non-JDK imports --------------------------------------------------------

import oracle.iam.identity.exception.NoSuchUserException;
import oracle.iam.identity.exception.SearchKeyNotUniqueException;
import oracle.iam.identity.exception.UserLookupException;
import oracle.iam.identity.exception.UserManagerException;
import oracle.iam.identity.exception.UserModifyException;
import oracle.iam.identity.exception.ValidationFailedException;
import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.api.UserManagerConstants;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.identity.vo.Identity;
import oracle.iam.notification.api.NotificationService;
import oracle.iam.notification.exception.EventException;
import oracle.iam.notification.exception.MultipleTemplateException;
import oracle.iam.notification.exception.NotificationException;
import oracle.iam.notification.exception.NotificationResolverNotFoundException;
import oracle.iam.notification.exception.TemplateNotFoundException;
import oracle.iam.notification.exception.UnresolvedNotificationDataException;
import oracle.iam.notification.exception.UserDetailsNotFoundException;
import oracle.iam.notification.vo.NotificationEvent;
import oracle.iam.platform.Platform;
import oracle.iam.platform.authz.exception.AccessDeniedException;
import oracle.iam.platform.entitymgr.EntityManager;
import oracle.iam.platform.entitymgr.vo.SearchCriteria;
import oracle.iam.platform.utils.logging.SuperLogger;
import oracle.iam.scheduler.vo.StoppableTask;
import oracle.iam.scheduler.vo.TaskSupport;
import Thor.API.tcResultSet;
import Thor.API.tcUtilityFactory;
import Thor.API.Exceptions.tcAPIException;
import Thor.API.Exceptions.tcChallengeNotSetException;
import Thor.API.Exceptions.tcColumnNotFoundException;
import Thor.API.Exceptions.tcInvalidLookupException;
import Thor.API.Exceptions.tcLoginAttemptsExceededException;
import Thor.API.Exceptions.tcPasswordExpiredException;
import Thor.API.Exceptions.tcPasswordResetAttemptsExceededException;
import Thor.API.Exceptions.tcUserAccountDisabledException;
import Thor.API.Exceptions.tcUserAccountInvalidException;
import Thor.API.Exceptions.tcUserAlreadyLoggedInException;
import Thor.API.Operations.tcLookupOperationsIntf;

import com.aptecllc.iam.oim.utils.notificationprovider.APTECEMAILNotificationProvider;
import com.aptecllc.iam.oim.utils.notificationprovider.APTECEMAILPayload;


//~--- JDK imports ------------------------------------------------------------














import com.thortech.xl.util.config.ConfigurationClient;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import java.util.logging.Level;

/**
* 
* This class searches users that meet a certain criteria and checks their start date. If the user has not accepted the 
* terms and conditions, has not been sent an account claiming email before, AND their start date is today, an account claiming email
* will be sent to them. The appropriate template will also be sent (Employee, Student, Contractor, New Innovation).
*
*
* @version        1.0.0, 14/03/24
* @author         Shine Thomas, APTEC LLC
*/
public class AccountClaimingNotificationTask extends TaskSupport implements StoppableTask {
    private static final Logger logger = Logger.getLogger(AccountClaimingNotificationTask.class.getName());
    private final static Logger LOGGER = SuperLogger.getLogger(AccountClaimingNotificationTask.class.getName());
    private StringBuffer results = new StringBuffer();
    private String crlf = "\r\n";
    List<String> userList = new ArrayList<String>();
    HashMap<Object,Object> allUsersMap = new HashMap<Object,Object>();
    Map<String,String> userMap = new HashMap<String,String>();
    ArrayList<String> exclusionList = new ArrayList<String>();
    String managerKey = null;
    
    String resultsTo = "";
    String smtpServer = "";
    String resultsFrom = "";
    String scheduledJobName = "";
               
     /**
      * Method description:
      * This method creates a hash map of users then checks if the users should receive an account claiming email.
      * If the account has not accepted the terms and conditions, has not received an account claiming email before, and their start date is today
      * an email will be sent to the user.
      *
      * @param attributes
      *
      * @throws Exception
      */
    public void execute(HashMap attributes) throws Exception {
                String methodName="execute";
                logger.logp(Level.FINE, getClass().getName(), methodName,"entering");
                System.out.println("Inside execute method of sponsor notification task");
                scheduledJobName = getName();
       
                logger.logp(Level.INFO, getClass().getName(), methodName, "MIA-DUT-001 Scheduled Job: " + scheduledJobName);
                results.append(crlf + "Results from Scheduled Job: " + scheduledJobName);
       
        
        
                resultsTo = ((String) attributes.get("ResultsTo"));
                logger.logp(Level.FINE, getClass().getName(), methodName, "MIA-DUT-005 ResultsTo=" + resultsTo);
                results.append(crlf + "ResultsTo=" + resultsTo);
                               
                resultsFrom = ((String) attributes.get("ResultsFrom"));
                logger.logp(Level.FINE, getClass().getName(), methodName, "MIA-DUT-006 ResultsFrom=" + resultsFrom);
                results.append(crlf + "ResultsFrom=" + resultsFrom);

                smtpServer = ((String) attributes.get("SMTPServer"));
                logger.logp(Level.FINE, getClass().getName(), methodName, "MIA-DUT-007 ResultsTo=" + smtpServer);
               results.append(crlf + "ResultsTo=" + smtpServer);
                               
                System.out.println("Before calling getUsers method");
                
                Tuple<HashMap<Object,Object>, String> userMapAndManagerKey = getUsers("TermsAccepted", "Y", "EmailSent", "Y", "Role","PreHire","JobCode","S10008");
                                
                accountClaimingCheck(userMapAndManagerKey.t1);
       
    }
     
    /**
      * Method Description:
      * This method first adds the attributes it wants to be looked up in OIM
      * It then searches in OIM for users
      *  who ( have NOT accepted terms yet OR Email has not been sent OR UserType is PreHire OR JobCode is S10008) and gets the attributes added to retAttrs.
      * Finally, It takes the information from the search and adds it to a hashmap in the following form
      * Key = userID:AlternateEmail;userType|userLogin 
      * Value = startDate
      * 
      * UPDATE: getUsers now uses the Tuple class. This data structure holds two objects which allows the method to return 
      * two different objects, which was needed to pass the manager key all the way down. Tuple holds the userHashMap, and the manager
      * Key, respectively. 
      * 
      * @return
      * Returns a Tuple, with the userHashMap and managerKey in it.
     * @throws AccessDeniedException 
     * @throws SearchKeyNotUniqueException 
      */
    private Tuple<HashMap<Object,Object>, String> getUsers(Object searchField1, Object searchCrit1, Object searchField2, Object searchCrit2, Object searchField3, Object searchCrit3, Object searchField4, Object searchCrit4) throws SearchKeyNotUniqueException, AccessDeniedException {
         
                 //Declare all variables for searching and storing users that will be considered for Account Claiming
                 
                 String managerKey = null;
                 String methodName="getUsers";
         System.out.println("Inside getUsers method");
         List<User> sponsoredindividual         = null;
         logger.logp(Level.FINE, getClass().getName(), methodName,"entering");
         UserManager             usrMgr         = Platform.getService(UserManager.class);
         HashMap<String, Object> configParams   = new HashMap<String, Object>();
         HashMap<Object, Object> userMap        = new HashMap<Object, Object>();   
         SearchCriteria          crit1          = new SearchCriteria(searchField1, searchCrit1,SearchCriteria.Operator.NOT_EQUAL);         
         SearchCriteria          crit2          = new SearchCriteria(searchField2, searchCrit2,SearchCriteria.Operator.NOT_EQUAL);
         SearchCriteria          crit3          = new SearchCriteria(searchField3, searchCrit3,SearchCriteria.Operator.NOT_EQUAL);
         SearchCriteria          crit4          = new SearchCriteria(searchField4, searchCrit4,SearchCriteria.Operator.NOT_EQUAL);
         //SearchCriteria          fullCriteria   = new SearchCriteria(crit1, crit2, SearchCriteria.Operator.AND);
         SearchCriteria          fullCriteria   = new SearchCriteria(crit1, crit2, SearchCriteria.Operator.AND);
         SearchCriteria          updatedCriteria   = new SearchCriteria(fullCriteria, crit3, SearchCriteria.Operator.AND);
         SearchCriteria          finalCriteria   = new SearchCriteria(updatedCriteria, crit4, SearchCriteria.Operator.AND);
         Set<String>             retAttrs       = new HashSet<String>();
         
         
         //Here is where you add attributes to lookup in OIM
         retAttrs.add(UserManagerConstants.AttributeName.FIRSTNAME.getId());
         retAttrs.add(UserManagerConstants.AttributeName.LASTNAME.getId());
         retAttrs.add("AlternateEmail");
        retAttrs.add(UserManagerConstants.AttributeName.ACCOUNT_START_DATE.getId());
         retAttrs.add(UserManagerConstants.AttributeName.EMPTYPE.getId());
         retAttrs.add(UserManagerConstants.AttributeName.USER_LOGIN.getId());
         retAttrs.add(UserManagerConstants.AttributeName.MANAGER_KEY.getId());
         //added on 06/29
         retAttrs.add("Company");
         //
       
         
         
         //Attempt to search for users
         try {
            
                 //sponsoredindividual = usrMgr.search(fullCriteria, retAttrs, configParams);
                      sponsoredindividual = usrMgr.search(finalCriteria, retAttrs, configParams);
                 System.out.println("Size of sponsor user population "+sponsoredindividual.size());
             for (Identity identity : sponsoredindividual) {
                         if (identity.getEntityId() != null) {
                             
                              //Here is where the information from the search is put into variables for each user then added to a hash map
                             String userName = (String) identity.getAttribute(UserManagerConstants.AttributeName.FIRSTNAME.getId()) + "+" 
                              + (String) identity.getAttribute(UserManagerConstants.AttributeName.LASTNAME.getId());
                             String alternateEmail = (String) identity.getAttribute("AlternateEmail");
                             
                             //Add condition for startDate being null
                             Date startDate = (Date) identity.getAttribute(UserManagerConstants.AttributeName.ACCOUNT_START_DATE.getId());
                             if(startDate == null){
                             Date currentDate = new Date();
                              startDate = currentDate;
                             }
                             
                             String userType = (String) identity.getAttribute(UserManagerConstants.AttributeName.EMPTYPE.getId());
                             String userLogin = (String) identity.getAttribute(UserManagerConstants.AttributeName.USER_LOGIN.getId());
                             String userCompany= (String) identity.getAttribute("Company");
                             UserManager userService = Platform.getService(UserManager.class);
                             User usr = userService.getDetails(identity.getEntityId().toString(), null, false);
                             managerKey = usr.getManagerKey();
                             System.out.println("User manager key inside Tuple is:"+managerKey+"*alternate email->*"+alternateEmail+":Company->"+userCompany);
                             
                             
                             //String loginAndEmail = userName + ":" + alternateEmail + ";" + userType + "$" + managerKey+ "|" + userLogin;
                             // Added on 06/29
                             String loginAndEmail = userName + ":" + alternateEmail + ";" + userType + "$" + managerKey+ "|" + userLogin+ "*" + userCompany;
                             System.out.println("loginAndEmail--->"+loginAndEmail);
                             
                             
                             System.out.println("User Login: "+userLogin);
                             //if ((alternateEmail!=null && managerKey==null)||(alternateEmail==null && managerKey!=null)){
                             //Added on 09/09 to fix job where employee with no manager have alternate email
                             System.out.println("Jenkins Test");
                             //if ((alternateEmail!=null && managerKey==null && (userType.equalsIgnoreCase("Contractor") || userType.equalsIgnoreCase("Student") || userType.equalsIgnoreCase("New Innovation")))||(alternateEmail==null && managerKey!=null) ||(alternateEmail!=null && managerKey!=null) ||(userCompany!=null && userCompany.equalsIgnoreCase("WUH"))){
                             if ((alternateEmail!=null && managerKey==null && (userType.equalsIgnoreCase("Contractor") || userType.equalsIgnoreCase("Student") || userType.equalsIgnoreCase("New Innovation")))||(alternateEmail==null && managerKey!=null) ||(alternateEmail!=null && managerKey!=null)){
                             //if ((alternateEmail!=null && managerKey==null && (userType.equalsIgnoreCase("Contractor") || userType.equalsIgnoreCase("Student") || userType.equalsIgnoreCase("New Innovation")))||(alternateEmail==null && managerKey!=null)){
                             userMap.put(loginAndEmail,startDate);
                             }
                            
                             /** Here we have a hash map of users with
                              *  Key = userID:AlternateEmail;userType|userLogin
                              *  Value = startDate
                              */
                         }
            }
                   
             
                    
                            
         } catch (UserManagerException e) {
  
                 logger.logp(Level.SEVERE, getClass().getName(), methodName,
                                 "MIA-DUT-012 UserManagerException"+e.getMessage());
                 results.append(crlf + "UserManagerException"+e.getMessage());
         }
         Tuple<HashMap<Object,Object>, String> tuple = new Tuple<HashMap<Object,Object>, String>(userMap, managerKey);
         return tuple;
     }
     
     /**
      * Method Description
      * This method separates the Key from userID:AlternateEmail;userType|userLogin into separate variables for each attribute
      * It then checks userType and sets the template for the notification even to use
      * If the user's start date is today, this method will call a method to send an email
      * The method calls upon the stringDateConvert method to convert a string date format to integers for easy comparisons, therefor it is
      * important that the currentDate and startDate both strings
      * 
      * @param hm
      * This method takes a hash map in the form of
      * Key = userID:AlternateEmail;userType|userLogin
      * Value = startDate
     * @throws AccessDeniedException 
     * @throws UserLookupException 
     * @throws NoSuchUserException 
      */
     @SuppressWarnings("rawtypes")
                private void accountClaimingCheck(HashMap<Object, Object> hm) throws NoSuchUserException, UserLookupException, AccessDeniedException{
    
                //Declare variables needed within the method
                String currentDate = new Date().toString();
                String templateName = "";
                                Set testSet = hm.entrySet();
                                Iterator i = testSet.iterator();
                                //Using a while loop to iterate through the HashMap
                               while(i.hasNext()){
                                               Map.Entry me = (Map.Entry)i.next();
                                               
                                                if(me.getValue() != null){

                                                               String hashKey = me.getKey().toString();
                                                               System.out.println("hashKey-->"+hashKey);
                                                               int splitAtSemicolon = findSemicolon(hashKey);
                                                               int splitAtColon = findColon(hashKey);
                                                               int splitAtPlus = findPlus(hashKey);
                                                               int splitAtBar = findBar(hashKey);
                                                               int splitAtDollar = findDollar(hashKey);
                                                               //added 06/29
                                                               int splitAtRate = findRate(hashKey);
                                                               String firstName = hashKey.substring(0,splitAtPlus);
                                                               System.out.println("Account Claiming email : firstName-> "+firstName);
                                                               String lastName = hashKey.substring(splitAtPlus + 1, splitAtColon);
                                                               System.out.println("Account Claiming email : lastName-> "+lastName);
                                                               String altEmail = hashKey.substring(splitAtColon + 1, splitAtSemicolon); 
                                                                System.out.println("Account Claiming email : altEmail-> "+altEmail);
                                                               String userType = hashKey.substring(splitAtSemicolon + 1, splitAtDollar);
                                                               System.out.println("Account Claiming email : userType-> "+userType);
                                                               String usermanager = hashKey.substring(splitAtDollar + 1, splitAtBar);
                                                               System.out.println("Account Claiming email : usermanager-> "+usermanager);
                                                               String userLogin = hashKey.substring(splitAtBar + 1, splitAtRate);
                                                               System.out.println("Account Claiming email : userLogin-> "+userLogin);
                                                               String userCompany = hashKey.substring(splitAtRate + 1, hashKey.length());
                                                               System.out.println("Account Claiming email : userCompany-> "+userCompany);
                                                               String startDate =  me.getValue().toString();         
                
                                                
                                                                //Set Email Template Here based on userType
                                                               //Logic change: this now pools all employee types into an else statement, to ensure that no emp type is getting glossed over. 
                                                                //So if you are a contractor, student, or new innovation, you are receiving a certain templated email, EVERYONE ELSE gets the employee email.
                                                               
                                                                if(userType.equalsIgnoreCase("Contractor") || userType.equalsIgnoreCase("Student") || userType.equalsIgnoreCase("New Innovation")  ){
                                                                               templateName = "AccountClaimingEmail"; 
                                                                }
                                                              /* else if (userCompany!=null && userCompany.equalsIgnoreCase("WUH"))
                                                                                               {
                                                                               templateName = "AccountClaimingEmailWinthrop";
                                                                                               }*/
                                                               else {
                                                                               templateName = "AccountClaimingEmailEmployee"; 
                                                                }
                               
                                                                
                                                
                                                
                                                                //The main if block that checks if start date is today, then it continues with the account claiming email
                                                               //If not, the code drops for the current user, and continues to check for another one. 
                                                
                                                                User u = new User(userLogin);
                                                               String sponsor = (String)u.getAttribute("SponsorId");
                                                               //System.out.println("Sponsor ID is: "+ sponsor);
                                                               //added on 04/22 for nancy Sanchez issue instead of exclusionListCheck(sponsor) exclusionListCheck(usermanager)
                                                               //if(stringDateConvert(startDate)==stringDateConvert(currentDate)){ 
                                                                // Added on June 1st 2017 to fix the issue of users not getting account claiming email if their manager is a VIP
                                                               if(!userType.equalsIgnoreCase("Contractor"))
                                                               {
                                                                               if(exclusionListCheck(usermanager)){
                                                                                               System.out.println("manager " + usermanager + " is on email exclusion list.");
                                                                                               continue;
                                                                               }
                                                               }
                                                       //sendAccountClaimingEmail(templateName, altEmail, firstName, lastName, userLogin, managerKey);
                                                                               System.out.println("before calling sendAccountClaimingEmail: parameters are :-> templateName"+templateName+":altEmail->"+altEmail+":firstName->"+firstName+":lastName:->"+lastName+":userLogin:->"+userLogin+":usermanager:->"+usermanager);
                                                                               sendAccountClaimingEmail(templateName, altEmail, firstName, lastName, userLogin, usermanager);
                                                                               
                                                                }//end if
                                               //}//end if
                               }//end while
     }//end method
     
     /**
      * Method Description
      * This method calls a method to create an email with the provided information then attempts to send the email.
      * 
      * @param templateName
      * @param altEmail
      * @param firstName
      * @param lastName
      * @param userLogin
     * @throws AccessDeniedException 
     * @throws UserLookupException 
     * @throws NoSuchUserException 
      */
                private void sendAccountClaimingEmail(String templateName, String altEmail, String firstName, String lastName, String userLogin, String managerKey) throws NoSuchUserException, UserLookupException, AccessDeniedException {
                                
                                //Declare variables for method
                                Platform.getService(EntityManager.class);
                               NotificationService notifServ = Platform.getService(NotificationService.class);
                               UserManager usrMgr = Platform.getService(UserManager.class);
                               NotificationEvent accountClaimingNotificationEvent = null;

                                //Use params to call createNotificationEvent method
                               System.out.println("Before accountClaimingNotificationEvent");
                               //if alternate email is not null
                               //if usermanager key is null
                               
                                System.out.println("Just before sending email altEmail and managerKey are-"+altEmail+"-"+managerKey);               
                               accountClaimingNotificationEvent = createNotificationEvent(templateName, altEmail, firstName, lastName, userLogin, managerKey);
                               System.out.println("After accountClaimingNotificationEvent");

                               
                                //Attempt to send e-mail with the given params. 
                                try {
                                               System.out.println("Attempting to send email.");
                                                                notifServ.notify(accountClaimingNotificationEvent);
                                                                System.out.println("After sending attempt.");
                                                                //After e-mail is sent,     the "email sent" attribute must be toggled to "Y", so another e-mail is not sent                                             
                                                                System.out.println("Now check EMAILSENT to 'Y'");
                                                                HashMap<String, Object>  updateAttrs = new HashMap<String, Object>();
                                                                updateAttrs.put("EmailSent", "Y");
                                                                User user = new User(userLogin, updateAttrs);
                                                                usrMgr.modify(UserManagerConstants.AttributeName.USER_LOGIN.getId(), userLogin, user);
                                                                
                                                                
                                                                
                                                //Catching a whole mess of things that can go wrong. With print lines so you know which one it is.        
                                                } catch (UserDetailsNotFoundException e) {
                                                                System.out.println("UserDetailsNotFoundException");
                                                                e.printStackTrace();
                                                } catch (EventException e) {
                                                                e.printStackTrace();
                                                                System.out.println("EventException");
                                                } catch (UnresolvedNotificationDataException e) {
                                                                e.printStackTrace();
                                                                System.out.println("UnresolvedNotificationDataException");
                                                } catch (TemplateNotFoundException e) {
                                                                e.printStackTrace();
                                                                System.out.println("TemplateNotFoundException");
                                                } catch (MultipleTemplateException e) {
                                                                e.printStackTrace();
                                                                System.out.println("MultipleTemplateException");
                                                } catch (NotificationResolverNotFoundException e) {
                                                                e.printStackTrace();
                                                                System.out.println("NotificationResolverNotFoundException");
                                                } catch (NotificationException e) {
                                                                e.printStackTrace();
                                                                System.out.println("NotificationException");
                                                } catch (AccessDeniedException e) {
                                                                e.printStackTrace();
                                                                System.out.println("AccessDeniedException");
                                                } catch (ValidationFailedException e) {
                                                                e.printStackTrace();
                                                                System.out.println("ValidationFailedException");
                                                } catch (UserModifyException e) {
                                                                e.printStackTrace();
                                                                System.out.println("UserModifyException");
                                                } catch (NoSuchUserException e) {
                                                                e.printStackTrace();
                                                                System.out.println("NoSuchUserException");
                                                } catch (SearchKeyNotUniqueException e) {
                                                                e.printStackTrace();
                                                                System.out.println("SearchKeyNotUniqueException");
                                                }
                               
                                                }
                                                
                /**
                  * Method Description
                  * This method takes the information passed into it in the parameters and creates a notification event
                  * This method also adds any variables you want to use in the body of the email to the Hashmap map
                  * 
                  * @param templateName
                  * @param altEmail
                  * @param firstName
                  * @param lastName
                  * @param userLogin
                  * @return NotificationEvent
                  * Returns a notification event to send an email to user's alternate email address
                * @throws AccessDeniedException 
                 * @throws UserLookupException 
                 * @throws NoSuchUserException 
                  */         
                private NotificationEvent createNotificationEvent(String templateName, String altEmail, String firstName, String lastName, String userLogin, String managerKey) throws NoSuchUserException, UserLookupException, AccessDeniedException {
                                
                                System.out.println("manager Key being passed in createNotificationEvent is :"+managerKey);
                                //Declare variables and load your dynamic variables into a HashMap to be sent to he Notification Event
                                NotificationEvent event = new NotificationEvent();
        //System.out.println("Template Name: "+templateName);
                                event.setTemplateName(templateName);
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("UserFirstName", firstName);
        map.put("UserLastName", lastName);
        
        //hashedkey uses the encrypt function to create a long hashed key of the userlogin, to put it in the notification event.
        BigInteger hashedkey = encrypt((String) userLogin);
        map.put("userkey", hashedkey);
        
        //Set all elements of the map to the notification event.
        event.setParams(map);
        
        //Some APTEC packages used here to sent the email.
        List<String> emailToList = new ArrayList<String>();
        
        //Deciding whether or not to send the email to the alternate email address or to the manager's email address, based upon
        //the template that is being sent. 
        
        
        if(templateName.equals("AccountClaimingEmail")){
               System.out.println("Sending template: "+templateName);
               emailToList.add(altEmail);
        }
        
        if(templateName.equals("AccountClaimingEmailWinthrop")){
               System.out.println("Sending template: "+templateName);
               emailToList.add(altEmail);
        }
        
        if(templateName.equals("AccountClaimingEmailEmployee") && managerKey != null){
               UserManager userService = Platform.getService(UserManager.class);
            User usr = userService.getDetails(managerKey, null, false);
               String managerEmail = usr.getEmail();
            System.out.println("Email being sent to manager: "+managerEmail+", with template: "+templateName);
               emailToList.add(managerEmail);
        }
        
        //Finish off setting up the e-mail notification event, then return the event to be sent.
        APTECEMAILPayload emailPayload = new APTECEMAILPayload(emailToList, "oimadmin@nyumc.org", true, "UTF-8");
        
        //Creating a new list to house the CC email addresses (9/24/2015 - Dan Melando)
        List<String> ccEmails = new ArrayList<String>();
        ccEmails.add("NYUSecAdminTeam@nyumc.org");
        
        //Putting the from email address and CC email list into the payload to be sent
        emailPayload.setSenderEmailID("oimadmin@nyumc.org");
        emailPayload.setCcMailIds(ccEmails);
        //emailPayload.setBccEmailIds(<OPTIONAL-LISTOFBCCADDRESSES>);
        APTECEMAILNotificationProvider notProvider = new APTECEMAILNotificationProvider(event, emailPayload, "http://iam1-soa.nyumc.org:8001/ucs/messaging/webservice");
        try {
                                                if (notProvider.sendMessage())
                                                    LOGGER.logp(Level.FINE, getClass().getName(), "sendEmailNotificationToUser", "Email Sent Successfully to User with Email Address '" +   "'.");
                                                } catch (Exception e) {
                                                                e.printStackTrace();
                                                }
        
        return event;
                }
                
/** 
 *  Below are various helper methods that are all in stable states. 
 *  No changes should need to be made on any of these methods. 
 *  
 *  Date 6/12/2015
*/
                
                static BigInteger p = new BigInteger(
            "186903683994784883974286803981340343409");
                static BigInteger q = new BigInteger(
            "238365206090827088390448048852555109291");
                static BigInteger n = p.multiply(q);
                static BigInteger phiN = (p.subtract(BigInteger.ONE)).multiply((q
            .subtract(BigInteger.ONE)));
                static BigInteger e = new BigInteger("449");
                static BigInteger d = new BigInteger(
            "17364106129279623304364236508747275879893306787892962132847318388210189166049");
                public static BigInteger encodeMessageAsBigInteger(String plaintext) {
     return new BigInteger(plaintext.getBytes());
     }
         
    public static String recoverTextFromBigInteger(BigInteger plaintext) {
     return new String(plaintext.toByteArray());
     }
         
    private static BigInteger encrypt(String input){
         
          BigInteger m = encodeMessageAsBigInteger(input);
         
          BigInteger c = m.modPow(e, n);
          return c;
          }
         
    private static BigInteger encryptInt(int input){
         
      String aString = Integer.toString(input);
      
          BigInteger m = encodeMessageAsBigInteger(aString);
         
          BigInteger c = m.modPow(e, n);
          return c;
          }
         
    private static BigInteger decrypt(BigInteger c){
     return c.modPow(d,n);
     }
                
                private int findColon(String input){
                
                                for(int i = 0; i < input.length(); i++){
                                                if(input.charAt(i) == ':'){
                                                                return i;
                                                }
                                }
                
                
                                return 0;
                }
                
                private int findSemicolon(String input){
                                
                                for(int i = 0; i < input.length(); i++){
                                                if(input.charAt(i) == ';'){
                                                                return i;
                                                }
                                }
                
                
                                return 0;
                }
                
                private int findPlus(String input){
                                
                                for(int i = 0; i < input.length(); i++){
                                                if(input.charAt(i) == '+'){
                                                                return i;
                                                }
                                }
                
                
                                return 0;
                }
                
                private int findBar(String input){
                                
                                for(int i = 0; i < input.length(); i++){
                                                if(input.charAt(i) == '|'){
                                                                return i;
                                                }
                                }
                
                
                                return 0;
                }
                
   private int findDollar(String input){
                                
                                for(int i = 0; i < input.length(); i++){
                                                if(input.charAt(i) == '$'){
                                                                return i;
                                                }
                                }
                
                
                                return 0;
                }
   
   // added on 06/29
   
   private int findRate(String input){
                                
                                for(int i = 0; i < input.length(); i++){
                                                if(input.charAt(i) == '*'){
                                                                return i;
                                                }
                                }
                
                
                                return 0;
                }
   
                private int stringDateConvert(String date){
                
                                //Setup initial Date Format of the to-be converted date and 
                                //initialize the tempDate to receive the method parameters. 
                
                                SimpleDateFormat format = new SimpleDateFormat("EEE MMM "
                                                + "dd HH:mm:ss z yyyy");
                                Date tempDate = new Date();
                
                
                                //Try to parse the parameter date.
                                try {
                                                tempDate = format.parse(date);
                                }
                
                                //Catch an error during parsing.
                                catch (ParseException e){
                                                e.printStackTrace();
                                                return 0;
                                                }
                
                //Setup a new DateFormat with the format we want and return the date we wanted. 
                String formattedDate = new SimpleDateFormat("yyyyMMdd").format(tempDate);
                return Integer.parseInt(formattedDate);
                
}
                
                private boolean exclusionListCheck(String userID){
                                //add exclusions here during testing
                //exclusionList.add("ZAIDIA01");
                String lookupCode = "";
                
                System.out.println("Inside exclusionListCheck and the user is:"+userID);
                
                ConfigurationClient.ComplexSetting config =ConfigurationClient.getComplexSettingByPath("Discovery.CoreServer");
                Hashtable env = config.getAllSettings();
                try {
            
                                
                                tcUtilityFactory ioUtilityFactory = new tcUtilityFactory(env,"xelsysadm","nyuL4ng0n3");
            //tcLookupOperationsIntf lookup    = oimClient.getTcLookupOpInt();
            tcLookupOperationsIntf lookup = (tcLookupOperationsIntf)ioUtilityFactory.getUtility("Thor.API.Operations.tcLookupOperationsIntf");
                  
                   tcResultSet rs= lookup.getLookupValues("Lookup.NYU.VIPList");
                  int count=rs.getRowCount();
                   System.out.println("Count is->"+count);
                  
                      
                        if ((rs != null) && (rs.getRowCount() > 0))
                        {
                            for (int i = 0; i < rs.getRowCount(); i++)
                            {
                                 rs.goToRow(i);
                                 lookupCode = rs.getStringValue("LKV_ENCODED");
                                 //System.out.println(lookupCode);
                                 exclusionList.add(lookupCode);
                            }
                        }
                   
                  
            } catch (tcAPIException e) {
                   // TODO Auto-generated catch block
                   e.printStackTrace();
            } catch (tcInvalidLookupException e) {
                   // TODO Auto-generated catch block
                   e.printStackTrace();
            } catch (tcColumnNotFoundException e) {
                   // TODO Auto-generated catch block
                   e.printStackTrace();
            } catch (tcChallengeNotSetException e) {
                                                                // TODO Auto-generated catch block
                                                                e.printStackTrace();
                                                } catch (tcLoginAttemptsExceededException e) {
                                                                // TODO Auto-generated catch block
                                                                e.printStackTrace();
                                                } catch (tcPasswordResetAttemptsExceededException e) {
                                                                // TODO Auto-generated catch block
                                                                e.printStackTrace();
                                                } catch (tcPasswordExpiredException e) {
                                                                // TODO Auto-generated catch block
                                                                e.printStackTrace();
                                                } catch (tcUserAccountDisabledException e) {
                                                                // TODO Auto-generated catch block
                                                                e.printStackTrace();
                                                } catch (tcUserAccountInvalidException e) {
                                                                // TODO Auto-generated catch block
                                                                e.printStackTrace();
                                                } catch (tcUserAlreadyLoggedInException e) {
                                                                // TODO Auto-generated catch block
                                                                e.printStackTrace();
                                                }
                
                
                
                
                for(int i = 0; i < exclusionList.size(); i++){
                                System.out.println("VIP List is"+exclusionList.get(i));
                                if(exclusionList.get(i).equalsIgnoreCase(userID)){
                                                System.out.println("Manager is a VIP");
                                                return true;
                                }
                }
                
                
                return false;
    }

/**
* These are required
*/
                @Override
                public HashMap getAttributes() {
                                // TODO Auto-generated method stub
                                return null;
                }

                @Override
                public void setAttributes() {
                                // TODO Auto-generated method stub
                                
                }
}
