package com.aptecllc.sms.message;

/**
 * Abstract SMS Message - Contains minimum expected fields for all gateways.
 *
 *
 * @version        1.0, 14/04/22
 * @author         Eric A. Fisher, APTEC LLC
 */
public abstract class AbstractSMSMessage {
    private String senderID;
    private String recipientID;
    private String messageBody;

    /**
     * Constructs ...
     *
     */
    public AbstractSMSMessage() {}

    /**
     * Constructs ...
     *
     *
     * @param pSenderID
     * @param pRecipientID
     * @param pMessage
     */
    public AbstractSMSMessage(String pSenderID, String pRecipientID, String pMessage) {
        this.senderID    = pSenderID;
        this.recipientID = pRecipientID;
        this.messageBody = pMessage;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getSenderID() {
        return senderID;
    }

    /**
     * Method description
     *
     *
     * @param senderID
     */
    public void setSenderID(String senderID) {
        this.senderID = senderID;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getRecipientID() {
        return recipientID;
    }

    /**
     * Method description
     *
     *
     * @param recipientID
     */
    public void setRecipientID(String recipientID) {
        this.recipientID = recipientID;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getMessageBody() {
        return messageBody;
    }

    /**
     * Method description
     *
     *
     * @param messageBody
     */
    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }

    public abstract Object getTransmittalObject();
}
