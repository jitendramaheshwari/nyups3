package com.aptecllc.sms.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.aptecllc.sms.SMSCommunicator;
import com.aptecllc.sms.gateway.ClickatellHTTPGateway;

public class ClickatelSMSCommunicatorTest {
	
	SMSCommunicator comm = new SMSCommunicator();
	
	ClickatellHTTPGateway clickatelGateway = new ClickatellHTTPGateway();

	@Before
	public void setUp() throws Exception {
		clickatelGateway.setApi_id("3474288");
		clickatelGateway.setSenderID("16468635283");
		clickatelGateway.setUsername("APTEC123");
		clickatelGateway.setPassword("OAAMOTP66");
		
		comm.setSmsGatewayClient(clickatelGateway);
		
	}

	@Test
	public void testSendSMSMessage() {
		String result=comm.sendSMSMessage("12014525576", "Test Message");
		assertNotSame("ERR", result);
	}

}
