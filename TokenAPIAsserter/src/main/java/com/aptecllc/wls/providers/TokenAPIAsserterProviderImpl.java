package com.aptecllc.wls.providers;

import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.login.AppConfigurationEntry;

import weblogic.management.security.ProviderMBean;
import weblogic.security.service.ContextHandler;
import weblogic.security.spi.AuthenticationProviderV2;
import weblogic.security.spi.IdentityAsserterV2;
import weblogic.security.spi.IdentityAssertionException;
import weblogic.security.spi.PrincipalValidator;
import weblogic.security.spi.SecurityServices;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;






import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.login.AppConfigurationEntry;
import javax.security.auth.login.AppConfigurationEntry.LoginModuleControlFlag;

import com.aptecllc.wls.providers.token.APITokenType;

import weblogic.management.security.ProviderMBean;
import weblogic.security.provider.PrincipalValidatorImpl;
import weblogic.security.service.ContextHandler;
import weblogic.security.spi.AuthenticationProviderV2;
import weblogic.security.spi.IdentityAsserterV2;
import weblogic.security.spi.IdentityAssertionException;
import weblogic.security.spi.PrincipalValidator;
import weblogic.security.spi.SecurityServices;

/**
 * The simple sample identity asserter's runtime implementation.
 *
 * It looks for tokens of type "SamplePerimeterAtnToken"
 * whose matching token is an array of bytes containing a
 * string in the form "username=someusername".
 *
 * It extracts the username from the token and stores it
 * in a SimpleSampleCallbackHandlerImpl.  This is returned to the
 * security framework who hands it to the authenticators'
 * login modules.  The login modules can use a NameCallback
 * to retrieve the user name from the simple sample identity
 * asserter's callback handler.
 *
 * Since it is an identity asserter, it must implement
 * the weblogic.security.spi.AuthenticationProvider and
 * the weblogic.security.spi.IdentityAsserter interfaces.
 *
 * It can either implement two classes, and use the
 * provider implementation as the factory as the
 * factory for the identity asserter, or it can implement
 * both interfaces in one class.  The simple sample identity
 * asserter implments both interfaces in one class.
 *
 * Note: The simple sample identity asserter's mbean's ProviderClassName
 * attribute must be set the the name of this class.
 *
 * @author Copyright (c) 2002 by BEA Systems. All Rights Reserved.
 */
public final class TokenAPIAsserterProviderImpl implements AuthenticationProviderV2, IdentityAsserterV2
{

  final static private String TOKEN_PREFIX = "username="; // the token contains a string in the form "username=someusername"

  private String description; // a description of this provider
  private LoginModuleControlFlag  controlFlag; // how this provider's login module should be used during the JAAS login
  private ArrayList<String> activeTypes = new ArrayList<String>();
  private HashMap<String,String> apiKeyPairs = new HashMap<String,String>();
  private boolean debugMode = true;

  /**
   * Initialize the simple sample identity asserter.
   *
   * @param mbean A ProviderMBean that holds the simple sample identity asserter's
   * configuration data.  This mbean must be an instance of the simple sample
   * identity asserter's mbean.
   *
   * @param services The SecurityServices gives access to the auditor
   * so that the provider can to post audit events.
   * The simple sample role mapper doesn't use this parameter.
   *
   * @see SecurityProvider
   */
  public void initialize(ProviderMBean mbean, SecurityServices services)
  {
    System.out.println("TokenAPIAsserterProviderImpl.initialize");
    TokenAPIAsserterMBean myMBean = (TokenAPIAsserterMBean)mbean;
    description  = myMBean.getDescription() + "\n" + myMBean.getVersion();
    
    System.out.println("Supports: " + Arrays.toString(myMBean.getSupportedTypes()));
    activeTypes.addAll(Arrays.asList(myMBean.getSupportedTypes()));
    System.out.println("Active: " + Arrays.toString(myMBean.getActiveTypes()));
    
    for (String pair : myMBean.getValidAPIKeyPairs()) {
    	System.out.println(" Pair: " + pair);
    	String[] pairVals = pair.split("\\|");
    	
    	apiKeyPairs.put(pairVals[0],pairVals[1]);
    } 
    
    debugMode = myMBean.getDebugMode();
    
    controlFlag = LoginModuleControlFlag.OPTIONAL;
   /* String flag = myMBean.getControlFlag();
    if (flag.equalsIgnoreCase("REQUIRED")) {
      controlFlag = LoginModuleControlFlag.REQUIRED;
    } else if (flag.equalsIgnoreCase("OPTIONAL")) {
      controlFlag = LoginModuleControlFlag.OPTIONAL;
    } else if (flag.equalsIgnoreCase("REQUISITE")) {
      controlFlag = LoginModuleControlFlag.REQUISITE;
    } else if (flag.equalsIgnoreCase("SUFFICIENT")) {
      controlFlag = LoginModuleControlFlag.SUFFICIENT;
    } else {
      throw new IllegalArgumentException("invalid flag value" + flag);
    }*/
  }

  /**
   * Get the simple sample identity asserter's description.
   *
   * @return A String containing a brief description of the simple sample identity asserter.
   *
   * @see SecurityProvider
   */
  public String getDescription()
  {
    return description;
  }

  /**
   * Shutdown the simple sample identity asserter.
   *
   * A no-op.
   *
   * @see SecurityProvider
   */
  public void shutdown()
  {
    System.out.println("TokenAPIAsserterProviderImpl.shutdown");
  }

  /**
   * Gets the simple sample identity assertion provider's identity asserter object.
   *
   * @return The simple sample identity assertion provider's IdentityAsserter object.
   *
   * @see AuthenticationProvider
   */
  public IdentityAsserterV2 getIdentityAsserter()
  {
    return this;
  }

  /**
   * Assert identity given a token that identifies the user.
   *
   * @param type A String containing the token type.  The simple sample identity
   * asserter only supports tokens of type "SamplePerimeterAtnToken".
   * Also, the simple sample identity asserter's mbean's "ActiveTypes" attribute
   * must be set to "SamplePerimeterAtnToken" (which is done by default
   * when the mbean is created).
   *
   * @param token An Object containing the token that identifies the user.
   * The simple sample identity asserter's token must be an array of bytes
   * containing a String of the form "username=someusername".
   *
   * @param handler A ContextHandler object that can optionally
   * be used to obtain additional information that may be used in
   * asserting the identity.  If the caller is unable to provide additional
   * information, a null value should be specified.  This sample
   * ignores the handler.
   *
   * While, for simplicity, this sample does not validate the
   * contents of the token, identity asserters typically should do
   * this (to prevent someone from forging a token).  For
   * example, when using Kerberos, the token may be generated
   * and "signed" by a Kerberos server and the identity asserter
   * hands the token back to the Kerberos server to get it
   * validated.  Another example: when asserting identity from
   * X509 certificates, then identity asserter should validate the
   * certificate - that it hasn't been tampered, that it's been
   * signed by a trusted CA, that it hasn't expired or revoked, etc.
   *
   * @return a CallbackHandler that stores the username from the token.
   * The username can only be retrieved from the callback handler by
   * passing in a NameCallback.  The sample returns an instance of
   * its CallbackHandler implementation (SimpleSampleCallbackHandlerImpl).
   *
   * @throws IdentityAssertionException if another token type is passed
   * in or the token doesn't have the correct form.
   */
  public CallbackHandler assertIdentity(String type, Object token, ContextHandler context) throws IdentityAssertionException
  {
	String userName = "";
    System.out.println("TokenAPIAsserterProviderImpl.assertIdentity");
    System.out.println("\tType\t\t= "  + type);
    System.out.println("\tToken\t\t= " + token);

    // check the token type
    if (!(activeTypes.contains(type))) {
      String error =
        "TokenAPIAsserter received unknown or not active token type \"" + type + "\"." +
        " Expected one of " + activeTypes.toString();
      System.out.println("\tError: " + error);
      throw new IdentityAssertionException(error);
    }

    // make sure the token is an array of bytes
    if (!(token instanceof byte[])) {
      String error = 
        "TokenAPIAsserter received unknown token class \"" + token.getClass() + "\"." +
        " Expected a byte[].";
      System.out.println("\tError: " + error);
      throw new IdentityAssertionException(error);
    }

    // convert the array of bytes to a string
    byte[] tokenBytes = (byte[])token;
    if (tokenBytes == null || tokenBytes.length < 1) {
      String error =
        "TokenAPIAsserter received empty token byte array";
      System.out.println("\tError: " + error);
      throw new IdentityAssertionException(error);
    }

    String tokenStr = new String(tokenBytes);

    if (type.equals(TokenAPIAsserterTokenTypes.USERNAMETOKEN)) {
    	userName = handleUserNameToken(type, tokenStr);
    } else if (type.equals(TokenAPIAsserterTokenTypes.APTECAPITOKEN)) {
    	userName = handleAptecAPIToken(type, tokenStr);
    } else if (type.equals(TokenAPIAsserterTokenTypes.REMOTE_USER)) {
    	userName = tokenStr;
    	 System.out.println("\tuserName\t= " + userName);
    }
    // store it in a callback handler that authenticators can use
    // to retrieve the username.
    System.out.println("TokenAPIAsserterProviderImpl.assertIdentity "+ userName);
    return new TokenAPICallbackHandlerImpl(userName);
  }

private String handleUserNameToken(String type, String tokenStr)
		throws IdentityAssertionException {
	// make sure the string contains "username=someusername
    if (!(tokenStr.startsWith(TOKEN_PREFIX))) {
      String error =
        "TokenAPIAsserter received unknown token string \"" + type + "\"." +
        " Expected " + TOKEN_PREFIX + "username";
      System.out.println("\tError: " + error);
      throw new IdentityAssertionException(error);
    }

    // extract the username from the token
    String userName = tokenStr.substring(TOKEN_PREFIX.length());
    System.out.println("\tuserName\t= " + userName);
	return userName;
}
  
private String handleAptecAPIToken(String type, String tokenStr) throws IdentityAssertionException {
	String userName = null;
	try {
		APITokenType apiToken = new APITokenType (tokenStr);
		
		String apiKey = apiToken.getApiKey();
		System.out.println("\tAPIKey\t= " + apiKey);
		String apiSecret = apiKeyPairs.get(apiKey);
		if (apiSecret == null) {
			System.out.println("Secret for APIKey " + apiKey + " not found.");
			throw new IdentityAssertionException("Secret for APIKey " + apiKey + " not found.");
		}
		
		if(apiToken.validateKey(apiSecret,debugMode))
		{
			userName = apiToken.getUserName();
		} else {
			System.out.println("Token Signature Invalid");
			throw new IdentityAssertionException("Token Signature Invalid");
		}
		
	} catch (Exception e) {
		
		
		throw new IdentityAssertionException("APIToken Format is Invalid " + e.getMessage());
	}
	
	return userName;
}
  /**
   * Create a JAAS AppConfigurationEntry (which tells JAAS
   * how to create the login module and how to use it) when
   * the simple sample authenticator is used to authenticate (vs. to
   * complete identity assertion).
   *
   * @return An AppConfigurationEntry that tells JAAS how to use the simple sample
   * authenticator's login module for authentication.
   */
  public AppConfigurationEntry getLoginModuleConfiguration()
  {
	  System.out.println("TokenAPIAsserterProviderImpl: getLoginModuleConfiguration");
    // Don't pass in any special options.
    // By default, the simple sample authenticator's login module
    // will authenticate (by checking that the passwords match).
    HashMap options = new HashMap();
   // return getConfiguration(options);
    return null;
  }

  /**
   * Create a JAAS AppConfigurationEntry (which tells JAAS
   * how to create the login module and how to use it).
   * This helper method is used both for authentication mode
   * and identity assertion mode.
   *
   * @param options A HashMap containing the options to pass to the
   * simple sample authenticator's login module.  This method adds the
   * "database helper" object to the options.  This allows the
   * login module to access the users and groups.
   *
   * @return An AppConfigurationEntry that tells JAAS how to use the simple sample
   * authenticator's login module.
   */
  private AppConfigurationEntry getConfiguration(HashMap options)
  {
      System.out.println("TokenAPIAsserterProviderImpl: getConfiguration");
    // make sure to specify the simple sample authenticator's login module
    // and to use the control flag from the simple sample authenticator's mbean.
    return new
      AppConfigurationEntry(
        "com.aptecllc.wls.providers.TokenAPILoginModuleImpl",
        controlFlag,
        options
      );
  }

  /**
   * Create a JAAS AppConfigurationEntry (which tells JAAS
   * how to create the login module and how to use it) when
   * the simple sample authenticator is used to complete identity
   * assertion (vs. to authenticate).
   *
   * @return An AppConfigurationEntry that tells JAAS how to use the simple sample
   * authenticator's login module for identity assertion.
   */
  public AppConfigurationEntry getAssertionModuleConfiguration()
  {
      System.out.println("TokenAPIAsserterProviderImpl: getAssertionModuleConfiguration");
    // Pass an option indicating that we're doing identity
    // assertion (vs. authentication) therefore the login module
    // should only check that the user exists (instead of checking
    // the password)
    HashMap options = new HashMap();
    options.put("IdentityAssertion","true");
    return null;
   // return getConfiguration(options);
  }

  /**
   * Return the principal validator that can validate the
   * principals that the authenticator's login module
   * puts into the subject.
   *
   * Since the simple sample authenticator uses the built in
   * WLSUserImpl and WLSGroupImpl principal classes, just
   * returns the built in PrincipalValidatorImpl that knows
   * how to handle these kinds of principals.
   */
  public PrincipalValidator getPrincipalValidator() 
  {
	  	return null;
      //return new PrincipalValidatorImpl();
  }
}