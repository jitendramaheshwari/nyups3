package com.aptecllc.wls.providers.test;

import static org.junit.Assert.*;


import org.junit.Test;

import com.aptecllc.wls.providers.token.APITokenType;

public class TestAPITokenType {
	private static final String API_KEY="DevBlazerIDCentral";
	private static final String SECRET="HF89s7hw89hsHFD8ds6927";

	

	@Test
	public void testGenerateAPIToken() {
		
		APITokenType token = new APITokenType(API_KEY,"xelsysadm");
		
		
		String generatedToken = token.generateAPIToken(SECRET);
		System.out.println(generatedToken);
		
		APITokenType checkToken =null;
		try {
			checkToken = new APITokenType(generatedToken);
			System.out.println(checkToken.generateAPIToken(SECRET));
			assertEquals("Equals Test",generatedToken, checkToken.generateAPIToken(SECRET));
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertTrue( "Matched Signature",checkToken.validateKey(SECRET,false));
		
		assertFalse("UnMatched Signature",checkToken.validateKey("noMatch",false));
		
		
	}



}
