package com.aptecllc.sms.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.aptecllc.sms.SMSCommunicator;

import com.aptecllc.sms.gateway.TwilioSMSGateway;

public class TwilioSMSCommunicatorTest {
	
	SMSCommunicator comm = new SMSCommunicator();
	
	TwilioSMSGateway gateway; 

	@Before
	public void setUp() throws Exception {
		gateway = new TwilioSMSGateway("ACea1cd58be7d4c556407db0d3e95a9542","077611363b5d7d4196d6214fcb5a99ff","+19892527832");
		
		comm.setSmsGatewayClient(gateway);
		
	}

	@Test
	public void testSendSMSMessage() {
		String result = comm.sendSMSMessage("12014525576", "Test Message");
		assertNotSame("ERR", result);
	}

}
