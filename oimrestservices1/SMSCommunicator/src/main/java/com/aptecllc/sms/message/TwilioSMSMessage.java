package com.aptecllc.sms.message;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

public class TwilioSMSMessage extends AbstractSMSMessage {
	

	



	@Override
	public Object getTransmittalObject() {
		 MultiValueMap<String, String> form = new LinkedMultiValueMap<String, String>();

	        form.add("From", this.getSenderID());
	        form.add("To", this.getRecipientID());
	        form.add("Body",this.getMessageBody());
	        

	        return form;
	}

}
