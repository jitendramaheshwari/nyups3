package com.aptecllc.sms.gateway;

import com.aptecllc.sms.message.AbstractSMSMessage;
import com.aptecllc.sms.message.DefaultMessage;

public class NoSMSGateway implements ISMSGateway {

	public AbstractSMSMessage getNewMessage() {
		
		return new DefaultMessage();
	}

	public String sendMessage(AbstractSMSMessage message) {
		// TODO Auto-generated method stub
		return "OK";
	}

}
