package com.aptecllc.oim.transformer;

//~--- non-JDK imports --------------------------------------------------------

import com.aptecllc.oim.converter.DataConverterIntf;
import com.aptecllc.oim.property.PropertyUtil;
import com.aptecllc.oim.util.ISO8601;
import com.aptecllc.oim.util.ServiceConstants;
import com.aptecllc.oim.vo.UserAccount;

import cz.jirutka.rsql.parser.ParseException;
import cz.jirutka.rsql.parser.RSQLParser;
import cz.jirutka.rsql.parser.TokenMgrError;
import cz.jirutka.rsql.parser.model.Comparison;
import cz.jirutka.rsql.parser.model.ComparisonExpression;
import cz.jirutka.rsql.parser.model.Expression;
import cz.jirutka.rsql.parser.model.Logical;
import cz.jirutka.rsql.parser.model.LogicalExpression;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.platform.entitymgr.vo.SearchCriteria;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//~--- JDK imports ------------------------------------------------------------

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class is a default implementation of a transformer that converts OIM User VOs
 * into REST server UserAccount VOs and vice versa.
 *
 *
 * @version 1.0, 13/11/06
 * @author Eric A. Fisher, APTEC LLC
 */
public class DefaultUserTransformer implements ObjectTransformerIntf {
    private static final Logger  logger  = LoggerFactory.getLogger(DefaultUserTransformer.class);
    private static final Pattern isoDate = Pattern.compile("\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}(Z|[+-][0-9:]*)?");

    /**
     * This method converts the UserAccount object used for REST transfers into
     * an OIM User VO for use in performing the updates. It leverages the
     * properties defined in the attributeMap.properties file to map the
     * UserAccount attribute name to the OIM Attribute Name
     *
     *------>Altered 12/4/2015 to allow non-String fields to pass back by Dan Donnelly
     *
     * @param pUserAccount
     *
     * @return
     */
    @Override
    public User convertUserAccount(UserAccount pUserAccount) {
        User                    user             = new User(null);
        HashMap<String, String> attributes       = getAttributeMap();
        HashMap<String, String> customAttributes = getCustomAttributeMap();
        HashMap<String, Object> customAttrValues = pUserAccount.getAttributes();

        for (String key : attributes.keySet()) {
            if (attributes.get(key) != null) {
                Object value = null;
                System.out.println(">>>>>>>>KeySS: "+key);
                if(key.equals("provisioningDate")){
                	try {
                        value = (Date) pUserAccount.getAttribute(key);
                        user.setAttribute(attributes.get(key), value);
                    } catch (IllegalArgumentException e) {
                        logger.error(e.getMessage());
                        e.printStackTrace();
                    }
                }
                
                // added on 02/24
               else if (key.equals("terminationDate")){
                	try {
                		System.out.println("Inside Default Transformer for terminationDate");
                        value = (Date) pUserAccount.getAttribute(key);
                        user.setAttribute(attributes.get(key), value);
                    } catch (IllegalArgumentException e) {
                        logger.error(e.getMessage());
                        e.printStackTrace();
                    }
                }
                // added on 02/24
                
                else{
                	try {
                		value = (String) pUserAccount.getAttribute(key);
                	} catch (IllegalArgumentException e) {
                		logger.error(e.getMessage());
                		e.printStackTrace();
                	}
                }

                logger.debug("Key is : " + key + " Value is : " + value);
                
                if (getAttributeConverterMap().containsKey(key) ) {
                	//Convert the value using the inbound converter.
                	DataConverterIntf converter = loadConverter((String) getAttributeConverterMap().get(key));
                	value = (String) converter.convertInbound(value);
                	logger.debug("Key is : " + key + " Converted Value is : " + value);
                }

                if (value != null) {
                    user.setAttribute(attributes.get(key), value);
                }
            }
        }

        for (String key : customAttributes.keySet()) {
            if (customAttributes.get(key) != null) {
                Object value = customAttrValues.get(key);
                
                if (getAttributeConverterMap().containsKey(key)) {
                	System.out.println("-------->Key is : " + key + " Value is : " + value);
                	//Convert the value using the inbound converter.
                	DataConverterIntf converter = loadConverter((String) getAttributeConverterMap().get(key));
            	             		
                	value = (String) converter.convertInbound(value);
                	System.out.println("--------------I>Key is : " + key + " Converted Value is : " + value);
                	
                	if (value != null) 
                		user.setAttribute(customAttributes.get(key), value);
                	
                }
                else if(value instanceof Long){
            		//System.out.println("\n\n------->Is a long in Transformer\n\n");
            		if (value != null) 
            			user.setAttribute(customAttributes.get(key), new Date((Long)value));
            	}
            	else if(value instanceof Date){
            		//System.out.println("\n\n-------->Is a Date in transformer\n\n");
            		if (value != null) 
            			user.setAttribute(customAttributes.get(key), value);
            	}
            	else{
            		//System.out.println("\n\n-------->Is Else case in transformer\n\n");
            		if(value != null) user.setAttribute(customAttributes.get(key), value);
            	}
                
            }
        }

        return user;
    }

    /**
     * This method takes an OIM user VO and converts it into the UserAccount VO.
     * It leverages the attributeMap to map the defined OIM fields into the
     * UserAccount and handle the custom attributes.
     *
     *
     * @param pUser
     *
     * @return
     */
    @Override
    public UserAccount convertUser(User pUser) {
        UserAccount acct = new UserAccount();

        acct.setUserName((String) pUser.getAttribute(PropertyUtil.getProperty(ServiceConstants.USER_NAME.getName())));
        acct.setAccountId(
            (String) pUser.getAttribute(PropertyUtil.getProperty(ServiceConstants.USER_ACCOUNTID.getName())));
        acct.setFirstName(
            (String) pUser.getAttribute(PropertyUtil.getProperty(ServiceConstants.USER_FIRSTNAME.getName())));
        acct.setLastName(
            (String) pUser.getAttribute(PropertyUtil.getProperty(ServiceConstants.USER_LASTNAME.getName())));
        acct.setMiddleInitial(
            (String) pUser.getAttribute(PropertyUtil.getProperty(ServiceConstants.USER_MIDDLEINITIAL.getName())));
        acct.setPrefFirstName(
            (String) pUser.getAttribute(PropertyUtil.getProperty(ServiceConstants.USER_PREF_FIRSTNAME.getName())));
        acct.setTitle((String) pUser.getAttribute(PropertyUtil.getProperty(ServiceConstants.USER_TITLE.getName())));
        acct.setEmailAddress(
            (String) pUser.getAttribute(PropertyUtil.getProperty(ServiceConstants.USER_EMAIL.getName())));
        acct.setPhoneNumber(
            (String) pUser.getAttribute(PropertyUtil.getProperty(ServiceConstants.USER_PHONE.getName())));
        acct.setDepartmentNumber(
            (String) pUser.getAttribute(PropertyUtil.getProperty(ServiceConstants.USER_DEPTNUM.getName())));
        acct.setOfficeLocation(
            (String) pUser.getAttribute(PropertyUtil.getProperty(ServiceConstants.USER_OFFICELOC.getName())));
        acct.setStatus((String) pUser.getAttribute(PropertyUtil.getProperty(ServiceConstants.USER_STATUS.getName())));
        acct.setLocked((String) pUser.getAttribute(PropertyUtil.getProperty(ServiceConstants.USER_LOCKED.getName())));
        acct.setManualLock(
            (String) pUser.getAttribute(PropertyUtil.getProperty(ServiceConstants.USER_MANUALLOCK.getName())));
        acct.setAcctClaimed(
            (String) pUser.getAttribute(PropertyUtil.getProperty(ServiceConstants.USER_CLAIMSTATUS.getName())));

        HashMap<String, String> configuredCustomAttrs = getCustomAttributeMap();

        for (String key : configuredCustomAttrs.keySet()) {
            if (pUser.getAttribute(configuredCustomAttrs.get(key)) instanceof Date) {
                acct.setAttribute(key, ISO8601.fromCalendar((Date) pUser.getAttribute(configuredCustomAttrs.get(key))));
            } else {
                acct.setAttribute(key, pUser.getAttribute(configuredCustomAttrs.get(key)));
            }
        }

        //Convert outbound if any.
        for (Object attribute : getAttributeConverterMap().keySet()) {
        	String key = (String) attribute;
        	
        	if (acct.getAttributes().containsKey(key)){
        		DataConverterIntf converter = loadConverter((String) getAttributeConverterMap().get(key));
        		Object newValue = converter.convertOutbound(acct.getAttribute(key));
        		acct.setAttribute(key, newValue);
        	}
        }
        
        return acct;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public HashMap getAllAttributesMap() {
        HashMap attribs = new HashMap();

        attribs.putAll(getAttributeMap());
        attribs.putAll(getCustomAttributeMap());

        return attribs;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public HashMap getAttributeMap() {
        HashMap<String, String> attrMap     = new HashMap<String, String>();
        HashMap<String, String> propertyMap = (HashMap<String, String>) PropertyUtil.getAppProps();

        for (String key : propertyMap.keySet()) {
            if (key.startsWith("service.useraccount.attribute.")) {
                String value = propertyMap.get(key);

                if (!"".equals(value)) {
                    String attrName = "";

                    if (key.startsWith("service.useraccount.attribute.custom")) {

                        // Skip
                    } else {
                        attrName = key.substring("service.useraccount.attribute.".length());
                        attrMap.put(attrName, value);
                    }
                }
            }
        }

        return attrMap;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public HashMap getCustomAttributeMap() {
        HashMap<String, String> attrMap     = new HashMap<String, String>();
        HashMap<String, String> propertyMap = (HashMap<String, String>) PropertyUtil.getAppProps();

        for (String key : propertyMap.keySet()) {
            if (key.startsWith("service.useraccount.attribute.custom.")) {
                String value    = propertyMap.get(key);
                String attrName = "";

                attrName = key.substring("service.useraccount.attribute.custom.".length());
                attrMap.put(attrName, value);
            }
        }

        return attrMap;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public HashMap getAttributeConverterMap() {
        HashMap<String, String> attrMap     = new HashMap<String, String>();
        HashMap<String, String> propertyMap = (HashMap<String, String>) PropertyUtil.getAppProps();

        for (String key : propertyMap.keySet()) {
            if (key.startsWith("service.useraccount.attributeconverter.")) {
                String value    = propertyMap.get(key);
                String attrName = "";

                attrName = key.substring("service.useraccount.attributeconverter.".length());
                logger.debug("Converter for attribute: " + attrName + " is " + value);
                attrMap.put(attrName, value);
            }
        }

        return attrMap;
    }

    /**
     * Method description
     *
     *
     * @param clazz
     * @param property
     *
     * @return
     */
    protected static Method getSetterforProperty(Class clazz, String property) {
        Method retMethod = null;

        try {
            PropertyDescriptor pd = new PropertyDescriptor(property, clazz);

            retMethod = pd.getWriteMethod();
        } catch (IntrospectionException e) {

            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return retMethod;
    }

    /**
     * Method description
     *
     *
     * @param clazz
     * @param property
     *
     * @return
     */
    protected static Method getGetterforProperty(Class clazz, String property) {
        Method retMethod = null;

        try {
            PropertyDescriptor pd = new PropertyDescriptor(property, clazz);

            retMethod = pd.getReadMethod();
        } catch (IntrospectionException e) {

            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return retMethod;
    }

    /**
     * Method description
     *
     *
     * @param searchCriteria
     *
     * @return
     */
    public SearchCriteria parseRSQL(String searchCriteria) {
        SearchCriteria sc = null;

        try {
            Expression searchExpression = RSQLParser.parse(searchCriteria);

            sc = getCriteria(searchExpression);
        } catch (ParseException e) {
        	logger.error(e.getMessage());
        } catch (TokenMgrError e) {

        	logger.error(e.getMessage());
        }

        return sc;
    }

    private SearchCriteria getCriteria(Expression searchExp) {
        SearchCriteria sc = null;

        if (searchExp.isLogical()) {
            LogicalExpression logExp = (LogicalExpression) searchExp;
            SearchCriteria    left   = getCriteria(logExp.getLeft());
            SearchCriteria    right  = getCriteria(logExp.getRight());

            sc = new SearchCriteria(left, right, getLogicalOperator(logExp.getOperator()));
        } else {
            ComparisonExpression comExp = (ComparisonExpression) searchExp;
            
            Object argument = parseArgument(comExp.getSelector(),comExp.getArgument());
            
            sc = new SearchCriteria(convertToOIMAttribute(comExp.getSelector()), argument,
                                    getOperator(comExp.getOperator()));
        }

        return sc;
    }

    private SearchCriteria.Operator getOperator(Comparison op) {
        switch (op) {
        case EQUAL :
            return SearchCriteria.Operator.EQUAL;

        case NOT_EQUAL :
            return SearchCriteria.Operator.NOT_EQUAL;

        case GREATER_THAN :
            return SearchCriteria.Operator.GREATER_THAN;

        case GREATER_EQUAL :
            return SearchCriteria.Operator.GREATER_EQUAL;

        case LESS_THAN :
            return SearchCriteria.Operator.LESS_THAN;

        case LESS_EQUAL :
            return SearchCriteria.Operator.LESS_EQUAL;
        }

        return null;
    }

    private SearchCriteria.Operator getLogicalOperator(Logical op) {
        switch (op) {
        case AND :
            return SearchCriteria.Operator.AND;

        case OR :
            return SearchCriteria.Operator.OR;
        }

        return null;
    }

    private String convertToOIMAttribute(String inAttr) {
        return (String) getAllAttributesMap().get(inAttr);
    }

    // COnverts date formatted query to Date object, returns string otherwise
	/**
	 * @deprecated Use {@link #parseArgument(String,String)} instead
	 */
	private Object parseArgument(String argument) {
		return parseArgument(null, argument);
	}

	// COnverts date formatted query to Date object, returns string otherwise
    private Object parseArgument(String selector, String argument) {
        Matcher matcher = isoDate.matcher(argument);

        if (matcher.matches()) {

            // Date format convert to Date object
            try {
                System.out.println("Convert to Date");

                Date searchDate = ISO8601.toCalendar(argument).getTime();

                return searchDate;
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
        }
        
        if (getAttributeConverterMap().containsKey(selector)) {
        	//Convert the value using the inbound converter.
        	DataConverterIntf converter = loadConverter((String) getAttributeConverterMap().get(selector));
        	argument = (String) converter.convertInbound(argument);
        	logger.debug("Converted argument: " + argument);
        }

        return argument;
    }

    protected DataConverterIntf loadConverter(String converterClass) {
        DataConverterIntf converter = null;

        try {
            Class converterClazz = Class.forName(converterClass);

            converter = (DataConverterIntf) converterClazz.newInstance();
        } catch (ClassNotFoundException e) {
            logger.error(e.getMessage());
        } catch (InstantiationException e) {
            logger.error(e.getMessage());
        } catch (IllegalAccessException e) {
            logger.error(e.getMessage());
        }

        return converter;
    }
}
