package com.aptecllc.oim.util;

import java.util.Hashtable;





import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import oracle.iam.identity.orgmgmt.api.OrganizationManager;
import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.notification.api.NotificationService;
import oracle.iam.notification.template.TemplateService;
import oracle.iam.passwordmgmt.api.PasswordMgmtService;
import oracle.iam.platform.OIMClient;
import oracle.iam.selfservice.uself.uselfmgmt.api.UnauthenticatedSelfService;
import Thor.API.Operations.tcLookupOperationsIntf;
import Thor.API.Operations.tcUserOperationsIntf;

import com.aptecllc.jps.CSFUtilities;
import com.aptecllc.oim.property.PropertyUtil;
import com.aptecllc.oim.property.ServiceProperty;

/**
 * This class shall give the handle to the OIM Client API's This bean is
 * injected during the start up of the IDL App
 * 
 * @author skandagadla
 * 
 */
@Deprecated
public class OimClient {

	@Autowired
	private ServiceProperty idlServiceProperty;
	private OIMClient identityClient;

	
	private static final Logger IdentityProfileServiceLogger = LoggerFactory.getLogger(OimClient.class);

	

	/**
	 * Initializes the OIM Client
	 * 
	 * @param idlServiceProperty
	 *            The IDL Service Property bean
	 * @param idProfSvcDel
	 *            The IDL Service Delegate bean
	 * @return
	 */
	@PostConstruct
	private void initializeOimClient() {
		
		IdentityProfileServiceLogger
				.info("Inside the OIM Initialization", this);
		
		try {
			boolean isOimInUse = new Boolean(
					PropertyUtil.getProperty(ServiceConstants.IS_USE_OIM.getName()));
			if (isOimInUse) {
				char[] password = null;
				String providerUrl = null;

				boolean isDevMode = new Boolean(
						PropertyUtil
								.getProperty(ServiceConstants.IS_DEV_MODE.getName()));

				if (isDevMode) {
					password = idlServiceProperty.getOimAdmPwd().toCharArray();
					providerUrl = idlServiceProperty.getOimProviderUrl();
				} else {
					password = CSFUtilities.getPasswordforCredential("oim",
							"sysadmin");
					//IdentityProfileServiceLogger.debug(
					//		"The password of the OIM admin is - "
					//				+ password.toString(), this);
					providerUrl = idlServiceProperty.getOimProviderUrl();
					IdentityProfileServiceLogger.debug(
							"The providerUrl of the OIM is " + providerUrl,
							this);
				}

				if (password.length == 0 || providerUrl == null) {
					IdentityProfileServiceLogger
							.error("Exception occured while getting the values of the credential from CSF map",
									this);
					throw new Exception(
							"Exception occured while getting the values of the credentials from CSF Map");
				}

				Hashtable<String, String> env = new Hashtable<String, String>();
				env.put(OIMClient.JAVA_NAMING_FACTORY_INITIAL,
						idlServiceProperty.getOimCf());

				// Obtain the provider url from the csf map
				env.put(OIMClient.JAVA_NAMING_PROVIDER_URL, providerUrl);
				IdentityProfileServiceLogger.info("Attempting Login to OIM",
						this);
				identityClient = new OIMClient(env);
				// Obtain the Password credential from the csf map
				identityClient.login("xelsysadm", password);

				IdentityProfileServiceLogger.info("Login Successful to OIM",
						this);

			}

		} catch (Exception ex) {
			ex.printStackTrace();
			IdentityProfileServiceLogger.error(
					"Error while getting handle for OIM Client - "
							+ ex.getMessage(), this);
		}
	}

	public PasswordMgmtService getPwdMgr() {

		return (PasswordMgmtService) identityClient
				.getService(PasswordMgmtService.class);
	}
	
	public NotificationService getNotificationMgr(){
		return (NotificationService) identityClient
				.getService(NotificationService.class);
	}

	public UnauthenticatedSelfService getUnAuthSelfSvc() {

		return (UnauthenticatedSelfService) identityClient
				.getService(UnauthenticatedSelfService.class);
	}

	public UserManager getUsrMgr() {

		return (UserManager) identityClient.getService(UserManager.class);
	}
	
	

	//tcLookupOperationsIntf lookup= client.getService(tcLookupOperationsIntf.class);
	public tcUserOperationsIntf getTcUsrOpInt() {

		return (tcUserOperationsIntf) identityClient
				.getService(tcUserOperationsIntf.class);
	}
	
	public tcLookupOperationsIntf getTcLookupOpInt() {

		return (tcLookupOperationsIntf) identityClient
				.getService(tcLookupOperationsIntf.class);
	}

	public OrganizationManager getOrgManager() {
		return identityClient.getService(OrganizationManager.class);
	}
	
	public TemplateService getTemplateMgr() {
		// TODO Auto-generated method stub
		return identityClient.getService(TemplateService.class);
	}

}