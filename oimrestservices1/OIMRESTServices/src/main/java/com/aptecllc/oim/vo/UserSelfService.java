package com.aptecllc.oim.vo;

//~--- JDK imports ------------------------------------------------------------

import java.io.Serializable;

import java.util.HashMap;

/**
 * Value Object for Self Service requests Used for password changes, resets, and unlocks.
 *
 *
 * @version        2.0, 13/12/04
 * @author         Eric A. Fisher, APTEC LLC    
 */
public class UserSelfService implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2L;
    private String            userLogin;
    private String            oldPassword;
    private String            newPassword;
    private String            confirmPassword;
    private HashMap           questions;
    private Boolean           forceChange = false;

    /**
     * Getter for Force Change Flag
     *
     *
     * @return the value of the force change flag
     */
    public Boolean getForceChange() {
        return forceChange;
    }

    /**
     * Setter for Force change flag.  This flag indicates if OIM will force
     * the user to reset their password at next login or not. Default is false.
     *
     *
     * @param forceChange Set to true to force change or false to not.
     */
    public void setForceChange(Boolean forceChange) {
        this.forceChange = forceChange;
    }

    /**
     * Getter for User Login
     *
     *
     * @return the user Login
     */
    public String getUserLogin() {
        return userLogin;
    }

    /**
     * Setter for user login
     *
     *
     * @param userLogin the User Login
     */
    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    /**
     * Getter for Old Password
     *
     *
     * @return the old password
     */
    public String getOldPassword() {
        return oldPassword;
    }

    /**
     * Setter for old password
     *
     *
     * @param oldPassword the old password for a password change operation.
     */
    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    /**
     * Getter for new password
     *
     *
     * @return the new password
     */
    public String getNewPassword() {
        return newPassword;
    }

    /**
     * Setter for new password
     *
     *
     * @param newPassword the new password to be set.
     */
    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    /**
     * Getter for confirmation password.
     *
     *
     * @return the confirmation password
     */
    public String getConfirmPassword() {
        return confirmPassword;
    }

    /**
     * Setter for confirmation password
     *
     *
     * @param confirmPassword the confirmation password
     */
    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    /**
     * Getter for hashmap of questions and answer pairs.
     *
     *
     * @return the map of question and answer pairs.
     */
    public HashMap getQuestions() {
        return questions;
    }

    /**
     * Setter of hashmap of questions and answer pairs.
     *
     *
     * @param questions map of question and answer pairs.
     */
    public void setQuestions(HashMap questions) {
        this.questions = questions;
    }
}
