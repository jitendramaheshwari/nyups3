package com.aptecllc.oim.vo;

//~--- JDK imports ------------------------------------------------------------

import java.io.Serializable;

/**
 * Class description
 *
 *
 * @version        1.0, 14/05/05
 * @author         Eric A. Fisher, APTEC LLC    
 */
public class Notification implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String            recipient;
    private String            message;
    private String            subject;

    /**
     * @return the recipient
     */
    public String getRecipient() {
        return recipient;
    }

    /**
     * @param recipient the recipient to set
     */
    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject the subject to set
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }
}
