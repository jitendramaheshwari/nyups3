package com.aptecllc.oim.util;

//~--- JDK imports ------------------------------------------------------------

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * Class description
 *
 *
 * 
 */
public final class ISO8601 {

    /**
     * Transform Calendar to ISO 8601 string. 
     *
     * @param calendar
     *
     * @return
     */
    public static String fromCalendar(final Calendar calendar) {
        Date   date      = calendar.getTime();
        String formatted = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").format(date);

        return formatted.substring(0, 22) + ":" + formatted.substring(22);
    }

    
    public static String fromCalendar(final Date date) {
        
        String formatted = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").format(date);

        return formatted.substring(0, 22) + ":" + formatted.substring(22);
    }
    /**
     * Get current date and time formatted as ISO 8601 string. 
     *
     * @return
     */
    public static String now() {
        return fromCalendar(GregorianCalendar.getInstance());
    }

    /**
     * Transform ISO 8601 string to Calendar. 
     *
     * @param iso8601string
     *
     * @return
     *
     * @throws ParseException
     */
    public static Calendar toCalendar(final String iso8601string) throws ParseException {
        Calendar calendar = GregorianCalendar.getInstance();
        SimpleDateFormat sdf = null;
        String   s      = iso8601string.replace("Z", "+00:00");
        
        if (s.length() == 19 ) {
        	//No Timezone, assume local timezone
        	sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

            Date date = sdf.parse(s);

            calendar.setTime(date);
        } else {
	
	        try {
	            s = s.substring(0, 22) + s.substring(23);
	        } catch (IndexOutOfBoundsException e) {
	            throw new ParseException("Invalid length", 0);
	        }
	        
	        sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

	        Date date = sdf.parse(s);

	        calendar.setTime(date);
        }
        

        System.out.println(calendar);
        return calendar;
    }
}
