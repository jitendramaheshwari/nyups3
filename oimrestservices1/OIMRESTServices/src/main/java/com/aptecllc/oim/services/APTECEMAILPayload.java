package com.aptecllc.oim.services;



import java.io.File;
import java.util.List;
import oracle.iam.notification.provider.Payload;

public class APTECEMAILPayload
  extends Payload
{
  private List<String> toEmailId;
  private String replyToMailId;
  private File[] attachments;
  private boolean html;
  private String encoding;
  private List<String> ccMailIds;
  private List<String> bccEmailIds;
  
  public APTECEMAILPayload(List<String> toEmailId, String replyToMailId, Boolean html, String encoding)
  {
    this.toEmailId = toEmailId;
    this.replyToMailId = replyToMailId;
    this.html = html.booleanValue();
    this.encoding = encoding;
  }
  
  public List<String> getCcMailIds()
  {
    return this.ccMailIds;
  }
  
  public void setCcMailIds(List<String> ccMailIds)
  {
    this.ccMailIds = ccMailIds;
  }
  
  public List<String> getBccEmailIds()
  {
    return this.bccEmailIds;
  }
  
  public void setBccEmailIds(List<String> bccEmailIds)
  {
    this.bccEmailIds = bccEmailIds;
  }
  
  public List<String> getToEmailId()
  {
    return this.toEmailId;
  }
  
  public String getReplyToMailId()
  {
    return this.replyToMailId;
  }
  
  public void setReplyToMailId(String replyToMailId)
  {
    this.replyToMailId = replyToMailId;
  }
  
  public File[] getAttachments()
  {
    return this.attachments;
  }
  
  public boolean isHtml()
  {
    return this.html;
  }
  
  public String getEncoding()
  {
    return this.encoding;
  }
  
  public void setEncoding(String encoding)
  {
    this.encoding = encoding;
  }
  
  public void setAttachments(File[] attachments)
  {
    this.attachments = attachments;
  }
}
