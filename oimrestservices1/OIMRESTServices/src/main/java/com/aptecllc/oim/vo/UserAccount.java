package com.aptecllc.oim.vo;

//~--- non-JDK imports --------------------------------------------------------

import org.codehaus.jackson.annotate.JsonIgnore;

//~--- JDK imports ------------------------------------------------------------








import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;

/**
 * UserAccount Value Object - Holds attributes for a given user account abstracted from
 * the actual OIM attribute names.  Some predefined attributes already exist, custom attributes are stored 
 * in the attributes HashMap.
 *
 *
 * @version        1.0, 14/04/21
 * @author         Eric A. Fisher, APTEC LLC    
 */
public class UserAccount implements Serializable {

    /**
     *
     */
    private static final long       serialVersionUID = 1L;
    private HashMap<String, Object> mAttributes;
    private String                  mUniqueID;

    /**
     * Constructs ...
     *
     */
    public UserAccount() {
        mAttributes = new HashMap<String, Object>();
    }

    /**
     * Getter for Organization Name
     *
     *
     * @return the Organization Name
     */
    public String getOrganizationName() {
        return (String) mAttributes.get("organizationName");
    }

    /**
     * Setter for OIM Organization Name
     *
     *
     * @param orgName Organization Name
     */
    public void setOrganizationName(String orgName) {
        mAttributes.put("organizationName", orgName);
    }

    /**
     * Getter for Role
     *
     *
     * @return the Role
     */
    public String getRole() {
        return (String) mAttributes.get("role");
    }

    /**
     * Setter for Role
     *
     *
     * @param role the Role
     */
    public void setRole(String role) {
        mAttributes.put("role", role);
    }

    /**
     * Getter for Title	
     *
     *
     * @return the Title
     */
    public String getTitle() {
        return (String) mAttributes.get("title");
    }

    /**
     * Setter for Title
     *
     *
     * @param title Title
     */
    public void setTitle(String title) {
        mAttributes.put("title", title);
    }

    /**
     * Getter for Email Address
     *
     *
     * @return the email address
     */
    public String getEmailAddress() {
        return (String) mAttributes.get("emailAddress");
    }

    /**
     * Setter for email address
     *
     *
     * @param emailAddress Email Address
     */
    public void setEmailAddress(String emailAddress) {
        mAttributes.put("emailAddress", emailAddress);
    }

    /**
     * Getter for Termination date
     *
     *
     * @return the Termination date
     */
    /*public String getTerminationDate() {
        return (String) mAttributes.get("terminationDate");
    }*/

    /**
     * Setter for Termination date
     *
     *
     * @param Termination date
     */
   /* public void setTerminationDate(String terminationDate) {
        mAttributes.put("terminationDate", terminationDate);
    }*/
    
    
    /**
     * Getter for Phone Number
     *
     *
     * @return the Phone Number
     */
    public String getPhoneNumber() {
        return (String) mAttributes.get("phoneNumber");
    }

    /**
     * Setter for Phone Number
     *
     *
     * @param phoneNumber Phone NUmber
     */
    public void setPhoneNumber(String phoneNumber) {
        mAttributes.put("phoneNumber", phoneNumber);
    }

    /**
     * Getter for Department Number
     *
     *
     * @return the Department Number
     */
    
    //
 
    public String getExternalaffiliation() {
		return (String)this.mAttributes.get("externalaffiliation");
	}

	public void setExternalaffiliation(String externalaffiliation) {
		this.mAttributes.put("externalaffiliation",externalaffiliation);
	}
	
	public String getSponsorAlternateEmail() {
		return (String)this.mAttributes.get("sponsorAlternateEmail");
	}

	public void setSponsorAlternateEmail(String sponsorAlternateEmail) {
		this.mAttributes.put("SponsorAlternateEmail",sponsorAlternateEmail);
	}
	
	public String getSponsorAlternatePhone() {
		return (String)this.mAttributes.get("sponsorAlternatePhone");
	}

	public void setSponsorAlternatePhone(String sponsorAlternatePhone) {
		this.mAttributes.put("SponsorAlternatePhone",sponsorAlternatePhone);
	}
	
	public String getPersonalEmail() {
		return (String)this.mAttributes.get("personalEmail");
	}

	public void setPersonalEmail(String personalEmail) {
		this.mAttributes.put("personalEmail",personalEmail);
	}
	
	// added on Dec 8th 2017
	
	public String getSilkRoadIntegration() {
		return (String)this.mAttributes.get("silkRoadIntegration");
	}

	public void setSilkRoadIntegration(String silkRoadIntegration) {
		this.mAttributes.put("silkRoadIntegration",silkRoadIntegration);
	}
	
	
	// ended on Dec 8th 2017
	
	public String getSSN() {
		return (String)this.mAttributes.get("SSN");
	}

	public void setSSN(String SSN) {
		this.mAttributes.put("SSN",SSN);
	}
	
	public String getEmailRequired() {
		return (String)this.mAttributes.get("emailRequired");
	}

	public void setEmailRequired(String emailRequired) {
		this.mAttributes.put("emailRequired",emailRequired);
	}
	
	public String getSponsorNetID() {
		return (String)this.mAttributes.get("sponsorNetID");
	}

	public void setSponsorNetID(String sponsorNetID) {
		this.mAttributes.put("sponsorNetID",sponsorNetID);
	}
	
	public String getVendor() {
		return (String)this.mAttributes.get("vendor");
	}

	public void setVendor(String vendor) {
		this.mAttributes.put("vendor",vendor);
	}
	
	public String getSponsor() {
		return (String)this.mAttributes.get("sponsor");
	}

	public void setSponsor(String sponsor) {
		this.mAttributes.put("sponsor",sponsor);
	}
	
	//04/11
	
	/*public String getUserType() {
		return (String)this.mAttributes.get("UserType");
	}

	public void setUserType(String UserType) {
		this.mAttributes.put("UserType",UserType);
	}*/
	
	/*public void setSponsorsFirstName(String SponsorsFirstName) {
		this.mAttributes.put("SponsorsFirstName",SponsorsFirstName);
	}
	
	public String getSponsorsFirstName() {
		return (String)this.mAttributes.get("SponsorsFirstName");
	}

	public void setSponsorsLastName(String SponsorsLastName) {
		this.mAttributes.put("SponsorsLastName",SponsorsLastName);
	}
	
	public String getSponsorsLastName() {
		return (String)this.mAttributes.get("SponsorsLastName");
	}*/

	
	
	
	
	//04/11
	
	
    //
    
    public String getDepartmentNumber() {
        return (String) mAttributes.get("departmentNumber");
    }

    /**
     * Setter for Department Number
     *
     *
     * @param departmentNumber Department Number
     */
    public void setDepartmentNumber(String departmentNumber) {
        mAttributes.put("departmentNumber", departmentNumber);
    }

    /**
     * Getter for Office Lcoation
     *
     *
     * @return the Office Location
     */
    public String getOfficeLocation() {
        return (String) mAttributes.get("officeLocation");
    }

    /**
     * Setter for Office Location
     *
     *
     * @param officeLocation Office Location
     */
    public void setOfficeLocation(String officeLocation) {
        mAttributes.put("officeLocation", officeLocation);
    }

    /**
     * Getter for Last Name
     *
     *
     * @return the Last Name
     */
    public String getLastName() {
        return (String) this.mAttributes.get("lastName");
    }

    /**
     * Setter for Last Name
     *
     *
     * @param lastName Last Name
     */
    public void setLastName(String lastName) {
        this.mAttributes.put("lastName", lastName);
    }

   
    /**
     * Getter for Middle Initial/Name
     *
     *
     * @return the Middle Initial/Name
     */
    public String getMiddleInitial() {
        return (String) this.mAttributes.get("middleInitial");
    }

    /**
     * Setter for Middle Initial/Name
     *
     *
     * @param middleInitial Middle Initial or Name
     */
    public void setMiddleInitial(String middleInitial) {
        this.mAttributes.put("middleInitial", middleInitial);
    }

    /**
     * Getter for Preferred First Name
     *
     *
     * @return the preferred first name
     */
    public String getPrefFirstName() {
        return (String) this.mAttributes.get("prefFirstName");
    }

    /**
     * Setter for PReferred First Name
     *
     *
     * @param prefFirstName Preferred FIrst Name
     */
    public void setPrefFirstName(String prefFirstName) {
        this.mAttributes.put("prefFirstName", prefFirstName);
    }

    /**
     * Setter for custom attribute
     *
     *
     * @param attr Attribute Name
     * @param val Attribute Value
     */
    public void setAttribute(String attr, Object val) {
        if (attr == null) {
            return;
        }

        if (val == null) {
            this.mAttributes.remove(attr);
        }

        this.mAttributes.put(attr, val);
    }

    /**
     *Getter for custom Attribute
     *
     * @param attr the Attribute Name to retrieve
     *
     * @return the attribute value for the name provided
     */
    public Object getAttribute(String attr) {
        if (attr == null) {
            return null;
        }

        return this.mAttributes.get(attr);
    }

    /**
     * Gets a set of all the defined attribute names. 
     *
     *
     * @return Set of Attribute Names
     */
    @JsonIgnore
    public Set getAttributeNames() {
        return this.mAttributes.keySet();
    }

    /**
     * Getter for hashmap of all attributes.
     *
     *
     * @return the attribute hashmap
     */
    public HashMap<String, Object> getAttributes() {
        return this.mAttributes;
    }

    /**
     * Setter for hashmap of all attributes
     *
     *
     * @param attribs Hashmap of attributes
     */
    public void setAttributes(HashMap attribs) {
        mAttributes = attribs;
    }

    /**
     * String representation of object.
     *
     *
     * @return String representation of obejct
     */
    public String toString() {
        StringBuffer buff = new StringBuffer();

        if (this.mUniqueID != null) {
            buff.append(this.mUniqueID);
            buff.append("\n");
        }

        if (this.mAttributes != null) {
            buff.append(this.mAttributes.toString());
        }

        return buff.toString();
    }

    /**
     * Getter for User Name
     *
     *
     * @return the UserName
     */
    public String getUserName() {
        return mUniqueID;
    }

    /**
     * Setter for User Name
     *
     *
     * @param userName the UserName
     */
    public void setUserName(String userName) {
        mUniqueID = userName;
    }

    /**
     * Getter for Account ID
     *
     *
     * @return the account ID
     */
    public String getAccountId() {
        return (String) this.mAttributes.get("accountId");
    }

    /**
     * Setter for Account ID
     *
     *
     * @param pAccountID the Account ID
     */
    public void setAccountId(String pAccountID) {
        this.mAttributes.put("accountId", pAccountID);
    }

    /**
     * Getter for First Name
     *
     *
     * @return the First Name
     */
    public String getFirstName() {
        return (String) this.mAttributes.get("firstName");
    }

    /**
     * Setter for First Name
     *
     *
     * @param pFirstName the First Name
     */
    public void setFirstName(String pFirstName) {
        this.mAttributes.put("firstName", pFirstName);
    }

    /**
     * Setter for Status
     *
     *
     * @param pStatus the user status
     */
    public void setStatus(String pStatus) {
        this.mAttributes.put("status", pStatus);
    }

    /**
     * Getter for Status
     *
     *
     * @return the user status
     */
    public String getStatus() {
        return (String) this.mAttributes.get("status");
    }

    /**
     * Setter for locked indicator
     *
     *
     * @param pLocked the Locked Value.
     */
    public void setLocked(String pLocked) {
        this.mAttributes.put("locked", pLocked);
    }

    /**
     * Getter for locked indicator
     *
     *
     * @return the locked indicator
     */
    public String getLocked() {
        return (String) this.mAttributes.get("locked");
    }

    /**
     * Setter for Manual Locked indicator
     *
     *
     * @param pLocked the Manual locked indicator
     */
    public void setManualLock(String pLocked) {
        this.mAttributes.put("manualLock", pLocked);
    }

    /**
     * Getter for Manual Locked indicator
     *
     *
     * @return value of 0 for not manually locked, any other value for manually locked (usually 1)
     */
    public String getManualLock() {
        return (String) this.mAttributes.get("manualLock");
    }

    /**
     * Setter for Account Claimed indicator
     *
     *
     * @param pLocked Account Locked indicator value
     */
    public void setAcctClaimed(String pLocked) {
        this.mAttributes.put("acctClaimed", pLocked);
    }

    /**
     * Getter for Acount Claimed Indicator
     *
     *
     * @return the value of the account claimed indicator.
     */
    public String getAcctClaimed() {
        return (String) this.mAttributes.get("acctClaimed");
    }
	public void setTerms(String terms) {
		this.mAttributes.put("terms", terms);
		
	}
	public String getTerms() {
		return (String) this.mAttributes.get("terms");
	}
	
	public void setDOB(String DOB) {
		this.mAttributes.put("DOB", DOB);
		
	}
	public String getDOB() {
		return (String) this.mAttributes.get("DOB");
	}
	
	
	
}
