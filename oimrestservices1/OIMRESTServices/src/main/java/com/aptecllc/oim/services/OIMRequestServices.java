package com.aptecllc.oim.services;

//~--- non-JDK imports --------------------------------------------------------

import com.aptecllc.oim.controller.OIMRESTController;
import com.aptecllc.oim.exception.UserException;
import com.aptecllc.oim.exception.UserNotFoundException;
import com.aptecllc.oim.property.ServiceProperty;
import com.aptecllc.oim.util.v2.OimClient;

import oracle.iam.identity.exception.NoSuchUserException;
import oracle.iam.identity.exception.OrganizationManagerException;
import oracle.iam.identity.exception.SearchKeyNotUniqueException;
import oracle.iam.identity.exception.UserEnableException;
import oracle.iam.identity.exception.UserLookupException;
import oracle.iam.identity.exception.UserModifyException;
import oracle.iam.identity.exception.UserSearchException;
import oracle.iam.identity.exception.ValidationFailedException;
import oracle.iam.identity.orgmgmt.api.OrganizationManager;
import oracle.iam.identity.orgmgmt.vo.Organization;
import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.api.UserManagerConstants;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.platform.authz.exception.AccessDeniedException;
import oracle.iam.platform.entitymgr.vo.SearchCriteria;
import oracle.iam.platform.utils.vo.OIMType;
import oracle.iam.request.api.RequestService;
import oracle.iam.request.exception.BulkBeneficiariesAddException;
import oracle.iam.request.exception.BulkEntitiesAddException;
import oracle.iam.request.exception.InvalidRequestDataException;
import oracle.iam.request.exception.InvalidRequestException;
import oracle.iam.request.exception.RequestServiceException;
import oracle.iam.request.vo.RequestConstants;
import oracle.iam.request.vo.RequestData;
import oracle.iam.request.vo.RequestEntity;
import oracle.iam.request.vo.RequestEntityAttribute;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

//~--- JDK imports ------------------------------------------------------------

import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.HashMap;

/**
 * Class description
 *
 *
 * @version        1.0, 14/04/06
 * @author         Eric A. Fisher Shine Thomas Dan Donnelly APTEC LLC
 */
@Service
public class OIMRequestServices {
    private static final Logger logger = LoggerFactory.getLogger(OIMRESTController.class);
    @Autowired
    private ServiceProperty     serviceProperties;

    /**
     * Method description
     *
     *
     *
     *
     * @param reason
     * @param user
     * @param orgName 
     */
    public String requestNewUser(String reason, User user, String orgName, String requestingUser) throws UserException, ValidationFailedException, UserModifyException, NoSuchUserException, SearchKeyNotUniqueException, AccessDeniedException {
        OimClient      oimClient = new OimClient(serviceProperties);
        
        System.out.println("\n\n------------------>Inside of request new user, inputs are as followed:\nString Reason: "
        		+reason+"\nUser:\n"+user.toString()+"\nOrgname: "+orgName+"\nrequestingUser: "+requestingUser);
        
        RequestService reqsrvc   = oimClient.getRequestService();
        String reqID = null;

        System.out.println("IN REQUEST NEW USER:"+reason+":"+user+":"+orgName+":"+requestingUser);
        //System.out.println("manager value is:"+Long.parseLong(user.getManagerKey()));
        System.out.println(user.toString());
        
        RequestData requestData = new RequestData();

        requestData.setJustification(reason);

        RequestEntity ent = new RequestEntity();

        ent.setRequestEntityType(OIMType.User);
        ent.setOperation(RequestConstants.MODEL_CREATE_OPERATION);    // New in R2

        List<RequestEntityAttribute> attrs = new ArrayList<RequestEntityAttribute>();
        RequestEntityAttribute       attr  =
            new RequestEntityAttribute(UserManagerConstants.AttributeName.LASTNAME.getId(), user.getLastName(),
                                       RequestEntityAttribute.TYPE.String);

        attrs.add(attr);
        attr = new RequestEntityAttribute(UserManagerConstants.AttributeName.FIRSTNAME.getId(), user.getFirstName(),
                                          RequestEntityAttribute.TYPE.String);
        attrs.add(attr);
       // attr = new RequestEntityAttribute("Organization", 4L,RequestEntityAttribute.TYPE.Long);
        //21 is for prod, 4 is for dev and stage
        attr = new RequestEntityAttribute("Organization", 21L,RequestEntityAttribute.TYPE.Long);
        attrs.add(attr);
        attr = new RequestEntityAttribute("User Type", false, RequestEntityAttribute.TYPE.Boolean);
        attrs.add(attr);
        attr = new RequestEntityAttribute(UserManagerConstants.AttributeName.EMPTYPE.getId(), "Contractor",
                                          RequestEntityAttribute.TYPE.String);
        attrs.add(attr);
       /* attr = new RequestEntityAttribute(UserManagerConstants.AttributeName.MIDDLENAME.getId(), user.getMiddleName(),
                                          RequestEntityAttribute.TYPE.String);
        attrs.add(attr);*/
        attr = new RequestEntityAttribute(UserManagerConstants.AttributeName.ACCOUNT_START_DATE.getId(),
                                          (Date)user.getAttribute("Start Date"), RequestEntityAttribute.TYPE.Date);
        attrs.add(attr);
        attr = new RequestEntityAttribute(UserManagerConstants.AttributeName.ACCOUNT_END_DATE.getId(),
        		(Date)user.getAttribute("End Date"), RequestEntityAttribute.TYPE.Date);
        attrs.add(attr);
        attr = new RequestEntityAttribute("AlternateEmail", (String) user.getAttribute("AlternateEmail"),RequestEntityAttribute.TYPE.String);
        attrs.add(attr);
        
        //
       System.out.println("Externalaffiliation"+(String) user.getAttribute("ExternalAffiliation"));
        attr = new RequestEntityAttribute("ExternalAffiliation", (String) user.getAttribute("ExternalAffiliation"),
                RequestEntityAttribute.TYPE.String);
        attrs.add(attr);
        
        //added on 02/02/2015
        
        System.out.println("Sponsored Individual Net ID if applicable"+(String) user.getAttribute("SponsorNetID"));
        attr = new RequestEntityAttribute("SponsorNetID", (String) user.getAttribute("SponsorNetID"),
                RequestEntityAttribute.TYPE.String);
        attrs.add(attr);
        
        System.out.println("Email required"+(String) user.getAttribute("EmailRequired"));
        attr = new RequestEntityAttribute("EmailRequired", (String) user.getAttribute("EmailRequired"),
                RequestEntityAttribute.TYPE.String);
        attrs.add(attr);
        
        //
        
        
        //System.out.println("SponsorsAlternateEmail"+(String) user.getAttribute("SponsorsAlternateEmail"));
        //attr = new RequestEntityAttribute("SponsorsAlternateEmail", (String) user.getAttribute("SponsorsAlternateEmail"),
                //RequestEntityAttribute.TYPE.String);
       // attrs.add(attr);
        //System.out.println("SponsorsAlternatePhone"+(String) user.getAttribute("SponsorsAlternatePhone"));
       // attr = new RequestEntityAttribute("SponsorsAlternatePhone", (String) user.getAttribute("SponsorsAlternatePhone"),
               // RequestEntityAttribute.TYPE.String);
        //attrs.add(attr);
        System.out.println("requestingusersponsorid"+(String) user.getAttribute("SponsorID"));
        //attr = new RequestEntityAttribute("SponsorID", requestingUser,RequestEntityAttribute.TYPE.String);
        attr = new RequestEntityAttribute("SponsorID", (String) user.getAttribute("SponsorID"),RequestEntityAttribute.TYPE.String);
        attrs.add(attr);
        
        System.out.println("vendor"+(String) user.getAttribute("VenderName"));
        //attr = new RequestEntityAttribute("SponsorID", requestingUser,RequestEntityAttribute.TYPE.String);
        attr = new RequestEntityAttribute("VenderName", (String) user.getAttribute("VenderName"),RequestEntityAttribute.TYPE.String);
        attrs.add(attr);
        if(user.getAttribute("VenderName").toString().equalsIgnoreCase("Lutheran Sponsored Individual")){
        	
        	System.out.println("Determining the Institutional identifier"+user.getAttribute("VenderName").toString());
        	attr = new RequestEntityAttribute("HelpDeskAttr", "LMC-SPO",RequestEntityAttribute.TYPE.String);
        	attrs.add(attr);
        }
        else if (!user.getAttribute("VenderName").toString().equalsIgnoreCase("Lutheran Sponsored Individual"))
        {
        System.out.println("Determining the Institutional identifier"+user.getAttribute("VenderName").toString());
        attr = new RequestEntityAttribute("HelpDeskAttr", "NYU-SPO",RequestEntityAttribute.TYPE.String);
        attrs.add(attr);
        }
        // 
        
        String value = (String) user.getAttribute("SponsorID");
        String usr_login = "User Login";
        try{
        HashSet userset = new HashSet();
        userset.add("First Name");
        userset.add("Last Name");
        // added to accomodate AdminDept commenting it out for 06/28 deployment 
        userset.add("AdminDept");
        User u = getUserByIdentifier(usr_login,value,userset);
        System.out.println("\n\n----------------------------->user.getEndDate = "+user.getAttribute("End Date")+"\n\n");
        System.out.println("Last name of the sponsor is"+u.getLastName());
        System.out.println("First name of the sponsor is"+u.getFirstName());
        // added to accomodate AdminDept commenting it out for 06/28 deployment
        System.out.println("Admin Department of the sponsor is"+u.getAttribute("AdminDept").toString());
        attr = new RequestEntityAttribute("SponsorsFirstName", u.getFirstName(),
                RequestEntityAttribute.TYPE.String);
        attrs.add(attr);
        attr = new RequestEntityAttribute("SponsorsLastName", u.getLastName(),
                RequestEntityAttribute.TYPE.String);
        attrs.add(attr);
        // added to accomodate AdminDept commenting it out for 06/28 deployment
        attr = new RequestEntityAttribute("AdminDept",u.getAttribute("AdminDept").toString(), RequestEntityAttribute.TYPE.String);
        attrs.add(attr);
       if(u.getAttribute("AdminDept").toString().equalsIgnoreCase("Winthrop")){
        	
        	
        	attr = new RequestEntityAttribute("HelpDeskAttr", "WUH-SPO",RequestEntityAttribute.TYPE.String);
        	attrs.add(attr);
        }
        }catch(Exception e)
        {e.printStackTrace();}
        
/*******Added with Sponsored Individual tool by Dan Donnelly 12/4/2015
 * 		Checks to see if SponsorNetID can be put in as the login
 * 		Piggybacks on check in Account_Admin -> OIMUserServicesClient.checkUpdateEnable
 */     
        List<User> result = null;
        HashSet userset = new HashSet();
        userset.add("First Name");
        userset.add("Last Name");
        
        SearchCriteria criteria = new SearchCriteria("User Login", user.getAttribute("SponsorNetID"), SearchCriteria.Operator.EQUAL);
        
        try{
        	result=searchUsers(criteria, userset);
        }
        catch(UserException e){
        	System.out.println("User Exception in searchUsers \n");
        }
        if(result.isEmpty()){
        	attr = new RequestEntityAttribute("User Login", (String)user.getAttribute("SponsorNetID"),
                    RequestEntityAttribute.TYPE.String);
            attrs.add(attr);
        }
        
        /*//////////////////////////**********************\\\\\\\\\\\\\\\\\\\\\\\\\\\
         --------------------------------Dan Donnelly
        	3 Cases to implement
        		Based on initial search of the system for login match, toggle on result number
        	--->No result case: SponsorNetID entered on form, but is not in OIM. Make 
        		a new account with that as the login, remove the SponsorNetID field
        	--->Single result case: SponsorNetID entered matches one in OIM, need to
        		check if it matches the info they've entered and is disabled. 
        		If yes, enable and update updatable fields
        		!If no, tbd
        	--->Multiple result case: 
            
        *////////////*****************************************************\\\\\\\\\\\\
       
       /* usr_login = "User Login";
        String net_id = (String) user.getAttribute("SponsorNetID");
        

        HashSet userset = new HashSet();
        userset.add("First Name");
        userset.add("Last Name");
        userset.add("Status");
        userset.add("Start Date");
        userset.add("End Date");
        userset.add("User Login");
        userset.add("SponsorID");
        userset.add("AlternateEmail");
        
        List<User> result = null;
        
        SearchCriteria criteria = new SearchCriteria("User Login", net_id, SearchCriteria.Operator.EQUAL);
        
        try{
        	result=searchUsers(criteria, userset);
        }
        catch(UserException e){
        	System.out.println("User Exception in searchUsers \n");
        }

//--------------------------------------->>>>Case 1        
        if(result.isEmpty()){
        	System.out.println("Inside isEmpty if"+"\n\n\n\n");
        	for(int i = 0; i<attrs.size();i++){
				if(attrs.get(i).getName().equals("SponsorNetID")){	
					System.out.println("\n\n------------->Setting SponsorNetId" + (String)attrs.get(i).getValue() + "As New Login\n\n");
					
			        attr = new RequestEntityAttribute("User Login", (String)attrs.get(i).getValue(),
			                RequestEntityAttribute.TYPE.String);
			        
			        attrs.add(attr);
			        System.out.println("Added login attribute \n\n\n\n");
			        attrs.remove(i);
				}
			}
        }
//--------------------------------------->>>>Case 2
        else if(result.size() == 1){
        	System.out.println("\n\nHave one login that is the same\n\n");		
       		
        	if(result.get(0).getFirstName().equals(user.getFirstName())
        		&& result.get(0).getLastName().equals(user.getLastName())
        		&& result.get(0).getStatus().equals("Disabled")){
        		
        		UserManager usrMgr = oimClient.getUsrMgr();
       			String login = result.get(0).getLogin();
       			System.out.println("\n\n------------->User Login is: " + login +"\n\n");
        		
//--------------------->Get Sponsor first/last, start/end Dates
       			String sponsorfname = null;
       			String sponsorlname = null;
       			for(int i = 0;i<attrs.size();i++){
       				if((attrs.get(i).getName()).equals("SponsorsFirstName")) 
       					sponsorfname = (String)attrs.get(i).getValue();
       				if((attrs.get(i).getName()).equals("SponsorsLastName")) 
       					sponsorlname = (String)attrs.get(i).getValue();
       			}
       		
       			Date start_date = new Date((Long) user.getAttribute("Start Date"));
        		Date end_date = new Date((Long) user.getAttribute("End Date"));
        	
//---------------------->Load Hashmap with changed values
       			HashMap<String, Object> attrMap = new HashMap<String, Object>();
       		
       			attrMap.put("Start Date",start_date);
       			attrMap.put("End Date",end_date);
       			attrMap.put("AlternateEmail", (String)user.getAttribute("AlternateEmail"));
       			attrMap.put("ExternalAffiliation", (String) user.getAttribute("ExternalAffiliation"));
       			attrMap.put("VenderName", (String) user.getAttribute("VenderName"));
       			attrMap.put("SponsorID", (String) user.getAttribute("SponsorID"));
       			attrMap.put("Telephone Number", (String) user.getAttribute("Telephone Number"));
       			attrMap.put("SponsorsFirstName", sponsorfname);
       			attrMap.put("SponsorsLastName", sponsorlname);
       			attrMap.put("usr_provisioning_date", new Date());
       		
       			System.out.println("\n\nHashmap output"+attrMap.toString()+"\n\n");
        		
       			User updateUser= new User(login, attrMap);
       		
       			System.out.println("\n\n------------->User ID for Modify: "+updateUser.getEntityId()+"\n\n");
       		
//---------------------->Modify user       		
       			try{
       				usrMgr.modify(UserManagerConstants.AttributeName.USER_LOGIN.getId(), login, updateUser);
        			
       			}catch (ValidationFailedException e) {
       				System.out.println("\n\n------------->VALIDATION FAILURE IN MODIFY\n\n");
       				e.printStackTrace();
       			}catch (UserModifyException e) {
               		System.out.println("\n\n------------->USER MODIFY FAILURE IN MODIFY\n\n");
               		e.printStackTrace();
            	} catch (NoSuchUserException e) {
               		System.out.println("\n\n------------->NO SUCH USER IN MODIFY\n\n");
               		e.printStackTrace();
            	} catch (AccessDeniedException e) {
               		System.out.println("\n\n------------->ACCESS FAILURE IN MODIFY\n\n");
               		e.printStackTrace();
            	}
        		
//------------------------>Enable user by login
       			try {
       				usrMgr.enable(result.get(0).getLogin(), true);
				
       			} catch (UserEnableException e) {
					System.out.println("\n\n------------->ENABLE FAILURE IN ENABLE");
					e.printStackTrace();
				}catch (ValidationFailedException e) {
       				System.out.println("\n\n------------->VALIDATION FAILURE IN ENABLE\n\n");
       				e.printStackTrace();
            	} catch (NoSuchUserException e) {
             		System.out.println("\n\n------------->NO SUCH USER IN ENABLE\n\n");
             		e.printStackTrace();
            	} catch (AccessDeniedException e) {
              		System.out.println("\n\n------------->ACCESS FAILURE IN ENABLE\n\n");
              		e.printStackTrace();
            	}
       			return "User Updated";

        	}
        }
//--------------------------------------->>>>Case 3
        else{
        	logger.error("More than one with unique identifier");
        }
        
/////////////////////////////////////////////////////////////////////////////////////////////////////endDan       
    
        */
        //
        
        //attr = new RequestEntityAttribute(UserManagerConstants.AttributeName.EMAIL.getId(), (String) user.getAttribute("PersonalEmail"),RequestEntityAttribute.TYPE.String);
        //attrs.add(attr);
        //System.out.println("user manager key is"+(Long)(user.getAttribute("user_manager_key")));
        //UserManagerConstants.AttributeName.MANAGER_NAME
        //attr = new RequestEntityAttribute("User Manager", Long.parseLong(user.getManagerKey()), RequestEntityAttribute.TYPE.Long);
        //attr = new RequestEntityAttribute("User Manager", (Long)(user.getAttribute("Last Name")), RequestEntityAttribute.TYPE.Long);
        //System.out.println("Middle Name value is:"+user.getMiddleName());
        
        //attrs.add(attr);
        //added on 06/12 hidden title, department name and department number
        /*attr = new RequestEntityAttribute(UserManagerConstants.AttributeName.TITLE.getId(),
                                          (String) user.getAttribute("Title"), RequestEntityAttribute.TYPE.String);
        attrs.add(attr);
        System.out.println("Department number concatenated is:"+(String) user.getAttribute("Department Number"));
        System.out.println("SSN  is:"+(String) user.getAttribute("SSN"));
        String strconcatDepartmentNumber = (String) user.getAttribute("Department Number");
        String[] parts = strconcatDepartmentNumber.split("-");
        String strDepartmentNumber = parts[0]; // 004
        String strDepartmentName = parts[1]; // 034556
        System.out.println("Department Number and name are respectivily:"+parts[0]+"&"+parts[1]);
        attr = new RequestEntityAttribute(UserManagerConstants.AttributeName.DEPARTMENT_NUMBER.getId(),strDepartmentNumber,RequestEntityAttribute.TYPE.String);
        attrs.add(attr);
        
        attr = new RequestEntityAttribute("DEPT_NAME", strDepartmentName,RequestEntityAttribute.TYPE.String);
        attrs.add(attr);*/
        
        //attr = new RequestEntityAttribute("SSN", (String) user.getAttribute("SSN"),RequestEntityAttribute.TYPE.String);
       // attrs.add(attr);
       
        //Long dob = Long.parseLong(user.getAttribute("DOB").toString());
        //Long dob = Long.parseLong(user.getAttribute("Birth Date").toString());
        //commented below line on 03/08
        System.out.println("DOBBBB value is"+user.getAttribute("DOB"));
        Date d1=null;
        if(user.getAttribute("DOB").toString().startsWith("1"))
        {
        	System.out.println("Inside if condition");
        d1 = new Date(Integer.parseInt(user.getAttribute("DOB").toString()));
        System.out.println("Inside if condition"+d1);
        attr = new RequestEntityAttribute("DOB", (Date)d1,RequestEntityAttribute.TYPE.Date);
        }
        else{
        	System.out.println("Inside else condition");
        attr = new RequestEntityAttribute("DOB", (Date)user.getAttribute("DOB"),RequestEntityAttribute.TYPE.Date);
        }
        //(String) user.getAttribute("SponsorID");
        //attr = new RequestEntityAttribute("Birth Date", new Date(dob),RequestEntityAttribute.TYPE.Date);
        attrs.add(attr);
        
        //03/08
        attr = new RequestEntityAttribute("Telephone Number", (String) user.getAttribute("Telephone Number"),
                                          RequestEntityAttribute.TYPE.String);
        
        attrs.add(attr);
        attr = new RequestEntityAttribute(UserManagerConstants.AttributeName.GENERATION_QUALIFIER.getId(),
                                          user.getGenerationQualifier(), RequestEntityAttribute.TYPE.String);
        attrs.add(attr);
        ent.setEntityData(attrs);

        List<RequestEntity> entities = new ArrayList<RequestEntity>();

        entities.add(ent);
        requestData.setTargetEntities(entities);
        System.out.println("requestData:"+requestData);
        
        
        try {
           reqID= reqsrvc.submitRequest(requestData);
            System.out.println("REQUEST SUBMITTED");
        } catch (InvalidRequestException e) {
            System.out.println("InvalidRequestException: " + e);
        } catch (InvalidRequestDataException e) {
            System.out.println("InvalidRequestDataException: " + e);
        } catch (RequestServiceException e) {
            System.out.println("RequestServiceException: " + e);
        } catch (BulkBeneficiariesAddException e) {
            System.out.println("BulkBeneficiariesAddException: " + e);
        } catch (BulkEntitiesAddException e) {
            System.out.println("BulkEntitiesAddException: " + e);
        }
        
        
        return reqID;
    }
    

    /**
     * Method description
     *
     *
     * @param orgName
     *
     * @return
     */
    private Long getOrgKeyforOrganization(String orgName) {
        OimClient           oimClient   = new OimClient(serviceProperties);
        OrganizationManager orgMgr      = oimClient.getOrgManager();
        
        Long                orgKey      = null;
        Set<String>         returnAttrs = new HashSet<String>();

        returnAttrs.add("act_key");
        returnAttrs.add(oracle.iam.identity.orgmgmt.api.OrganizationManagerConstants.AttributeName.ID_FIELD.getId());
        returnAttrs.add(oracle.iam.identity.orgmgmt.api.OrganizationManagerConstants.AttributeName.ORG_NAME.getId());

        Organization org = null;

        try {
            org    = orgMgr.getDetails(orgName, returnAttrs, true);
            orgKey = (Long) org.getAttribute(
                oracle.iam.identity.orgmgmt.api.OrganizationManagerConstants.AttributeName.ID_FIELD.getId());
        } catch (OrganizationManagerException e) {

            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (AccessDeniedException e) {

            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return orgKey;
    }
    
    public List searchUsers(SearchCriteria searchCriteria, Set attributesToReturn) throws UserException{
        List foundUsers = null;
        OimClient   oimClient = new OimClient(serviceProperties);
        UserManager usrMgr    = oimClient.getUsrMgr();
      
        
        try {
			foundUsers = usrMgr.search(searchCriteria, attributesToReturn, null);
		} catch (UserSearchException e) {
			
			e.printStackTrace();
			throw new UserException(e);
		} catch (AccessDeniedException e) {
			
			e.printStackTrace();
			throw new UserException(e);
		}

        return foundUsers;
    }
    
    
    public User getUserByIdentifier(String fieldName, String value, Set attributesToReturn) throws UserException {
        User        user      = null;
        OimClient   oimClient = new OimClient(serviceProperties);
        UserManager usrMgr    = oimClient.getUsrMgr();

        logger.debug("Attributes to Return: " + attributesToReturn.toString());

        try {
            user = usrMgr.getDetails(fieldName, value, attributesToReturn);
        } catch (NoSuchUserException e) {

            logger.error(e.getMessage());
            throw new UserNotFoundException("Not Found");
        } catch (UserLookupException e) {
        	logger.error(e.getMessage());
        	throw new UserException("Lookup Error");
       
        } catch (SearchKeyNotUniqueException e) {

            logger.error(e.getMessage());
            throw new UserException("Duplicates Found");
        } catch (AccessDeniedException e) {

          
            
            logger.error(e.getMessage());
            throw new UserException("Access Denied");
        }

        return user;
    }
    
}
