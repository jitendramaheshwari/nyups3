package com.aptecllc.oim.vo;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import oracle.iam.identity.usermgmt.vo.User;

@XmlRootElement(name = "person")
public class NewInnovationsPersonCreate {
	

	private String ssn;	
	private String kerberos;
	private String firstName;
	private String lastName;
	private String email;
	private String dob;
	private String role;
	private String rmsId;
	
	public NewInnovationsPersonCreate(){}
	
	public NewInnovationsPersonCreate(String kid, String fname, String lname, 
			String ssn, String dob, String email, String role, String rmsId){
		setKerberos(kid);
		setFirstName(fname);
		setLastName(lname);
		setSsn(ssn);
		setDob(dob);
		//setEmail(email);
		setRole(role);
		setRmsId(rmsId);
	}
	@XmlElement(nillable=true)
	public String getRmsId() {
		return rmsId;
	}

	public void setRmsId(String rmsId) {
		this.rmsId = rmsId;
	}
	@XmlElement(nillable=true)
	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	@XmlElement(nillable=true)
	public String getKerberos() {
		return kerberos;
	}

	public void setKerberos(String kerberos) {
		this.kerberos = kerberos;
	}
	@XmlElement(nillable=true)
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	@XmlElement(nillable=true)
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/*@XmlElement(nillable=true)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}*/
	@XmlElement(nillable=true)
	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}
	@XmlElement(nillable=true)
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("KID: " + kerberos);
		sb.append(" SSN: " + ssn);
		sb.append(" Fname: " + firstName);
		sb.append(" Lname: " + lastName);
		//sb.append(" Email: " + email);
		sb.append(" DOB: " + dob);
		sb.append(" Role: " + role);
		sb.append(" rmsId: " + rmsId);
		return sb.toString();
	}
	
}
