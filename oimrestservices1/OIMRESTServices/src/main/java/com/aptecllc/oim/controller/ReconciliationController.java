package com.aptecllc.oim.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aptecllc.oim.services.OIMReconciliationService;
import com.aptecllc.oim.vo.Reconciliation;

@Controller
@RequestMapping("/reconciliation")
public class ReconciliationController {
	@Autowired
	private OIMReconciliationService reconciliationService;

	@RequestMapping(method = RequestMethod.POST, headers = "content-type=application/json")
	@ResponseBody
	public void reconciliation(@RequestBody Reconciliation reconObject) {
		System.out.println("reconciliation method entered");
		if (reconObject.getReconAction().equalsIgnoreCase("createnew"))
			reconciliationService.createNew(reconObject);
		else if (reconObject.getReconAction().equalsIgnoreCase(
				"linktoexistinguser"))
			reconciliationService.linkToExistingUser(reconObject);
		else if (reconObject.getReconAction().equalsIgnoreCase("closeevent"))
			reconciliationService.closeEvent(reconObject);
		System.out.println("reconciliation method exited");
	}

}
