package com.aptecllc.oim.controller;

//~--- non-JDK imports --------------------------------------------------------

import com.aptecllc.oim.exception.UserException;
import com.aptecllc.oim.exception.UserNotFoundException;
import com.aptecllc.oim.property.PropertyUtil;
import com.aptecllc.oim.services.OIMRequestServices;
import com.aptecllc.oim.services.OIMUserServices;
import com.aptecllc.oim.transformer.ObjectTransformerIntf;
import com.aptecllc.oim.util.ServiceConstants;
import com.aptecllc.oim.vo.NewInnovationsPerson;
import com.aptecllc.oim.vo.NewInnovationsPersonCreate;
import com.aptecllc.oim.vo.NewInnovationsSearchResult;
import com.aptecllc.oim.vo.NewInnovationsSearchResultList;
import com.aptecllc.oim.vo.Request;
import com.aptecllc.oim.vo.ServiceError;
import com.aptecllc.oim.vo.UserAccount;
import com.aptecllc.oim.vo.UserRequest;
import com.aptecllc.oim.vo.UserSelfService;

import oracle.iam.identity.exception.NoSuchUserException;
import oracle.iam.identity.exception.SearchKeyNotUniqueException;
import oracle.iam.identity.exception.UserModifyException;
import oracle.iam.identity.exception.ValidationFailedException;
import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.notification.api.NotificationService;
import oracle.iam.notification.exception.EventException;
import oracle.iam.notification.exception.MultipleTemplateException;
import oracle.iam.notification.exception.NotificationException;
import oracle.iam.notification.exception.NotificationResolverNotFoundException;
import oracle.iam.notification.exception.TemplateNotFoundException;
import oracle.iam.notification.exception.UnresolvedNotificationDataException;
import oracle.iam.notification.exception.UserDetailsNotFoundException;
import oracle.iam.notification.vo.NotificationEvent;
import oracle.iam.platform.Platform;
import oracle.iam.platform.authz.exception.AccessDeniedException;
import oracle.iam.platform.entitymgr.EntityManager;
import oracle.iam.platform.entitymgr.vo.SearchCriteria;
import oracle.iam.platform.entitymgr.vo.SearchCriteria.Operator;
import oracle.iam.platform.utils.NoSuchServiceException;
import oracle.iam.selfservice.exception.UserLookupException;

import com.aptecllc.iam.oim.utils.notificationprovider.APTECEMAILNotificationProvider;
import com.aptecllc.iam.oim.utils.notificationprovider.APTECEMAILPayload;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.w3c.dom.Node;

import com.thortech.xl.crypto.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;

//~--- JDK imports ------------------------------------------------------------



import java.math.BigInteger;
import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;

import javax.ws.rs.QueryParam;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;

/**
* Class description
*
*
* @version        1.0, 13/11/06
* @author         Eric A. Fisher, APTEC LLC
*/
@Controller
@RequestMapping("api/v1")
public class OIMRESTController {
    private static final Logger   logger = LoggerFactory.getLogger(OIMRESTController.class);
    @Autowired
    private OIMUserServices       userServices;
    @Autowired
    private OIMRequestServices    requestServices;
    @Autowired
    private ObjectTransformerIntf objTransformer;

    /**
     * This method allows for user search based on any of the defined attributes in the attributeMap.
     * It will return a list of UserAccounts that match the passed criteria.  Criteria should be
     * formatted in RSQL format for example.
     *
     * lastName==Smith;firstName==John (Matches Users with First Name of John AND Last Name of Smith
     *
     * Operators
     * EQUAL ==
     * NOT EQUAL !=
     * GT >
     * GTE >=
     * LT <
     * LTE <=
     *
     * AND ; or 'and'
     * OR , or 'or'
     *
     *
     *
     * @param searchCriteria
     *
     * @return List of UserAccount objects
     *
     * @throws UserException
     */
    @RequestMapping(
        value    = "users",
        method   = RequestMethod.GET
    )
    public @ResponseBody
    List<UserAccount> findUsers(@RequestParam(
        value    = "query",
        required = true
    ) String searchCriteria) throws UserException {
        List<UserAccount> accounts = new ArrayList<UserAccount>();

        logger.info("Search Criteria: " + searchCriteria);

        SearchCriteria oimSC = objTransformer.parseRSQL(searchCriteria);

        logger.info("Parsed OIM Search: " + oimSC);

       List<User> foundUsers = userServices.searchUsers(oimSC,
                                    new HashSet(objTransformer.getAllAttributesMap().values()));

        for (User oimUser : foundUsers) {
            accounts.add(objTransformer.convertUser(oimUser));
        }

        return accounts;
    }
    

    /**
     * REST Get Method that returns an UserAccount VO object given the User Login.
     * Response is JSON by default.
     *
     *
     *
     *
     * @param userName - Typically mapped to User Login value in OIM
     *
     * @return UserAccount object
     *
     * @throws UserException 404-Not Found
     */
    @RequestMapping(
        value  = "user/{userName}",
        method = RequestMethod.GET
    )
    public @ResponseBody
    UserAccount getUserByUserName(@PathVariable String userName) throws UserException {
        logger.error(objTransformer.getAllAttributesMap().values().toString());

        User user = userServices.getUserByIdentifier(PropertyUtil.getProperty(ServiceConstants.USER_NAME.getName()),
                        userName, new HashSet(objTransformer.getAllAttributesMap().values()));

        if (user == null) {
            throw new UserNotFoundException("Username " + userName + " Not Found");
        }

        UserAccount uAcct = objTransformer.convertUser(user);

        return uAcct;
    }

    /**
     * REST Get Method that returns an UserAccount VO object given the Entity ID/USR_KEY.
     * Response is JSON by default.
     *
     * @param userKey
     *
     * @return UserAccount object
     *
     * @throws UserException 404-Not Found
     */
    @RequestMapping(
        value  = "user/key/{userKey}",
        method = RequestMethod.GET
    )
    public @ResponseBody
    UserAccount getUserByKey(@PathVariable String userKey) throws UserException {
        logger.error(objTransformer.getAllAttributesMap().values().toString());

        User user = userServices.getUserByIdentifier("usr_key", userKey,
                        new HashSet(objTransformer.getAllAttributesMap().values()));

        if (user == null) {
            throw new UserNotFoundException("Not Found");
        }

        UserAccount uAcct = objTransformer.convertUser(user);

        return uAcct;
    }

    /**
     * REST Method that returns an UserAccount VO object given the Unique Account ID.
     * The OIM field that contains this value is defined in the attributeMap.properties and is
     * typically an Employee ID, or some other unique value different from the User Login
     *
     * Response is JSON by default.
     *
     *
     *
     * @param accountID
     *
     * @return UserAccount object
     *
     * @throws UserException 404-Not Found
     */
    @RequestMapping(
        value  = "user/acctid/{accountID}",
        method = RequestMethod.GET
    )
    public @ResponseBody
    UserAccount getUserByAccountID(@PathVariable String accountID) throws UserException {
        User user =
            userServices.getUserByIdentifier(PropertyUtil.getProperty(ServiceConstants.USER_ACCOUNTID.getName()),
                accountID, new HashSet(objTransformer.getAllAttributesMap().values()));

        if (user == null) {
            throw new UserException("Null User");
        }

        UserAccount uAcct = objTransformer.convertUser(user);

        return uAcct;
    }

    /**
     * REST Method that is used for creating a new OIM user via POST.  Expects the RequestBody to by a JSON request matching the UserAccount
     * VO.  Will return the URL to the user created.
     *
     *
     *
     * @param account
     *
     * @return 201-Created on success and Header with location to created user.
     *
     * @throws UserException
     */
    @RequestMapping(
        value  = "user",
        method = RequestMethod.POST
    )
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<byte[]> createUser(@RequestBody UserAccount account) throws UserException {

        // String userAccountID = account.getAccountId();
        User user = objTransformer.convertUserAccount(account);

        logger.error(user.toString());

        String      response = userServices.createUser(user, account.getOrganizationName());
        HttpHeaders headers  = new HttpHeaders();

        headers.add("Location",
                    ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/v1/user/key/"
                        + response).build().toUriString());

        return new ResponseEntity<byte[]>(null, headers, HttpStatus.CREATED);
    }

    //start
    /**
     * REST Method that is used for enabling a user via POST.  Expects the RequestBody to by a JSON request matching the UserAccount
     * VO.  Will return the URL to the user created.
     *
     *
     *
     * @param account
     *
     * @return 201-Created on success and Header with location to created user.
     *
     * @throws UserException
     */
    @RequestMapping(
        value  = "user/{userName}/enable",
        method = RequestMethod.GET
    )
    //@ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody String  enableUser(@PathVariable("userName") String userName) throws UserException {

        // String userAccountID = account.getAccountId();
        //User user = objTransformer.convertUserAccount(account);

        //logger.error(user.toString());
                System.out.println("Account"+userName);
        String      response = userServices.enableUser(userName);
        /*HttpHeaders headers  = new HttpHeaders();

        headers.add("Location",
                    ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/v1/user/key/"
                        + response).build().toUriString());*/

       // return new ResponseEntity<byte[]>(null, headers, HttpStatus.CREATED);
        System.out.println("response"+response);
        return "SUCCESS";
    }

    //end
    
    //
    
    @RequestMapping(
            value  = "user/{lookupName}/getlookup",
            method = RequestMethod.GET
        )
        //@ResponseStatus(HttpStatus.CREATED)
        public @ResponseBody Map  getlookup(@PathVariable("lookupName") String lookupName) throws UserException {

            // String userAccountID = account.getAccountId();
            //User user = objTransformer.convertUserAccount(account);

            //logger.error(user.toString());
                System.out.println("lookup name"+lookupName);
            //String      response = userServices.enableUser(userName);
                Map<String,String> lookupPairs= new HashMap<String,String>();
                lookupPairs = userServices.getLookupValues(lookupName);
                Map<String, String> treeMap = new TreeMap<String, String>(lookupPairs);
            /*HttpHeaders headers  = new HttpHeaders();

            headers.add("Location",
                        ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/v1/user/key/"
                            + response).build().toUriString());*/

           // return new ResponseEntity<byte[]>(null, headers, HttpStatus.CREATED);
            System.out.println("response"+lookupPairs.size());
            //return lookupPairs;
            return treeMap;
        }

    
    //
    /**
     * Request PUT Method that updates an OIM User given the User Login and values to be updated.
     * Performs either a direct modification as the admin user or a self modify user profile if the request user
     * is te same as the authenticated user.
     *
     *
     *
     * @param account
     * @param userName
     * @param principal
     *
     * @return 204-No Content on Success or 400-Bad Request of Failure
     *
     * @throws UserException
     */
    @RequestMapping(
        value  = "user/{userName}",
        method = RequestMethod.PUT
    )
    public @ResponseBody
    ResponseEntity<byte[]> updateUserByLogin(@RequestBody UserAccount account, @PathVariable String userName,
            Principal principal)
            throws UserException {
        String requestingUser = principal.getName();
        String returnCode     = "";

        logger.debug(account.toString());

        if (userName.equalsIgnoreCase(requestingUser)) {
            returnCode = userServices.modifyUserProfile(account.getAttributes());
        } else {
            returnCode = updateUser(account, userName, PropertyUtil.getProperty(ServiceConstants.USER_NAME.getName()));
        }

        if ("OK".equals(returnCode)) {
            return new ResponseEntity<byte[]>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<byte[]>(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Request PUT Method that updates an OIM User given the Account ID and values to be updated.
     * Performs either a direct modification as the admin user or a self modify user profile if the request user
     * is te same as the authenticated user.
     *
     *
     *
     *
     * @param account
     * @param accountID
     *
     * @return 204-No Content on Success or 400-Bad Request of Failure
     *
    * @throws UserException
     */
    @RequestMapping(
        value  = "user/acctid/{accountID}",
        method = RequestMethod.PUT
    )
    public @ResponseBody
    ResponseEntity<byte[]> updateUserByAccountID(@RequestBody UserAccount account, @PathVariable String accountID)
            throws UserException {
        UserAccount updatedUser = null;
        String      returnCode  = updateUser(account, accountID,
                                      PropertyUtil.getProperty(ServiceConstants.USER_ACCOUNTID.getName()));

        if ("OK".equals(returnCode)) {
            return new ResponseEntity<byte[]>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<byte[]>(HttpStatus.BAD_REQUEST);
        }
    }

    // Returns the list of set challenges for a given user login

    /**
     * Retrieves the configured user challenge questions either as the user or using
     * unauthenticated service.
     *
     *
     * @param userName
     * @param principal
     *
     * @return List of challenges set for the user.
     *
     * @throws UserException 500 Error on any error, for lack of a better response.
     */
    @RequestMapping(
        value  = "user/{userName}/challenges",
        method = RequestMethod.GET
    )
    public @ResponseBody
    List getUserChallengesByLogin(@PathVariable String userName, Principal principal) throws UserException {
        String requestingUser = principal.getName();
        List   challenges     = null;

        try {
            if (userName.equalsIgnoreCase(requestingUser)) {
                System.out.println("SelfRequest");
                challenges = userServices.getUserChallenges();
            } else {
                System.out.println("3rdPartyRequest");
                challenges = getUserChallenges(userName);
            }
        } catch (UserException e) {
            System.out.println(e.getMessage());

            throw e;
        }

        return challenges;
    }

    /**
     * Updates the users challenge questions and answers, this must be performed as the
     * user to be updated.
     *
     *
     * @param userName
     * @param uSS
     * @param principal
     *
     * @return 204-No Content on success.  400-Bad Request on failure.  403-Forbidden if user asserted is not the user being changed.
     */
    @RequestMapping(
        value  = "user/{userName}/challenges",
        method = RequestMethod.PUT
    )
    public @ResponseBody
    ResponseEntity<byte[]> setUserChallenges(@PathVariable String userName, @RequestBody UserSelfService uSS,
            Principal principal) {
        String requestingUser = principal.getName();

        if (userName.equalsIgnoreCase(requestingUser)) {
            try {
                userServices.setUserChallenges(uSS.getQuestions());
            } catch (UserException e) {
                e.printStackTrace();

                return new ResponseEntity<byte[]>(e.getMessage().getBytes(), HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<byte[]>(HttpStatus.FORBIDDEN);
        }

        return new ResponseEntity<byte[]>(HttpStatus.NO_CONTENT);
    }

    /**
     * Performs validation on password prior to password change.  Requires user login and
     * new password.
     *
     *
     * @param userName
     * @param uSS
     *
     * @return 200-OK on success.  400-Bad Request on failure.
     */
    @RequestMapping(
        value  = "user/{userName}/validatepassword",
        method = RequestMethod.PUT
    )
    public @ResponseBody
    ResponseEntity<byte[]> validatePassword(@PathVariable String userName, @RequestBody UserSelfService uSS) {
        String passwordResult = userServices.validatePassword(userName, uSS.getNewPassword().toCharArray());

        if ("Valid".equals(passwordResult)) {
            return new ResponseEntity<byte[]>(HttpStatus.OK);
        } else {
            return new ResponseEntity<byte[]>(passwordResult.getBytes(), HttpStatus.BAD_REQUEST);
        }
    }

    // Uses different methods based on the calling user. Self or 3rd Party

    /**
     * Changes the user password.  This method performs various operations based on the input information,
     * if no QA are defined and and old and new password is defined, the system will change the password for the
     * authenticated user.
     *
     * If QA are defined and there is no old password, the system attempts a forgot password reset.
     *
     * Otherwise if the authenticated user is not the user to be update it attempts an administrator password set, this is
     * of course subject to the permissions in OIM for the administrator account being used.
     *
     *
     * @param userName
     * @param uSS
     * @param principal
     *
     * @return 204-No Content on success
     */
    @RequestMapping(
        value  = "user/{userName}/password",
        method = RequestMethod.PUT
    )
    public @ResponseBody
    ResponseEntity<byte[]> changeUserPassword(@PathVariable String userName, @RequestBody UserSelfService uSS,
            Principal principal) {
        String requestingUser = principal.getName();

        if ((uSS.getQuestions() == null) && (uSS.getOldPassword() != null)) {

            // Normal Password Change
            try {
                userServices.changePasswordAsUser(uSS.getOldPassword(), uSS.getNewPassword(), uSS.getConfirmPassword());
            } catch (UserException e) {
                e.printStackTrace();

                ResponseEntity re = new ResponseEntity<byte[]>(e.getMessage().getBytes(), HttpStatus.METHOD_FAILURE);

                return re;
            }
        } else if ((uSS.getQuestions() != null) && (uSS.getOldPassword() == null)) {

            // QUestion Reset
            try {
                userServices.resetPasswordWithChallenges(uSS.getUserLogin(), uSS.getQuestions(), uSS.getNewPassword());
            } catch (UserException e) {
                e.printStackTrace();

                ResponseEntity re = new ResponseEntity<byte[]>(e.getMessage().getBytes(), HttpStatus.METHOD_FAILURE);

                return re;
            }
        } else if (!requestingUser.equalsIgnoreCase(userName)) {

            // Admin Password Set
            try {
                userServices.setUserPassword(userName, uSS.getNewPassword(), uSS.getForceChange());
            } catch (UserException e) {
                e.printStackTrace();

                ResponseEntity re = new ResponseEntity<byte[]>(e.getMessage().getBytes(), HttpStatus.METHOD_FAILURE);

                return re;
            }
        } else {
            return new ResponseEntity<byte[]>(HttpStatus.NOT_ACCEPTABLE);
        }

        return new ResponseEntity<byte[]>(HttpStatus.NO_CONTENT);
    }

    // Returns the list of system challenges

    /**
     * This service returns the list of defined questions in the system for use in creating input forms
     * for user selection if security questions.
     *
     *
     * @return
     */
    @RequestMapping(
        value  = "challenges",
        method = RequestMethod.GET
    )
    public @ResponseBody
    List getSystemChallenges() {
        return userServices.getSystemChallenges();
    }

    /**
     * Initiates a create user request rather than a direct creation.  This would trigger any approvals if required.
     * Returns the request identifier for the generated request.  The requesting user is set as the User Manager on the requested
     * object.
     *
     * @param request
     * @param principal
     *
     * @return 202-Accepted on Request Receipt
     *
     * @throws UserException
     * @throws AccessDeniedException 
     * @throws SearchKeyNotUniqueException 
     * @throws NoSuchUserException 
     * @throws UserModifyException 
     * @throws ValidationFailedException 
     */
    @RequestMapping(
        value  = "request/user",
        method = RequestMethod.POST
    )
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity<byte[]> createUserRequest(@RequestBody UserRequest request, Principal principal)
            throws UserException, ValidationFailedException, UserModifyException, NoSuchUserException, SearchKeyNotUniqueException, AccessDeniedException {
        String requestingUser = principal.getName();

        // String userAccountID = account.getAccountId();
        UserAccount account = (UserAccount) request.getRequestObject();
        User        user    = objTransformer.convertUserAccount(account);
        User        manager =
            userServices.getUserByIdentifier(PropertyUtil.getProperty(ServiceConstants.USER_NAME.getName()),
                requestingUser, new HashSet(objTransformer.getAllAttributesMap().values()));

        user.setManagerKey(manager.getEntityId());
        logger.error(user.toString());

        String response = requestServices.requestNewUser(request.getJustification(), user,
                              account.getOrganizationName(), requestingUser);
        HttpHeaders headers = new HttpHeaders();

        headers.add("Location",
                    ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/v1/request/"
                        + response).build().toUriString());

        return new ResponseEntity<byte[]>(null, headers, HttpStatus.ACCEPTED);
    }
    
    
/** ------------------------>New Innovations REST Methods<-----------------------
* ***********************Dan Donnelly: Revised 12/23/2015***********************
* 
 *-DONE                               1.            GET method that takes Kerberos ID of New Innovations User
*                                                            Returns: NewInnovationsPerson of user              
*-DONE                               2.            POST method that takes fname, lname, ssn, dob
*                                                            Returns: NewInnovationsSearchResultList with an exact match, 
 *                                                            as well as fname+dob AND lname+dob partial matches
*-DONE                               3.            PUT method that takes kerberos id and role and sets 
 *                                                            the corresponding flag, 
 *                                                            Returns: NewInnovationsPerson of user
*-DONE                               4.            POST method that takes fname, lname, ssn, dob, role 
 *                                                            and makes a new person
*                                                            Returns: NewInnovationsPerson of user
*------------------------------------------------------------------------------*/
        
       /**
        * GET method that takes a Kerberos ID as an input
        * Returns the SSN, DOB, Name, Email and the ID
        * 
        * ----->Dan Donnelly: Sprint 8
        * 
        * @param id
        * 
        * @return HashMap with specified info
        * @throws UserException 404-Not Found
        * 
        */
        @RequestMapping(
                value = "/ni/user/{id}",
                method = RequestMethod.GET
        )
        public @ResponseBody
        NewInnovationsPerson getUserDetails(@PathVariable(value = "id") String id) throws UserException{
                System.out.println("\n\n------------------->Inside of OIMRESTController.getUserDetails\n\n");
                String ssn = null,role = null,dob = null,rmsId=null;
                                User user = userServices.getUserByIdentifier(PropertyUtil.getProperty(ServiceConstants.USER_NAME.getName()),
                                                                id, new HashSet(objTransformer.getAllAttributesMap().values()));
                                
                                if(user.getAttribute("SSN")!=null) ssn = decryptSSN((String)user.getAttribute("SSN"));
                                
                                if(user.getAttribute("InrotatorFlag")!= null && user.getAttribute("InrotatorFlag").equals("Y")) 
                                                role = "PendingInRotator";
                                else if(user.getAttribute("ApplicantFlag")!=null && user.getAttribute("ApplicantFlag").equals("Y"))
                                                role = "HSApplicant";
                                
                                dob = convertDateToString((Date)user.getAttribute("DOB"));
                                
                                NewInnovationsPerson person = new NewInnovationsPerson(id, user.getFirstName(), user.getLastName(),ssn, dob ,user.getEmail(), role,rmsId);
                                System.out.println("\n>>>>>>>>>>>>>>>>Person:"+person.toString());
                                
                return person;
        }
       // started for New beginings
        
        /**
         * GET method that takes a Kerberos ID as an input
         * Returns the Response to New beginings Application
         * 
         * ----->Shine Thomas: Sprint 51
         * 
         * @param id, type
         * 
         * @return HashMap with specified info
         * @throws UserException 404-Not Found
         * 
         */
         @RequestMapping(
                 value = "/nb/user/{id}/type/{type}",
                 method = RequestMethod.GET
         )
         public @ResponseBody
         String getUserDetailsNB(@PathVariable(value = "id") String id, @PathVariable(value = "type") String type) throws UserException{
                 System.out.println("\n\n------------------->Inside of OIMRESTController.getUserDetailsNB\n\n id is"+id+"-type-"+type);
                 
                 SearchCriteria criteria = new SearchCriteria("User Login", id, SearchCriteria.Operator.EQUAL);
                 List<UserAccount> retUsers = new ArrayList<UserAccount>();
                 List<String> ids = new ArrayList<String>();
                 List<String> templatetype = new ArrayList<String>();
                 templatetype.add("OBNYU");
                 templatetype.add("OBBLKN");
                 templatetype.add("OBNTV");
                 templatetype.add("OBFHC");
                 templatetype.add("OBFGPA");
                 templatetype.add("NBFOCUS");
                 templatetype.add("PSFGPMGR");
                 templatetype.add("NTV");
                 if(!templatetype.contains(type)) return "Unrecognized Event Type Passed, please try again ";
                 
                 List<User> foundUsers = userServices.searchUsers(criteria,new HashSet(objTransformer.getAllAttributesMap().values()));
                 System.out.println("foundUsers:"+foundUsers.size());
                 String link = "<a href='https://identity.nyumc.org/identity' target='_blank'>Click here to login to OIM and search users to send account claiming emails</a>";
                 if(foundUsers.size()<=0) return link;
                 for(int i=0;i<foundUsers.size();i++){
                     User user = foundUsers.get(0);
                	 String strTermsAccepted= (String) user.getAttribute("TermsAccepted");
                	 String strAlterEmailAddress= (String) user.getAttribute("AlternateEmail");
                	 String strAltEmail = (String) user.getAttribute("AlternateEmail");
                	 List<String> emailToList = new ArrayList<String>();
                	 String strFirstName = (String) user.getAttribute("First Name");
                	 String strLastName = (String) user.getAttribute("Last Name");
                	 String strUserLogin = (String) user.getAttribute("User Login");
                	 HashMap<String, Object> attrs = new HashMap<String, Object>();
                	 // added on 11/28/2017 to remove dependency on terms accepted flag so that email can be triggered any number of times
                     System.out.println("Terms Accepted Flag is:"+strTermsAccepted);
                     /*if(strTermsAccepted!=null && strTermsAccepted.equalsIgnoreCase("Y"))
                    	 return "Account Already Cliamed";*/
                     if(strAlterEmailAddress==null ||strAlterEmailAddress.isEmpty())
                    	 return "No Alternate Email Configured on User Profile, Please Contact Sec-Admin";
                     emailToList.add(strAltEmail);
                     //APTECEMAILPayload emailPayload = new APTECEMAILPayload(emailToList, "oimadmin@nyumc.org", true, "UTF-8");
                   
							try {
								
								SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
								Date date = new Date();
								System.out.println(dateFormat.format(date));
								attrs.put("SilkRoadIntegration", dateFormat.format(date)+"+"+type);
								String retcode = userServices.updateUser(attrs, strUserLogin);
								//userServices.sendAccountClaimingEmail(type, strAltEmail, strFirstName, strLastName, strUserLogin);
							} catch (AccessDeniedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
	                  
     				}
               return "Email Sent Successfully to User:"+id;
         }
         
             

               //
        /**
         * POST method that returns exact matches for
         * PIMS firstName, lastName, ssn and dob, as well
         * as partial matches with first name and DOB, and 
         * last name and DOB 
         * 
         * ----->Dan Donnelly: Sprint 8
         * 
         * @param firstName
         * @param lastname
         * @param dob
         * @param ssn
         * 
         * @return List of User accounts
         * @throws UserException 404-Not Found  
         */
        @RequestMapping(
                value = "ni/user/search",
                method = RequestMethod.POST             
        )
        public @ResponseBody 
        NewInnovationsSearchResultList searchUsersByDetails(@RequestBody String body) 
                                                throws UserException, ParserConfigurationException, UnsupportedEncodingException, SAXException, IOException {
                
                String firstName = null, lastName = null, dob=null, ssn=null, rmsId=null;
                
                Element element=getXMLElement(body);
                firstName=element.getElementsByTagName("firstName").item(0).getTextContent();
                                                lastName=element.getElementsByTagName("lastName").item(0).getTextContent();
                                                dob=element.getElementsByTagName("dob").item(0).getTextContent();
                                                ssn=element.getElementsByTagName("ssn").item(0).getTextContent();
                                                rmsId=element.getElementsByTagName("rmsId").item(0).getTextContent();
                                                
                System.out.println("\n\n--------------------------->Inside of createUserDetails,"
                                                +"\n\tFirst Name: "+firstName+"\n\tLast Name: "+lastName+"\n\tDOB: "
                                                +dob+"\n\tSSN: "+ssn+"\n\n");
                
                //Setup Steps:  Convert the date from a string to a date object, set up search criteria for partial 
                //                                                            and full match, instantiate variables to use
                
                Date DOB = convertDate(dob);
                
                SearchCriteria crit1 = new SearchCriteria("First Name", firstName, SearchCriteria.Operator.EQUAL);
                SearchCriteria crit2 = new SearchCriteria("Last Name", lastName, SearchCriteria.Operator.EQUAL);
                SearchCriteria crit3 = new SearchCriteria("DOB", DOB, SearchCriteria.Operator.EQUAL);
                SearchCriteria crit4 = new SearchCriteria(crit1, crit2, Operator.AND);
                
                SearchCriteria exactMatch = new SearchCriteria(crit3, crit4, Operator.AND);
                SearchCriteria partialFirstNameMatch = new SearchCriteria(crit1, crit3, Operator.AND);
                SearchCriteria partialLastNameMatch = new SearchCriteria(crit2, crit3, Operator.AND);
                
                List<UserAccount> retUsers = new ArrayList<UserAccount>();
                
                List<String> ids = new ArrayList<String>();
                NewInnovationsSearchResultList retList = new NewInnovationsSearchResultList();
                //Get users from exact search, if decrypted SSN is a match, add to UserAccounts to return
                
                List<User> foundUsers = userServices.searchUsers(exactMatch,
                    new HashSet(objTransformer.getAllAttributesMap().values()));

                for(User user : foundUsers){
                                //String userSSN = decryptSSN((String) user.getAttribute("SSN"));
                                
                                String userSSN = (String) user.getAttribute("SSN");
                                System.out.println("userSSN in encrypted format is ------->"+userSSN);
                                
                                if(userSSN!=null && !userSSN.isEmpty() && userSSN.length()>10) {
                                	System.out.println("When the SSN is more than 10 digit long");
                                   userSSN = decryptSSN(userSSN);}
                                else if (userSSN!=null && userSSN.length()==9) {
                            		System.out.println("When the SSN is 9 digit long");
                					userSSN=userSSN;
                		
                				}
                                System.out.println("decypted format is->"+userSSN);
                                if(userSSN!=null && !userSSN.isEmpty()) {
                                if(userSSN.equals(ssn)){
                                                retList.add(new NewInnovationsSearchResult(convertUserToNewInnovations(user,rmsId), true));
                                ids.add(user.getEntityId());
                                }  
                                }
                                
                }
                
                //Partial first name search, if ID matches the ID gotten from exact match, don't add to other users
                
                List<User> partialMatchFirstNameUsers = userServices.searchUsers(partialFirstNameMatch,
                    new HashSet(objTransformer.getAllAttributesMap().values()));
                
                for(User user: partialMatchFirstNameUsers){
                                System.out.println(user.toString());
                                Boolean addFlag = false;
                                
                                for(String id: ids){
                                                if(user.getEntityId().equals(id))addFlag = true;
                                }
                                
                                if(!addFlag){ 
                                                retList.add(new NewInnovationsSearchResult(convertUserToNewInnovations(user,rmsId), false));
                                                ids.add(user.getEntityId());
                                }
                }
                
                //Partial Last Name search, same as the first name search
                List<User> partialMatchLastNameUsers = userServices.searchUsers(partialLastNameMatch,
                   new HashSet(objTransformer.getAllAttributesMap().values()));
                
                for(User user: partialMatchLastNameUsers){
                                System.out.println(user.toString());
                                Boolean addFlag = false;
                                
                                for(String id: ids){
                                                if(user.getEntityId().equals(id)) addFlag = true;
                                }
                                
                                if(!addFlag){
                                                retList.add(new NewInnovationsSearchResult(convertUserToNewInnovations(user,rmsId), false));
                                                ids.add(user.getEntityId());
                                }
                                                
                }
                
                System.out.println(">>>>>>>>Returning from OIMRESTController.searchUsersByDetails\n");
                return retList;
        }
        
        
        /**
         * Put Method that takes the kerberos ID and the role from 
         * New Innovations, and sets the corresponding flag
         * 
         * ----->Dan Donnelly: Sprint 8
         * 
         * @param id
         * @param role
         * 
         * @return Status 204 No body on success, 400 for Bad Request
         * @throws UserException 
         * @throws IOException 
         * @throws SAXException 
         * @throws UnsupportedEncodingException 
         * @throws ParserConfigurationException 
         * 
         */
        @RequestMapping(
                value = "ni/user/register",
                method = RequestMethod.PUT
        )
        public @ResponseBody NewInnovationsPerson updateNewInnovationRole(@RequestBody String body) throws UserException, UnsupportedEncodingException, SAXException, IOException, ParserConfigurationException{
                System.out.println("\n\n--------------------------> Inside of update N.I. Role\n\n");
                String id = null, role = null, rmsId=null,status=null,enableStatus=null,ssn=null,email=null;
                
                String retcode = null;
                Element element = getXMLElement(body);
                id=element.getElementsByTagName("kerberos").item(0).getTextContent();
                System.out.println("NI payload kerberos id:->"+id);
                role=element.getElementsByTagName("role").item(0).getTextContent();
                System.out.println("NI payload role is:->"+role);
                rmsId=element.getElementsByTagName("rmsId").item(0).getTextContent();
                System.out.println("NI payload rmsId is:->"+rmsId);
                ssn=element.getElementsByTagName("ssn").item(0).getTextContent();
                System.out.println("NI payload matchId is:->"+ssn);
                email=element.getElementsByTagName("email").item(0).getTextContent();
                System.out.println("NI payload external email is:->"+email);
                
                
                
                
                
                //Get user by username
                User user = userServices.getUserByIdentifier(PropertyUtil.getProperty(ServiceConstants.USER_NAME.getName()),
                                                                id, new HashSet(objTransformer.getAllAttributesMap().values()));
                HashMap<String, Object> attrs = new HashMap<String, Object>();
              //added on 04/10/2017 to handle various housestaff rehire scenario
                status=user.getStatus();
                System.out.println("Status of the user:" +id+"is<-:->"+user.getStatus());
                if(user!=null && status.equalsIgnoreCase("Active"))
                {
                	// Perform all the updates needed to make user a housestaff
                	attrs.put("End Date", null);
                	retcode = userServices.updateUser(attrs, user.getLogin());
                	System.out.println("End Date is nullified");
                	attrs.put("Role", "New Innovation");
                	System.out.println("User Type is updated");
                	attrs.put("HelpDeskAttr",null);
                	System.out.println("HelpDeskAttr is nullifed");
                	if (user.getEmployeeType().equalsIgnoreCase("Contractor"))
                	{
                		attrs.put("SponsorID", null);
                    	System.out.println("SponsorID is nullifed");
                    	attrs.put("SponsorsFirstName", null);
                    	System.out.println("SponsorFirstName is nullifed");
                    	attrs.put("SponsorsLastName", null);
                    	System.out.println("SponsorLastName is nullifed");
                    	attrs.put("ExternalAffiliation",null);
                    	System.out.println("ExternalAffiliation is nullifed");
                    	attrs.put("SSN", encryptSSN(ssn));
                    	System.out.println("SSN is updated");
                    	
                    	
                    	
                	}
                	
                	
                }
                
                else if(user!=null && status.equalsIgnoreCase("Disabled"))
                {
                	// Perform all the updates needed to make user active and update attributes
                	System.out.println("New Innovations Update method called and the user is disabled");
                	attrs.put("End Date", null);
                	retcode = userServices.updateUser(attrs, user.getLogin());
                	System.out.println("End Date is nullified");
                	enableStatus=userServices.enableUser(id);
                	System.out.println("enableStatus of the user is->"+enableStatus);
                	attrs.put("Role", "New Innovation");
                	System.out.println("User Type is updated");
                	attrs.put("EmailSent", "N");
                	System.out.println("EmailSent flag is updated");
                	attrs.put("TermsAccepted", "N");
                	System.out.println("TermsAccepted flag is updated to N");
                	attrs.put("SSN", encryptSSN(ssn));
                	System.out.println("SSN is updated");
                	attrs.put("AlternateEmail", email);
                	System.out.println("AlternateEmail is updated");
                	System.out.println("User Type of the user is:"+user.getEmployeeType());
                	if (user.getEmployeeType().equalsIgnoreCase("Contractor"))
                	{
                		attrs.put("SponsorID", null);
                    	System.out.println("SponsorID is nullifed");
                    	attrs.put("SponsorsFirstName", null);
                    	System.out.println("SponsorFirstName is nullifed");
                    	attrs.put("SponsorsLastName", null);
                    	System.out.println("SponsorLastName is nullifed");
                    	attrs.put("ExternalAffiliation",null);
                    	System.out.println("ExternalAffiliation is nullifed");
                    	attrs.put("HelpDeskAttr",null);
                    	System.out.println("HelpDeskAttr is nullifed");
                    	
                    	
                	}
                	                	
                	//user.setEmployeeType("New Innovation");
                    
                	
                	
                }
                
                //userServices.
                
                //If User is Active and present in OIM, change the user type to New Innovations, empty out the end date
                //if User is Inactive and present in OIM, change the user type to New Innovations, activate the user and nullify the end date
               
                
                
                if(user!=null){
                	
                                if(role.equalsIgnoreCase("HSApplicant")){  
                                                attrs.put("ApplicantFlag", "Y");
                                                if(user.getAttribute("InrotatorFlag")!=null && user.getAttribute("InrotatorFlag").equals("Y")){               
                                                                attrs.put("InrotatorFlag", null);
                                                }
                                }else if(role.equalsIgnoreCase("PendingInRotator")){
                                                attrs.put("InrotatorFlag", "Y");
                                                if(user.getAttribute("ApplicantFlag")!=null && user.getAttribute("ApplicantFlag").equals("Y")){
                                                                
                                                                attrs.put("ApplicantFlag", null);
                                                }
                                }
                }
                System.out.println("\nAttributes toString: "+attrs.toString());
                
                retcode = userServices.updateUser(attrs, user.getLogin());
                System.out.println("\n\n>>>>>>>>>>>>>User Updated successfully");
                if(retcode.equals("OK")){
                                user = userServices.getUserByIdentifier(PropertyUtil.getProperty(ServiceConstants.USER_NAME.getName()),
                        id, new HashSet(objTransformer.getAllAttributesMap().values()));
                                
                                System.out.println(">>>>>>>>Returning from OIMRESTController.updateNewInnovationRole\n");
                                return convertUserToNewInnovations(user,rmsId);
                }
                else
                                return null;
        }
       
        
        
        /**
         * Post Method to create a new user with the following params
         * coming from New Innovations
         * 
         * ----->Dan Donnelly: Sprint 8
         * @body xmlstring
         * 
         * @return 201 - Created
         * @throws IOException 
         * @throws SAXException 
         * @throws ParserConfigurationException 
         * 
         */
        @RequestMapping(
                value = "ni/user/create",
                method = RequestMethod.POST             
        )
        public @ResponseBody NewInnovationsPersonCreate createUserDetails(@RequestBody String body) throws SAXException, IOException, ParserConfigurationException{
                System.out.println("\n\n>>>>>>>>>>>>>>>>>Inside of OIMRESTServices.createUserDetails");
                String dob=null,fname=null,lname=null,ssn=null, role=null, email=null,rmsId=null;
                User user = new User(null);
                
                Element element = getXMLElement(body);
                dob=element.getElementsByTagName("dob").item(0).getTextContent();
                                                fname=element.getElementsByTagName("firstName").item(0).getTextContent();
                                                lname=element.getElementsByTagName("lastName").item(0).getTextContent();
                                                ssn=element.getElementsByTagName("ssn").item(0).getTextContent();
                                                role=element.getElementsByTagName("role").item(0).getTextContent();
                                                email=element.getElementsByTagName("email").item(0).getTextContent();
                                                rmsId=element.getElementsByTagName("rmsId").item(0).getTextContent();
                                                System.out.println("\n\nElements from XML parse: dob="+dob+" fname="+fname+" lname="+lname+" ssn="+ssn);
                
                user.setLastName(lname);
                                user.setFirstName(fname);
                                user.setUserType("End-User");
                                user.setOrganizationKey("4");
                                user.setEmployeeType("New Innovation");
                                user.setLogin(null);
                                user.setAttribute("DOB", convertDate(dob));
                                user.setAttribute("SSN", encryptSSN(ssn));
                                user.setStartDate(new Date());
                                //user.setEmail(email);
                                user.setAttribute("AlternateEmail",email);
                                
                                //Set Role
                                if(!role.isEmpty() && role != null){
                                                if(role.equalsIgnoreCase("HSApplicant")){
                                                                user.setAttribute("ApplicantFlag", "Y");
                                                }
                                                else if(role.equalsIgnoreCase("PendingInRotator")){
                                                                user.setAttribute("InrotatorFlag", "Y");
                                                }
                                                else                                                        
                                                                System.out.println("\n>>>>>Failed to set either flag\n");                                                            
                                }
                
                                String response = null;
                                try {
                                                response = userServices.createUser(user, "NYULMC");
                                } catch (UserException e) {
                                                e.printStackTrace();
                                }

                                try {
                                                                user = userServices.getUserByIdentifier("usr_key", response,
                                                                        new HashSet(objTransformer.getAllAttributesMap().values()));
                                                } catch (UserException e) {
                                                                e.printStackTrace();
                                                }              
            return convertUserToNewInnovationsCreate(user,rmsId);
        }
        
        
        
    /******************************************************************
     * --------------New Innovations Utility Functions---------------
     *                                                           Dan Donnelly Revisited: 12/23/2015     
     * 
     *        1.            Convert Date to and from a String function
     *        2.            Encrypt/Decrypt SSN functions for handling incoming
     *                        or outgoing NNS string
     *        3.            Convert User to NewInnovationsPerson for sending back to 
     *                        NewInnovations
     *        4.            Getter for the person element in New Innovations, only for 
     *                        one currently, can return a list if functionality needs to 
     *                        be expanded
     */
        
        private Date convertDate(String date){
                SimpleDateFormat dayform = new SimpleDateFormat("MM/dd/yyyy");
                Date retDate = null;
                try {
                                                 retDate=dayform.parse(date);
                                } catch (ParseException e1) {
                                                System.out.println("\n\n Parse Exception :(");
                                                e1.printStackTrace();
                                }
                return retDate;
        }
        
        private String convertDateToString(Date dob){
                String retDate = null;
                SimpleDateFormat dayform = new SimpleDateFormat("MM/dd/yyyy");
                
                retDate = dayform.format(dob);
                
                return retDate;
        }
        
        private String encryptSSN(String ssn){
                String SSN = null;
                try {
                SSN=tcCryptoUtil.encrypt(ssn,"DBSecretKey");
                } catch (tcCryptoException e) {
                System.out.println("\n\nCryptoception");
                e.printStackTrace();
                }
                                System.out.println("\n>>>>>Encrypted SSN: "+SSN+"\n");
                return SSN;
        }
        
        private String decryptSSN(String ssn){
                String SSN = null;
                try {
                                                SSN=tcCryptoUtil.decrypt(ssn, "DBSecretKey");
                                } catch (tcCryptoException e) {
                                                System.out.println("\n\nDecryptoception");
                                                e.printStackTrace();
                                                return "";
                                }
                return SSN;
        }
        
        private NewInnovationsPerson convertUserToNewInnovations(User u,String rmsId){
                String ssn=null, dob=null, role=null, StrrmsId=null;
                System.out.println("------------->>>"+rmsId);
                StrrmsId=rmsId;
                if(u.getAttribute("SSN")!=null) ssn=decryptSSN((String)u.getAttribute("SSN"));
                
                if(u.getAttribute("DOB")!=null) dob=convertDateToString((Date)u.getAttribute("DOB"));
                
                if(u.getAttribute("InrotatorFlag")!=null && u.getAttribute("InrotatorFlag").equals("Y")){
                                role="PendingInRotator";
                }
                else if(u.getAttribute("ApplicantFlag")!=null && u.getAttribute("ApplicantFlag").equals("Y")){
                                role="HSApplicant";
                }
                
                return new NewInnovationsPerson(u.getLogin(),u.getFirstName(),u.getLastName(),ssn,dob,u.getEmail(),role,StrrmsId);
        }
        
        private NewInnovationsPersonCreate convertUserToNewInnovationsCreate(User u,String rmsId){
            String ssn=null, dob=null, role=null, StrrmsId=null;
            System.out.println("------convertUserToNewInnovationsCreate------->>>"+rmsId);
            StrrmsId=rmsId;
            if(u.getAttribute("SSN")!=null) ssn=decryptSSN((String)u.getAttribute("SSN"));
            
            if(u.getAttribute("DOB")!=null) dob=convertDateToString((Date)u.getAttribute("DOB"));
            
            if(u.getAttribute("InrotatorFlag")!=null && u.getAttribute("InrotatorFlag").equals("Y")){
                            role="PendingInRotator";
            }
            else if(u.getAttribute("ApplicantFlag")!=null && u.getAttribute("ApplicantFlag").equals("Y")){
                            role="HSApplicant";
            }
            
            return new NewInnovationsPersonCreate(u.getLogin(),u.getFirstName(),u.getLastName(),ssn,dob,u.getEmail(),role,StrrmsId);
    }
        
        private Element getXMLElement(String body) throws ParserConfigurationException, SAXException, IOException{
                Element element = null;
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(new InputSource(new ByteArrayInputStream(body.getBytes("utf-8"))));
                doc.getDocumentElement().normalize();
                
                NodeList nList = doc.getElementsByTagName("person");
                
                if(nList.getLength()==1){
                                Node node = nList.item(0);
                                
                                if (node.getNodeType() == Node.ELEMENT_NODE){
                                                element = (Element)node;
                                }
                }
                                
                return element;
        }
        

    ///////////////////////////////END Dan-Sprint 8

    
    private List getUserChallenges(String userName) throws UserException {
        return userServices.getUserChallenges(userName);
    }

    private String updateUser(UserAccount account, String identifier, String keyField) throws UserException {
        User   inputUser  = objTransformer.convertUserAccount(account);
        String returnCode = userServices.modifyUser(inputUser, keyField, identifier);

        return returnCode;
    }

    /**
     * Default response handler for UserNotFOundExceptions,
     * returns a 404 response.
     *
     *
     * @return
     */
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = { UserNotFoundException.class })
    protected @ResponseBody
    ServiceError handleNotFoundException() {
        return new ServiceError("Not Found", "");
    }

    /**
     * Method description
     *
     *
     * @param e
     *
     * @return
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = { UserException.class })
    protected @ResponseBody
    ServiceError handleUserException(Exception e) {
        return new ServiceError(e.getMessage(), "");
    }

    /**
     * Method description
     *
     *
     * @param e
     *
     * @return
     */
    @ExceptionHandler(value = { NoSuchServiceException.class })
    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    protected @ResponseBody
    ServiceError handleNoServiceException(Exception e) {
        ServiceError re = new ServiceError(e.getMessage(), "");

        return re;
    }
}
