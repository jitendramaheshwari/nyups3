package com.aptecllc.oim.property;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Hashtable;

import oracle.iam.platform.OIMClient;

import org.springframework.beans.factory.InitializingBean;

public class ServiceProperty implements Serializable, InitializingBean {

	private static final long serialVersionUID = -6666764934472927348L;

	private String addIdKey;
	private String modifyIdKey;
	private String retrieveIdKey;
	private String authIdKey;
	private String addIdRqTypeKeys;
	private String modifyIdRqTypeKeys;
	private String retIdRqTypeKeys;
	private String authIdAuthKeys;
	private Boolean aclVal;
	private Boolean apiKeySecurity;
	private String oimCf;
	private String oimProviderUrl;
	private String oimAdmUsr;
	private String oimAdmPwd;
	private Hashtable oimEnvMap;
	
	public void afterPropertiesSet() throws Exception {
		
		oimEnvMap = new Hashtable();
		oimEnvMap.put(OIMClient.JAVA_NAMING_PROVIDER_URL,oimProviderUrl);
	}

	public Hashtable getOimEnvMap() {
		return oimEnvMap;
	}

	public Boolean getAclVal() {
		return aclVal;
	}

	public void setAclVal(Boolean aclVal) {
		this.aclVal = aclVal;
	}

	public String getAddIdKey() {
		return addIdKey;
	}

	public void setAddIdKey(String addIdKey) {
		this.addIdKey = addIdKey;
	}

	public String getModifyIdKey() {
		return modifyIdKey;
	}

	public void setModifyIdKey(String modifyIdKey) {
		this.modifyIdKey = modifyIdKey;
	}

	public String getRetrieveIdKey() {
		return retrieveIdKey;
	}

	public void setRetrieveIdKey(String retrieveIdKey) {
		this.retrieveIdKey = retrieveIdKey;
	}

	public String getAddIdRqTypeKeys() {
		return addIdRqTypeKeys;
	}

	public void setAddIdRqTypeKeys(String addIdRqTypeKeys) {
		this.addIdRqTypeKeys = addIdRqTypeKeys;
	}

	public String getModifyIdRqTypeKeys() {
		return modifyIdRqTypeKeys;
	}

	public void setModifyIdRqTypeKeys(String modifyIdRqTypeKeys) {
		this.modifyIdRqTypeKeys = modifyIdRqTypeKeys;
	}

	public String getRetIdRqTypeKeys() {
		return retIdRqTypeKeys;
	}

	public void setRetIdRqTypeKeys(String retIdRqTypeKeys) {
		this.retIdRqTypeKeys = retIdRqTypeKeys;
	}

	public Boolean getApiKeySecurity() {
		return apiKeySecurity;
	}

	public void setApiKeySecurity(Boolean apiKeySecurity) {
		this.apiKeySecurity = apiKeySecurity;
	}

	public String getOimAdmPwd() {
		return oimAdmPwd;
	}

	public void setOimAdmPwd(String oimAdmPwd) {
		
		this.oimAdmPwd = oimAdmPwd;
	}

	public String getOimAdmUsr() {
		return oimAdmUsr;
	}

	public void setOimAdmUsr(String oimAdmUsr) {
		this.oimAdmUsr = oimAdmUsr;
	}

	public String getOimCf() {
		return oimCf;
	}

	public void setOimCf(String oimCf) {
		this.oimCf = oimCf;
	}

	public String getOimProviderUrl() {
		return oimProviderUrl;
	}

	public void setOimProviderUrl(String oimProviderUrl) {
		this.oimProviderUrl = oimProviderUrl;
	}

	public String getAuthIdAuthKeys() {
		return authIdAuthKeys;
	}

	public void setAuthIdAuthKeys(String authIdAuthKeys) {
		this.authIdAuthKeys = authIdAuthKeys;
	}

	public String getAuthIdKey() {
		return authIdKey;
	}

	public void setAuthIdKey(String authIdKey) {
		this.authIdKey = authIdKey;
	}
}
