package com.aptecllc.oim.util.v2;

import oracle.iam.identity.orgmgmt.api.OrganizationManager;
import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.notification.api.NotificationService;
import oracle.iam.notification.template.TemplateService;
import oracle.iam.passwordmgmt.api.PasswordMgmtService;
import oracle.iam.platform.OIMClient;
import oracle.iam.request.api.RequestService;
import oracle.iam.selfservice.self.selfmgmt.api.AuthenticatedSelfService;
import oracle.iam.selfservice.uself.uselfmgmt.api.UnauthenticatedSelfService;
import oracle.iam.reconciliation.api.EventMgmtService;
import oracle.iam.reconciliation.api.ReconOperationsService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import Thor.API.Operations.tcLookupOperationsIntf;
import Thor.API.Operations.tcUserOperationsIntf;




import com.aptecllc.oim.property.ServiceProperty;

public class OimClient {
	
	private ServiceProperty idlServiceProperty;
	private OIMClient identityClient;
	

	private static final Logger IdentityProfileServiceLogger = LoggerFactory.getLogger(OimClient.class);
	
	public OimClient() {
		
	}
	
	public OimClient(ServiceProperty pSP) {
		idlServiceProperty = pSP;
		identityClient = new OIMClient(pSP.getOimEnvMap());
		
	}
	
	public PasswordMgmtService getPwdMgr() {

		return (PasswordMgmtService) identityClient
				.getService(PasswordMgmtService.class);
	}

	public UnauthenticatedSelfService getUnAuthSelfSvc() {

		return (UnauthenticatedSelfService) identityClient
				.getService(UnauthenticatedSelfService.class);
	}

	public AuthenticatedSelfService getAuthSelfSvc() {

		return (AuthenticatedSelfService) identityClient
				.getService(AuthenticatedSelfService.class);
	}
	public UserManager getUsrMgr() {

		return (UserManager) identityClient.getService(UserManager.class);
	}

	public tcUserOperationsIntf getTcUsrOpInt() {

		return (tcUserOperationsIntf) identityClient
				.getService(tcUserOperationsIntf.class);
	}

	public OrganizationManager getOrgManager() {
		return identityClient.getService(OrganizationManager.class);
	}

	public RequestService getRequestService() {
		return identityClient.getService(RequestService.class);
	}

	public tcLookupOperationsIntf getTcLookupOpInt() {
		// TODO Auto-generated method stub
		return identityClient.getService(tcLookupOperationsIntf.class);
	}
	

	public ReconOperationsService getReconSvc() {
		return identityClient.getService(ReconOperationsService.class);
	}

	public EventMgmtService getEventMgmtSvc() {
		return identityClient.getService(EventMgmtService.class);
	}

	public NotificationService getNotificationMgr() {
		// TODO Auto-generated method stub
		return identityClient.getService(NotificationService.class);
	}
	
	public NotificationService getTemplateMgr() {
		// TODO Auto-generated method stub
		return identityClient.getService(NotificationService.class);
	}
}
