package com.aptecllc.oim.converter.impl;

import com.aptecllc.oim.converter.DataConverterIntf;

public class RemoveHyphensConverter implements DataConverterIntf {

	@Override
	public Object convertInbound(Object inbound) {
		// TODO Auto-generated method stub
		return ((String)inbound).replaceAll("\\-", "");
	}

	@Override
	public Object convertOutbound(Object outbound) {
		return "+1" + (String)outbound;
	}

}
