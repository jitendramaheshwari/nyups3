package com.aptecllc.oim.exception;

public class UserException extends Exception {

	public UserException(String message, Throwable cause) {
		super(message, cause);
		
	}

	public UserException(String message) {
		super(message);
		
	}

	public UserException(Throwable cause) {
		super(cause);
		
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserException() {
		super();
	}
	

}
