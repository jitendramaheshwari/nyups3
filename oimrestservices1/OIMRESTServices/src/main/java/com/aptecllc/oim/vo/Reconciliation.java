package com.aptecllc.oim.vo;

import org.codehaus.jackson.annotate.JsonProperty;

public class Reconciliation {

	@JsonProperty("Recon-Key")
	private String reconKey;
	@JsonProperty("Recon-Action")
	private String reconAction;
	@JsonProperty("User-Key")
	private String userKey;

	/**
	 * @return the reconKey
	 */
	public String getReconKey() {
		return reconKey;
	}

	/**
	 * @param reconKey
	 *            the reconKey to set
	 */
	public void setReconKey(String reconKey) {
		this.reconKey = reconKey;
	}

	/**
	 * @return the reconAction
	 */
	public String getReconAction() {
		return reconAction;
	}

	/**
	 * @param reconAction
	 *            the reconAction to set
	 */
	public void setReconAction(String reconAction) {
		this.reconAction = reconAction;
	}

	/**
	 * @return the userKey
	 */
	public String getUserKey() {
		return userKey;
	}

	/**
	 * @param userKey
	 *            the userKey to set
	 */
	public void setUserKey(String userKey) {
		this.userKey = userKey;
	}

}