package com.aptecllc.oim.matching.vo;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;



public class ReconTargetAttribute implements Serializable {
	private static final long serialVersionUID = 6711525717446212484L;
	
	  private Double reKey;
	  private String tadName;
	  private String tadDatatype;
	  private String stringVal;
	  private Double numericVal;
	  private Timestamp dateVal;
	  private String allData;
	  private String faliureReason;
	  private String oimMappedFieldDescription;
	  private boolean encrypted;
	  private String EmDataType;
	
	public Double getReKey() {
		return reKey;
	}
	public void setReKey(Double reKey) {
		this.reKey = reKey;
	}
	public String getTadName() {
		return tadName;
	}
	public void setTadName(String tadName) {
		this.tadName = tadName;
	}
	public String getTadDatatype() {
		return tadDatatype;
	}
	public void setTadDatatype(String tadDatatype) {
		this.tadDatatype = tadDatatype;
	}
	public String getStringVal() {
		return stringVal;
	}
	public void setStringVal(String stringVal) {
		this.stringVal = stringVal;
	}
	public Double getNumericVal() {
		return numericVal;
	}
	public void setNumericVal(Double numericVal) {
		this.numericVal = numericVal;
	}
	public Timestamp getDateVal() {
		return dateVal;
	}
	public void setDateVal(Timestamp dateVal) {
		this.dateVal = dateVal;
	}
	public String getAllData() {
		return allData;
	}
	public void setAllData(String allData) {
		this.allData = allData;
	}
	public String getFaliureReason() {
		return faliureReason;
	}
	public void setFaliureReason(String faliureReason) {
		this.faliureReason = faliureReason;
	}
	public String getOimMappedFieldDescription() {
		return oimMappedFieldDescription;
	}
	public void setOimMappedFieldDescription(String oimMappedFieldDescription) {
		this.oimMappedFieldDescription = oimMappedFieldDescription;
	}
	public boolean isEncrypted() {
		return encrypted;
	}
	public void setEncrypted(boolean encrypted) {
		this.encrypted = encrypted;
	}
	public String getEmDataType() {
		return EmDataType;
	}
	public void setEmDataType(String emDataType) {
		EmDataType = emDataType;
	}

}
