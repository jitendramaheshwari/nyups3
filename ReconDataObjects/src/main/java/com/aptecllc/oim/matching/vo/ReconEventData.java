package com.aptecllc.oim.matching.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ReconEventData implements Serializable {
	private static final long serialVersionUID = -985564013437258168L;
	private List<ReconTargetAttribute> singleValuedAttrs = null;

	public ReconEventData() {
		singleValuedAttrs = new ArrayList<ReconTargetAttribute>();
	}
	public List<ReconTargetAttribute> getSingleValuedAttrs() {
		return singleValuedAttrs;
	}

	public void setSingleValuedAttrs(List<ReconTargetAttribute> singleValuedAttrs) {
		this.singleValuedAttrs = singleValuedAttrs;
	}

	
}
