package org.nyumc.iam.policy;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import oracle.iam.platform.Platform;
import Thor.API.Exceptions.tcAPIException;
import Thor.API.Operations.tcLookupOperationsIntf;

public class DBUtil {

	private String jndiDataSource = null;
	private Connection connection;
	private String tableName = null;
	private String userName = null;
	private String passWord = null;
	private String dbUrl = null;
	private String jdbcDriver = "oracle.jdbc.driver.OracleDriver";
	public static String MGR_PW = "";
	public static String MGR_PW1 = "";
	//public static Logger logger = Logger.getLogger(DBUtil.class);

	/**
	 * Connect to the specified DB by reading the parameters from properties
	 * file
	 */
	private void connect(String dbUrl, String userName, String passWord)
			throws ClassNotFoundException, InstantiationException,
			IllegalAccessException, SQLException, NamingException {
		System.out.println("Inside connect");
		Class.forName(jdbcDriver).newInstance();
		//System.out.println("dbUrl:" + dbUrl + "userName:" + userName
				//+ "password:" + passWord);
		this.connection = DriverManager
				.getConnection(dbUrl, userName, passWord);
		// }
		System.out.println("Exiting connect");
	}
	
	private String getLookupValue(String strEncode, String strLookupName) {
		//System.out.println("encode value is:"+strEncode+strLookupName);

		tcLookupOperationsIntf lookupOpsIntf = Platform
				.getService(tcLookupOperationsIntf.class);
		String lookupValue = "Decode value not found";
		try {
			lookupValue = lookupOpsIntf.getDecodedValueForEncodedValue(
					strLookupName, strEncode);
		} catch (tcAPIException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return lookupValue;
		}
		//System.out.println("decode value is:"+lookupValue);
		return lookupValue;

	}

	private boolean isConnected() throws SQLException {
		if ((this.connection == null) || (this.connection.isClosed())) {
			return (false);
		}
		return (true);
	}

	public String getDatafromWSQSP(String strSSN, String strDOB) {
		//System.out.println("strDOB length is"+strDOB.length());
		//System.out.println("strDOB length after trimming is"+strDOB.trim().length());
		//System.out.println("strDOB value is*****"+strDOB+"#####");
		

		Statement stmt = null;
		CallableStatement cStmt1 = null;
		CallableStatement cStmt = null;
		ResultSet rs = null;
		CredentialUtil credutil = new CredentialUtil();
		String strKerberosID = "";
		String outputValue ="";
		ResultSet wsqrs =null;
		// Map recordMap;

		Map<String, String> recordMap = new HashMap<String, String>();
		try {
			if (!isConnected()) {
				
					System.out.println("Inside WSQ SP");
					//MGR_PW = credutil.getCredentials("wsqregistry");
					//MGR_PW1="schema4oim4som";
					//System.out.println("MGR_PW for WSQ is:" + MGR_PW);
					String jdbcURL = getLookupValue("jdbcURL","NYU.Lookup.WSQ.JdbcURL");
					String user = getLookupValue("user","NYU.Lookup.WSQ.JdbcURL");
					String pw = getLookupValue("pw","NYU.Lookup.WSQ.JdbcURL");
					connect(jdbcURL,user, pw);
					System.out.println("Connection is:"+connection);
			}
			
			//CallableStatement cStmt = connection.prepareCall("{call idm.p_pims_check(?, ?)}");
			System.out.println("before creating connection");
			cStmt1 = connection.prepareCall("{call IDM.PIMS_CHECK_MGR}");
			System.out.println("cStmt1"+cStmt1);
			
			cStmt1.executeQuery();
			cStmt = connection.prepareCall("{call idm.p_pims_check (:1, :2, :3, :4, :5)}");
			System.out.println("cStmt"+cStmt);
            cStmt.registerOutParameter(3, java.sql.Types.VARCHAR);
            cStmt.setString(1, strSSN);
            cStmt.setString(2, strDOB.trim());
            cStmt.registerOutParameter(4, java.sql.Types.VARCHAR);
            cStmt.registerOutParameter(5, java.sql.Types.VARCHAR);
            if(cStmt!=null){
		    boolean hadResults = cStmt.execute();
		    System.out.println("hadResults"+hadResults);
		   
				    while (hadResults) {
				        wsqrs = cStmt.getResultSet();
				        hadResults = cStmt.getMoreResults();
				    }
				    outputValue = cStmt.getString(3); // index-based
				    System.out.println("outPutValue from idm.p_pims_check is 1:"+cStmt.getString(3));
				    System.out.println("outPutValue from idm.p_pims_check is 2:"+cStmt.getString(4));
				    System.out.println("outPutValue from idm.p_pims_check is 3:"+cStmt.getString(5));
				    
            }
            System.out.println("outPutValue from idm.p_pims_check is:"+outputValue);
		    //outputValue = cStmt.getString("P_OUTNETID"); // index-based
		    

			

		} catch (Exception e) {
			e.printStackTrace();
			//logger.error("Error executing query: " + e);
			//System.out.println(e.printStackTrace(););
			e.printStackTrace();
			// return "FAILURE";
		} finally {
			try {
				if (cStmt != null) {
					cStmt.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				//logger.warn("Unable to close Statement: " + e);
			}
			
			try {
				if (cStmt1 != null) {
					cStmt1.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				//logger.warn("Unable to close Statement: " + e);
			}

			if (wsqrs != null) {
				try {
					wsqrs.close();
				} catch (SQLException e) {
					e.printStackTrace();
					//logger.warn("Unable to close ResultSet: " + e);
				}
			}

			if (this.connection != null) {
				try {
					this.connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
					//logger.warn("Unable to close Connection: " + e);
				}
			}

		}
		System.out.println("Successfully retrieved NETid value from the WSQ View "+outputValue);
		return outputValue;
	}

}
