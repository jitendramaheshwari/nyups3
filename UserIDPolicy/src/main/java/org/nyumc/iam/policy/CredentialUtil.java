package org.nyumc.iam.policy;



import java.net.InetAddress;
import java.util.Hashtable;

import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.platform.OIMClient;
import oracle.security.jps.service.credstore.GenericCredential;
import oracle.security.jps.service.credstore.PasswordCredential;
//import oracle.security.jps.service.credstore.GenericCredential;
//import oracle.security.jps.service.credstore.PasswordCredential;
import Thor.API.Operations.tcITResourceInstanceOperationsIntf;

import com.thortech.util.logging.Logger;

public class CredentialUtil {
	
	private Boolean devMode;
	private String devModePassword;
	private String devModeUrl;

	// private UserManager usrMgr;
	// private tcITResourceInstanceOperationsIntf
	// tcITResourceInstanceOperationsIntf;
	private static final Logger logger = Logger.getLogger(CredentialUtil.class
			.getName());

	public void setDevMode(Boolean devMode) {
		this.devMode = devMode;
	}

	public void setDevModePassword(String devModePassword) {
		this.devModePassword = devModePassword;
	}

	public void setDevModeUrl(String devModeUrl) {
		this.devModeUrl = devModeUrl;
	}

	/*public String getCredentials(String strKey) {
		char[] password = null;
		String hostName = "Unknown";
		String strCrdenetials = "";
		String passwordCSF = "";
		try {

			logger.debug("Inside getCredentials()");
			System.out.println("inside getCredentials");

			String providerURL = null;

			password = CredentialUtil.getPasswordforCredential(
					"oim", strKey);
			passwordCSF = new String(password);
			//System.out.println("inside getCredentials password for :" + strKey+ ": is" + passwordCSF.toString());

		} catch (Exception ex) {
			logger.error("OIMClient Error", ex);
		}
		return passwordCSF.toString();
	}*/

	/*public static char[] getPasswordforCredential(final String mapName,
			final String csfKey) {
		System.out.println("Inside getpasswordforCredential method");
		PasswordCredential credential = (PasswordCredential) getCredentialsFromCSF(
				mapName, csfKey);
		System.out.println("Inside getpasswordforCredential method"
				+ credential.toString());
		if (credential == null) {
			return ("").toCharArray();
		}

		return credential.getPassword();
	}*/

	/*public static String getCSFStringProperty(final String mapName,
			final String csfKey) {
		GenericCredential credential = (GenericCredential) getCredentialsFromCSF(
				mapName, csfKey);
		Object obj = credential.getCredential();
		if (obj == null)
			return null;

		String val = null;
		if (obj instanceof String) {
			val = (String) obj;
			if (val.trim().length() == 0)
				return null;
		}
		return val;
	}*/

	/*public static Object getCredentialsFromCSF(final String mapName,
			final String csfKey) {
		Object passwordCredential = null;

		try {
			if (null != csfKey) {
				final oracle.security.jps.service.credstore.CredentialStore credStore = getCredStore();
				if (null != credStore) {
					passwordCredential = (Object) java.security.AccessController
							.doPrivileged(new java.security.PrivilegedExceptionAction<oracle.security.jps.service.credstore.Credential>() {
								public oracle.security.jps.service.credstore.Credential run()
										throws Exception {
									return (credStore.getCredential(mapName,
											csfKey));
								}
							});

				} else {
					logger.error("Exception occured while obtaining the password credentials. Credentials are null");
				}
			}

		} catch (final java.security.PrivilegedActionException ex) {
			logger.error(ex.getMessage(), ex);
		} catch (final oracle.security.jps.JpsException jpse) {
			logger.error(jpse.getMessage(), jpse);
		} catch (final java.security.AccessControlException ex) {
			logger.error(ex.getMessage(), ex);
		}

		return passwordCredential;
	}*/

	/*private static oracle.security.jps.service.credstore.CredentialStore getCredStore()
			throws oracle.security.jps.JpsException {
		oracle.security.jps.service.credstore.CredentialStore csfStore;
		oracle.security.jps.service.credstore.CredentialStore appCsfStore = null;
		oracle.security.jps.service.credstore.CredentialStore systemCsfStore = null;

		final oracle.security.jps.internal.api.runtime.ServerContextFactory factory = (oracle.security.jps.internal.api.runtime.ServerContextFactory) oracle.security.jps.JpsContextFactory
				.getContextFactory();

		final oracle.security.jps.JpsContext jpsCtxSystemDefault = factory
				.getContext(oracle.security.jps.internal.api.runtime.ServerContextFactory.Scope.SYSTEM);

		final oracle.security.jps.JpsContext jpsCtxAppDefault = factory
				.getContext(oracle.security.jps.internal.api.runtime.ServerContextFactory.Scope.APPLICATION);

		appCsfStore = (jpsCtxAppDefault != null) ? jpsCtxAppDefault
				.getServiceInstance(oracle.security.jps.service.credstore.CredentialStore.class)
				: null;

		if (appCsfStore == null) {
			systemCsfStore = jpsCtxSystemDefault
					.getServiceInstance(oracle.security.jps.service.credstore.CredentialStore.class);
			csfStore = systemCsfStore;
		} else {
			// use Credential Store defined in app-level jps-config.xml
			csfStore = appCsfStore;
		}

		return csfStore;
	}*/
	
}
