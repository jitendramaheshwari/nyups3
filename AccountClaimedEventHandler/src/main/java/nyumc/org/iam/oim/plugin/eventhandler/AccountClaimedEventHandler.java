package nyumc.org.iam.oim.plugin.eventhandler;


import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import oracle.iam.identity.exception.AccessDeniedException;
import oracle.iam.identity.exception.NoSuchUserException;
import oracle.iam.identity.exception.SearchKeyNotUniqueException;
import oracle.iam.identity.exception.UserLookupException;
import oracle.iam.identity.exception.UserModifyException;
import oracle.iam.identity.exception.UserSearchException;
import oracle.iam.identity.exception.ValidationFailedException;
import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.api.UserManagerConstants;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.identity.vo.Identity;
import oracle.iam.notification.api.NotificationService;
import oracle.iam.notification.exception.EventException;
import oracle.iam.notification.exception.MultipleTemplateException;
import oracle.iam.notification.exception.NotificationException;
import oracle.iam.notification.exception.NotificationResolverNotFoundException;
import oracle.iam.notification.exception.TemplateNotFoundException;
import oracle.iam.notification.exception.UnresolvedNotificationDataException;
import oracle.iam.notification.exception.UserDetailsNotFoundException;
import oracle.iam.notification.vo.NotificationEvent;
import oracle.iam.platform.Platform;
import oracle.iam.platform.entitymgr.EntityManager;
import oracle.iam.platform.entitymgr.InvalidDataFormatException;
import oracle.iam.platform.entitymgr.InvalidDataTypeException;
import oracle.iam.platform.entitymgr.NoSuchEntityException;
import oracle.iam.platform.entitymgr.ProviderException;
import oracle.iam.platform.entitymgr.StaleEntityException;
import oracle.iam.platform.entitymgr.UnknownAttributeException;
import oracle.iam.platform.entitymgr.UnsupportedOperationException;
import oracle.iam.platform.entitymgr.vo.SearchCriteria;
import oracle.iam.platform.kernel.EventFailedException;
import oracle.iam.platform.kernel.vo.AbstractGenericOrchestration;
import oracle.iam.platform.kernel.vo.EventResult;
import Thor.API.Exceptions.tcAPIException;
import Thor.API.Operations.tcLookupOperationsIntf;
import Thor.API.Operations.tcUserOperationsIntf;

import com.aptecllc.eventhandler.BaseUserPostProcessHandler;
import com.aptecllc.iam.oim.utils.notificationprovider.APTECEMAILNotificationProvider;
import com.aptecllc.iam.oim.utils.notificationprovider.APTECEMAILPayload;

/**
* event handler to handle the changes to display name
* <p>
* Creates a display name if any of the other names change
*
 * <table border="1">
* <tr>
* <th>Name</th>
* <th>USR Table Reference</th>
* <th>Label</th>
* </tr>
* <tr>
* <td>Employee Job Code</td>
* <td>USR_UDF_EMPJOBCODE</td>
* <td>EmpJobCode</td>
* </tr>
* <tr>
* <td>First Name</td>
* <td>USR_FIRST_NAME</td>
* <td>First Name</td>
* </tr>
* <tr>
* <td>Middle Name</td>
* <td>USR_MIDDLE_NAME</td>
* <td>Middle Name</td>
* </tr>
* <tr>
* <td>Last Name</td>
* <td>USR_LAST_NAME</td>
* <td>Last Name</td>
* </tr>
* <tr>
* <td>Name Suffix</td>
* <td>USR_UDF_NAMESUFFIX</td>
* <td>Name Suffix</td>
* </tr>
* <tr>
* <td>Preferred First Name</td>
* <td>USR_UDF_PREFFIRSTNAME</td>
* <td>PrefFName</td>
* </tr>
* <tr>
* <td>Preferred Middle Name</td>
* <td>USR_UDF_PREFMIDDLENAME</td>
* <td>PrefMName</td>
* </tr>
* <tr>
* <td>Preferred Last Name</td>
* <td>USR_UDF_PREFLASTNAME</td>
* <td>PrefLName</td>
* </tr>
* </table>
*
 * <dl>
* <dt>Revision History</dt>
* <dd>
* <ol>
* <li>Rev 1.0 2013-09-13 base functionality</li>
* </ol>
* </dd>
* </dl>
*
 * @author Keith Smith
*
 */
public class AccountClaimedEventHandler extends BaseUserPostProcessHandler {
                private static final Logger logger = Logger
                                                .getLogger(AccountClaimedEventHandler.class.getName());

                private static final String USR_KEY = UserManagerConstants.AttributeName.USER_KEY
                                                .getId();
                private static final String EMP_JOB_CODE = "EmpJobCode";
                private static final String FIRST_NAME = UserManagerConstants.AttributeName.FIRSTNAME
                                                .getId();
                private static final String MIDDLE_NAME = UserManagerConstants.AttributeName.MIDDLENAME
                                                .getId();
                private static final String LAST_NAME = UserManagerConstants.AttributeName.LASTNAME
                                                .getId();
                private static final String NAME_SUFFIX = "NameSuffix";
                private static final String PREF_FIRST_NAME = "PrefFirstName";
                private static final String PREF_MIDDLE_NAME = "PrefMiddleName";
                private static final String PREF_LAST_NAME = "PrefLastName";
                private static final String DISPLAY_NAME = UserManagerConstants.AttributeName.DISPLAYNAME
                                                .getId();

                // private static final String SPONSOR_EMAIL =
                // UserManagerConstants.AttributeName.

                protected EventResult ProcessUserEvent(AbstractGenericOrchestration orch,
                                                String entityId, HashMap interEventData,
                                                HashMap<String, Serializable> orchParams,
                                                Identity currentUserState, Identity newUserState)
                                                throws EventFailedException {
                                String methodName = "ProcessUserEvent";
                                String straccountClaimed = (String) orchParams.get("TermsAccepted");
                                String departmentNumber = (String) orchParams.get("Department Number");
                                String jobCode = (String) orchParams.get("JobCode");
                                String businessUnit = (String) orchParams.get("BusinessUnit");
                                String Status = (String) orchParams.get("Status");
                                String setID ="";
                                String strjobTitle ="";
                                if (businessUnit != null && !businessUnit.isEmpty()) {
                                setID = businessUnit.substring(0, businessUnit.length()-2)+"ID";
                                strjobTitle           = setID + "+"+jobCode;
                                System.out.println("strjobTitle is"+strjobTitle);
                                System.out.println("jobCode"+jobCode);
                                System.out.println("businessUnit is"+businessUnit);
                                }

                                String strFIRST_NAME = "";
                                String strLAST_NAME = "";
                                String sponsoremail = "";
                                String strUSER_EMAIL="";
                                String strUSER_LOGIN = "";
                                String strAltEmail="";
                                logger.entering(getClass().getName(), methodName);
                                System.out.println("Inside AccountClaimedEvent Handler"
                                                                + straccountClaimed);
                                System.out.println("Inside AccountClaimedEvent Handler "
                                                                + (String) orchParams.get("AlternateEmail"));
                                System.out.println("Inside AccountClaimedEvent Handler department "
                                                                + departmentNumber);

                                //

                                HashMap<String, Serializable> interParameters = orch
                                                                .getInterEventData(); // contains old and new values of user
                                Object currUserStateObject =  interParameters.get("CURRENT_USER"); // Get
                                Identity currentUserState1 = null;
                                
                                if(currUserStateObject instanceof Identity) {
                                	currentUserState1 = (Identity)currUserStateObject;
                                	// target
                                } else if(currUserStateObject instanceof User) {
                                	// target
                                	currentUserState1 = (User)currUserStateObject;
                                } else if(currUserStateObject instanceof Identity[]) {
                                	Identity[] currentUserStateArray = (Identity[])currUserStateObject;
                                	if(currentUserStateArray.length >0) 
                                		currentUserState1 = currentUserStateArray[0];
                                	else currentUserState1 = currentUserState;    
                                	// target
                                } else currentUserState1 = currentUserState;                                                                                                                                                                                                                                                                           // user's
                                                                                                                                                                                                                                                                                                                                 // current
                                                                                                                                                                                                                                                                                                                                // (old)
                                                                                                                                                                                                                                                                                                                                // info
                                                                                                                                                                                                                                                                                                                                // state
                                /* jobCode = currentUserState1.getAttribute("USR_UDF_JOBCODE").toString();
                                businessUnit = currentUserState1.getAttribute("BusinessUnit").toString();
                                System.out.println("jobCode and businessunit are"+jobCode+":"+businessUnit);*/
                                String userKey = orch.getTarget().getEntityId(); // Get the target


                                // user's key
                                if (currentUserState1 != null) {
                                                if (currentUserState1.getAttribute("AlternateEmail") != null) {
                                                                sponsoremail = currentUserState1.getAttribute("AlternateEmail")
                                                                                                .toString();
                                                                strAltEmail = currentUserState1.getAttribute("AlternateEmail").toString();
                                                }
                                                strFIRST_NAME = (String)currentUserState1.getAttribute(FIRST_NAME);
                                                strLAST_NAME = (String)currentUserState1.getAttribute(LAST_NAME);
                                                strUSER_LOGIN = (String)currentUserState1.getAttribute(UserManagerConstants.AttributeName.USER_LOGIN.getId());
                                                strUSER_EMAIL = (String)currentUserState1.getAttribute(UserManagerConstants.AttributeName.EMAIL.getId());

                                                //System.out.println("SPONSOREMAIL" + sponsoremail);
                                                System.out.println("STAAAATUSUPDATED:"+currentUserState1.getAttribute("Status".toString()));
                                }

                                //

                                EventResult rval = new EventResult();
                                HashMap<String, Object> attrs = new HashMap<String, Object>();
                                EntityManager em = Platform.getService(EntityManager.class);
                                String operation = orch.getOperation();
                                System.out
                                                                .println("Inside AccountClaimedEvent Handler and the operation is:"
                                                                                                + operation);
                                UserManager usrMgr = Platform.getService(UserManager.class);
                                EntityManager entityMgr = Platform.getService(EntityManager.class);

                                /*
                                * Map<String,Object>
                                * orchClone=(HashMap<String,Object>)orchParams.clone(); for
                                * (Entry<String, Object> entry: orchClone.entrySet()) {
                                * logger.logp(Level.FINE, getClass().getName(), methodName,
                                * " Key:"+entry.getKey()+", Value:"+entry.getValue()); }
                                */





                                System.out.println("OPERATION--->" + operation);
                                if (operation.equalsIgnoreCase("MODIFY")) {
                                                System.out.println("Inside Modify method" + straccountClaimed);

//added on 02/17/2016

                                                String employeeStatusCurrent = "";
                                                String employeeStatusNew = "";
                                                String employeeUserTypeCurrent = "";
                                                String employeeUserTypeNew="";
                                                String contractorSponsorCurrent = "";
                                                String contractorSponsorNew = "";
                                                String UserLastNameCurrent = "";
                                                String UserLastNameNew = "";
                                                String UserFirstNameCurrent = "";
                                                String UserFirstNameNew = "";
                                                String NBTriggerCurrent="";
                                                String NBTriggerNew="";
                                                String displayName="";
                                                Map displayNameMap = new HashMap();




                                                employeeStatusCurrent = (String) currentUserState.getAttribute("Status".toString());
                                                System.out.println("Inside Modify User Event handler CurrentemployeeStatus is: "+employeeStatusCurrent);
                                                employeeUserTypeCurrent = (String) currentUserState.getAttribute("Role".toString());
                                                System.out.println("Inside Modify User Event handler User Type is: "+employeeUserTypeCurrent);

                                                employeeStatusNew = (String) newUserState.getAttribute("Status".toString());
                                                System.out.println("Inside Modify User Event handler New employeeStatus is: "+employeeStatusNew);

                                                employeeUserTypeNew = (String) newUserState.getAttribute("Role".toString());
                                                System.out.println("Inside Modify User Event handler New User Type is: "+employeeUserTypeNew);

                                                //06/08/2016

                                                contractorSponsorCurrent = (String) currentUserState.getAttribute("SponsorID".toString());
                                                System.out.println("Inside Modify User Event handler contractorSponsorCurrent is: "+contractorSponsorCurrent);

                                                contractorSponsorNew = (String) newUserState.getAttribute("SponsorID".toString());
                                                System.out.println("Inside Modify User Event handler New contractorSponsorNew is: "+contractorSponsorNew);




                                                // added on 01/11/2018
                                                if(contractorSponsorCurrent!=null && contractorSponsorNew!=null)
                                                {
                                                if(contractorSponsorCurrent.toString().equalsIgnoreCase(contractorSponsorNew.toString()))
                                                {
                                                                System.out.println("No change in the sponsor");
                                                }

                                                else if (!contractorSponsorCurrent.toString().equalsIgnoreCase(contractorSponsorNew.toString())) {

                                                                //To do
                                                                try{
                                                                System.out.println("Before Calling setSponsorInfo");
                                                                setSponsorInfo(orch,entityId,orchParams,newUserState,usrMgr,entityMgr,contractorSponsorNew.toString());
                                                                }catch(Exception e)
                                                                {

                                                                                System.out.println("Exception caught"+e);
                                                                }
                                                }
                                                }
                                                // ended on 01/11/2018
                                                //currentUserState1.getFirstName();

                                                UserLastNameCurrent = (String) currentUserState.getAttribute("Last Name".toString());
                                                System.out.println("Inside Modify User Event handler UserLastNameCurrent is: "+UserLastNameCurrent);



                                                UserLastNameNew = (String) newUserState.getAttribute("Last Name".toString());
                                                System.out.println("Inside Modify User Event handler New UserLastNameNew is: "+UserLastNameNew);

                                                UserFirstNameCurrent = (String) currentUserState.getAttribute("First Name".toString());
                                                System.out.println("Inside Modify User Event handler UserFirstNameCurrent is: "+UserFirstNameCurrent);

                                                UserFirstNameNew = (String) newUserState.getAttribute("First Name".toString());
                                                System.out.println("Inside Modify User Event handler New UserFirstNameNew is: "+UserFirstNameNew);

                                                //started on Aug 21
                                                String emailtemplate="";
                                                String part1 ="";
                                                String part2="";
                                                if(currentUserState.getAttribute("SilkRoadIntegration")!=null){
                                                 NBTriggerCurrent = (String) currentUserState.getAttribute("SilkRoadIntegration".toString());
                                                System.out.println("Inside Modify User Event handler SilkRoadIntegrationCurrent is: "+NBTriggerCurrent);   }
                                                if(newUserState.getAttribute("SilkRoadIntegration")!=null){
                                                NBTriggerNew = (String) newUserState.getAttribute("SilkRoadIntegration".toString());
                                                System.out.println("Inside Modify User Event handler SilkRoadIntegrationNew is: "+NBTriggerNew);

                                                //if (NBTriggerNew!=null){

                                                //String[] parts = NBTriggerNew.split("+");
                                                String[] parts = NBTriggerNew.split("\\+");
                                                part1 = parts[0];
                                                part2 = parts[1];
                                                }
                                                System.out.println("emailtemplate in event handler is "+part2+"and the altemail is "+strAltEmail);
                                                emailtemplate = part2;
                                                if(NBTriggerNew!=null && NBTriggerCurrent!=null){
                                                if(!NBTriggerNew.equalsIgnoreCase(NBTriggerCurrent)){
                                                                try {
                                                                                sendAccountClaimingEmail(part2, strAltEmail, strFIRST_NAME, strLAST_NAME, strUSER_LOGIN);
                                                                                attrs.put("EmailSent","Y");
                                                                } catch (NoSuchUserException e) {
                                                                                // TODO Auto-generated catch block
                                                                                e.printStackTrace();
                                                                } catch (UserLookupException e) {
                                                                                // TODO Auto-generated catch block
                                                                                e.printStackTrace();
                                                                } catch (AccessDeniedException e) {
                                                                                // TODO Auto-generated catch block
                                                                                e.printStackTrace();
                                                                }
                                                }
                                                }

                                                // ended on Aug 21
                                                //sendAccountClaimingEmail

                                                if(UserLastNameCurrent.toString().equalsIgnoreCase(UserLastNameNew.toString()))
                                                {
                                                                System.out.println("No change in the Last Name");
                                                }


                                                else {

                                                                System.out.println("Change in last name detected");
                                                                attrs.put("Last Name",UserLastNameNew.toString());
                                                                String strDisplayFirstName="";
                                                                if(UserFirstNameNew==null || UserFirstNameNew.equalsIgnoreCase("") )
                                                                                strDisplayFirstName = UserFirstNameCurrent.toString();
                                                                else
                                                                                strDisplayFirstName=UserFirstNameNew.toString();
                                                                System.out.println("Changed display name is:"+strDisplayFirstName+ " "+UserLastNameNew.toString());
                                                                displayName=strDisplayFirstName+ " "+UserLastNameNew.toString();
                                                                displayNameMap.put("base", displayName);
                                                                attrs.put(DISPLAY_NAME, (Serializable) displayNameMap);
                                                                //attrs.put(DISPLAY_NAME,strDisplayFirstName.toString()+ " "+UserLastNameNew.toString());
                                                                try {
                                                                                em.modifyEntity(orch.getTarget().getType(), entityId, attrs);
                                                                } catch (InvalidDataTypeException e) {
                                                                                // TODO Auto-generated catch block
                                                                                e.printStackTrace();
                                                                } catch (InvalidDataFormatException e) {
                                                                                // TODO Auto-generated catch block
                                                                                e.printStackTrace();
                                                                } catch (NoSuchEntityException e) {
                                                                                // TODO Auto-generated catch block
                                                                                e.printStackTrace();
                                                                } catch (StaleEntityException e) {
                                                                                // TODO Auto-generated catch block
                                                                                e.printStackTrace();
                                                                } catch (UnsupportedOperationException e) {
                                                                                // TODO Auto-generated catch block
                                                                                e.printStackTrace();
                                                                } catch (UnknownAttributeException e) {
                                                                                // TODO Auto-generated catch block
                                                                                e.printStackTrace();
                                                                } catch (ProviderException e) {
                                                                                // TODO Auto-generated catch block
                                                                                e.printStackTrace();
                                                                }
                                                                orchParams.put(DISPLAY_NAME, (Serializable) displayNameMap);
                                                                newUserState.setAttribute(DISPLAY_NAME, displayNameMap);
                                                                }

                                                if(UserFirstNameCurrent.toString().equalsIgnoreCase(UserFirstNameNew.toString()))
                                                {
                                                                System.out.println("No change in the First Name");
                                                }

                                                else {

                                                                System.out.println("Change in first name detected");
                                                                attrs.put("First Name",UserFirstNameNew.toString());
                                                                String strDisplayLastName="";
                                                                if(UserLastNameNew==null || UserLastNameNew.equalsIgnoreCase(""))
                                                                                strDisplayLastName = UserLastNameCurrent.toString();
                                                                else
                                                                                strDisplayLastName=UserLastNameNew.toString();
                                                                System.out.println("Changed display name is:"+UserFirstNameNew.toString()+ " "+strDisplayLastName);
                                                                displayName=UserFirstNameNew.toString()+ " "+strDisplayLastName;
                                                                displayNameMap.put("base", displayName);
                                                                attrs.put(DISPLAY_NAME, (Serializable) displayNameMap);
                                                                //attrs.put(DISPLAY_NAME,UserFirstNameNew.toString()+ " "+strDisplayLastName.toString());
                                                               try {
                                                                                em.modifyEntity(orch.getTarget().getType(), entityId, attrs);
                                                                } catch (InvalidDataTypeException e) {
                                                                                // TODO Auto-generated catch block
                                                                                e.printStackTrace();
                                                                } catch (InvalidDataFormatException e) {
                                                                                // TODO Auto-generated catch block
                                                                                e.printStackTrace();
                                                                } catch (NoSuchEntityException e) {
                                                                                // TODO Auto-generated catch block
                                                                                e.printStackTrace();
                                                                } catch (StaleEntityException e) {
                                                                                // TODO Auto-generated catch block
                                                                                e.printStackTrace();
                                                                } catch (UnsupportedOperationException e) {
                                                                                // TODO Auto-generated catch block
                                                                                e.printStackTrace();
                                                                } catch (UnknownAttributeException e) {
                                                                                // TODO Auto-generated catch block
                                                                                e.printStackTrace();
                                                                } catch (ProviderException e) {
                                                                                // TODO Auto-generated catch block
                                                                                e.printStackTrace();
                                                                }
                                                                orchParams.put(DISPLAY_NAME, (Serializable) displayNameMap);
                                                                newUserState.setAttribute(DISPLAY_NAME, displayNameMap);
                                                                }

                                                //


                                                                //newUserState.setAttribute("USR_UDF_OLDUSERSTATUS",employeeStatusCurrent.toString());
             //added on 07/20/2017 for 180 day suspension
                                                /*
                                                String userSuspensionStatusCurrent = (String) currentUserState.getAttribute("UserSuspended".toString());
                                                System.out.println("Inside Modify User Event handler userSuspensionStatusCurrent is: "+userSuspensionStatusCurrent);

                                                if(userSuspensionStatusCurrent!=null){
                                                                attrs.put("UserSuspended", userSuspensionStatusCurrent);}
                                                */
                                                //ended on 07/20/2017 for 180 day suspension

			try {
				if (employeeStatusCurrent.equals("Disabled"))
					attrs.put("OldUserStatus", employeeStatusCurrent.toString());
				else
					attrs.put("OldUserStatus", "Active");
				em.modifyEntity(orch.getTarget().getType(), entityId, attrs);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

                                                //ended on 02/17/2016

                                                if (straccountClaimed != null
                                                                                && straccountClaimed.equalsIgnoreCase("Y")) {
                                                                logger.logp(Level.FINE, getClass().getName(), methodName,
                                                                                                "MIA-DNEH-002 Found key, calling setDisplayName");
                                                                try {
                                                                                // setDisplayName(orch,entityId,orchParams,newUserState,usrMgr,
                                                                                // entityMgr);
                                                                                System.out.println("Inside Modify method before sending the last email"
                                                                                                                + straccountClaimed);
                                                                                System.out.println("Inside Modify method before sending the last email User Type is: "+employeeUserTypeCurrent);
                                                                                //if (straccountClaimed.equalsIgnoreCase("Y")) {
                                                                                if (straccountClaimed.equalsIgnoreCase("Y") && sponsoremail!=null && !sponsoremail.endsWith("nyuwinthrop.org") && !employeeUserTypeCurrent.endsWith("PreHire") ) {
                    //!strEmail.endsWith("nyuwinthrop.org")
                                                                                                System.out.println("Account Claimed Send Email");
                                                                                                // SendAttachmentInEmail((String)
                                                                                                // orchParams.get("AlternateEmail"));
                                                                                                SendAttachmentInEmail(sponsoremail, strFIRST_NAME,
                                                                                                                                strLAST_NAME, strUSER_LOGIN, strUSER_EMAIL);
                                                                                }
                                                                                else if (straccountClaimed.equalsIgnoreCase("Y") && sponsoremail!=null && !sponsoremail.endsWith("nyuwinthrop.org") && employeeUserTypeCurrent.endsWith("PreHire"))
                                                                                {
                                                                                	System.out.println("Inside condition where user is a prehire");
                                                                                	String employeesilkroadintegrationCurrent = (String) currentUserState.getAttribute("SilkRoadIntegration".toString());
                                                                                    System.out.println("Inside Modify User Event handler User Type is: "+employeesilkroadintegrationCurrent);
                                                                                    String templatetype=employeesilkroadintegrationCurrent.substring(employeesilkroadintegrationCurrent.indexOf("+")+1, employeesilkroadintegrationCurrent.length());
                                                                                    SendAttachmentInEmailPreHire(sponsoremail,templatetype,strUSER_LOGIN);
                                                                                    //2017/12/04 13:20:15+OBBLKN
                                                                                //check if user is OBBLKN
                                                                                    if (templatetype.equalsIgnoreCase("OBBLKN"))
                                                                                    {
                                                                                    	System.out.println("OBBLKN chosen, email to be triggered for OBBLKN ");
                                                                                    }

                                                                                //check if user is OBNYU
                                                                                    if (templatetype.equalsIgnoreCase("OBNYU"))
                                                                                    {
                                                                                    	System.out.println("OBNYU chosen, email to be triggered for OBNYU ");
                                                                                    }

                                                                                // check if user is OBNTV
                                                                                    if (templatetype.equalsIgnoreCase("OBNTV"))
                                                                                    {
                                                                                    	System.out.println("OBNTV chosen, email to be triggered for OBNTV ");
                                                                                    }

                                                                                // check if user in OBFHC
                                                                                    if (templatetype.equalsIgnoreCase("OBFHC"))
                                                                                    {
                                                                                    	System.out.println("OBFHC chosen, email to be triggered for OBFHC ");
                                                                                    }
                                                                                }
                                                                                else{
                                                                                                System.out.println("User is a winthrop employee or do not have an alternate email address or PreHire User");
                                                                                }

                                                                } catch (Exception e) {
                                                                                logger.logp(Level.SEVERE, getClass().getName(), methodName,
                                                                                                                "Send Attachment Email Failure" + e);
                                                                }
                                                }

                                                //added on 14th Nov
                                                /*
                                                System.out.println("STTTTAAATUS2->"+Status);
                                                if(Status!= null && Status.equalsIgnoreCase("Disabled")){

                                                                attrs.put("End Date", "");

                                                                try {
                                                                                em.modifyEntity(orch.getTarget().getType(), entityId, attrs);
                                                                } catch (InvalidDataTypeException e) {
                                                                                // TODO Auto-generated catch block
                                                                                e.printStackTrace();
                                                                } catch (InvalidDataFormatException e) {
                                                                                // TODO Auto-generated catch block
                                                                                e.printStackTrace();
                                                                } catch (NoSuchEntityException e) {
                                                                                // TODO Auto-generated catch block
                                                                                e.printStackTrace();
                                                                } catch (StaleEntityException e) {
                                                                                // TODO Auto-generated catch block
                                                                                e.printStackTrace();
                                                                } catch (UnsupportedOperationException e) {
                                                                                // TODO Auto-generated catch block
                                                                                e.printStackTrace();
                                                                } catch (UnknownAttributeException e) {
                                                                                // TODO Auto-generated catch block
                                                                                e.printStackTrace();
                                                                } catch (ProviderException e) {
                                                                                // TODO Auto-generated catch block
                                                                               e.printStackTrace();
                                                                }
                                                }*/


                                                //ended on 14th Nov
                                                if (departmentNumber != null && !departmentNumber.isEmpty()) {

                                                                System.out.println("departmentNumber is ->" + departmentNumber);


                                                                HashMap<String, Serializable> parameters = orch.getParameters();
                                                                String deptName = getLookupValue(departmentNumber,"Lookup.NYU.Department");
                                                                String jobTitle = getLookupValue(strjobTitle,"Lookup.NYU.JobTitle");
                                                                String jobFamily = getLookupValue(strjobTitle,"Lookup.NYU.JobCode");

                                                                attrs.put("DEPT_NAME", deptName);
                                                                attrs.put("Title", jobTitle);
                                                                attrs.put("JobFamily", jobFamily);
                                                                try { 
                                                                                em.modifyEntity(orch.getTarget().getType(), entityId, attrs);
                                                                } catch (InvalidDataTypeException e) {
                                                                                // TODO Auto-generated catch block
                                                                                e.printStackTrace();
                                                                } catch (InvalidDataFormatException e) {
                                                                                // TODO Auto-generated catch block
                                                                                e.printStackTrace();
                                                                } catch (NoSuchEntityException e) {
                                                                                // TODO Auto-generated catch block
                                                                                e.printStackTrace();
                                                                } catch (StaleEntityException e) {
                                                                                // TODO Auto-generated catch block
                                                                                e.printStackTrace();
                                                                } catch (UnsupportedOperationException e) {
                                                                                // TODO Auto-generated catch block
                                                                                e.printStackTrace();
                                                                } catch (UnknownAttributeException e) {
                                                                                // TODO Auto-generated catch block
                                                                                e.printStackTrace();
                                                                } catch (ProviderException e) {
                                                                                // TODO Auto-generated catch block
                                                                                e.printStackTrace();
                                                                }
                                                                // em.modifyEntity(parameters, orch.getTarget().getType(),
                                                                // orch.getTarget().getEntityId());
                                                                // tcUserOperationsIntf userOperationsService =
                                                                // Platform.getService(tcUserOperationsIntf.class);
                                                                // userOperationsService.se
                                                                // orchParams.put("DEPT_NAME", "Testing");
                                                                // newUserState.setAttribute("DEPT_NAME", "Testing");

                                                }
                                }

                                return rval;
                }


                // started on 21st Aug

                private void sendAccountClaimingEmail(String templateName, String altEmail, String firstName, String lastName, String userLogin) throws NoSuchUserException, UserLookupException, AccessDeniedException {

                                                //Declare variables for method
                                                Platform.getService(EntityManager.class);
                                               NotificationService notifServ = Platform.getService(NotificationService.class);
                                               UserManager usrMgr = Platform.getService(UserManager.class);
                                               NotificationEvent accountClaimingNotificationEvent = null;

                                                //Use params to call createNotificationEvent method
                                               System.out.println("Before accountClaimingNotificationEvent");
                                               //if alternate email is not null
                                               //if usermanager key is null

                                                System.out.println("Just before sending email altEmail and managerKey are-"+altEmail+"-");
                                               System.out.println("Just before sending email templateName-"+templateName+"-");
                                               accountClaimingNotificationEvent = createNotificationEvent(templateName, altEmail, firstName, lastName, userLogin);
                                               System.out.println("After accountClaimingNotificationEvent");


                                                //Attempt to send e-mail with the given params.
                                                try {
                                                               System.out.println("Attempting to send email.");
                                                                                notifServ.notify(accountClaimingNotificationEvent);
                                                                                System.out.println("After sending attempt.");
                                                                                //After e-mail is sent,     the "email sent" attribute must be toggled to "Y", so another e-mail is not sent
                                                                                System.out.println("Now check EMAILSENT to 'Y'");
                                                                                //HashMap<String, Object>  updateAttrs = new HashMap<String, Object>();
                                                                                //updateAttrs.put("EmailSent", "Y");
                                                                                //User user = new User(userLogin, updateAttrs);
                                                                                //usrMgr.modify(UserManagerConstants.AttributeName.USER_LOGIN.getId(), userLogin, user);



                                                                //Catching a whole mess of things that can go wrong. With print lines so you know which one it is.
                                                                } catch (UserDetailsNotFoundException e) {
                                                                                System.out.println("UserDetailsNotFoundException");
                                                                                e.printStackTrace();
                                                                } catch (EventException e) {
                                                                                e.printStackTrace();
                                                                                System.out.println("EventException");
                                                                } catch (UnresolvedNotificationDataException e) {
                                                                                e.printStackTrace();
                                                                                System.out.println("UnresolvedNotificationDataException");
                                                                } catch (TemplateNotFoundException e) {
                                                                                e.printStackTrace();
                                                                                System.out.println("TemplateNotFoundException");
                                                                } catch (MultipleTemplateException e) {
                                                                                e.printStackTrace();
                                                                                System.out.println("MultipleTemplateException");
                                                                } catch (NotificationResolverNotFoundException e) {
                                                                                e.printStackTrace();
                                                                                System.out.println("NotificationResolverNotFoundException");
                                                                } catch (NotificationException e) {
                                                                                e.printStackTrace();
                                                                                System.out.println("NotificationException");
                                                                } catch (AccessDeniedException e) {
                                                                                e.printStackTrace();
                                                                                System.out.println("AccessDeniedException");
                                                                }

                                                                }








                                /**
                                  * Method Description
                                  * This method takes the information passed into it in the parameters and creates a notification event
                                  * This method also adds any variables you want to use in the body of the email to the Hashmap map
                                  *
                                  * @param templateName
                                  * @param altEmail
                                  * @param firstName
                                  * @param lastName
                                  * @param userLogin
                                  * @return NotificationEvent
                                  * Returns a notification event to send an email to user's alternate email address
                                * @throws AccessDeniedException
                                 * @throws UserLookupException
                                 * @throws NoSuchUserException
                                  */
                //PSFGPMGR
                                private NotificationEvent createNotificationEvent(String templateName, String altEmail, String firstName, String lastName, String userLogin) throws NoSuchUserException, UserLookupException, AccessDeniedException {

                                                System.out.println("manager Key being passed in createNotificationEvent is :");
                                                //Declare variables and load your dynamic variables into a HashMap to be sent to he Notification Event
                                                NotificationEvent event = new NotificationEvent();
                        //System.out.println("Template Name: "+templateName);
                                                event.setTemplateName(templateName);
                        HashMap<String, Object> map = new HashMap<String, Object>();
                        map.put("UserFirstName", firstName);
                        map.put("UserLastName", lastName);

                        //hashedkey uses the encrypt function to create a long hashed key of the userlogin, to put it in the notification event.
                        BigInteger hashedkey = encrypt((String) userLogin);
                        map.put("userkey", hashedkey);

                        //Set all elements of the map to the notification event.
                        event.setParams(map);

                        //Some APTEC packages used here to sent the email.
                        List<String> emailToList = new ArrayList<String>();
                        emailToList.add(altEmail);

                        //Deciding whether or not to send the email to the alternate email address or to the manager's email address, based upon
                        //the template that is being sent.


                       /* if(templateName.equals("AccountClaimingEmail")){
                               System.out.println("Sending template: "+templateName);
                               emailToList.add(altEmail);
                        }

                        if(templateName.equals("AccountClaimingEmailWinthrop")){
                               System.out.println("Sending template: "+templateName);
                               emailToList.add(altEmail);
                        }

                        if(templateName.equals("AccountClaimingEmailEmployee") ){
                               emailToList.add(altEmail);
                        }*/

                        //Finish off setting up the e-mail notification event, then return the event to be sent.
                        System.out.println("Before setting emailPayload");
                        APTECEMAILPayload emailPayload = null;
                                                try {
                                                                //emailPayload = new APTECEMAILPayload(emailToList, "oimadmin@nyumc.org", true, "UTF-8");
                                                	            emailPayload = new APTECEMAILPayload(emailToList, "NYULangoneHealthOnboarding@nyumc.org", true, "UTF-8");
                                                } catch (Exception e1) {
                                                                // TODO Auto-generated catch block
                                                                e1.printStackTrace();
                                                }
                        System.out.println("After setting emailPayload");

                        //Creating a new list to house the CC email addresses (9/24/2015 - Dan Melando)
                        List<String> ccEmails = new ArrayList<String>();
                        ccEmails.add("NYUSecAdminTeam@nyumc.org");
                        //added on 02/08/2018
                        if(templateName.equalsIgnoreCase("PSFGPMGR")){ccEmails.add("FGPAmb-Onboarding@nyumc.org");}
                        //ended on 02/08/2018

                        //Putting the from email address and CC email list into the payload to be sent
                        //emailPayload.setSenderEmailID("oimadmin@nyumc.org");
                        emailPayload.setSenderEmailID("NYULangoneHealthOnboarding@nyumc.org");
                        emailPayload.setCcMailIds(ccEmails);
                        //emailPayload.setAttachments("");
                        //emailPayload.setBccEmailIds(<OPTIONAL-LISTOFBCCADDRESSES>);
                        System.out.println("Before calling APTECEMAILNotificationProvider");
                        APTECEMAILNotificationProvider notProvider = new APTECEMAILNotificationProvider(event, emailPayload, "http://iam1-soa.nyumc.org:8001/ucs/messaging/webservice");
                        System.out.println("After calling APTECEMAILNotificationProvider");
                        try {
                                                                if (notProvider.sendMessage())
                                                                    System.out.println("Email Sent Successfully to User with Email Address '" +   "'.");

                                                                } catch (Exception e) {
                                                                                e.printStackTrace();
                                                                }

                        return event;
                                }

                /**
                 *  Below are various helper methods that are all in stable states.
                 *  No changes should need to be made on any of these methods.
                 *
                 *  Date 6/12/2015
                */

                                static BigInteger p = new BigInteger(
                            "186903683994784883974286803981340343409");
                                static BigInteger q = new BigInteger(
                            "238365206090827088390448048852555109291");
                                static BigInteger n = p.multiply(q);
                                static BigInteger phiN = (p.subtract(BigInteger.ONE)).multiply((q
                            .subtract(BigInteger.ONE)));
                                static BigInteger e = new BigInteger("449");
                                static BigInteger d = new BigInteger(
                            "17364106129279623304364236508747275879893306787892962132847318388210189166049");
                                public static BigInteger encodeMessageAsBigInteger(String plaintext) {
                     return new BigInteger(plaintext.getBytes());
                     }

                    public static String recoverTextFromBigInteger(BigInteger plaintext) {
                     return new String(plaintext.toByteArray());
                     }

                    private static BigInteger encrypt(String input){

                          BigInteger m = encodeMessageAsBigInteger(input);

                          BigInteger c = m.modPow(e, n);
                          return c;
                          }

                    private static BigInteger encryptInt(int input){

                      String aString = Integer.toString(input);

                          BigInteger m = encodeMessageAsBigInteger(aString);

                          BigInteger c = m.modPow(e, n);
                          return c;
                          }

                    private static BigInteger decrypt(BigInteger c){
                     return c.modPow(d,n);
                     }



                // ended on 21st Aug

                private String getLookupValue(String strDeptID, String strLookupName) {
                                System.out.println("encode value is:"+strDeptID+strLookupName);

                                tcLookupOperationsIntf lookupOpsIntf = Platform
                                                                .getService(tcLookupOperationsIntf.class);
                                String lookupValue = "Decode value not found";
                                try {
                                                lookupValue = lookupOpsIntf.getDecodedValueForEncodedValue(
                                                                                strLookupName, strDeptID);
                                } catch (tcAPIException e) {
                                                // TODO Auto-generated catch block
                                                e.printStackTrace();
                                                return lookupValue;
                                }
                                System.out.println("decode value is:"+lookupValue);
                                return lookupValue;

                }


                private String SendAttachmentInEmail(String strtoemail,
                                                String strSponsorFirstName, String strSponsorLastName, String userLogin, String stremail) {

                                try {
                                                // System.out.println("Inside NYUSendCustomizedEmail: sendEmail()"+otp.getpersonalEmail()+":"+otp.getMessage().toString());
                                                Properties props = System.getProperties();
                                                props.put("mail.smtp.host", "Smtp.nyumc.org");
                                                props.put("mail.smtp.port", "25");
                                                Session session = Session.getInstance(props, null);
                                                Message message = new MimeMessage(session);
                                                // MimeMessage msg = new MimeMessage(session);
                                                // set message headers
                                                // msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
                                                // msg.addHeader("format", "flowed");
                                                // msg.addHeader("Content-Transfer-Encoding", "8bit");

                                                BodyPart messageBodyPart = new MimeBodyPart();
                                                StringBuilder sb = new StringBuilder();
                                                String servicecatalog = " <a href=\"http://servicecatalog.nyumc.org/my.policy\">Self Service</a>";
                                                String helpanddocumenattaion = " <a href=\"http://atnyulmc.org/help-documentation\">this information</a>";
                                                String download = " <a href=\"http://atnyulmc.org/help-documentation/installation-downloads\">download and install</a>";
                                                String mobile = " <a href=\"http://servicecatalog.nyumc.org/mobile-tablets/Pages/BYOD.aspx\">here</a>";
                                                System.out.println("Inside sending attachment email");
                                                if (stremail==null || stremail.equalsIgnoreCase(""))
                                                {
                                                                stremail="not assigned";
                                                }

                                                // msg.setFrom(new
                                                // InternetAddress("oimadmin@nyumc.org","NYUMC - OTP Code"));

                                                // msg.setReplyTo(InternetAddress.parse(strtoemail, false));

                                                // msg.setSubject("Account Claimed in OIAM");

                                                sb.append("<p>");
                                                sb.append("Dear ");
                                                sb.append(strSponsorFirstName
                                                                                                                + " "
                                                                                                                + strSponsorLastName+" ,");
                                                sb.append("</br>");
                                                sb.append("</p>");
                                                sb.append(" Welcome and congratulations! You have successfully activated your NYU Langone Health account.");
                                                sb.append("</p>");
                                                sb.append("</br>");
                                                sb.append("</br>");
                                                sb.append("<p>");
                                                sb.append("Your username is: "+userLogin);
                                                sb.append("</br>");
                                                sb.append("</p>");
                                                sb.append("Your email address is: "+stremail);
                                                sb.append("</br>");
                                                sb.append("<p>");

                                                //sb.append(",you have successfully claimed your account, Please read the NYU terms and conditions attached with this email as an attachment.");
                                                //sb.append("You now have access to the www.atNYULMC.org portal.  The portal provides access to NYULMC e-mail, network drives, Microsoft Office applications, and other tools. If additional access is needed, please open a ticket using " +servicecatalog+" or contact the Help Desk at 212-263-6868 (36868 internal) or 866-276-1892 (toll free) to request it.");
                                                sb.append("The www.atNYULMC.org portal provides access to NYULMC e-mail, network drives, Microsoft Office applications, and other tools.  If this or any additional access is needed, your Manager should open a ticket using " +servicecatalog+" or contact the Help Desk at 212-263-6868 (36868 internal) or 866-276-1892 (toll free) to request it.");
                                                sb.append("</br>");
                                                sb.append("</br>");
                                                sb.append("<p>If you wish to configure your mobile device to receive NYULMC email, please click  " +mobile+" for instructions.</p>");
                                                sb.append("<p>Please familiarize yourself with " +helpanddocumenattaion+" about your access.</p>");
                                                sb.append("<p>Enclosed, please find the NYU Langone Health Privacy, Information Security and Confidentiality Agreement for your records.</p>");
                                                sb.append("</br>");
                                                sb.append("</br>");
                                                sb.append("</br>");
                                                sb.append("Welcome again,");
                                                sb.append("</br>");
                                                sb.append("<p>");
                                                sb.append("NYULH Account Administration Team");
                                                sb.append("</br>");

                                                //messageBodyPart.setText(sb.toString());
                                                messageBodyPart.setContent(sb.toString(), "text/html");
                                                //message.setContent(sb.toString(), "text/html");


                                                message.setFrom(new InternetAddress("oimadmin@nyumc.org"));

                                                // Set To: header field of the header.
                                                message.setRecipients(Message.RecipientType.TO,
                                                                                InternetAddress.parse(strtoemail));

                                                // Set Subject: header field
                                                message.setSubject("NYULH Account Activated Successfully: PLEASE READ");
                                                Multipart multipart = new MimeMultipart();
                                                multipart.addBodyPart(messageBodyPart);

                                                // msg.setText("Congratulations, you have successfully claimed your account",
                                                // "UTF-8");

                                                // msg.setSentDate(new Date());

                                                // msg.setRecipients(Message.RecipientType.TO,
                                                // InternetAddress.parse(strtoemail, false));
                                                System.out.println("Message is ready");
                                                messageBodyPart = new MimeBodyPart();
                                                String file = "/app/oracle/NYULMCPrivacyandSecurityAgreement.pdf";
                                                String filename = "NYULMCPrivacyandSecurityAgreement.pdf";
                                                DataSource source = new FileDataSource(file);
                                                messageBodyPart.setDataHandler(new DataHandler(source));
                                                messageBodyPart.setFileName(filename);
                                                multipart.addBodyPart(messageBodyPart);

                                                // Send the complete message parts
                                                message.setContent(multipart);

                                                // Send message
                                                Transport.send(message);
                                                // Transport.send(msg);

                                                System.out.println("EMail Sent Successfully!!");
                                } catch (Exception e) {
                                                e.printStackTrace();
                                                return "Message Not Sent";
                                }
                                return "Email Message Sent";

                }



                // added on Dec 6th 2017 for NB2.0

                private String SendAttachmentInEmailPreHire(String strtoemail,String strtemplate,String userLogin) {

        try {
                        // System.out.println("Inside NYUSendCustomizedEmail: sendEmail()"+otp.getpersonalEmail()+":"+otp.getMessage().toString());
                        Properties props = System.getProperties();
                        props.put("mail.smtp.host", "Smtp.nyumc.org");
                        props.put("mail.smtp.port", "25");
                        Session session = Session.getInstance(props, null);
                        Message message = new MimeMessage(session);
                        // MimeMessage msg = new MimeMessage(session);
                        // set message headers
                        // msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
                        // msg.addHeader("format", "flowed");
                        // msg.addHeader("Content-Transfer-Encoding", "8bit");

                        BodyPart messageBodyPart = new MimeBodyPart();
                        StringBuilder sb = new StringBuilder();
                        String link="";
                        String logo = "";
                        String nextsteptext="";
                        String contactinfotext="";

                        String linknyu = " <a href=\"https://nyulangone-redcarpet.silkroad.com/eprise/system/JSAuth/AttemptSSOLogin.html?referer=https://nyulangone-redcarpet.silkroad.com:443/Onboarding/Home.html\">this link</a>";
                        String linkbkln = "<a href=\"https://nyulangone-redcarpet.silkroad.com/eprise/system/JSAuth/AttemptSSOLogin.html?referer=https://nyulangone-redcarpet.silkroad.com:443/eprise/main/SiteGen/Lutheran/Content/Home.html\">this link</a>";
                        String linkfhc = " <a href=\"https://nyulangone-redcarpet.silkroad.com/eprise/system/JSAuth/AttemptSSOLogin.html?referer=https://nyulangone-redcarpet.silkroad.com:443/eprise/main/SiteGen/FHC/Content/Home.html\">this link</a>";
                        String linkntv = " <a href=\"https://nyulangone-redcarpet.silkroad.com/eprise/system/JSAuth/AttemptSSOLogin.html?referer=https://nyulangone-redcarpet.silkroad.com:443/eprise/main/SiteGen/NonTraditional/Content/Home.html\">this link</a>";
                        String linkfgp = " <a href=\"https://nyulangone-redcarpet.silkroad.com/eprise/system/JSAuth/AttemptSSOLogin.html?referer=https://nyulangone-redcarpet.silkroad.com:443/Acquisition/Home.html\">this link</a>";


                        String logonyu = "<img src=\"http://atnyulmc.org/sites/default/files/purple_logo.png\" alt =\"NYULMC Logo\" width=\"100\"><br><br>";
                        String logofhc = "<img src=\"http://atnyulmc.org/sites/default/files/FHC.png\" alt =\"NYULMC Logo\" width=\"100\"><br><br>";

                        String nextsteptextnyu="using the username above, and the password created during the activation step. Here you will complete all your required new-employee documents, and be further introduced to NYU Langone Health, and learn more about our programs and perks.";
                        String nextsteptextntv="using the username above, and the password created during the activation step. Here you will complete all your required volunteer documents.";


                        String contactinfotextnyu="If at any point you have questions or need assistance, please contact your Talent Acquisition Assistant or Specialist. ";
                        String contactinfotextntv="If at any point you have questions or need assistance, please contact HRNTV@nyumc.org";

                        if(strtemplate.equalsIgnoreCase("OBNYU"))
                        {
                        	logo=logonyu;
                        	nextsteptext=nextsteptextnyu;
                        	contactinfotext=contactinfotextnyu;
                        	link=linknyu;
                        }
                        if(strtemplate.equalsIgnoreCase("OBBLKN"))
                        {
                        	logo=logonyu;
                        	nextsteptext=nextsteptextnyu;
                        	contactinfotext=contactinfotextnyu;
                        	link=linkbkln;
                        }
                        if(strtemplate.equalsIgnoreCase("OBFHC"))
                        {
                        	logo=logofhc;
                        	nextsteptext=nextsteptextnyu;
                        	contactinfotext=contactinfotextnyu;
                        	link=linkfhc;
                        }

                        if(strtemplate.equalsIgnoreCase("OBNTV"))
                        {
                        	logo=logonyu;
                        	nextsteptext=nextsteptextntv;
                        	contactinfotext=contactinfotextntv;
                        	link=linkntv;
                        }

                        if(strtemplate.equalsIgnoreCase("OBFGPA"))
                        {
                        	logo=logonyu;
                        	nextsteptext=nextsteptextnyu;
                        	contactinfotext=contactinfotextnyu;
                        	link=linkfgp;
                        }


                        System.out.println("Inside sending attachment email for user of type"+strtemplate);

                        // msg.setFrom(new
                        // InternetAddress("oimadmin@nyumc.org","NYUMC - OTP Code"));

                        // msg.setReplyTo(InternetAddress.parse(strtoemail, false));

                        // msg.setSubject("Account Claimed in OIAM");


                        sb.append("<html>");
                        sb.append("<head></head>");
                        sb.append("<body>");
                        sb.append("<p>");
                        //sb.append("<img src="http://atnyulmc.org/sites/default/files/purple_logo.png" alt ="NYULMC Logo" width="300"><br><br>");
                         //<b><h3><font color="purple">Welcome and Congratulations! </font></h3></b>
                        sb.append(logo);

                        //sb.append("<b><h3>");
                        sb.append("<b><h3><font color=\"purple\"> Account Activation Successful </font></h3></b>");
                        //sb.append("<font color=\"purple\"");
                        //sb.append(" Account Activation Successful </font></h3></b>");
                        //sb.append("</font></h3></b>");

                        sb.append("</br>");
                        sb.append("You have successfully activated your NYU Langone Health account. Enclosed, please find the Privacy, Information Security and Confidentiality Agreement for your records");
                        sb.append("</br>");
                        sb.append("<p>");
                        sb.append("Your username is: ");
                        sb.append("<b>");
                        sb.append(userLogin);
                        sb.append("</b>");
                        sb.append("</br>");
                        sb.append("</p>");


                        //sb.append(",you have successfully claimed your account, Please read the NYU terms and conditions attached with this email as an attachment.");
                        //sb.append("You now have access to the www.atNYULMC.org portal.  The portal provides access to NYULMC e-mail, network drives, Microsoft Office applications, and other tools. If additional access is needed, please open a ticket using " +servicecatalog+" or contact the Help Desk at 212-263-6868 (36868 internal) or 866-276-1892 (toll free) to request it.");
                        sb.append("<b><font color=\"red\"> Next Steps: </font></b>");
                        sb.append("Lets get started bringing you onboard! Please access your customized onboarding tour by clicking on  " +link+" ");
                        sb.append(nextsteptext);
                        sb.append("</br>");
                        sb.append("</br>");
                        sb.append("<p>");
                        sb.append(contactinfotext);
                        sb.append("</p>");
                        sb.append("<p>Again, congratulations on your new opportunity and welcome to NYU Langone Health!</p>");
                        //sb.append("</br>");
                        //sb.append("NYU Langone Health Human Resources");
                        sb.append("</body></html>");

                        //messageBodyPart.setText(sb.toString());
                        messageBodyPart.setContent(sb.toString(), "text/html");
                        //message.setContent(sb.toString(), "text/html");


                        message.setFrom(new InternetAddress("NYULangoneHealthOnboarding@nyumc.org"));

                        // Set To: header field of the header.
                        message.setRecipients(Message.RecipientType.TO,
                                                        InternetAddress.parse(strtoemail));

                        // Set Subject: header field
                        message.setSubject("NYULH Account Activated Successfully: PLEASE READ");
                        Multipart multipart = new MimeMultipart();
                        multipart.addBodyPart(messageBodyPart);

                        // msg.setText("Congratulations, you have successfully claimed your account",
                        // "UTF-8");

                        // msg.setSentDate(new Date());

                        // msg.setRecipients(Message.RecipientType.TO,
                        // InternetAddress.parse(strtoemail, false));
                        System.out.println("Message is ready");
                        messageBodyPart = new MimeBodyPart();
                        String file = "/app/oracle/NYULMCPrivacyandSecurityAgreement.pdf";
                        String filename = "NYULMCPrivacyandSecurityAgreement.pdf";
                        DataSource source = new FileDataSource(file);
                        messageBodyPart.setDataHandler(new DataHandler(source));
                        messageBodyPart.setFileName(filename);
                        multipart.addBodyPart(messageBodyPart);

                        // Send the complete message parts
                        message.setContent(multipart);

                        // Send message
                        Transport.send(message);
                        // Transport.send(msg);

                        System.out.println("EMail Sent Successfully!!");
        } catch (Exception e) {
                        e.printStackTrace();
                        return "Message Not Sent";
        }
        return "Email Message Sent";

}

                //

                //added logger in setSponsorInfo
                private void setSponsorInfo(AbstractGenericOrchestration orch,
                                                String entityId, HashMap orchParams, Identity newUserState,
                                                UserManager usrMgr, EntityManager entityMgr,String SponsorID) throws Exception
                {
                	           System.out.println("SponsorID passed to setSponsorInfo method is: "+SponsorID);
                                SearchCriteria searchCriteria = new SearchCriteria("User Login", SponsorID.toUpperCase(),
                                                                SearchCriteria.Operator.EQUAL);
                                Set<String> retAttrs = new HashSet<String>();
                                retAttrs.add("First Name");
                                retAttrs.add("Last Name");
                                //retAttrs.add("SponsorsLastName");
                                //retAttrs.add("AdminDept");

                                List<User> usrList = null;
                                User usr = null;
                                try {
                                                usrList = usrMgr.search(searchCriteria, retAttrs, null);
                                                if (usrList.size() > 0) {
                                                                usr = usrList.get(0);
                                                                System.out.println("users found with the criteria and size of list is"+usrList.size());
                                                } else if (usrList.size() == 0) {
                                                                throw new Exception("No Users found with " + USR_KEY + "="
                                                                                                + entityId);
                                                } else {
                                                                throw new Exception(usrList.size() + " Users found with "
                                                                                                + USR_KEY + "=" + entityId);
                                                }
                                } catch (UserSearchException use) {
                                                throw use;
                                } catch (AccessDeniedException ade) {
                                                throw ade;
                                }
                                HashMap<String, Object> attrs = new HashMap<String, Object>();

                                System.out.println("test sponsor**:->"+(String) usr.getAttribute("First Name"));
                                ;
                                String SponsorsFirstName = (String) usr.getAttribute("First Name");
                                System.out.println("SponsorsFirstName:->"+SponsorsFirstName);
                                String SponsorsLastName = (String) usr.getAttribute("Last Name");
                                System.out.println("SponsorsLastName:->"+SponsorsLastName);

                                attrs.put("SponsorsFirstName", SponsorsFirstName);
                                attrs.put("SponsorsLastName", SponsorsLastName);

                                entityMgr.modifyEntity(orch.getTarget().getType(), entityId, attrs);


                                orchParams.put("SponsorsFirstName", SponsorsFirstName);
                                newUserState.setAttribute("SponsorsFirstName", SponsorsFirstName);

                                orchParams.put("SponsorsLastName", SponsorsLastName);
                                newUserState.setAttribute("SponsorsLastName", SponsorsLastName);


                }
                private void setDisplayName(AbstractGenericOrchestration orch,
                                                String entityId, HashMap orchParams, Identity newUserState,
                                                UserManager usrMgr, EntityManager entityMgr) throws Exception {
                                String methodName = "setDisplayName";
                                logger.entering(getClass().getName(), methodName);
                                SearchCriteria searchCriteria = new SearchCriteria(USR_KEY, entityId,
                                                                SearchCriteria.Operator.EQUAL);
                                Set<String> retAttrs = new HashSet<String>();
                                retAttrs.add(EMP_JOB_CODE);
                                retAttrs.add(FIRST_NAME);
                                retAttrs.add(MIDDLE_NAME);
                                retAttrs.add(LAST_NAME);
                                retAttrs.add(NAME_SUFFIX);
                                retAttrs.add(PREF_FIRST_NAME);
                                retAttrs.add(PREF_MIDDLE_NAME);
                                retAttrs.add(PREF_LAST_NAME);
                                List<User> usrList = null;
                                User usr = null;
                                try {
                                                usrList = usrMgr.search(searchCriteria, retAttrs, null);
                                                if (usrList.size() == 1) {
                                                                usr = usrList.get(0);
                                                } else if (usrList.size() == 0) {
                                                                throw new Exception("No Users found with " + USR_KEY + "="
                                                                                                + entityId);
                                                } else {
                                                                throw new Exception(usrList.size() + " Users found with "
                                                                                                + USR_KEY + "=" + entityId);
                                                }
                                } catch (UserSearchException use) {
                                                throw use;
                                } catch (AccessDeniedException ade) {
                                                throw ade;
                                }

                                String firstName = usr.getFirstName();
                                if (orchParams.containsKey(FIRST_NAME)) {
                                                firstName = (String) orchParams.get(FIRST_NAME);
                                }
                                logger.logp(Level.FINE, getClass().getName(), methodName,
                                                                "MIA-DNEH-004 " + FIRST_NAME + "=" + firstName);

                                String middleName = usr.getMiddleName();
                                if (orchParams.containsKey(MIDDLE_NAME)) {
                                                middleName = (String) orchParams.get(MIDDLE_NAME);
                                }
                                logger.logp(Level.FINE, getClass().getName(), methodName,
                                                                "MIA-DNEH-005 " + MIDDLE_NAME + "=" + middleName);

                                String lastName = usr.getLastName();
                                if (orchParams.containsKey(LAST_NAME)) {
                                                lastName = (String) orchParams.get(LAST_NAME);
                                }
                                logger.logp(Level.FINE, getClass().getName(), methodName,
                                                                "MIA-DNEH-006 " + LAST_NAME + "=" + lastName);

                                String empJobCode = (String) usr.getAttribute(EMP_JOB_CODE);
                                if (orchParams.containsKey(EMP_JOB_CODE)) {
                                                empJobCode = (String) orchParams.get(EMP_JOB_CODE);
                                }
                                logger.logp(Level.FINE, getClass().getName(), methodName,
                                                                "MIA-DNEH-007 " + EMP_JOB_CODE + "=" + empJobCode);

                                String nameSuffix = (String) usr.getAttribute(NAME_SUFFIX);
                                if (orchParams.containsKey(NAME_SUFFIX)) {
                                                nameSuffix = (String) orchParams.get(NAME_SUFFIX);
                                }
                                logger.logp(Level.FINE, getClass().getName(), methodName,
                                                                "MIA-DNEH-008 " + NAME_SUFFIX + "=" + nameSuffix);

                                String prefFirstName = (String) usr.getAttribute(PREF_FIRST_NAME);
                                if (orchParams.containsKey(PREF_FIRST_NAME)) {
                                                prefFirstName = (String) orchParams.get(PREF_FIRST_NAME);
                                }
                                logger.logp(Level.FINE, getClass().getName(), methodName,
                                                                "MIA-DNEH-009 " + PREF_FIRST_NAME + "=" + prefFirstName);

                                String prefMiddleName = (String) usr.getAttribute(PREF_MIDDLE_NAME);
                                if (orchParams.containsKey(PREF_MIDDLE_NAME)) {
                                                prefMiddleName = (String) orchParams.get(PREF_MIDDLE_NAME);
                                }
                                logger.logp(Level.FINE, getClass().getName(), methodName,
                                                                "MIA-DNEH-010 " + PREF_MIDDLE_NAME + "=" + prefMiddleName);

                                String prefLastName = (String) usr.getAttribute(PREF_LAST_NAME);
                                if (orchParams.containsKey(PREF_LAST_NAME)) {
                                                prefLastName = (String) orchParams.get(PREF_LAST_NAME);
                                }
                                logger.logp(Level.FINE, getClass().getName(), methodName,
                                                                "MIA-DNEH-011 " + PREF_LAST_NAME + "=" + prefLastName);

                                String displayName = constructDisplayName(empJobCode, firstName,
                                                                middleName, lastName, nameSuffix, prefFirstName,
                                                                prefMiddleName, prefLastName);

                                // User curUsr = new User(entityId);
                                logger.logp(Level.FINE, getClass().getName(), methodName,
                                                               "MIA-DNEH-012 Setting " + DISPLAY_NAME + "=" + displayName
                                                                                                + " for User=" + entityId);
                                // curUsr.setAttribute(DISPLAY_NAME,displayName);
                                // usrMgr.modify(curUsr);
                                HashMap<String, Object> attrs = new HashMap<String, Object>();
                                //
                                // Modified 2013-11-02 KCS
                                // Display Name has to be put into the orchestration as a Map
                                //
                                // attrs.put(DISPLAY_NAME, displayName);
                                // updateUserUsingEntityMgr(entityId,
                                // (String)orch.getTarget().getType(), attrs);

                                Map displayNameMap = new HashMap();
                                displayNameMap.put("base", displayName);
                                attrs.put(DISPLAY_NAME, (Serializable) displayNameMap);
                                entityMgr.modifyEntity(orch.getTarget().getType(), entityId, attrs);

                                orchParams.put(DISPLAY_NAME, displayNameMap);
                                newUserState.setAttribute(DISPLAY_NAME, displayNameMap);

                                logger.exiting(getClass().getName(), methodName);
                }

                /**
                *
                 *
                 * @param empJobCode
                * @param firstName
                * @param middleName
                * @param lastName
                * @param suffix
                * @param prefFirstName
                * @param prefMiddleName
                * @param prefLastName
                * @return
                */
                public String constructDisplayName(String empJobCode, String firstName,
                                                String middleName, String lastName, String suffix,
                                                String prefFirstName, String prefMiddleName, String prefLastName) {
                                String rval = "";
                                String trimmedFirstName = "";
                                String trimmedMiddleName = "";
                                String trimmedLastName = "";
                                String trimmedSuffix = "";
                                String trimmedPrefFirstName = "";
                                String trimmedPrefMiddleName = "";
                                String trimmedPrefLastName = "";
                                String trimmedEmpJobCode = "";
                                // First trim everything
                                if (firstName != null)
                                                trimmedFirstName = firstName.trim();
                                if (middleName != null)
                                                trimmedMiddleName = middleName.trim();
                                if (lastName != null)
                                                trimmedLastName = lastName.trim();
                                if (suffix != null)
                                                trimmedSuffix = suffix.trim();
                                if (prefFirstName != null)
                                                trimmedPrefFirstName = prefFirstName.trim();
                                if (prefMiddleName != null)
                                                trimmedPrefMiddleName = prefMiddleName.trim();
                                if (prefLastName != null)
                                                trimmedPrefLastName = prefLastName.trim();
                                // Next substitute the preferred names if the preferred last name is
                                // present.
                                if (!trimmedPrefLastName.isEmpty()) {
                                                trimmedLastName = trimmedPrefLastName;
                                                trimmedFirstName = trimmedPrefFirstName;
                                                trimmedMiddleName = trimmedPrefMiddleName;
                                                trimmedSuffix = "";
                                }
                                // Next determine if the user display name should contain comma space
                                if (trimmedEmpJobCode.equalsIgnoreCase("Consultant/GroupOrg")) {
                                                // In the HR.DBO.DISPLAYNAME function, a user with job code X does
                                                // not get the comma space between their last and first name
                                                rval = trimmedLastName + trimmedFirstName;
                                                if (!trimmedMiddleName.isEmpty())
                                                                rval += " " + trimmedMiddleName;
                                } else {
                                                rval = trimmedLastName;
                                                if (!trimmedFirstName.isEmpty())
                                                                rval += ", " + trimmedFirstName;
                                                if (!trimmedMiddleName.isEmpty())
                                                                rval += " " + trimmedMiddleName;
                                }
                                return rval;
                }
}

