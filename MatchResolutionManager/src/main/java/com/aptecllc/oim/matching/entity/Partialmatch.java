package com.aptecllc.oim.matching.entity;

import java.io.Serializable;

import javax.persistence.*;

import com.aptecllc.oim.matching.vo.ReconEventData;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PARTIALMATCH database table.
 * 
 */
@Entity
@NamedQuery(name="Partialmatch.findAll", query="SELECT p FROM Partialmatch p")
public class Partialmatch implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long matchid;

	@Temporal(TemporalType.DATE)
	private Date createdon;

	private String matchedentityid;

	private String matchstatus;

	private String modifiedby;

	@Temporal(TemporalType.DATE)
	private Date modifiedon;

	@Column(name="RE_KEY")
	private BigDecimal reKey;

	@Lob
	@Basic(fetch=FetchType.LAZY)
	private ReconEventData sourcedata;

	private String sourceid;

	private String srcresobject;

	//bi-directional many-to-one association to Candidate
	@OneToMany(mappedBy="partialmatch",fetch = FetchType.EAGER)
	private List<Candidate> candidates;

	public Partialmatch() {
	}

	public long getMatchid() {
		return this.matchid;
	}

	public void setMatchid(long matchid) {
		this.matchid = matchid;
	}

	public Date getCreatedon() {
		return this.createdon;
	}

	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}

	public String getMatchedentityid() {
		return this.matchedentityid;
	}

	public void setMatchedentityid(String matchedentityid) {
		this.matchedentityid = matchedentityid;
	}

	public String getMatchstatus() {
		return this.matchstatus;
	}

	public void setMatchstatus(String matchstatus) {
		this.matchstatus = matchstatus;
	}

	public String getModifiedby() {
		return this.modifiedby;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}

	public Date getModifiedon() {
		return this.modifiedon;
	}

	public void setModifiedon(Date modifiedon) {
		this.modifiedon = modifiedon;
	}

	public BigDecimal getReKey() {
		return this.reKey;
	}

	public void setReKey(BigDecimal reKey) {
		this.reKey = reKey;
	}

	public ReconEventData getSourcedata() {
		return this.sourcedata;
	}

	public void setSourcedata(ReconEventData sourcedata) {
		this.sourcedata = sourcedata;
	}

	public String getSourceid() {
		return this.sourceid;
	}

	public void setSourceid(String sourceid) {
		this.sourceid = sourceid;
	}

	public String getSrcresobject() {
		return this.srcresobject;
	}

	public void setSrcresobject(String srcresobject) {
		this.srcresobject = srcresobject;
	}

	public List<Candidate> getCandidates() {
		return this.candidates;
	}

	public void setCandidates(List<Candidate> candidates) {
		this.candidates = candidates;
	}

	public Candidate addCandidate(Candidate candidate) {
		getCandidates().add(candidate);
		candidate.setPartialmatch(this);

		return candidate;
	}

	public Candidate removeCandidate(Candidate candidate) {
		getCandidates().remove(candidate);
		candidate.setPartialmatch(null);

		return candidate;
	}

}