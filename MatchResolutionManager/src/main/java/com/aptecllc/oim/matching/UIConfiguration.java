package com.aptecllc.oim.matching;

//~--- non-JDK imports --------------------------------------------------------

import com.aptecllc.oim.matching.entity.SourceField;
import com.aptecllc.oim.matching.entity.UISourceConfig;
import com.aptecllc.oim.matching.repository.UISourceConfigRepository;
import com.aptecllc.oim.matching.vo.UIConfigSettings;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

//~--- JDK imports ------------------------------------------------------------

import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Class description
 *
 *
 * @version        1.0, 28.Feb 2015
 * @author         Eric A. Fisher, APTEC LLC    
 */
@Service
public class UIConfiguration {
    private static final Logger      LOGGER = LoggerFactory.getLogger(UIConfiguration.class);
    @Autowired
    private UISourceConfigRepository configRepository;

    /**
     * Method description
     *
     *
     * @return
     */
    @Bean
    public UIConfigSettings getUiConfigSettings() {
        LOGGER.info("Getting Config Settings");

        UIConfigSettings                               uiConfigSettings = new UIConfigSettings();
        HashMap<String, LinkedHashMap<String, String>> sourceMaps       = new HashMap<String,
                                                                              LinkedHashMap<String, String>>();

        for (UISourceConfig config : configRepository.findAll()) {
            String                        sourceName = config.getSourceName();
            LinkedHashMap<String, String> fieldMap   = new LinkedHashMap<String, String>();

            for (SourceField field : config.getSourceFields()) {
                fieldMap.put(field.getSourceFieldName(), field.getCandidateFieldName());
            }

            sourceMaps.put(sourceName, fieldMap);
        }

        uiConfigSettings.setSourceFieldMap(sourceMaps);
       
        LOGGER.info(uiConfigSettings.toString());
        
        return uiConfigSettings;
    }
}
