package com.aptecllc.oim.matching.service;

//~--- non-JDK imports --------------------------------------------------------

import com.aptecllc.oim.matching.entity.Candidate;
import com.aptecllc.oim.matching.entity.History;
import com.aptecllc.oim.matching.entity.Partialmatch;
import com.aptecllc.oim.matching.repository.PartialMatchRepository;
import com.aptecllc.oim.matching.vo.MatchComparison;
import com.aptecllc.oim.matching.vo.MatchPair;
import com.aptecllc.oim.matching.vo.ReconEventData;
import com.aptecllc.oim.matching.vo.ReconTargetAttribute;
import com.aptecllc.oim.matching.vo.UIConfigSettings;
import org.apache.commons.collections.IteratorUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//~--- JDK imports ------------------------------------------------------------








import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Class description
 *
 *
 * @version        1.0, 27.Feb 2015
 * @author         Eric A. Fisher, APTEC LLC
 */
@Service
public class PartialMatchService {
    private final static Logger    LOGGER = LoggerFactory.getLogger(PartialMatchService.class);
    @Autowired
    private PartialMatchRepository partialMatchRepository;
    @Autowired
    private UIConfigSettings       uiConfiguration;

    /**
     * Method description
     *
     *
     * @return
     */
    public List<Partialmatch> findAllPendingMatches() {
    	   List<Partialmatch> results = partialMatchRepository.findBymatchstatus("REVIEW");
        return results;
    }

    public Iterable<Partialmatch> findAll() {
    	Iterable<Partialmatch> iterable=partialMatchRepository.findAll();
    	return IteratorUtils.toList(iterable.iterator());
       }

    /**
     * Method description
     *
     *
     * @param matchID
     *
     * @return
     */
    public Map<Long, MatchComparison> getMatchComparisonsByID(Long matchID) {
        Partialmatch                   match           = getPartialmatchByID(matchID);
        HashMap<String, Object>        sourceData      = new HashMap<String, Object>();
        HashMap<Long, MatchComparison> allComparisons  = new HashMap<Long, MatchComparison>();
        ReconEventData                 eventData       = match.getSourcedata();
        List<ReconTargetAttribute>     reconAttributes = eventData.getSingleValuedAttrs();

        for (ReconTargetAttribute reconAttribute : reconAttributes) {
            sourceData.put(reconAttribute.getTadName(), getEventDataValue(reconAttribute));
        }

        LOGGER.trace(sourceData.toString());

        for (Candidate candidate : match.getCandidates()) {
            MatchComparison         comparison    = new MatchComparison();
            HashMap<String, Object> candidateData = candidate.getCandidatedata();

            comparison.setMatchID(matchID);
            comparison.setCandidateID(candidate.getCandidateid());
            comparison.setCandidateEntityId(candidate.getCandidateentityid());

            ArrayList<MatchPair>          pairs    = new ArrayList<MatchPair>();
            LinkedHashMap<String, String> fieldMap = uiConfiguration.getSourceFieldMap().get(match.getSrcresobject());

            for (String fieldName : fieldMap.keySet()) {
                MatchPair pair    = new MatchPair();
                String    oimName = fieldMap.get(fieldName);

                pair.setFieldName(oimName);
                pair.setSourceValue(sourceData.get(fieldName));
                pair.setCandidateValue(candidateData.get(oimName));
                pairs.add(pair);
            }

            comparison.setMatchPairs(pairs);
            allComparisons.put(comparison.getCandidateID(), comparison);
        }

        return allComparisons;
    }

    /**
     * Method description
     *
     *
     * @param matchID
     *
     * @return
     */
    public Partialmatch getPartialmatchByID(Long matchID) {
        return partialMatchRepository.findOne(matchID);
    }
    
    public Partialmatch save(Partialmatch partialmatch) {
		return partialMatchRepository.save(partialmatch);
    }

    private Object getEventDataValue(ReconTargetAttribute attribute) {
        String type   = attribute.getTadDatatype();
        Object retObj = null;

        switch (type.charAt(0)) {
        case 'N' :
        case 'n' :
            retObj = attribute.getNumericVal();

            break;

        case 'S' :
        case 's' :
            retObj = attribute.getStringVal();

            break;

        case 'D' :
        case 'd' :
            retObj = attribute.getDateVal();
            break;
        default :
            retObj = attribute.getStringVal();
        }

        return retObj;
    }
}