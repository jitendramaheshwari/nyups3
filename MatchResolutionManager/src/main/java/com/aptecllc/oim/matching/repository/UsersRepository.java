package com.aptecllc.oim.matching.repository;

import com.aptecllc.oim.matching.entity.Users;
import org.springframework.data.repository.CrudRepository;

public abstract interface UsersRepository extends CrudRepository<Users, Long>
{
  public abstract Users save(Users paramUsers);
}
