package com.aptecllc.oim.matching.entity;

import java.io.Serializable;

import javax.persistence.*;

import java.math.BigDecimal;
import java.util.HashMap;


/**
 * The persistent class for the CANDIDATES database table.
 * 
 */
@Entity
@Table(name="CANDIDATES")
@NamedQuery(name="Candidate.findAll", query="SELECT c FROM Candidate c")
public class Candidate implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="CANDIDATES_CANDIDATEID_GENERATOR", sequenceName="SEQ_CANDIDATEID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CANDIDATES_CANDIDATEID_GENERATOR")
	private long candidateid;

	@Lob
	@Basic(fetch=FetchType.LAZY)
	private HashMap<String,Object> candidatedata;

	private String candidateentityid;

	private BigDecimal matchscore;

	//bi-directional many-to-one association to Partialmatch
	@ManyToOne
	@JoinColumn(name="MATCHID")
	private Partialmatch partialmatch;

	public Candidate() {
	}

	public long getCandidateid() {
		return this.candidateid;
	}

	public void setCandidateid(long candidateid) {
		this.candidateid = candidateid;
	}

	public HashMap<String,Object> getCandidatedata() {
		return this.candidatedata;
	}

	public void setCandidatedata(HashMap<String,Object> candidatedata) {
		this.candidatedata = candidatedata;
	}

	public String getCandidateentityid() {
		return this.candidateentityid;
	}

	public void setCandidateentityid(String candidateentityid) {
		this.candidateentityid = candidateentityid;
	}

	public BigDecimal getMatchscore() {
		return this.matchscore;
	}

	public void setMatchscore(BigDecimal matchscore) {
		this.matchscore = matchscore;
	}

	public Partialmatch getPartialmatch() {
		return this.partialmatch;
	}

	public void setPartialmatch(Partialmatch partialmatch) {
		this.partialmatch = partialmatch;
	}

}