package com.aptecllc.oim.matching.vo;

import java.util.List;

public class MatchComparison
{
  private Long matchID;
  private List<MatchPair> matchPairs;
  private Long candidateID;
  private String candidateEntityId;

  public String getCandidateEntityId()
  {
    return this.candidateEntityId;
  }

  public void setCandidateEntityId(String candidateEntityId) {
   this.candidateEntityId = candidateEntityId;
  }

  public Long getCandidateID()
  {
     return this.candidateID;
  }

  public Long getMatchID()
  {
     return this.matchID;
  }

  public List<MatchPair> getMatchPairs()
  {
     return this.matchPairs;
  }

  public void setCandidateID(Long candidateID)
  {
    this.candidateID = candidateID;
  }

  public void setMatchID(Long matchID)
  {
    this.matchID = matchID;
  }

  public void setMatchPairs(List<MatchPair> matchPairs)
  {
    this.matchPairs = matchPairs;
  }
}