package com.aptecllc.oim.matching.service;

import com.aptecllc.oim.matching.entity.Users;
import com.aptecllc.oim.matching.repository.UsersRepository;
import java.io.PrintStream;
import java.util.List;
import org.apache.commons.collections.IteratorUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UsersService {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(UsersService.class);

	@Autowired
	private UsersRepository usersRepository;

	public Users findUsersById(Long id) {
		return (Users) this.usersRepository.findOne(id);
	}

	public Users findUsersByUserName(String userName) {
		List<Users> users = findAllUsers();
		Users u1 = new Users();
		for (Users user : users) {
			System.out.println("username:" + user.getUserName());
			if (user.getUserName().equalsIgnoreCase(userName))
				u1 = user;
		}
		return u1;
	}

	public Users saveUsers(Users users) {
		return this.usersRepository.save(users);
	}

	public List<Users> findAllUsers() {
		Iterable iterable = this.usersRepository.findAll();
		return IteratorUtils.toList(iterable.iterator());
	}

	public void deleteUsers(Users users) {
		this.usersRepository.delete(users);
	}

	public String getHashPassword(String password) {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String hashedPassword = passwordEncoder.encode(password);

		System.out.println(hashedPassword);
		return hashedPassword;
	}
}