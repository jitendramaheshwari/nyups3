package com.aptecllc.oim.matching.repository;

import com.aptecllc.oim.matching.entity.History;
import com.aptecllc.oim.matching.entity.Partialmatch;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

public abstract interface HistoryRepository extends CrudRepository<History, Long>
{
  public abstract List<History> findByPartialmatch(Partialmatch paramPartialmatch);

  public abstract History save(History paramHistory);
}
