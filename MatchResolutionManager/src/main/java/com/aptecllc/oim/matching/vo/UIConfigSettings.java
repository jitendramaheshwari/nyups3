package com.aptecllc.oim.matching.vo;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class UIConfigSettings
{
  private HashMap<String, LinkedHashMap<String, String>> sourceFieldMap;

  public String toString()
  {
    return "UIConfigSettings [sourceFieldMap=" + this.sourceFieldMap + "]";
  }

  public HashMap<String, LinkedHashMap<String, String>> getSourceFieldMap()
  {
    return this.sourceFieldMap;
  }

  public void setSourceFieldMap(HashMap<String, LinkedHashMap<String, String>> sourceFieldMap)
  {
     this.sourceFieldMap = sourceFieldMap;
  }
}
