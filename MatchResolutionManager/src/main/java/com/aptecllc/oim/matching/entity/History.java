package com.aptecllc.oim.matching.entity;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the History database table.
 * 
 */
@Entity
@NamedQuery(name="History.findAll", query="SELECT p FROM History p")
public class History implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name="HISTORY_HISTORYID_GENERATOR", sequenceName="SEQ_HISTORYID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="HISTORY_HISTORYID_GENERATOR")
	@Column(name="HISTORYID")
	private long historyId;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATEDON")
	private Date createdon;

	@Column(length=1500,name="COMMENTS")
	private String comments;

	@Column(name="CREATEDBY")
	private String createdBy;

	@ManyToOne
	@JoinColumn(name="MATCHID")
	private Partialmatch partialmatch;


	public long getHistoryId() {
		return historyId;
	}

	public void setHistoryId(long historyId) {
		this.historyId = historyId;
	}

	public Date getCreatedon() {
		return createdon;
	}

	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Partialmatch getPartialmatch() {
		return partialmatch;
	}

	public void setPartialmatch(Partialmatch partialmatch) {
		this.partialmatch = partialmatch;
	}

	

}