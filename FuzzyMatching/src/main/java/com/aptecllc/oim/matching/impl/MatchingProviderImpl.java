package com.aptecllc.oim.matching.impl;

//~--- non-JDK imports --------------------------------------------------------

//~--- JDK imports ------------------------------------------------------------
import java.io.File;
import java.util.HashMap;
import java.util.List;

import javax.sql.DataSource;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aptecllc.oim.fuzzymatching.config.MatchConfiguration;
import com.aptecllc.oim.fuzzymatching.config.SourceConfigurationType;
import com.aptecllc.oim.fuzzymatching.impl.FuzzyMatchingProviderImpl;
import com.aptecllc.oim.matching.api.MatchResult;
import com.aptecllc.oim.matching.api.MatchingProvider;
import com.aptecllc.oim.sequentialmatching.impl.SequentialMatchingProviderImpl;

/**
 * Class description
 * 
 * 
 * @version 1.0, 14/06/19
 * @author Eric A. Fisher, APTEC LLC
 */
public class MatchingProviderImpl implements MatchingProvider {
	private static final String className = FuzzyMatchingProviderImpl.class
			.getName();
	private final static Logger LOGGER = LoggerFactory
			.getLogger(MatchingProviderImpl.class);
	private HashMap<String, MatchingProvider> sourceProviders;
	private MatchConfiguration matchConfiguration;

	/**
	 * Constructs ...
	 * 
	 * 
	 * @param configPath
	 * @throws Exception
	 */
	public MatchingProviderImpl(String configPath, DataSource dataSource,
			String resourceName) throws Exception {
		try {
			File file = new File(configPath);
			System.out.println("File:" + file);
			JAXBContext jaxbContext = JAXBContext
					.newInstance(MatchConfiguration.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

			matchConfiguration = (MatchConfiguration) jaxbUnmarshaller
					.unmarshal(file);
		} catch (JAXBException e) {
			LOGGER.error(e.getMessage(), e);

			throw new Exception("Configuration File Initialization Error");
		}

		sourceProviders = new HashMap<String, MatchingProvider>();

		List<SourceConfigurationType> sourceConfigs = matchConfiguration
				.getSourceConfiguration();

		for (SourceConfigurationType sourceConfig : sourceConfigs) {
			LOGGER.info("Source: " + sourceConfig.getMatchSource());

			if ("FuzzyMatch".equalsIgnoreCase(sourceConfig.getMatchType())) {
				sourceProviders
						.put(sourceConfig.getMatchSource(),
								new FuzzyMatchingProviderImpl(sourceConfig,
										dataSource));
			} else if ("SequentialMatch".equalsIgnoreCase(sourceConfig
					.getMatchType())) {
				sourceProviders.put(sourceConfig.getMatchSource(),
						new SequentialMatchingProviderImpl(sourceConfig,
								resourceName));
			} else {
				throw new Exception("Match Type Not Defined: "
						+ sourceConfig.getMatchType());
			}
		}
	}

	/**
	 * Method description
	 * 
	 * 
	 * @param sourceDataSet
	 * 
	 * @return
	 * 
	 * @throws Exception
	 */
	@Override
	public MatchResult match(HashMap<String, Object> sourceDataSet,
			String resourceName) throws Exception {
		MatchResult result = MatchResult.ERROR_MATCH;
		String source = (String) sourceDataSet.get("Source");
		MatchingProvider provider = sourceProviders.get(source);

		if (provider == null) {
			throw new Exception("Provider Not Found for Source: " + source);
		}

		result = provider.match(sourceDataSet, resourceName);

		return result;
	}

	/**
	 * Method description
	 * 
	 * 
	 * @param reconKey
	 * @param sourceDataSet
	 * 
	 * @return
	 * 
	 * @throws Exception
	 */
	@Override
	public MatchResult match(Long reconKey,
			HashMap<String, Object> sourceDataSet, String resourceName)
			throws Exception {
		MatchResult result = MatchResult.ERROR_MATCH;
		String source = (String) sourceDataSet.get("Source");
		MatchingProvider provider = sourceProviders.get(source);

		if (provider == null) {
			throw new Exception("Provider Not Found for Source: " + source);
		}

		result = provider.match(reconKey, sourceDataSet, resourceName);

		return result;
	}
}
