package com.aptecllc.oim.matching.api;

//~--- JDK imports ------------------------------------------------------------

import java.util.Map;

public interface Matcher {
	public enum MatcherParam {
		FULLMATCHPOINTS("FullMatchPoints"), NOMATCHPOINTS("NoMatchPoints"), CASESENSITIVE(
				"CaseSensitive"), LEVENSTEINSWEIGHT("LevenSteinsWeight");

		private String value;

		private MatcherParam(String value) {
			this.value = value;
		}

		public String getId() {
			return value;
		}

	}
	

    /**
     * Method description
     *
     *
     * @param parameterMap
     */
    public void initializeMatcher(Map parameterMap);

    /**
     * Method description
     *
     *
     * @param sourceAttribute
     * @param candidateAttribute
     *
     * @return
     */
    public int performMatch(Object sourceAttribute, Object candidateAttribute);
}
