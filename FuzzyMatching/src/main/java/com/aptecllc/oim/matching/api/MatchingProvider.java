package com.aptecllc.oim.matching.api;

import java.util.HashMap;

public interface MatchingProvider {

	MatchResult match(HashMap<String, Object> sourceDataSet, String resourceName)
			throws Exception;

	MatchResult match(Long reconKey, HashMap<String, Object> sourceDataSet,
			String resourceName) throws Exception;

}
