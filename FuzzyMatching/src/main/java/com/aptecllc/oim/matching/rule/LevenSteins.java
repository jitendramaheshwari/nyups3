package com.aptecllc.oim.matching.rule;

//~--- non-JDK imports --------------------------------------------------------

//~--- JDK imports ------------------------------------------------------------
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aptecllc.oim.matching.api.AbstractMatcher;
import com.aptecllc.oim.matching.api.Matcher;

public class LevenSteins extends AbstractMatcher implements Matcher {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(LevenSteins.class);

	/**
	 * Method description
	 * 
	 * 
	 * @param sourceAttribute
	 * @param candidateAttribute
	 * 
	 * @return
	 */
	@Override
	public int performMatch(Object sourceAttribute, Object candidateAttribute) {
		boolean result = false;
		String sourceFullName = (String) sourceAttribute;
		String candidateFullName = (String) candidateAttribute;
		LOGGER.debug("sourceFullName: " + sourceFullName
				+ " candidateFullName: " + candidateFullName);
		System.out.println("sourceFullName: " + sourceFullName
				+ " candidateFullName: " + candidateFullName);
		int points = LDPoints(sourceFullName, candidateFullName,
				this.levensteinsWeight);
		System.out.println("LevenSteinsPoints");
		// LDPoints(sourceFullName, candidateFullName, );
		return points;

	}

	public static int LDPoints(String sourceFullName, String oimFullName,
			int totalWeight) {

		String srcFname = sourceFullName.toUpperCase();
		String oimFname = oimFullName.toUpperCase();

		int LD = StringUtils.getLevenshteinDistance(srcFname, oimFname);
		float temp = (float) (srcFname.length() + oimFname.length()) / 2;
		float percent = 1 - ((LD / temp));
		int score = (int) (percent * totalWeight);
		return score;
	}

	public static void main(String[] args) throws Exception {
		/*
		 * String source = "Paige"; //System.out.println("Source Length: "
		 * +source.length());
		 * 
		 * 
		 * SRC First Name Dakota SRC Middle Name Name null SRC Last Name Abrams
		 * OIM First Name DAKOTA OIM Middle Name Name TYLER OIM Last Name ABRAMS
		 * 
		 * 
		 * String oim = "Leigh"; System.out.println("New Score: " +
		 * LDFloor(source, oim, 10, 25)); //System.out.println("OIM Length: " +
		 * oim.length());
		 * 
		 * 
		 * System.out.println("Traditional Score : " +LDPoints(source, oim,10));
		 */
		String source_ssn = "421549686";
		String oim_ssn = "421558686";

		System.out.println(StringUtils.getLevenshteinDistance("vinay kumar",
				"binay kumar"));

		System.out.println(LDPoints("vinay kumar", "sinay Kumar", 20));
		/*
		 * System.out.println(nameDistance(source_ssn, oim_ssn));
		 * System.out.println(LDPoints("Vinay Kumat", "Vinay Kumad", 20)); }
		 */
	}
}
