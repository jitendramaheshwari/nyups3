package com.aptecllc.oim.matching.rule;

//~--- non-JDK imports --------------------------------------------------------

import com.aptecllc.oim.matching.api.AbstractMatcher;
import com.aptecllc.oim.matching.api.Matcher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//~--- JDK imports ------------------------------------------------------------

import java.text.SimpleDateFormat;

import java.util.Date;

/**
 * Class description
 *
 *
 * @version        1.0, 26.Feb 2015
 * @author         Eric A. Fisher, APTEC LLC
 */
public class DateEqualityMatcher extends AbstractMatcher implements Matcher {
    private static final Logger LOGGER = LoggerFactory.getLogger(DateEqualityMatcher.class);

    /**
     * Method description
     *
     *
     * @param sourceAttribute
     * @param candidateAttribute
     *
     * @return
     */
    @Override
    public int performMatch(Object sourceAttribute, Object candidateAttribute) {
        boolean result = false;
        Date    date1  = (Date) sourceAttribute;
        Date    date2  = (Date) candidateAttribute;

        LOGGER.debug("Date1: " + date1 + " Date2: " + date2);

        if ((date1 != null) && (date2 != null)) {
            SimpleDateFormat sdf         = new SimpleDateFormat("MMddyyyy");
            String           dateString1 = sdf.format(date1);
            String           dateString2 = sdf.format(date2);

            LOGGER.debug("Date1: " + dateString1 + " Date2: " + dateString2);

            if (dateString1.equals(dateString2)) {
                result = true;
            }
        }

        if (result) {
            return this.fullMatchPoints;
        }

        return this.noMatchPoints;
    }
}
