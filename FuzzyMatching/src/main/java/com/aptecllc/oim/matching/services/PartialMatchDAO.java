package com.aptecllc.oim.matching.services;

//~--- non-JDK imports --------------------------------------------------------

import com.aptecllc.oim.matching.vo.CandidateMatch;
import com.aptecllc.oim.matching.vo.PartialMatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

//~--- JDK imports ------------------------------------------------------------

import javax.naming.InitialContext;

import javax.sql.DataSource;

/**
 * Class description
 *
 *
 * @version        1.0, 23.Feb 2015
 * @author         Eric A. Fisher, APTEC LLC
 */
public class PartialMatchDAO {
    private final static Logger        LOGGER    = LoggerFactory.getLogger(PartialMatchDAO.class);
    private NamedParameterJdbcTemplate jdbcMatch = null;

    /**
     * Constructs ...
     *
     */
    public PartialMatchDAO() {}

    /**
     * Method description
     *
     *
     * @param sourceObj
     * @param sourceKey
     *
     * @return
     */
    public Long checkForPendingResolution(String sourceObj, String sourceKey) {
        Long   matchID = null;
        String sql     =
            "select matchid from partialmatch where srcresobject = ? and sourceid = ? and matchstatus in ('REVIEW')";

        try {
            matchID = jdbcMatch.getJdbcOperations().queryForObject(sql, new Object[] { sourceObj, sourceKey },
                    Long.class);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }

        return matchID;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public Long getMatchID() {
        String sql     = "select seq_matchid.nextval from dual";
        Long   matchID = jdbcMatch.getJdbcOperations().queryForObject(sql, Long.class);

        return matchID;
    }

    /**
     * Method description
     *
     *
     * @param match
     *
     * @return
     */
    public boolean insertPartialMatch(PartialMatch match) {
        String sql =
            "insert into PartialMatch (matchid, re_key, srcresobject,matchstatus,sourceid,createdon,sourcedata) values (:matchID, :reconKey, :sourceResource, :matchStatus,:sourceID,:createdOn,:reconEventData)";
        BeanPropertySqlParameterSource params   = new BeanPropertySqlParameterSource(match);
        int                            rescount = jdbcMatch.update(sql, params);

        LOGGER.debug("MatchInsert Count: " + rescount);

        if (rescount == 0) {
            return false;
        }

        for (CandidateMatch candidate : match.getCandidates()) {
            candidate.setMatchID(match.getMatchID());
            insertCandidateMatch(candidate);
        }

        return true;
    }

    /**
     * Method description
     *
     *
     * @param ds
     */
    public void setDataSource(DataSource ds) {
        jdbcMatch = new NamedParameterJdbcTemplate(ds);
    }

    /**
     * Method description
     *
     *
     * @param jndiLookup
     *
     * @throws Exception
     */
    public void setDataSource(String jndiLookup) throws Exception {
        InitialContext context;
        DataSource     ds = null;

        context = new InitialContext();
        ds      = (DataSource) context.lookup(jndiLookup);
        setDataSource(ds);
    }

    /**
     * Method description
     *
     *
     * @param match
     *
     * @return
     */
    public boolean updatePartialMatch(PartialMatch match) {
        String sql =
            "update partialmatch set re_key = :reconKey, srcresobject = :sourceResource, matchstatus = :matchStatus, modifiedon = :modifiedOn, sourcedata = :reconEventData where matchid = :matchID";
        BeanPropertySqlParameterSource params   = new BeanPropertySqlParameterSource(match);
        int                            rescount = jdbcMatch.update(sql, params);

        LOGGER.debug("MatchInsert Count: " + rescount);

        if (rescount == 0) {
            return false;
        }

        // Replace all candidates
        deleteAllCandidateMatches(match.getMatchID());

        for (CandidateMatch candidate : match.getCandidates()) {
            candidate.setMatchID(match.getMatchID());
            insertCandidateMatch(candidate);
        }

        return true;
    }

    private void deleteAllCandidateMatches(Long matchID) {
        String sql = "delete from candidates where matchid = :matchID";

        jdbcMatch.getJdbcOperations().update(sql, new Object[] { matchID });
    }

    private void insertCandidateMatch(CandidateMatch candidate) {
        String sqlchild =
            "insert into candidates (candidateid, matchid, candidateentityid, matchscore, candidatedata) values (seq_candidateid.nextval,:matchID, :candidateEntityID, :matchScore, :candidateDataBlob)";
        BeanPropertySqlParameterSource paramchild = new BeanPropertySqlParameterSource(candidate);

        try {
            jdbcMatch.update(sqlchild, paramchild);
        } catch (DataIntegrityViolationException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
}
