package com.aptecllc.oim.matching.api;

//~--- non-JDK imports --------------------------------------------------------

//~--- JDK imports ------------------------------------------------------------
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class description
 * 
 * 
 * @version 1.0, 23.Feb 2015
 * @author Eric A. Fisher, APTEC LLC
 */
public abstract class AbstractMatcher implements Matcher {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(AbstractMatcher.class);
	protected Integer fullMatchPoints = 0;
	protected Integer noMatchPoints = 0;
	protected Integer levensteinsWeight = 0;
	protected HashMap<String, String> matcherParams;

	/**
	 * Method description
	 * 
	 * 
	 * @param parameterMap
	 */
	@Override
	public void initializeMatcher(Map parameterMap) {
		matcherParams = (HashMap<String, String>) parameterMap;

		LOGGER.debug("Parameter Map: " + parameterMap.toString());

		if (matcherParams.containsKey(Matcher.MatcherParam.FULLMATCHPOINTS
				.getId())) {
			fullMatchPoints = Integer.valueOf(matcherParams
					.get(Matcher.MatcherParam.FULLMATCHPOINTS.getId()));
		}

		if (matcherParams.containsKey(Matcher.MatcherParam.NOMATCHPOINTS
				.getId())) {
			noMatchPoints = Integer.valueOf(matcherParams
					.get(Matcher.MatcherParam.NOMATCHPOINTS.getId()));
		}
		if (matcherParams.containsKey(Matcher.MatcherParam.LEVENSTEINSWEIGHT.getId())) {
			levensteinsWeight = Integer.valueOf(matcherParams
					.get(Matcher.MatcherParam.LEVENSTEINSWEIGHT.getId()));
		}
	}
}
