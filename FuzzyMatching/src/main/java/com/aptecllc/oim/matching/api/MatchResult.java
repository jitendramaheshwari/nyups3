package com.aptecllc.oim.matching.api;

public enum MatchResult {
	NO_MATCH(0), ONE_MATCH(1), MULTI_MATCH(2), ERROR_MATCH(3);
    private int value;

    private MatchResult(int value) {
            this.value = value;
    }

}
