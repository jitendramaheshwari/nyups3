package com.aptecllc.oim.sequentialmatching.impl;

//~--- non-JDK imports --------------------------------------------------------

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
//~--- JDK imports ------------------------------------------------------------
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.iam.identity.exception.UserSearchException;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.platform.authz.exception.AccessDeniedException;
import oracle.iam.platform.entitymgr.vo.SearchCriteria;

import com.aptecllc.oim.fuzzymatching.config.MatchRuleType;
import com.aptecllc.oim.fuzzymatching.config.SourceConfigurationType;
import com.aptecllc.oim.matching.api.MatchResult;
import com.aptecllc.oim.matching.api.MatchingProvider;
import com.aptecllc.oim.matching.services.MatchServices;
import com.aptecllc.oim.utils.OimUserUtils;

/**
 * Class description
 * 
 * 
 * @version 1.0, 14/06/19
 * @author Eric A. Fisher, APTEC LLC
 */
public class SequentialMatchingProviderImpl implements MatchingProvider {
	private static final String className = SequentialMatchingProviderImpl.class
			.getName();
	private final static Logger LOGGER = Logger.getLogger(className);
	private SourceConfigurationType sourceConfiguration;
	private String resourceName;

	// OIMClient client;

	/**
	 * Constructs ...
	 * 
	 * 
	 * @param config
	 * @param resourceName
	 */
	public SequentialMatchingProviderImpl(SourceConfigurationType config,
			String resourceName) {
		this.sourceConfiguration = config;
		this.resourceName = resourceName;
	}

	/**
	 * Constructs ...
	 * 
	 * 
	 * 
	 * @param sourceDataSet
	 * 
	 * @return
	 * 
	 * @throws Exception
	 */

	// public SequentialMatchingProviderImpl(OIMClient client,
	// SourceConfigurationType config) {
	// this.client = client;
	// this.sourceConfiguration = config;
	// }

	/**
	 * Method description
	 * 
	 * 
	 * @param sourceDataSet
	 * 
	 * @return
	 * 
	 * @throws Exception
	 */
	public MatchResult match(HashMap<String, Object> sourceDataSet,
			String resourceName) throws Exception {
		Long reconKey = (Long) sourceDataSet.get("ReconEventKey");

		return match(reconKey, sourceDataSet, resourceName);
	}

	/**
	 * Method description
	 * 
	 * 
	 * @param reconKey
	 * @param sourceDataSet
	 * 
	 * @return
	 * 
	 * @throws Exception
	 */
	public MatchResult match(Long reconKey,
			HashMap<String, Object> sourceDataSet, String resourceName)
			throws Exception {
		int numMatches = 0;
		MatchResult result = MatchResult.ERROR_MATCH;
		HashMap<String, HashMap<String, Object>> matchResultSet = new HashMap<String, HashMap<String, Object>>();

		// For each Sequential rule. Perform search. Stop if one match found.
		// Create if no match found.
		List<MatchRuleType> matchRules = sourceConfiguration.getMatchRule();

		for (MatchRuleType rule : matchRules) {
			String sourceValue = (String) sourceDataSet.get(rule
					.getSourceAttribute());

			LOGGER.fine("SourceAttribute " + rule.getSourceAttribute() + ": "
					+ sourceValue);

			if ((sourceValue != null) && !sourceValue.isEmpty()) {

				// Perform search
				matchResultSet = searchForMatch(
						buildSearchCriteria(rule.getCandidateAttribute(),
								sourceValue), rule.isCaseSensitive());

				if (matchResultSet.size() == 1) {

					// One Match found
					numMatches = 1;
					LOGGER.info("One Match Found");

					break;
				} else if (matchResultSet.size() > 1) {

					// Multiple matches
					numMatches = matchResultSet.size();

					break;
				}
			}
		}

		if (numMatches == 0) {

			// Create as New
			LOGGER.info("No Match Found");
			MatchServices.createNewUser(reconKey, resourceName);
			result = MatchResult.NO_MATCH;
		} else if (numMatches == 1) {

			// Link - Set Unique ID on matched user.
			LOGGER.info("validate: Initializing User Modify Process..");

			List<String> userKeys = getUsrKeys(matchResultSet);
			String usrKey = userKeys.get(0);

			MatchServices.linkUser(reconKey, Long.parseLong(usrKey),
					resourceName);
			result = MatchResult.ONE_MATCH;
		} else {

			// Uh oh. In a sequential matcher this is an error condition!
			LOGGER.info("Multi Matches Found");
			result = MatchResult.ERROR_MATCH;
		}

		return result;
	}

	/**
	 * Method description
	 * 
	 * 
	 * @param userSearchCriteria
	 * @param caseSensitive
	 * 
	 * @return
	 * 
	 * @throws AccessDeniedException
	 * @throws UserSearchException
	 */
	public HashMap<String, HashMap<String, Object>> searchForMatch(
			SearchCriteria userSearchCriteria, boolean caseSensitive)
			throws UserSearchException, AccessDeniedException {
		String methodName = "candidateSearchSet";

		LOGGER.fine(methodName);
		LOGGER.info("Entering " + methodName);

		HashMap<String, HashMap<String, Object>> oimresultsTable = new HashMap<String, HashMap<String, Object>>();
		List<User> results = new ArrayList<User>();
		OimUserUtils oimUserUtils = null;

		oimUserUtils = new OimUserUtils();
		LOGGER.info("Search Criteria: " + userSearchCriteria);
		results = oimUserUtils.getUser(userSearchCriteria,
				(String) userSearchCriteria.getFirstArgument());
		LOGGER.info(methodName + "User Search Results: " + results.size());

		for (User user : results) {
			LOGGER.finest("SourceValue: "
					+ userSearchCriteria.getSecondArgument());
			LOGGER.finest("CandidateValue: "
					+ user.getAttributes().get(
							userSearchCriteria.getFirstArgument()));

			if (caseSensitive) {
				String source = (String) userSearchCriteria.getSecondArgument();
				String candidate = (String) user.getAttributes().get(
						userSearchCriteria.getFirstArgument());

				if (source.equals(candidate)) {
					oimresultsTable.put(user.getId(), user.getAttributes());
				}
			} else {
				oimresultsTable.put(user.getId(), user.getAttributes());
			}
		}

		LOGGER.info(methodName + "OIM Candidate Match Data: " + oimresultsTable);
		LOGGER.fine(methodName);

		return oimresultsTable;
	}

	private SearchCriteria buildSearchCriteria(String candidateAttribute,
			String searchValue) {
		return new SearchCriteria(candidateAttribute, searchValue,
				SearchCriteria.Operator.EQUAL);
	}

	private List<String> getUsrKeys(
			HashMap<String, HashMap<String, Object>> candidateMap) {
		String methodName = "getUsrKeys";

		LOGGER.entering(getClass().getName(), methodName);

		List<String> usrKey = new ArrayList<String>();
		Set<String> keys = candidateMap.keySet();

		for (String key : keys) {
			LOGGER.logp(Level.FINE, getClass().getName(), methodName,
					"Value of Key --> " + key + " is: " + candidateMap.get(key));
			usrKey.add(key);
		}

		LOGGER.exiting(getClass().getName(), methodName);

		return usrKey;
	}
}
