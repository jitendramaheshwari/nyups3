package org.nyumc.idm.tasks;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import oracle.iam.connectors.icfcommon.exceptions.IntegrationException;
import oracle.iam.connectors.icfcommon.util.MapUtil;
import oracle.iam.connectors.icfcommon.util.TypeUtil;
import org.identityconnectors.common.Assertions;
import org.identityconnectors.common.StringUtil;
import org.identityconnectors.common.logging.Log;
import org.identityconnectors.common.security.GuardedString;
import org.identityconnectors.framework.api.APIConfiguration;
import org.identityconnectors.framework.api.ConfigurationProperties;
import org.identityconnectors.framework.api.ConfigurationProperty;
import org.identityconnectors.framework.api.ConnectorFacade;
import org.identityconnectors.framework.api.ConnectorFacadeFactory;
import org.identityconnectors.framework.api.ConnectorInfo;
import org.identityconnectors.framework.api.ConnectorInfoManager;
import org.identityconnectors.framework.api.ConnectorInfoManagerFactory;
import org.identityconnectors.framework.api.ConnectorKey;
import org.identityconnectors.framework.api.RemoteFrameworkConnectionInfo;
import org.identityconnectors.framework.common.exceptions.ConnectorException;

public final class ConnectorFactory {

    private static final Log log = Log.getLog(ConnectorFactory.class);
    private static final String BUNDLE_NAME = "Bundle Name";
    private static final String BUNDLE_VERSION = "Bundle Version";
    private static final String CONNECTOR_NAME = "Connector Name";
    private static final String SERVER_HOST = "Host";
    private static final String SERVER_PORT = "Port";
    private static final String SERVER_KEY = "Key";
    private static final String SERVER_USE_SSL = "UseSSL";
    private static final String SERVER_TIMEOUT = "Timeout";
    private static final String POOL_MAX_IDLE = "Pool Max Idle";
    private static final String POOL_MAX_SIZE = "Pool Max Size";
    private static final String POOL_MAX_WAIT = "Pool Max Wait";
    private static final String POOL_MIN_EVICT_IDLE_TIME = "Pool Min Evict Idle Time";
    private static final String POOL_MIN_IDLE = "Pool Min Idle";
   
    private ConnectorFactory() {
        throw new AssertionError();
    }

    public static ConnectorFacade createConnectorFacade(Map resourceDetails,Map configLookup) {

        if ((configLookup == null) && (resourceDetails == null)) {
            throw new RuntimeException("need remote info and config");
        }

        log.info("Create Facade", null);
        ConnectorInfoManager infoManager;

        String host = (String) MapUtil.getRequiredValue(resourceDetails, "Host");
        System.out.println("--Host Value is->"+host);
        int port = ((Integer) TypeUtil.convertValueType((String) MapUtil.getRequiredValue(resourceDetails, "Port"), Integer.class)).intValue();
        System.out.println("--port Value is->"+port);

        GuardedString key = new GuardedString(((String) MapUtil.getRequiredValue(resourceDetails, "Key")).toCharArray());
        boolean useSSL = ((Boolean) TypeUtil.convertValueType((String) MapUtil.getValue(resourceDetails, "UseSSL", "false"), Boolean.class)).booleanValue();
        
        int timeout = ((Integer) TypeUtil.convertValueType((String) MapUtil.getValue(resourceDetails, "Timeout", "0"), Integer.class)).intValue();
        System.out.println("--timeout Value is->"+timeout);
        log.info("Create remoteInfo", null);
        RemoteFrameworkConnectionInfo remoteInfo = new RemoteFrameworkConnectionInfo(host, port, key, useSSL, null, timeout);

        infoManager = getRemoteConnectorInfoManager(remoteInfo);
        

        String bundleName = (String)MapUtil.getRequiredValue(configLookup,"Bundle Name");
        System.out.println("--bundleName Value is->"+bundleName);
        String bundleVersion = (String)MapUtil.getRequiredValue(configLookup,"Bundle Version");
        System.out.println("--bundleVersion Value is->"+bundleVersion);
        String connectorName = (String)MapUtil.getRequiredValue(configLookup,"Connector Name");
        System.out.println("--connectorName Value is->"+connectorName);
        ConnectorKey connectorKey = new ConnectorKey(bundleName, bundleVersion, connectorName);

        log.info("Create connectorInfo", null);
        ConnectorInfo connectorInfo = infoManager.findConnectorInfo(connectorKey);

        if (connectorInfo == null) {
            throw new IntegrationException("Connector " + connectorKey + " not found");
        }

        APIConfiguration config = connectorInfo.createDefaultAPIConfiguration();

        configurePool(configLookup, config);
        Map resMap = new HashMap();
        resMap.putAll(resourceDetails);
        resMap.remove("UseSSL");
        log.info("Create configurator", null);
        ConnectorConfigurator configurator = new ConnectorConfigurator(resMap,configLookup);
        configurator.configure(config);

        ConnectorFacade connector = ConnectorFacadeFactory.getInstance().newInstance(config);

        return connector;
    }

    private static void configurePool(Map configLookup, APIConfiguration config) {
        //Integer maxIdle = (Integer) configLookup.get("Pool Max Idle");
    	
    	Integer maxIdle = ((Integer) TypeUtil.convertValueType((String) MapUtil.getValue(configLookup, "Pool Max Idle", "0"), Integer.class)).intValue();
        System.out.println("--maxIdle->"+maxIdle);
    	
    	
        if (maxIdle != null) {
            config.getConnectorPoolConfiguration().setMaxIdle(maxIdle.intValue());
        }
        //Integer maxSize = (Integer) configLookup.get("Pool Max Size");
        Integer maxSize = ((Integer) TypeUtil.convertValueType((String) MapUtil.getValue(configLookup, "Pool Max Size", "0"), Integer.class)).intValue();
        if (maxSize != null) {
            config.getConnectorPoolConfiguration().setMaxObjects(maxSize.intValue());
        }
        //Integer maxWait = (Integer) configLookup.get("Pool Max Wait");
        Integer maxWait = ((Integer) TypeUtil.convertValueType((String) MapUtil.getValue(configLookup, "Pool Max Wait", "0"), Integer.class)).intValue();
        if (maxWait != null) {
            config.getConnectorPoolConfiguration().setMaxWait(maxWait.intValue());
        }
        //Integer minEvictTime = (Integer) configLookup.get("Pool Min Evict Idle Time");
        Integer minEvictTime = ((Integer) TypeUtil.convertValueType((String) MapUtil.getValue(configLookup, "Pool Min Evict Idle Time", "0"), Integer.class)).intValue();
        if (minEvictTime != null) {
            config.getConnectorPoolConfiguration().setMinEvictableIdleTimeMillis(minEvictTime.intValue());
        }
        //Integer minIdle = (Integer) configLookup.get("Pool Min Idle");
        Integer minIdle = ((Integer) TypeUtil.convertValueType((String) MapUtil.getValue(configLookup, "Pool Min Idle", "0"), Integer.class)).intValue();
        if (minIdle != null) {
            config.getConnectorPoolConfiguration().setMinIdle(minIdle.intValue());
        }
    }

    

    private static synchronized ConnectorInfoManager getRemoteConnectorInfoManager(RemoteFrameworkConnectionInfo remoteInfo) {
        return ConnectorInfoManagerFactory.getInstance().getRemoteManager(remoteInfo);
    }

    private static class ConnectorConfigurator {

        private static final Log log = Log.getLog(ConnectorConfigurator.class);
        private final Map itResource;
        private final Map resConfigLookup;

        public ConnectorConfigurator(Map resourceDetails,Map configLookup) {
            this.itResource = resourceDetails;
            this.resConfigLookup = configLookup;
        }

        public void configure(APIConfiguration configProps) {
            log.ok("Enter", new Object[0]);
            Assertions.nullCheck(configProps, "configProps");

            ConfigurationProperties config = configProps.getConfigurationProperties();
            List<String> names = config.getPropertyNames();
            for (String name : names) {
                ConfigurationProperty configProperty = config.getProperty(name);
                String value = getConfigValue(name);
                if (StringUtil.isBlank(value)) {
                    if (configProperty.isRequired()) {
                        throw new ConnectorException(String.format("Missing required configuration option [%s] ", new Object[]{name}));
                    }

                    log.ok("Value not found for [{0}]", new Object[]{name});
                } else {
                    Class type = configProperty.getType();
                    Object cValue = TypeUtil.convertValueType(value, type);
                    configProperty.setValue(cValue);
                    //log.info("Setting [{0}] to [{1}]", new Object[]{name, cValue});
                }
            }
            log.ok("Return", new Object[0]);
        }

        private String getConfigValue(String name) {
            String ret = (String)this.itResource.get(name);
            log.ok("Got [{0}] from IT resource", new Object[]{name});
            if (StringUtil.isBlank(ret)) {
                ret = (String)this.resConfigLookup.get(name);
                log.ok("Got [{0}] from Config Lookup", new Object[]{name});
            }
            return ret;
        }
    }

}
