package org.nyumc.idm.tasks;

import org.nyumc.idm.tasks.ConnectorFactory;

import com.thortech.util.logging.Logger;

import java.io.FileNotFoundException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import oracle.iam.connectors.icfcommon.ConnectorOpHelper;
import oracle.iam.platform.OIMClient;

import org.identityconnectors.framework.api.ConnectorFacade;
import org.identityconnectors.framework.common.objects.AttributeBuilder;
import org.identityconnectors.framework.common.objects.AttributeUtil;
import org.identityconnectors.framework.common.objects.OperationOptionsBuilder;
import org.identityconnectors.framework.common.objects.ScriptContext;
import org.identityconnectors.framework.common.objects.ScriptContextBuilder;

import com.aptecllc.oim.api.OIMProperties;
import com.aptecllc.oim.exceptions.OIMHelperException;

/**
*
* @author foresfr
*/
public class ICFScriptRunner {
    
    private static Logger logger = Logger.getLogger(ICFScriptRunner.class.getName());
    
    private ConnectorFacade connFacade;
    private ConnectorOpHelper connectorOpHelper;
    
    //String URGENTERMSCRIPT = "powershell.exe -command Import-Module EYFunctions; Disable-EYLeaverAccess -Identity %OBJECTGUID%";
    String str = "__NAME__";
    String REMOTESCRIPT= "Powershell.exe -File C:\\scripts\\customhomedircreatescript.ps1 %sAMAccountName%  %credential% \"%dn%\"";
    String REMOTEENABLESCRIPT="Powershell.exe -File C:\\scripts\\customenablescript.ps1 %sAMAccountName% %status% %credential%";
    
    //String REMOTEENABLESCRIPT="/app/iam/adscripts/customenablescript.bat";

    public ICFScriptRunner() {
    }
    
    public void dummy()
    {
        
    }
    
    public void printhashmap(Map map)
    {
                System.out.println("Printing map"+map);
                Iterator<String> itr = map.values().iterator();
        while (itr.hasNext()) {
            System.out.println(itr.next());
        }
    }
    public void init(String url)
    {
                final String LOOKUPCONFIG = "Lookup.Configuration.ActiveDirectory";
        final String ITRESOURCE = "Active Directory-non-prod";
        final String CONNSERVER_ITRESOURCE = "CDCconnectorserver"; 
        Hashtable env = new Hashtable();
        env.put(OIMClient.JAVA_NAMING_FACTORY_INITIAL, "weblogic.jndi.WLInitialContextFactory");
        //env.put(OIMClient.JAVA_NAMING_PROVIDER_URL, "t3://iam1-oim-stage.nyumc.org:14000");
        env.put(OIMClient.JAVA_NAMING_PROVIDER_URL, url);
       OIMClient oimClient = new OIMClient(env);
        OIMProperties props;
                                try {
                                                props = new OIMProperties(oimClient);
                                      
       
        Map<String,String> configLookup = new HashMap<String, String>();
        Map<String,String> resourceDetails = new HashMap<String, String>();
        Map lookupconfig = props.getLookupProperties(LOOKUPCONFIG);
        printhashmap(lookupconfig);
        configLookup.putAll(lookupconfig);
        
        Map itconfig = props.getITResourceProperties(ITRESOURCE);
        printhashmap(itconfig);
        configLookup.putAll(itconfig);
        //config = props.getTaskProperties(JOBNAME);
        //logger.info("Task Props:" + config);
        //configLookup.putAll(config);
        Map connservconfig = props.getITResourceProperties(CONNSERVER_ITRESOURCE);
        resourceDetails.putAll(connservconfig); 
                
        icfConnect(configLookup,resourceDetails);
                                } catch (OIMHelperException e) {
                                                // TODO Auto-generated catch block
                                                e.printStackTrace();
                                }
                
    }
    
    public  String runremotePSScript(String oimGuid, String cred, String distinguishedname, String url, String action)
    {
                init(url);
                try{
                System.out.println("Inside runremotePSScript"+oimGuid+"-dn"+distinguishedname+"----Action----"+action);
        String[] parts = distinguishedname.split("~");
                                System.out.println("-----------------------> parts[0]"+parts[0]);
                                System.out.println("-----------------------> parts[1]"+parts[1]);
    
        Set attrSet = new HashSet();
        ScriptContextBuilder scb=null;
        attrSet.add(AttributeBuilder.build("sAMAccountName", oimGuid));
        attrSet.add(AttributeBuilder.build("credential", cred));
        attrSet.add(AttributeBuilder.build("status", action));
        
        String distName="CN="+oimGuid+","+parts[1];
        System.out.println("distinguished name ->"+distName);
        attrSet.add(AttributeBuilder.build("dn", distName));
        if(action.equalsIgnoreCase("Create")){
         scb = new ScriptContextBuilder("Shell",REMOTESCRIPT);
         scb.addScriptArgument("timing", "AFTER_CREATE");
         }
        else if(action.equalsIgnoreCase("Enable")){
            scb = new ScriptContextBuilder("Shell",REMOTEENABLESCRIPT);
            scb.addScriptArgument("timing", "AFTER_UPDATE");
            }
        
        System.out.println("scb"+scb);
        OperationOptionsBuilder oob = new OperationOptionsBuilder();
        System.out.println("oob"+oob);
        scb.addScriptArgument("attributes", AttributeUtil.toMap(attrSet));
        //scb.addScriptArgument("timing", "AFTER_CREATE");
        ScriptContext sc = scb.build();
        System.out.println("sc-->"+sc);
        //System.out.println("sc"+sc.getScriptText());
        //logger.debug(sc.getScriptText());
        //connFacade.runScriptOnConnector(sc, oob.build());
        Object retobj = connFacade.runScriptOnResource(sc, oob.build());
        System.out.println("retobj"+retobj.toString());
        String retCode = retobj.toString();

        if (retCode.endsWith("COMPLETED_SUCCESSFULLY"))
        {
               
            logger.error("Successful calling script");
            return "SUCCESS";
        }
        else return "FAILURE";
                }catch(Exception e)
                {
                                e.printStackTrace();
                                return "FAILURE";
                }
                
                //return "SUCCESS";
    }
    
    private void icfConnect(Map configLookup,Map resourceDetails)
    {
                System.out.println("before creating connFacade--"+configLookup+"--resourceDetails are--"+resourceDetails);
        connFacade = ConnectorFactory.createConnectorFacade(resourceDetails,configLookup);
        connectorOpHelper = new ConnectorOpHelper(connFacade);
        System.out.println("Facade Created");
    }
    
    /*
     * <GUID=34b97b353864394ca4742026c9c23cfc>
     * 34b97b35-3864-394c-a474-2026c9c23cfc
     */
    private String convertOimGuid(String oimGuid)
    {
        String oldGuid = oimGuid.replace("<GUID=", "");
        oldGuid = oldGuid.replace(">", "");
        if (oldGuid.length() < 32)
            return null;
        String newGuid = oldGuid.substring(0,8) + "-" +
                         oldGuid.substring(8,8+4) + "-" +
                         oldGuid.substring(12,12+4) + "-" +
                         oldGuid.substring(16,16+4) + "-" +
                         oldGuid.substring(20);
        return newGuid;
    }
}