package org.nyumc.idm.tasks;

import Thor.API.Exceptions.tcAPIException;
import Thor.API.Exceptions.tcColumnNotFoundException;
import Thor.API.Exceptions.tcFormNotFoundException;

import com.thortech.util.logging.Logger;
import com.thortech.xl.crypto.tcCryptoException;
import com.thortech.xl.crypto.tcCryptoUtil;

import Thor.API.tcResultSet;
import Thor.API.Exceptions.tcInvalidAttributeException;
import Thor.API.Exceptions.tcInvalidLookupException;
import Thor.API.Exceptions.tcInvalidValueException;
import Thor.API.Exceptions.tcNotAtomicProcessException;
import Thor.API.Exceptions.tcProcessNotFoundException;
import Thor.API.Exceptions.tcRequiredDataMissingException;
import Thor.API.Exceptions.tcUserNotFoundException;
import Thor.API.Operations.tcFormInstanceOperationsIntf;
import Thor.API.Operations.tcLookupOperationsIntf;
import Thor.API.Operations.tcUserOperationsIntf;

import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Interval;

import oracle.iam.identity.exception.NoSuchUserException;
import oracle.iam.identity.exception.SearchKeyNotUniqueException;
import oracle.iam.identity.exception.UserLookupException;
import oracle.iam.identity.exception.UserManagerException;
import oracle.iam.identity.exception.UserModifyException;
import oracle.iam.identity.exception.UserSearchException;
import oracle.iam.identity.exception.ValidationFailedException;
import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.api.UserManagerConstants;
import oracle.iam.identity.usermgmt.api.UserManagerConstants.AttributeName;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.identity.usermgmt.vo.UserManagerResult;
import oracle.iam.notification.api.NotificationService;
import oracle.iam.notification.exception.EventException;
import oracle.iam.notification.exception.MultipleTemplateException;
import oracle.iam.notification.exception.NotificationException;
import oracle.iam.notification.exception.NotificationResolverNotFoundException;
import oracle.iam.notification.exception.TemplateNotFoundException;
import oracle.iam.notification.exception.UnresolvedNotificationDataException;
import oracle.iam.notification.exception.UserDetailsNotFoundException;
import oracle.iam.notification.vo.NotificationEvent;
import oracle.iam.passwordmgmt.domain.generator.RandomPasswordGeneratorImpl;
import oracle.iam.platform.OIMClient;
import oracle.iam.platform.Platform;
import oracle.iam.platform.authz.exception.AccessDeniedException;
import oracle.iam.platform.entitymgr.vo.SearchCriteria;

public class NYUTasks {
	private  final Logger gLogger = Logger.getLogger(NYUTasks.class.getName());
    tcLookupOperationsIntf lookupOpsIntf = Platform.getService(tcLookupOperationsIntf.class);
    tcUserOperationsIntf userIntf=Platform.getService(tcUserOperationsIntf.class);
    //tcFormInstanceOperationsIntf oimFormUtility =(tcFormInstanceOperationsIntf) oimUtilityFactory.getUtility("Thor.API.Operations.tcFormInstanceOperationsIntf");
    tcFormInstanceOperationsIntf oimFormUtility = Platform.getService(tcFormInstanceOperationsIntf.class);
    tcResultSet rs =null;
               
    
    public String getDepartment(String lookupName, String academicdept,String admindept)
    {
    	String lookupKey = "";
    	String lookupCode = null;
    	String lookupValue = null;
    	if (academicdept!=null || academicdept.isEmpty() && (academicdept != admindept))
    	{
    		lookupKey= academicdept;
    	} 
    	else {
    		lookupKey= admindept;    
    	}
    	try {
    		lookupValue = lookupOpsIntf.getDecodedValueForEncodedValue(lookupName, lookupKey);
    		
    		
    	} catch (tcAPIException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	return lookupValue;
    }
    //get LDAP date
    
    //get Status
    
    

    
    public String getStatus(String oldStatus, String newStatus,String adColName, long pikey)
        {
    	System.out.println("------Inside getStatus method -coming from Connector--"+adColName);
    	System.out.println("Oldstatus"+oldStatus);
    	System.out.println("Newstatus"+newStatus);
    	String oldstatus = "";
    	String newstatus = "";
    	String returnCode = "";
    	if(oldStatus!=null && !oldStatus.isEmpty())
    	{
    		oldstatus= oldStatus;
    	}
    	if(newStatus!=null && !newStatus.isEmpty())
    	{
    		newstatus= newStatus;
    	}
    	if (oldstatus.equals("Disabled") && newstatus.equals("Active")){
    		System.out.println("Inside else block");
    	//returnCode = setFormFieldData(pikey, adColName, "Active");	
    	return "REHIRED";
    	} else if(newstatus.equals("Active") )
    	{
    		returnCode = setFormFieldData(pikey, adColName, "Active");
    		return "SUCCESS";
    	}
		return "FAILURE";
    	
    }
    
    // reset users password on disable 01/16/2018
    
    public String resetUserPasswordonDisable(String userlogin)
    {
	System.out.println("------Inside resetUserPasswordonDisable method -coming from Connector--"+userlogin);
	UserManager userManager = (UserManager) Platform.getService(UserManager.class);
	  try {
		//userManager.resetPassword(userlogin, true, true);
		
		RandomPasswordGeneratorImpl randomPasswordGenerator = new RandomPasswordGeneratorImpl();
		char new_pwd[] = randomPasswordGenerator.generatePassword(new User(null));
		userManager.changePassword(userlogin,new_pwd, true,false);
		System.out.println("User: "+userlogin+"'s password changed to->" + Arrays.toString(new_pwd));
		
		
		//userManager.res
	} catch (NoSuchUserException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (UserManagerException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (AccessDeniedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  System.out.println("Password for user " + userlogin	    + " has been reset successfully!");
	return "SUCCESS";
	
}
 // reset users password on disable 01/16/2018
    
    //return alternative email for winthrop users
    public String getAlternativeEmail(String company, String AlternateEmail, String AdminDept)
    {
    	System.out.println("Inside the Exchange pre-populate adapter to set the external email address");
    	if((company!=null && !company.isEmpty()) || (AdminDept!=null && !AdminDept.isEmpty()))
    	{
    		if(company.equalsIgnoreCase("WUH") || AdminDept.equalsIgnoreCase("Winthrop"))
    			return AlternateEmail;
    	}
    	return "";
    }
    
    public String getActivationStatus(String oldStatus, String newStatus,String adColName, long pikey)
    {
	System.out.println("------Inside getActivationStatus method -coming from Connector--"+adColName);
	System.out.println("Oldstatus"+oldStatus);
	System.out.println("Newstatus"+newStatus);
	String oldstatus = "";
	String newstatus = "";
	String returnCode = "";
	if(oldStatus!=null && !oldStatus.isEmpty())
	{
		oldstatus= oldStatus;
	}
	if(newStatus!=null && !newStatus.isEmpty())
	{
		newstatus= newStatus;
	}
	
	if(oldstatus.equals("Disabled") && newstatus.equals("Active") )
	{
		//This has already been handled
		System.out.println("Old status is Disabled and New Status is Active");
		return "SUCCESS";
	}
	else if (oldstatus.equals("Active") && newstatus.equals("Active")){
		System.out.println("Old status is Active and New Status is Active");
	returnCode = setFormFieldData(pikey, adColName, "Active");	
	return "SUCCESS";
	}
	return "FAILURE";
	
}
    //get LDAP date
    public String getLDAPDateFormat(String endDate)
    {
 
    	long formattedDate = 0;
    	Interval interval =null;
    	int year=0;
    	int month=0;
    	int day=0;
    	try{
    	    
    	    java.util.Calendar Calendar = null;
    	    System.out.println("end date passed from OIM is->"+endDate);
    	    //2016-02-20
    	    if (endDate!=null && !endDate.isEmpty()){
    		String[] output = endDate.split("-");
    		System.out.println(output[0]);
    		System.out.println(output[1]);
    		System.out.println(output[2]);
    	    year = Integer.parseInt(output[0]);
    	    month = Integer.parseInt(output[1]);
    	    day = Integer.parseInt(output[2]);
    	    }else return "0";
    	    //
    	    
    	    //DateTime LDAP_START_DATE = new DateTime(1601, 1, 1, 0, 0,0,0, DateTimeZone.UTC);
    	    DateTime LDAP_START_DATE = new DateTime(1601, 1, 1, 0, 0,0,0, DateTimeZone.UTC);
    	    
    	    //DateTime dt = new DateTime(year, month, day, 12, 0, 0, 0);
    	    DateTime dt = new DateTime(year, month, day, 23, 59, 0, 0);
    	    //DateTime dt = new DateTime(year, month, day, 24, 1, 0, 0);
    	    
    	    //dt.plusHours(11);
    	    //dt.plusMinutes(59);
            interval = new Interval(LDAP_START_DATE, dt);
            
    	    System.out.println("Interval is->"+interval);
    	    //
    	    Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
    	    c.clear();
    	    if(year<2016)
    	    {
    	    	return "0";
    	    	
    	    }else
    	    System.out.println("Just before setting year->"+year+"--Month:->"+month+"--day-->"+day);
    	    c.set(year, month, day);
    	    long t1 = c.getTimeInMillis();
    	    //c.set(1601, 0, 1);
    	    c.clear();
    	    c.set(1601, 1, 1);
    	    long t2 = c.getTimeInMillis();
    	    formattedDate = (t1 - t2) * 10000;
    	    System.out.println("formattedDate->"+formattedDate);
    		}catch(Exception e)
    		{e.printStackTrace();}
 	  	return String.valueOf(interval.toDurationMillis() * 10000);
    }
    public void modifyUser(SearchCriteria searchCriteria,
    		HashMap<String, Object> mhUpdateAttr)
    				throws ValidationFailedException, UserModifyException,
    				NoSuchUserException, AccessDeniedException, UserSearchException {
    	String methodName = "modifyUser";
    	//LOGGER.entering(getClass().getName(), methodName);
    	UserManager userService = null;
    	
    	userService = Platform.getService(UserManager.class);
    	
    	List<User> userSeach = userService.search(searchCriteria, null, null);
    	
    	if (userSeach.size() == 1) {
    		User modifyUser = new User(userSeach.get(0).getId(), mhUpdateAttr);
    		userService.modify(modifyUser);
    	}
    	//LOGGER.logp(Level.FINE, getClass().getName(), methodName,"User Modify Operation Complete");
    	//LOGGER.exiting(getClass().getName(), methodName);
    }
    
    // 03/05/2016 to copy end date from user def to AD process form 
    //there are three scenarios when an end date on a sponsored individual's end date can change
    //1) When Sec Admin extends end date to extend the contract
    //2) When sec admin updates the end date to enable a sponsored individual
    //3) When a Sponsored individual is rehired as employee and end date gets nullified
    public String changeEndDate(String enddate,long pikey)
    {
    	System.out.println("end date and pikey is $$$$$$->"+enddate+": pi key->"+pikey);
    	String epochenddate=null;
    	String returnCode = "FAILURE";
    	if(enddate!=null && !enddate.isEmpty()){
    		epochenddate = getLDAPDateFormat(enddate);
    		System.out.println("epochdate");
    		returnCode=setFormFieldData(pikey,"UD_ADUSER_ENDDATE",epochenddate);
    		System.out.println("Before returning NORMALUPDATESUCCESS");
    		return "NORMALUPDATESUCCESS";
    	}
    	epochenddate = getLDAPDateFormat(enddate);
		System.out.println("epochdate when rehire");
		returnCode=setFormFieldData(pikey,"UD_ADUSER_ENDDATE",epochenddate);
		System.out.println("Before returning REHIREUPDATESUCCESS");
    	return "REHIREUPDATESUCCESS";
    }
    
    //END
    
//--Dan's Code, Sprint 9  
    /**
     *---------------------Dan Donnelly, Added 12/21/2015
     *>>>>>>>>>>>AIA-179 fix, Change User Password flowing into AD
     *---->	Function to check if the usr_change_pwd_at_next_logon flag set 
     *		by OIM when a helpdesk admin resets a users password. If it's
     *		1, push the value of 1 down to AD, then reset the flag in OIM
     *---->	Maps to the 'Change User Must Change Password At Next Logon' 
     *		Process Task
     *					
     *		1) 	Search for User with that login, return usr_change_pwd_at...
     *		2) 	a. 	If flag is 1, helpdesk has requested change password,
     *				go to 3.
     *			b.	If flag is 0, return FAILURE code
     *		3)	Search for the form data
     *		4) 	Go through the form to find the value of UD_ADUSER_MUST (The flag to
     *			be toggled in AD)
     *		5)	a.	If ADFlag is 1, just reset the OIM usr_change flag to 0
     *			b. 	If ADFlag is 0, Change the ADFlag to 1, triggers update 
     *				adapter which sets OIM flag back to 0 on its own
     *
     *@param userlogin
     *@param piKey
     *@return String SUCCESS || FAILURE
     *
     * */
    public String checkChangePassword(String userlogin, long piKey){
    	System.out.println("\n\n>>>>>>>>>>Inside ChangePassword in adapter, User: "+userlogin+"\n\n");
    	UserManager usrMgr = Platform.getService(UserManager.class);
    	   	
    //------->Search Users
    	
    	Set<String> retAttrs = new HashSet<String>();
    	retAttrs.add("usr_change_pwd_at_next_logon");
    	
    	SearchCriteria srchCriteria = new SearchCriteria("User Login",userlogin, SearchCriteria.Operator.EQUAL);
    	List<User> retUsers = new ArrayList<User>();
    	
    	try {
			retUsers = usrMgr.search(srchCriteria, retAttrs, null);
		} catch (UserSearchException e) {
			System.out.println("\n\n>>>>>>!:User Search Exception in setChangePassword Adapter");
			e.printStackTrace();
		} catch (AccessDeniedException e) {
			System.out.println("\n\n>>>>>>!:User Search Exception in setChangePassword Adapter");
			e.printStackTrace();
		}
    	  
    //------->Check if the Change Logon Attribute is set to 1
    	System.out.println("Change Password Value: "+retUsers.get(0).getAttribute("usr_change_pwd_at_next_logon"));
    	if(retUsers.get(0).getAttribute("usr_change_pwd_at_next_logon").equals("1")){
    		System.out.println("\n\n---------------------->Attempting to change password in checkChangePassword\n\n");
    	
    	//------->If yes, change ADUser must change variable to 1
    		String adColumnName="UD_ADUSER_MUST";
    		
    		oimFormUtility = Platform.getService(tcFormInstanceOperationsIntf.class);
    		
    		//Get Result Set of fields from UD_ADUSER form instance, then search
    		tcResultSet results = getFormData(piKey);
    		
    		String flagValue = null;
    		try {
				flagValue = results.getStringValue(adColumnName);
			} catch (tcAPIException e) {
				System.out.println("tcAPIException");
				e.printStackTrace();
			} catch (tcColumnNotFoundException e) {
				System.out.println("Column Not Found in Search");
				e.printStackTrace();
			}
    		System.out.println("\n\n>>>>>>>>>>Value of the flag in UD_ADUSER TABLE: "+flagValue+"\n\n");
    		String returnCode = "FAILURE";
    		
    		if(flagValue.equals("1")){
    			System.out.println("Flag in AD is 1, flipping OIM flag");
    			//Added by Shine on 1/22/2016
    			
    			returnCode = setFormFieldData(piKey, adColumnName, "0");
    			System.out.println("ad column name updated to ZERO");
    			returnCode = setFormFieldData(piKey, adColumnName, "1");
    			System.out.println("ad column name updated to ONE");
    			//Ended by shine
    			returnCode = toggleResetPasswordFlag(userlogin);
    		}else if(flagValue.equals("0")){
    			System.out.println("Flag in AD is 0, setting AD Flag");
    			returnCode = setFormFieldData(piKey, adColumnName, "1");
    		}
    		
    		System.out.println("\n\n>>>>>>>>>>>>>>>>>>>>>>>>>RETURNING "+returnCode+"\n\n");
    		return returnCode;
    		
    	}//endif usr_change_pwd_at_next_logon == 1
    	
	//------->Else, return failure
    	else{
    		System.out.println("\nRETURNING FAILURE IN CHECKCHANGEPASSWORD");
    		return "FAILURE";
    	}
    }
    /**
     *---------------------Dan Donnelly, Added 12/21/2015
     *>>>>>>>>>>>AIA-179 fix, Change User Password flowing into AD
     * ---->Function to zero the usr_change_pwd_at_next_logon 
     * 		flag when the value of that flag is pushed into AD.
     * ---->Maps to the 'NYU Clear Reset Password Flag' Process Task
     * 
     * @param userlogin
     * @return SUCCESS
     */
    public String toggleResetPasswordFlag(String userlogin){
    	System.out.println("\n\n>>>>>>>>>>>>>>>>>>>>>>>>...Inside of togglePasswordResetFlag in Adapter\n\n");
    	UserManager usrMgr = Platform.getService(UserManager.class);
    	
    	HashMap<String, Object> attrs = new HashMap<String, Object>();
		attrs.put("usr_change_pwd_at_next_logon", "0");
		
		User user = new User(userlogin, attrs);
		System.out.println("\n\n--------------->Attempting to modify the user in toggleResetPasswordFlag, Login is: "+userlogin+" \n\n");
		
		try {
			usrMgr.modify(UserManagerConstants.AttributeName.USER_LOGIN.getId(), userlogin, user);
		} catch (ValidationFailedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UserModifyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchUserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AccessDeniedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SearchKeyNotUniqueException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	return "SUCCESS";
    }
    
    /**
     *----------------Dan Donnelly, 12/22/2015
     * --->	Helper Method to return the process form data 
     * 		for a table
 	 *
     * @param piKey
     * @return tcResultSet from piKey
     */
    private tcResultSet getFormData(long piKey){
    	tcResultSet retSet = null;
    	try {
			retSet = oimFormUtility.getProcessFormData(piKey);
		} catch (tcAPIException e) {
			System.out.println("API Exception in get Results");
			e.printStackTrace();
		} catch (tcNotAtomicProcessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (tcFormNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (tcProcessNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return retSet;
    }
    
    /**
     *----------------Shine Thomas, 08/07/2017
     * --->	Helper Method to return the process form data value
     * 		for a table column
 	 *
     * @param piKey, tcColumn
     * @return String Value from piKey and tcColumn
     */
    private String getFormDataForColumn(long piKey, String strColumnName){
    	System.out.println("Inside getFormDataForColumn and the column passed is-> "+strColumnName);
    	tcResultSet trs = null;
    	try {
			trs = oimFormUtility.getProcessFormData(piKey);
			int count=trs.getRowCount();
			for(int i=0;i<count;i++){
			                trs.goToRow(i);
			 
			        String columnNames[] = trs.getColumnNames();
			        for (String string : columnNames) 
			        {
			            try {
			                System.out.println(string + " - " + trs.getStringValue(string));
			                if(string.equalsIgnoreCase(strColumnName)){
			                	return trs.getStringValue(string);
			                }
			                		
			            } catch (tcAPIException | tcColumnNotFoundException e)
			           {
			                e.printStackTrace();
			            }
			        }
			}
		} catch (tcAPIException e) {
			System.out.println("API Exception in get Results");
			e.printStackTrace();
		} catch (tcNotAtomicProcessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (tcFormNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (tcProcessNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return "";
    }
    
    public String setSponsor(long piKey, String columnName, String value){
    	System.out.println("Inside setSponsor method"+value);
    	value=value.trim();
    	try{
    	return setFormFieldData(piKey,columnName,value);
    	}catch (Exception e)
    	{
    		return "FAILURE";
    	}
    }
    /**
     *----------------Shine Thomas & Dan Donnelly, 12/22/2015
     * --->	Helper Method to set a column value in a 
     * 		process form
     * 
     * @param piKey
     * @param columnName
     * @param value
     * @return SUCCESS 
     */
    public String setFormFieldData(long piKey, String columnName, String value){
    	System.out.println("\n>>>>setProcessFormData>>>In set form Data\n\t PIKey"+piKey+" Column "+columnName+" Value "+value+"\n\n");
    	
    	oimFormUtility = Platform.getService(tcFormInstanceOperationsIntf.class);
		Map updateData=new HashMap();
		updateData.put(columnName, value);
		
		try {
			oimFormUtility.setProcessFormData(piKey, updateData);
			System.out.println("\n>>>>>>>>>>>Column "+columnName+" Updated");
		} catch (tcAPIException e) {
			System.out.println("\n\ntcAPIException in setProcessFormData");
			e.printStackTrace();
		} catch (tcInvalidValueException e) {
			System.out.println("\n\ntcInvalidValueException in setProcessFormData");
			e.printStackTrace();
		} catch (tcNotAtomicProcessException e) {
			System.out.println("\n\ntcNotAtomicException in setProcessFormData");
			e.printStackTrace();
		} catch (tcFormNotFoundException e) {
			System.out.println("\n\ntcFormNotFoundException in setProcessFormData");
			e.printStackTrace();
		} catch (tcRequiredDataMissingException e) {
			System.out.println("\n\ntcRequiredMissingDataException in setProcessFormData");
			e.printStackTrace();
		} catch (tcProcessNotFoundException e) {
			System.out.println("\n\ntcProcessNotFoundException in setProcessFormData");
			e.printStackTrace();
		}
		return "SUCCESS";
    }
//EndDan  
    
    public String returnEndDate(Date enddate)
    {
       Date date=null;
       Date parseDate = null;
       Date todayDate = new Date();
       System.out.println("Inside -- enddate from PIMS Prepopulate connector is:"+enddate);
       if(enddate.before(todayDate)) {
    	   System.out.println("When the end date is in the past");
    	   return "";
    	   }
       if(enddate != null && enddate.after(todayDate)){
    	   
    	   long milliseconds = enddate.getTime();
           System.out.println("When end date is in the future and formatted date is"+milliseconds);

              return String.valueOf(milliseconds);
                     }
       else{return "";}
       
    }
    //returns the termination date
    public String returntermdate(String enddate, long pikey)
    {
    	
    	System.out.println("Termination date passed for employee from HR and pikey is ->"+enddate+": pi key->"+pikey);
    	
    	String returnCode = "FAILURE";
    	if(enddate!=null ){
    		
    		
    		String retdate=setDateFormat(enddate);
    		System.out.println("termdate  for user"+retdate);
    		returnCode=setFormFieldData(pikey,"UD_ADUSER_EMPTERMDATE",retdate);
    		System.out.println("Before returning NORMALUPDATESUCCESS");
    		return "SUCCESS";
    	}
    	
		System.out.println("termdate when rehire, should be set to 0");
		returnCode=setFormFieldData(pikey,"UD_ADUSER_EMPTERMDATE","0");
		System.out.println("Before returning REHIREUPDATESUCCESS");
    	return "SUCCESS";
    }
    
    
  //returns the rehire date
    public String returnrehiredate(String rehiredate, long pikey)
    {
    	
    	System.out.println("Termination date passed for employee from HR and pikey is ->"+rehiredate+": pi key->"+pikey);
    	
    	String returnCode = "FAILURE";
    	
    	DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		Date today = Calendar.getInstance().getTime();        
		String rehireDate = df.format(today);
		System.out.println("Rehire Date when user is rehired: " + rehireDate);
    	
    	if(rehireDate!=null ){
    		
    		
    		//String retdate=setDateFormat(rehireDate);
    		System.out.println("rehire date  for user"+rehireDate);
    		returnCode=setFormFieldData(pikey,"UD_ADUSER_REHIREDATE",rehireDate);
    		System.out.println("Before returning rehire date to AD form");
    		return "SUCCESS";
    	}
    	
		System.out.println("termdate when rehire, should be set to 0");
		returnCode=setFormFieldData(pikey,"UD_ADUSER_REHIREDATE","0");
		System.out.println("Before returning REHIREUPDATESUCCESS");
    	return "SUCCESS";
    }
    
    
    public String setStartDate(String strstartDate,String userlogin)
    {
    	System.out.println("Inside setStartDate in the adapter "+strstartDate+"-->"+userlogin);
    	UserManager usrMgr = Platform.getService(UserManager.class);
    	HashMap<String, Object> attrs = new HashMap<String, Object>();
    	SimpleDateFormat sdf   = new SimpleDateFormat("MMM dd, yyyy");
    	Date date=null;
    	try {
    		date = sdf.parse(strstartDate);
    	} catch (ParseException e1) {
    		// TODO Auto-generated catch block
    		e1.printStackTrace();
    	}
    	attrs.put("Start Date",date);
    	
    	
    	/*SearchCriteria currentUserSC = new SearchCriteria(UserManagerConstants.AttributeName.USER_LOGIN.getId(),userlogin,SearchCriteria.Operator.EQUAL);
                                  HashMap<String, Object> mhUpdateAttr = new HashMap<String, Object>();
                                  	mhUpdateAttr.put("Start Date",sdf.format(new Date(strstartDate)));*/
                                  	
                                   	
                                  //	oimUserUtils.modifyUser(currentUserSC, mhUpdateAttr);
    	
    	
    	
    	User retUser = new User(userlogin, attrs);
    	System.out.println("\n\n>>>>>>>>>>>>>>>>User to Modify"+retUser.toString());
    	UserManagerResult res = null;
    	try {
    		System.out.println("\n\n>>>>>>>>>>>>>>>>Before");
    		res = usrMgr.modify(UserManagerConstants.AttributeName.USER_LOGIN.getId(), userlogin, retUser);
    		System.out.println("\n\n>>>>>>>>>>>>>>>>After");
    	} catch (ValidationFailedException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	} catch (UserModifyException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	} catch (NoSuchUserException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	} catch (SearchKeyNotUniqueException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	} catch (AccessDeniedException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	
    	return "SUCCESS";
    }
    
    public String getCustomStatusforEnable(String usrKey)
    
    {
    	
    	System.out.println("getLdapManagerDN");
    	System.out.println("getLdapManagerDN usrKey:" +usrKey);
    	
    	String managerKey = "";
    	long procInsKey = -1;
    	String managerID = null;
    	String orgName = null;
    	String customStatus = null;
    	String LDAPUSER_OBJECT_NAME = "AD User";
    	String PROCESS_INSTANCE_KEY = "Process Instance.Key";
    	String LDAP_USER_CUSTOM_STATUS = "UD_ADUSER_CUSTOMSTATUS";
    	//String LDAP_USER_ORGNAME = "UD_LDAP_USR_ORGANIZATION";
    	
    	try {
    		
    		
    		tcResultSet resultSet = userIntf.getObjects(Long.parseLong(usrKey));
    		
    		for (int j = 0; j < resultSet.getRowCount(); j++) {
    			resultSet.goToRow(j);
    			String[] columnNames = resultSet.getColumnNames();
    			
    			for (int i = 0; i < columnNames.length; i++) {
    				
    				if (resultSet.getStringValue(columnNames[i]).equalsIgnoreCase(LDAPUSER_OBJECT_NAME)){
    					System.out.println("Found AD User Object. getting the process Ins Key");
    					
    					procInsKey = resultSet.getLongValue(PROCESS_INSTANCE_KEY);
    					System.out.println("procInsKey is:"+procInsKey+"'" );
    					
    					tcResultSet processFormDataRS = oimFormUtility.getProcessFormData(procInsKey);
    					System.out.println("processFormDataRS row count is"+processFormDataRS.getRowCount()+"'" );
    					
    					String[] processFormColumnNames = processFormDataRS.getColumnNames();
    					for (int x = 0; x < processFormColumnNames.length; x++) {
    						
    						if (processFormColumnNames[x].equalsIgnoreCase(LDAP_USER_CUSTOM_STATUS)){
    							customStatus = processFormDataRS.getStringValue(processFormColumnNames[x]);
    							System.out.println("customStatus value is: "+customStatus);
    						}
    						
    						
    					} // END - for (int x = 0; x < processFormColumnNames.length; i++) {               
    				} // END - if (resultSet.getStringValue(columnNames[i]).equalsIgnoreCase(ADUSER_OBJECT_NAME)){
    			} // END - for (int i = 0; i < columnNames.length; i++) {
    		} // END - for (int j = 0; j < resultSet.getRowCount(); j++) {
    		
    		
          	}catch (tcAPIException ae){
          		System.out.println("AP-PP-1-000001 : tcAPIException occured"+ ae);
        	  
          	}catch (tcColumnNotFoundException cnfe){
          		System.out.println("AP-PP-1-000002 : 'tcColumnNotFoundException' occured"+cnfe);
        	  
          	}catch (tcUserNotFoundException unfe){
          		System.out.println("AP-PP-1-000003 : 'tcUserNotFoundException' occured"+ unfe);
                         
          	} catch (tcNotAtomicProcessException nape){
          		System.out.println("AP-PP-1-000004 : 'tcNotAtomicProcessException' occured"+ nape);
                         
          
          	} catch (tcProcessNotFoundException pnfe){
          		System.out.println("AP-PP-1-000006 : 'tcProcessNotFoundException' occured"+pnfe);               
                         
          	} catch (AccessDeniedException ade){
          		System.out.println("AP-PP-1-000008 : 'AccessDeniedException' occured"+ ade);
                         
          	} catch (tcFormNotFoundException fnfe){
          		System.out.println("AP-PP-1-000010 : 'tcFormNotFoundException' occured"+fnfe);
                         
          	} catch (Exception e){
          		System.out.println("AP-PP-1-000011 : 'Exception' occured"+e);
                         
          	}

          	System.out.println("Exiting");
          	if(customStatus.equalsIgnoreCase("Terminated")){
          		System.out.println("Returning Success");
          		return "Success";}
          	else{
          		System.out.println("Returning Failure");
          		return "Failure";}

    }
    public String getClassYear()
    {
    	GregorianCalendar cal      = new GregorianCalendar();
    	System.out.println("cal object is:"+cal.getWeekYear());
    	int i=cal.getWeekYear();
    	i=i+4;
    	String strClassYear = Integer.toString(i);
    	System.out.println("cal object is Plus 4 years:"+i);
    	System.out.println("cal object in string is Plus 4 years:"+strClassYear);
    	return strClassYear;
    }
                  
    public String descryptSSN(String strSSN,String userlogin)
    {
    	
                                  
    	String decryptssn ="";
    	//System.out.println("userType is"+userType);
    	
    	if(strSSN!=null && strSSN.length()>10)
    	{
    		System.out.println("Encrypted SSN is for Employees:"+strSSN);
    		try {
    			decryptssn = tcCryptoUtil.decrypt(strSSN,"DBSecretKey" );
    			System.out.println("check value is:"+decryptssn);
    		} catch (tcCryptoException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    			
    		}
    	}
    	else if (strSSN!=null && strSSN.length()==9) {
    		
    		return strSSN;
    		
    	}
    	return decryptssn;
    	
                                  
    }
    
    
    public String setNaturalPerson(String userLogin){
    	
    	System.out.println("Before setting the extAttribute16 to in the AD form");
    	  	return "Natural Person";
    }
    
    public String setDisplayName(String strFirstName,String strLastName)
    {
                                  
                                  
    	String strconcatName ="";
    	strconcatName =           strLastName+", "+strFirstName;
    	System.out.println("strconcatName to show in display name is:"+strconcatName);
    	return strconcatName;
                                  
    }
    
    public String setSharePointDisplayName(String strFirstName,String strLastName)
    {
                                  
                                  
    	String strconcatSPName ="";
    	strconcatSPName =strFirstName+" "+strLastName;
    	System.out.println("strconcatName to show in setSharePointDisplayName is:"+strconcatSPName);
    	return strconcatSPName;
                                  
    }
                  
    public String setDateFormat(String strDate)
    {
    	System.out.println("Inside setDate Format for Active Directory date is:" + strDate);
    	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
    	Date tempDate = new Date();
    	try
    	{
    		tempDate = format.parse(strDate);
    	}
    	catch (ParseException e)
    	{
    		e.printStackTrace();
    		return null;
    	}
    	String formattedDate = new SimpleDateFormat("MM/dd/yyyy").format(tempDate);
    	System.out.println("date is: " + formattedDate);
    	return formattedDate;
    }
    
    public String setSuspendedFlag(String suspendFlag, Long pikey)
    {
    	System.out.println("Inside setSuspendedFlag for suspending user:" + suspendFlag);
    	String strInitialSuspend="";
    	String strEnableAfterSuspend="";
    	String returnCode="";
    	if (suspendFlag!=null)
    	{
    		
    		if (suspendFlag.equalsIgnoreCase("Y") || suspendFlag.equalsIgnoreCase("YES")){
    			returnCode=setFormFieldData(pikey,"UD_ADUSER_INITIAL_SUSPEND","SUSPENDED");
    			if(getFormDataForColumn(pikey,"UD_ADUSER_ENABLE_AFTER_SUSPEND").equalsIgnoreCase("REENABLED"))
    					{
    			returnCode=setFormFieldData(pikey,"UD_ADUSER_ENABLE_AFTER_SUSPEND","");
    			}
    			return "SUSPEND";}
    		else if (suspendFlag.equalsIgnoreCase("N")|| suspendFlag.equalsIgnoreCase("NO")){
    			if(getFormDataForColumn(pikey,"UD_ADUSER_INITIAL_SUSPEND").equalsIgnoreCase("SUSPENDED")){
    			returnCode=setFormFieldData(pikey,"UD_ADUSER_INITIAL_SUSPEND","");
    			}
    			returnCode=setFormFieldData(pikey,"UD_ADUSER_ENABLE_AFTER_SUSPEND","REENABLED");
    			return "REENABLE";}
    		else if (suspendFlag.equalsIgnoreCase("T") || suspendFlag.equalsIgnoreCase("TER")){
    			returnCode=setFormFieldData(pikey,"UD_ADUSER_TERMINATE_AFTER_SUSP","TERMINATED");
    			return "TERMINATE";}
    			
    	}
    	return "FAILURE";
    }
    
    public String checkFlagStatustoTriggerTasks(String flagValue)
    {
    	System.out.println("Inside checkFlagStatustoTriggerTasks for suspending user:" + flagValue);
    	String returnCode="";
    	if (flagValue!=null && !flagValue.isEmpty())
    	{
    		return "SUCCESS";
    			
    	}
    	return "FAILURE";
    }
    

    public String setUID(String strUsrLogin)
    {
    	String lowerLogin = strUsrLogin.toLowerCase();
    	String completeUID = lowerLogin + "@nyumc.org";
    	System.out.println("strconcatName to show in display name is: " + completeUID);
    	return completeUID;
    }

    public String concateUserPrincipal(String userLogin, String domainName)
    {
    	System.out.println("Entering concateUserPrincipal with domainName "+ domainName + ", userLogin "+userLogin);
    	if(userLogin==null || userLogin.isEmpty()) return null;
    	
    	String lowerLogin = userLogin.toLowerCase();
    	String completeUID = lowerLogin + "@" + domainName;
    	System.out.println("Exiting UserPrincipalName "+completeUID);
    	return completeUID;
    }

    
    
    public String descryptSSNforAD(String strSSN)
    {
    	String decryptSSN ="";
    	String truncatedSSN="";
    	System.out.println("Encrypted SSN is:"+strSSN);
    	try {
    		decryptSSN = tcCryptoUtil.decrypt(strSSN,"DBSecretKey" );
    		System.out.println("Decrypted SSN is:"+decryptSSN);
    		truncatedSSN=decryptSSN.substring(decryptSSN.length()-4, decryptSSN.length());
    		System.out.println("Decrypted and truncated SSN is:"+truncatedSSN);
    		
    	} catch (tcCryptoException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	return truncatedSSN;
    }
                  
    public String getAndIncrementUID(String lookupName, String lookupkey)
    {
    	String lookupKey = "";
    	String lookupCode = null;
    	String lookupValue = null;
    	
    	try {
    		lookupValue = lookupOpsIntf.getDecodedValueForEncodedValue(lookupName, lookupkey);
    		System.out.println("Lookup name and lookup key is"+lookupName+":"+lookupkey);
    		System.out.println("Current value in lookup.NYU.UID is"+lookupValue);
    		int y = Integer.parseInt(lookupValue);
    		y=y+1;
    		String x = Integer.toString(y);
    		System.out.println("Incremented value is"+x);
    		//lookupOpsIntf.addLookupValue("<>", lookupkey,x, "", "");
    		//lookupOpsIntf.addLookupValue(lookupName, lookupkey, x, Locale.getDefault().getLanguage(), Locale.getDefault().getCountry());
    		Map<String,String> attrMap = new HashMap<String,String>();
    		attrMap.put("Lookup Definition.Lookup Code Information.Decode", x);
    		lookupOpsIntf.updateLookupValue(lookupName, lookupkey, attrMap);
    			
                                                                
    	} catch (tcAPIException | tcInvalidLookupException | tcInvalidValueException | tcInvalidAttributeException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	return lookupValue;
    }
     
    public String truncateSSNforAD(String strSSN, String strUsrLogin)
    {
    	String decryptssn ="";
    	String truncatedSSN="";
    	System.out.println("Encrypted SSN is:"+strSSN);
    	if (strSSN!=null && !strSSN.isEmpty()){
    	try {
    		decryptssn = descryptSSN(strSSN, strUsrLogin);
    		System.out.println("Decrypted SSN is:"+decryptssn);
    		truncatedSSN=decryptssn.substring(decryptssn.length()-4, decryptssn.length());
    		System.out.println("Decrypted and truncated SSN is:"+truncatedSSN+"for user"+strUsrLogin);
    		
    	} catch (Exception e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	}
    	return truncatedSSN;
    }
    
       
    public String getAttributeValuefromUserLogin(String usrLogin, String returnAttribute)
    { 
    	System.out.println("Inside getAttributeValuefromUserLogin->"+usrLogin+","+returnAttribute); 
    	UserManager userService = Platform.getService(UserManager.class);
    	List<User> users = null;
    	String retAttrValue = null;
    	try {
    		
    		Set <String>retAttributes = new HashSet<String>();  
    		retAttributes.add(returnAttribute);  
    		
    		//SearchCriteria srchCriteria = new SearchCriteria("EmployeeID",fieldvalue, SearchCriteria.Operator.EQUAL);
    		
    		SearchCriteria srchCriteria = new SearchCriteria("User Login",usrLogin, SearchCriteria.Operator.EQUAL);
    		//AttributeName.USER_KEY.getId()
    		//User user = userService.getDetails(userMgr, null, true);
    		
    		UserManager mgr = Platform.getService(UserManager.class);  
    		
    		
    		
    		users = mgr.search(srchCriteria, retAttributes, null);
    		
    		
    		if(users != null && users.size()!=0)
    		{
    			System.out.println("User Login  : " + usrLogin + ":returnAttribute"+returnAttribute+"  --  Return Attribute Value  : " + users.get(0).getAttribute(returnAttribute));
    			retAttrValue = users.get(0).getAttribute(returnAttribute).toString();
    		}
    		
    		
    	} catch (oracle.iam.platform.authz.exception.AccessDeniedException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	} catch (UserSearchException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	
    	
    	
    	return retAttrValue;
    }
    
    public String notifyAdmin(String toField, String fromField,String attributeName, String userLogin)
    {
    	NotificationService notifServ = Platform.getService(NotificationService.class);
    	System.out.println("Before tNotificationEvent");
    	ArrayList tolist = new ArrayList();
    	NotificationEvent studentNotificationEvent = createNotificationEvent("Attribute Updated Email",tolist,"xelsysadm",attributeName,userLogin);
    	try {
    		notifServ.notify(studentNotificationEvent);
    	} catch (UserDetailsNotFoundException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	} catch (EventException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	} catch (UnresolvedNotificationDataException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	} catch (TemplateNotFoundException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	} catch (MultipleTemplateException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	} catch (NotificationResolverNotFoundException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	} catch (NotificationException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	System.out.println("Email sent");
    	
    	
    	return "SUCCESS";
    }
    
    private NotificationEvent createNotificationEvent(String poTemplateName, ArrayList<String> receiverLogins, String attributeName, String userLogin,String senderLogin) {
    	String methodName = "createNotificationEvent";
    	//logger.logp(Level.FINE, getClass().getName(), methodName, "Entered. poTemplateName=" +poTemplateName + " " + "receiverLogins=" + receiverLogins + " " + senderLogin);
    	System.out.println("Entered. poTemplateName=" +poTemplateName + " " + "receiverLogins=" + receiverLogins + " " + senderLogin);
    	NotificationEvent event = new NotificationEvent();
    	String[] receiverUserIds = new String[receiverLogins.size()];
    	receiverUserIds = receiverLogins.toArray(receiverUserIds);
    	event.setUserIds(receiverUserIds);
    	event.setTemplateName(poTemplateName);
    	event.setSender(senderLogin);
    	HashMap<String, Object> templateParams = new HashMap<String, Object>();
    	templateParams.put("UserId", senderLogin);
    	gLogger.info("Template Params:"+templateParams);
    	event.setParams(templateParams);
    	gLogger.info("Exited.");
    	return event;
   }	
    
    
    public String getmailboxDatabase(String lookupName)
    {
    	
    	gLogger.info("got lookup service");
    	System.out.println("Inside getrandommailbox");
    	String lookupCode = null;
    	String lookupValue = null;
    	List<String> list = new ArrayList<String>();
    	Random random = new Random();
    	int index=0;
    	
    	try {
    		
    		
    		rs= lookupOpsIntf.getLookupValues(lookupName);
    		int count=rs.getRowCount();
    		System.out.println("Count is->"+count);
    		
    		
    		if ((rs != null) && (rs.getRowCount() > 0)) 
    		{
    			for (int i = 0; i < rs.getRowCount(); i++) 
    			{
    				rs.goToRow(i);
    				lookupCode = rs.getStringValue("LKV_ENCODED");
    				lookupValue =rs.getStringValue("LKV_DECODED");
    				System.out.println("lookupValues are:"+lookupValue);
    				list.add(lookupValue);
    				
    			}
    		}
    		index = random.nextInt(list.size());
    		System.out.println("\nIndex :" + index );
    		System.out.println("Just before returning the mail database 1-->"+list.get(index));
    		return list.get(index); 
    		
    	} catch (tcAPIException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	} catch (tcInvalidLookupException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	} catch (tcColumnNotFoundException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	
    	
        System.out.println("Just before returning the mail database 2"+list.get(index));                          
    	return list.get(index); 
    }
                  
    public String getLowerCaseCN(String UserLogin)
    {
    	
    	
    	System.out.println("Inside getLowerCaseCN"+UserLogin.toLowerCase());
    	
    	return UserLogin.toLowerCase(); 
    }
    
    public String getStringConcat(String str1, String str2)
    {
    	
    	
    	
    	
    	return str1.concat(str2); 
    }
    
    public String removeADGrouponDisable(long  processInstanceKey, String chTableName,String bounceGroup){
    	System.out.println("ProcessTaskAdapters removeAcceptOnlyFromDLGroup");
    	String result = "FAILED";
    	tcFormInstanceOperationsIntf oimFormUtility = null; 
    	Map<String,Long> childData= new HashMap<String,Long>();
    	//List childData = new ArrayList();
    	
    	try {
    		oimFormUtility = Platform.getService(tcFormInstanceOperationsIntf.class);
    		tcResultSet childFormDef = oimFormUtility.getChildFormDefinition(oimFormUtility.getProcessFormDefinitionKey(processInstanceKey),-1);
    		int formCount = childFormDef.getRowCount();
    		System.out.println("formCount is:"+formCount);
    		//System.out.println(Level.FINEST, getClass().getName(), "removeAcceptOnlyFromDLGroup:","Child Form Count for Process instance key '"+processInstanceKey+"'  : '" + formCount + "'");
    		for(int i = 0; i < childFormDef.getRowCount(); i++){
    			childFormDef.goToRow(i);
    			long childFormKey = childFormDef.getLongValue("Structure Utility.Child Tables.Child Key");
    			int version = childFormDef.getIntValue("Structure Utility.Child Tables.Child Version");
    			String childTableName = childFormDef.getStringValue("Structure Utility.Table Name");
    			System.out.println("childTableName"+childTableName);
    			//LOGGER.logp(Level.FINEST, getClass().getName(), "removeAcceptOnlyFromDLGroup:","Table Name: '"+childTableName+"'"+" Version: '"+version+"' Child Form Key: '"+childFormKey +"'");
    			if(childTableName.equals(chTableName)){
    				tcResultSet childFormData = oimFormUtility.getProcessFormChildData(childFormKey, processInstanceKey);
    				System.out.println("childFormData size is:"+childFormData.getTotalRowCount());
    				for (int k = 0 ; k<childFormData.getRowCount();k++)  {  
    					childFormData.goToRow(k);  
    					long childFormPKey = childFormData.getLongValue(childTableName + "_KEY");  
    					String groupName=childFormData.getStringValue("UD_EX_CH_DISTRIBUTIONGROUP");
    					System.out.println("Distribution groups reconciled from Exchange--> "+groupName);
    				    long groupKey = childFormData.getLongValue("UD_EX_CH_KEY");
    				    //childData.put(groupName, groupKey);
    					//code on 02/27/2017
    					//String childtablename=childFormData.getStringValue(childTableName);
    					//System.out.println("Child Table values are :"+childtablename);
    				    //34~CN=Email Bounce For Terminated Users,OU=Distribution List,OU=Exchange,DC=nyumctest,DC=org
    					if(groupName.equalsIgnoreCase(bounceGroup))
    					{
    					System.out.println("before removing the group:"+bounceGroup+": from exchange");
    					oimFormUtility.removeProcessFormChildData(childFormKey,childFormPKey );
    					}
    					
    				}  
    				result ="SUCCESS";
    				break;
    			}              
    		}
    	} catch (Exception e) {
    		//LOGGER.logp(Level.SEVERE, getClass().getName(), "removeAcceptOnlyFromDLGroup:", "AP-PT-10-000001 :Exception encountered.",e);
    	} finally {
    		if(oimFormUtility!=null)
    			oimFormUtility.close();
    	}
    	return result;
    }
    public String addGroupsFromLookup(long procInstKey, String chTableName, String userType, String EmailRequired){
    	String retValue ="SUCCESS";
    	String lookupName = "";
    	String lookupCode = "";
    	String lookupValue = "";
    	List li = new ArrayList();
    	tcFormInstanceOperationsIntf oimFormUtility = null;  
    	try {
    		oimFormUtility = Platform.getService(tcFormInstanceOperationsIntf.class);
    		tcResultSet childFormDef = oimFormUtility.getChildFormDefinition(oimFormUtility.getProcessFormDefinitionKey(procInstKey),-1);
    		for(int i = 0; i < childFormDef.getRowCount(); i++){
    			childFormDef.goToRow(i);
    			long childFormKey = childFormDef.getLongValue("Structure Utility.Child Tables.Child Key");
    			String childTableName = childFormDef.getStringValue("Structure Utility.Table Name");
    			if(childTableName.equals(chTableName)){
    				HashMap<String,String> attrChildData = new HashMap<String,String>();
    				//OIMUtils oimUtils = new OIMUtils();
    				if(!userType.equalsIgnoreCase("Contractor"))
    					lookupName="NYU.Lookup.EmployeeADGroup";
    				else if (userType.equalsIgnoreCase("Contractor") && EmailRequired.equalsIgnoreCase("1") )
    					lookupName = "NYU.Lookup.ContractorADGroup";
    				else
    					return retValue;
    				System.out.println("LookupName based on the usertype is "+lookupName);
    				tcResultSet rs= lookupOpsIntf.getLookupValues(lookupName);
    				int count=rs.getRowCount();
    				System.out.println("Count is->"+count);
    				
    				
    				if ((rs != null) && (rs.getRowCount() > 0)) 
    				{
    					for (int j = 0; j< rs.getRowCount(); j++) 
    					{
    						rs.goToRow(j);
    						lookupCode = rs.getStringValue("LKV_ENCODED");
    						lookupValue =rs.getStringValue("LKV_DECODED");
    						li.add(rs.getStringValue("LKV_DECODED"));
    						
    						
    					}
    				}
    				for (int k=0;k<li.size();k++)
    				{
    					System.out.println("Lookup Value added each time is "+k+":"+li.get(k));            
    					addChildTableData(procInstKey,childFormKey,li.get(k).toString(),"UD_ADUSRC_GROUPNAME",oimFormUtility);
    				}
    				
    				
    				
    				//attrChildData= oimUtils.getLookupInfo(lookupName);
    				
    			} 
    		}
    	} catch (Exception e) {
    		System.out.println("Error Occurred while provisioning Entitilements to User with ORC  Key");
    		retValue = "FAILED";
    	} finally {
    		if(oimFormUtility != null)
    			oimFormUtility.close();
    	}
    	return retValue;
    }
    
    
    // Code to add user to bounce group upon disable
    
    public String addBounceGroupUponDisable(long procInstKey, String chTableName, String userType, String lookupName){
    	String retValue ="SUCCESS";
    	//String lookupName = "";
    	String lookupCode = "";
    	String lookupValue = "";
    	List li = new ArrayList();
    	tcFormInstanceOperationsIntf oimFormUtility = null;  
    	try {
    		oimFormUtility = Platform.getService(tcFormInstanceOperationsIntf.class);
    		tcResultSet childFormDef = oimFormUtility.getChildFormDefinition(oimFormUtility.getProcessFormDefinitionKey(procInstKey),-1);
    		for(int i = 0; i < childFormDef.getRowCount(); i++){
    			childFormDef.goToRow(i);
    			long childFormKey = childFormDef.getLongValue("Structure Utility.Child Tables.Child Key");
    			System.out.println("childFormKey in addBounceGroupUponDisable: "+childFormKey);
    			String childTableName = childFormDef.getStringValue("Structure Utility.Table Name");
    			System.out.println("childTableName in addBounceGroupUponDisable: "+childTableName);
    			if(childTableName.equals(chTableName)){
    				HashMap<String,String> attrChildData = new HashMap<String,String>();
    				//OIMUtils oimUtils = new OIMUtils();
    				System.out.println("addBounceGroupUponDisable LookupName based on the usertype is "+lookupName);
    				tcResultSet rs= lookupOpsIntf.getLookupValues(lookupName);
    				int count=rs.getRowCount();
    				System.out.println("Count is->"+count);
    				
    				
    				if ((rs != null) && (rs.getRowCount() > 0)) 
    				{
    					for (int j = 0; j< rs.getRowCount(); j++) 
    					{
    						rs.goToRow(j);
    						lookupCode = rs.getStringValue("LKV_ENCODED");
    						lookupValue =rs.getStringValue("LKV_DECODED");
    						li.add(rs.getStringValue("LKV_DECODED"));
    						
    						
    					}
    				}
    				for (int k=0;k<li.size();k++)
    				{
    					System.out.println("Lookup Value added each time is "+k+":"+li.get(k));            
    					//addBounceChildTableData(procInstKey,childFormKey,li.get(k).toString(),"UD_ADUSRC_GROUPNAME",oimFormUtility);
    					addBounceChildTableData(procInstKey,childFormKey,li.get(k).toString(),"UD_EX_CH_DISTRIBUTIONGROUP",oimFormUtility);
    				}
    				
    				
    				
    				//attrChildData= oimUtils.getLookupInfo(lookupName);
    				
    			} 
    		}
    	} catch (Exception e) {
    		System.out.println("Error Occurred while provisioning Entitilements to User with ORC  Key");
    		retValue = "FAILED";
    	} finally {
    		if(oimFormUtility != null)
    			oimFormUtility.close();
    	}
    	return retValue;
    }
   
    
    //
    private void addChildTableData(long processInstanceKey, long childFormKey, String childTableValue, String childAttrName, tcFormInstanceOperationsIntf oimFormUtility) throws Exception {
    	System.out.println("addChildTableData processInstanceKey:"+processInstanceKey+":childFormKey:"+childFormKey+":childTableValue:"+childTableValue+":childAttrName:"+childAttrName);
    	tcResultSet rSet = oimFormUtility.getProcessFormChildData(childFormKey, processInstanceKey);
    	Set<String> curValues = new HashSet<String>();
    	for(int i=0;i<rSet.getRowCount();i++){
            rSet.goToRow(i); // goto the row
    		curValues.add(rSet.getStringValue(childAttrName).toUpperCase());
    	}
    	Map<String,String> attrMap = new HashMap<String,String>();
    	if(!curValues.contains(childTableValue.toUpperCase())){
    		attrMap.put(childAttrName, childTableValue);
    		oimFormUtility.addProcessFormChildData(childFormKey, processInstanceKey, attrMap);
    		System.out.println("addChildTableDat" + childTableValue + "' assigned to ORC_KEY " + processInstanceKey);
    		
    	} else {
    		System.out.println("addChildTableData" + childTableValue + "' already assigned to ORC_KEY " + processInstanceKey);
    	}
    	System.out.println("addChildTableData");
    }  
    
    // Add bounce group child data
    private void addBounceChildTableData(long processInstanceKey, long childFormKey, String childTableValue, String childAttrName, tcFormInstanceOperationsIntf oimFormUtility) throws Exception {
    	System.out.println("addBounceChildTableData processInstanceKey:"+processInstanceKey+":childFormKey:"+childFormKey+":childTableValue:"+childTableValue+":childAttrName:"+childAttrName);
    	tcResultSet rSet = oimFormUtility.getProcessFormChildData(childFormKey, processInstanceKey);
    	Map<String,String> attrMap = new HashMap<String,String>();
    	attrMap.put(childAttrName, childTableValue);
    	Set<String> curValues = new HashSet<String>();
    	if(rSet!=null && rSet.getRowCount()>0){
    	for(int i=0;i<rSet.getRowCount();i++){
    		curValues.add(rSet.getStringValue(childAttrName).toUpperCase());
    	}
    	
    	
    	try{
    		if(!curValues.contains(childTableValue.toUpperCase())){
    		
    		System.out.println("Just before adding the bounce group in child table");
    		oimFormUtility.addProcessFormChildData(childFormKey, processInstanceKey, attrMap);
    		
    		System.out.println("addBounceChildTableData" + childTableValue + "' assigned to ORC_KEY " + processInstanceKey);
    		}
    	
    	else{
    		System.out.println("addChildTableData" + childTableValue + "' already assigned to ORC_KEY " + processInstanceKey);
    	}
    	}
    		catch(Exception e){e.printStackTrace();}
    	System.out.println("addBounceChildTableData");
    }  
    	else{ oimFormUtility.addProcessFormChildData(childFormKey, processInstanceKey, attrMap);}
    		
    }
}	
	
		