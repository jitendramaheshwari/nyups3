package com.NYU.OIMCustom;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.security.auth.login.LoginException;

import Thor.API.tcResultSet;
import Thor.API.Exceptions.tcAPIException;
import Thor.API.tcUtilityFactory;
import Thor.API.Exceptions.tcColumnNotFoundException;
import Thor.API.Operations.tcUserOperationsIntf;
import oracle.iam.platform.OIMClient;
import oracle.iam.platform.authz.exception.AccessDeniedException;
import oracle.iam.platform.entitymgr.vo.SearchCriteria;
import oracle.iam.platform.entitymgr.vo.SearchCriteria.Operator;
import oracle.iam.platform.utils.logging.SuperLogger;
import oracle.iam.connectors.psft.common.handler.APIProvider;
import oracle.iam.identity.exception.UserSearchException;
import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.vo.User;

public class PreHirePhoneTransformer {
	/** Variables for logging */
	private final static Logger LOGGER = SuperLogger
			.getLogger(PreHirePhoneTransformer.class.getName());

	

	/*
	 * Description:Abstract method for transforming the attributes param
	 * hmUserDetails<String,Object> HashMap containing parent data details param
	 * hmEntitlementDetails <String,Object> HashMap containing child data
	 * details
	 */

	public Object transform(HashMap hmUserDetails,
			HashMap hmEntitlementDetails, String sField) throws ParseException {
		/*
		 * You must write code to transform the attributes. Parent data
		 * attribute values can be fetched by using
		 * hmUserDetails.get("Field Name").To fetch child data values, loop
		 * through the ArrayList/Vector fetched by
		 * hmEntitlementDetails.get("Child Table") Return the transformed
		 * attribute.
		 */
		LOGGER.logp(Level.INFO, getClass().getName(), "transform",
				"Entered");
		System.out.println("PreHireTransform Phone transformation code entered"+sField);
		System.out.println("hmUserDetails:"+hmUserDetails.values());
		
		System.out.println("hmUserDetails entryset:"+hmUserDetails.entrySet());
		System.out.println("sField:"+sField);
	

		
		String fieldvalue=(String) (hmUserDetails.get(sField)==null?"":hmUserDetails.get(sField));	
		if(fieldvalue!=null && !fieldvalue.trim().isEmpty()){
			
			System.out.println(sField+" is not null");
		try{
		System.out.println("Unformatted phone number is \n"+fieldvalue);
		String firstPart = fieldvalue.substring(0,fieldvalue.indexOf("/"));
		System.out.println("--1st part-->"+firstPart);
		String secondPart = fieldvalue.substring(fieldvalue.indexOf("/")+1,fieldvalue.indexOf("-"));
		System.out.println("--2nd part-->"+secondPart);
		String thirdPart = fieldvalue.substring(fieldvalue.indexOf("-")+1,fieldvalue.length());
		System.out.println("--3nd part-->"+thirdPart);
		StringBuffer st = new StringBuffer();
		st.append("+1");
		st.append(" ");
		st.append(firstPart);
		st.append(" ");
		st.append(secondPart);
		st.append(" ");
		st.append(thirdPart);
		System.out.println("Formatted phone number is\n"+st.toString());
		return st.toString();
		}catch(Exception e){
			return fieldvalue;
		}
		
		}
		else{
			
			System.out.println(sField+" is null - checking OIM");
			OIMClient client= new OIMClient();
			
		
			tcUserOperationsIntf userAPI = client.getService(tcUserOperationsIntf.class);
			System.out.println("got service");
			
			 Hashtable mhSearchCriteriaupdated = new Hashtable();
			 System.out.println("adding criteria ");
			 String fieldvalueLN=(String) (hmUserDetails.get("Last Name")==null?"":hmUserDetails.get("Last Name"));
			 System.out.println("adding criteria - LN");
			 String fieldvalueFN=(String) (hmUserDetails.get("First Name")==null?"":hmUserDetails.get("First Name"));
			 System.out.println("adding criteria - FN");
			 Date returndate;
			 
			 if(hmUserDetails.get("Birth Date") instanceof String)
			 {
				String fieldvalueDOB=(String) (hmUserDetails.get("Birth Date")==null?"":hmUserDetails.get("Birth Date"));
				 System.out.println("adding criteria - DOB String"+fieldvalueDOB);
				 String DOB = fieldvalueDOB.substring(0, 10);
				 DOB = DOB.replace("/", "-");
				 
				 SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z");
					
					String dateInString = fieldvalueDOB;
				 
					formatter.format(new Date(fieldvalueDOB));
				
					System.out.println("Formatted date is:"+formatter.format(new Date(fieldvalueDOB)));
					returndate = formatter.parse(formatter.format(new Date(fieldvalueDOB)));
			 }
			 else
			 {
				 Date fieldvalueDOB=(Date) (hmUserDetails.get("Birth Date")==null?"":hmUserDetails.get("Birth Date"));
				 System.out.println("adding criteria - DOB Date"+fieldvalueDOB);
				 
				 	returndate = fieldvalueDOB;
				 
			 }
			 
			
			 
			 //System.out.println("birth date is"+fieldvalueDOB);
			 System.out.println("Formatted DOB is "+returndate);
			 System.out.println("Last Name is "+fieldvalueLN);
			 System.out.println("First Name is "+fieldvalueFN);
			 
			 
			
			 SearchCriteria criteria, criteria1, criteria2, criteria3,criteria4;
             
             criteria1 = new SearchCriteria("Last Name",fieldvalueLN,SearchCriteria.Operator.EQUAL);
             criteria2 = new SearchCriteria("First Name",fieldvalueFN,SearchCriteria.Operator.EQUAL);
             criteria3 = new SearchCriteria("DOB", returndate,SearchCriteria.Operator.EQUAL);

             criteria4 = new SearchCriteria(criteria1, criteria2, Operator.AND);
             criteria = new SearchCriteria(criteria4, criteria3, Operator.AND);
            
		     UserManager usrMgr = client.getService(UserManager.class);
             Set retSet = new HashSet();
             retSet.add("WorkCell");
             retSet.add("HomeCell");
             retSet.add("HomePhone");
             if(sField.trim().equalsIgnoreCase("Home Phone"))
             {
            	 System.out.println("Updated sField for HomePhone");
            	 sField="HomePhone";
             }

             try {
            	 System.out.println("Searching users");
				List<User> users = usrMgr.search(criteria, retSet, null);
				
				 for(User user : users) {
					 
					 
					    System.out.println("Found users");
			            
			            String phone = (String)user.getAttribute(sField);
			            if(phone!=null && !phone.trim().isEmpty()){
			            	 System.out.println("Found users : Returned Phone");
							return phone;  
						  }
						  else
						  {
							  System.out.println("Found users "+sField+" is empty");
							  return fieldvalue;
						  }

			            
			        }
				 
				 
				 if (users.isEmpty())
				 {
					 System.out.println("No users found");
					 return fieldvalue;
				 }
				
			} catch (UserSearchException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (AccessDeniedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            
		}
		
		return fieldvalue;
	}

}
