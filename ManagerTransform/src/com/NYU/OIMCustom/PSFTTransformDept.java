package com.NYU.OIMCustom;

import Thor.API.tcUtilityFactory;
import Thor.API.Exceptions.tcAPIException;
import Thor.API.Operations.tcLookupOperationsIntf;
import Thor.API.Operations.tcUserOperationsIntf;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Properties;
import java.util.logging.Logger;
import javax.security.auth.login.LoginException;

import oracle.iam.connectors.psft.common.handler.APIProvider;
import oracle.iam.platform.OIMClient;
import oracle.iam.platform.utils.logging.SuperLogger;

public class PSFTTransformDept
{
  private static final Logger LOGGER = SuperLogger.getLogger(PSFTTransformDept.class.getName());
  InputStream inputStream;
	private APIProvider apiProvider = null;
	public tcUtilityFactory apiFactory = null;

  public Object transform(HashMap hmUserDetails, HashMap hmEntitlementDetails, String sField)
  {
    LOGGER.entering("PSFTTransformDept", "transform");
    LOGGER.info("Field =" + sField);
    String UsrDept = (String)hmUserDetails.get(sField);
    
    LOGGER.info("User dept =" + UsrDept);
    String AdminDept = null;
    
    Hashtable env = new Hashtable();
    try
    {
      this.apiProvider = new APIProvider("xelsysadm", 1, 5);
      this.apiProvider.initializeOIMApiFactory();
      tcLookupOperationsIntf lkpSvc = apiProvider.getLookupAPI();
      AdminDept = lkpSvc.getDecodedValueForEncodedValue("Lookup.NYU.AdminDept", UsrDept);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    LOGGER.exiting("PSFTTransformDept", "transform");
    return AdminDept;
  }
}
