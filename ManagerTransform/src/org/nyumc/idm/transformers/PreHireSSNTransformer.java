package org.nyumc.idm.transformers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.iam.platform.Platform;
import oracle.iam.platform.utils.logging.SuperLogger;
import Thor.API.Exceptions.tcAPIException;
import Thor.API.Operations.tcLookupOperationsIntf;

import com.thortech.xl.crypto.*;

public class PreHireSSNTransformer {
	/** Variables for logging */
	private final static Logger LOGGER = SuperLogger
			.getLogger(UserManagerTransformer.class.getName());

	/*
	 * Description:Abstract method for transforming the attributes param
	 * hmUserDetails<String,Object> HashMap containing parent data details param
	 * hmEntitlementDetails <String,Object> HashMap containing child data
	 * details
	 */

	public Object transform(HashMap hmUserDetails,
			HashMap hmEntitlementDetails, String sField) throws ParseException {
		/*
		 * You must write code to transform the attributes. Parent data
		 * attribute values can be fetched by using
		 * hmUserDetails.get("Field Name").To fetch child data values, loop
		 * through the ArrayList/Vector fetched by
		 * hmEntitlementDetails.get("Child Table") Return the transformed
		 * attribute.
		 */
		LOGGER.logp(Level.INFO, getClass().getName(), "transform",
				"Entered");
		System.out.println("Transform SSN transformation code enetered"+sField);
		System.out.println("hmUserDetails:"+hmUserDetails.values());
		
		System.out.println("hmUserDetails entryset:"+hmUserDetails.entrySet());
		System.out.println("sField:"+sField);
		
		String fieldvalueEMPLID=(String) (hmUserDetails.get("EMPLID")==null?"":hmUserDetails.get("EMPLID")); 
		System.out.println("Transform SSN transformation code enetered and the value of EMPLID is"+fieldvalueEMPLID);
		if (fieldvalueEMPLID.length()>7 && fieldvalueEMPLID.startsWith("S") ) 
		{
			System.out.println("Inside sourceSSN transformation for contractors");
			return null;
		}
		//System.out.println("hmEntitlementDetails:"+hmEntitlementDetails.values());
		//Date date =null;

		
		String fieldvalue=(String) (hmUserDetails.get(sField)==null?"":hmUserDetails.get(sField));			
		String encryptedValue="";
		System.out.println("fieldvalue==="+fieldvalue); //have date here from prehire in format dd-mm-yy 
		try {
			encryptedValue=tcCryptoUtil.encrypt(fieldvalue,"DBSecretKey");
		} catch (tcCryptoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("encrypted value is:->"+encryptedValue);
		return encryptedValue;
		
	}

}
