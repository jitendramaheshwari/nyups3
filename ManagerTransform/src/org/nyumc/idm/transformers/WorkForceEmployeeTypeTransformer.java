package org.nyumc.idm.transformers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.iam.connectors.psft.common.handler.APIProvider;
import oracle.iam.identity.usermgmt.api.UserManagerConstants.AttributeName;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.platform.Platform;
import oracle.iam.platform.utils.logging.SuperLogger;
import Thor.API.tcResultSet;
import Thor.API.tcUtilityFactory;
import Thor.API.Exceptions.tcAPIException;
import Thor.API.Operations.tcLookupOperationsIntf;
import Thor.API.Operations.tcUserOperationsIntf;

import com.thortech.xl.crypto.*;

public class WorkForceEmployeeTypeTransformer {
	private final static Logger LOGGER = SuperLogger
			.getLogger(UserManagerTransformer.class.getName());
	private APIProvider apiProvider = null;
	public tcUtilityFactory apiFactory = null;
	String userEmplid = "";
	String usr_mgr_key = null;
	String usr_emp_type = "Full-Time";
	List<User> users = null;
	

	private tcLookupOperationsIntf lookupAPI;

	/*
	 * Description:Abstract method for transforming the attributes param
	 * hmUserDetails<String,Object> HashMap containing parent data details param
	 * hmEntitlementDetails <String,Object> HashMap containing child data
	 * details
	 */

	public Object transform(HashMap hmUserDetails,
			HashMap hmEntitlementDetails, String sField) throws ParseException {
		/*
		 * You must write code to transform the attributes. Parent data
		 * attribute values can be fetched by using
		 * hmUserDetails.get("Field Name").To fetch child data values, loop
		 * through the ArrayList/Vector fetched by
		 * hmEntitlementDetails.get("Child Table") Return the transformed
		 * attribute.
		 */
		LOGGER.logp(Level.INFO, getClass().getName(), "transform", "Entered");
		System.out
				.println("Inside Status Transformer to check for MCIT workforce update"
						+ hmUserDetails.values());
		String fieldvalueCompany = (String) (hmUserDetails.get("Company") == null ? ""
				: hmUserDetails.get("Company"));
		System.out
				.println("fieldvalueCompany: is not null inside Employee Type converter in Job record"
						+ fieldvalueCompany);
		String fieldvaluePerOrg = (String) (hmUserDetails.get("Per Org") == null ? ""
				: hmUserDetails.get("Per Org"));
		System.out
				.println("fieldvaluePerOrg: is not null inside Employee Type converter in Job record"
						+ fieldvaluePerOrg);
		String fieldvalueRegTemp = (String) (hmUserDetails.get("Reg Temp") == null ? ""
				: hmUserDetails.get("Reg Temp"));
		System.out
				.println("fieldvalueRegTemp: is not null inside Employee Type converter in Job record"
						+ fieldvalueRegTemp);
		String fieldvalueFullPartTime = (String) (hmUserDetails
				.get("Full Part Time") == null ? "" : hmUserDetails
				.get("Full Part Time"));
		System.out
				.println("fieldvalueFullPartTime: is not null inside Employee Type converter in Job record"
						+ fieldvalueFullPartTime);
		String fieldvalueEMPLID = (String) (hmUserDetails.get("EMPLID") == null ? ""
				: hmUserDetails.get("EMPLID"));
		String fieldvalueStatus = (String) (hmUserDetails.get("Status") == null ? ""
				: hmUserDetails.get("Status"));
		
		
		this.apiProvider = new APIProvider("xelsysadm", 1, 5);
		this.apiProvider.initializeOIMApiFactory();
		tcUserOperationsIntf userAPI = apiProvider.getUserAPI();
		   
		List<User> contractorSearchresult =null;
		StringBuffer strEncode=null;
		String strencodeValue = "";
		String lookupValue = "Full-Time";
		lookupAPI = apiProvider.getLookupAPI();
		Hashtable mhSearchCriteriaupdated = new Hashtable();
		List<User> users = null;
		if (fieldvalueCompany != null && !fieldvalueCompany.trim().isEmpty()
				&& fieldvalueCompany.equalsIgnoreCase("SPI")) {
			//Added code on Jan 11th 
			mhSearchCriteriaupdated.put("EMPLID", fieldvalueEMPLID);
			try {
				tcResultSet moResultSetupdated = userAPI.findUsers(mhSearchCriteriaupdated);
				if(moResultSetupdated.getRowCount()>1) 
					   
				      for (int i=0; i<moResultSetupdated.getRowCount(); i++){
				    	  moResultSetupdated.goToRow(i);
				   System.out.println("Employee Type from OIM is : "+ String.valueOf(moResultSetupdated.getStringValue("Users.Role")) + " for user id : "+ moResultSetupdated.getStringValue("Users.User ID"));
				   String usr_user_type = moResultSetupdated.getStringValue("Users.Role");
				   System.out.println("User Type from OIM in WorkForce Employee Type Transformer:"+usr_user_type);
				   String usr_user_emplid = moResultSetupdated.getStringValue("EMPLID");
				   System.out.println("Employee ID from OIM in WorkForce Employee Type Transformer:"+usr_user_emplid);
				   //1st if case is for PreHire users
				   if(fieldvalueEMPLID.equalsIgnoreCase(usr_user_emplid) && !usr_user_type.equalsIgnoreCase("Contractor") && !fieldvalueStatus.equalsIgnoreCase("Active"))
				   {
					   System.out.println("When an Active Employee Record is present as PreHire and same user is terminated as SPI from PSFT");
					   return "Inactive-PSFT-Spi";
				   }
				   //2nd if case if for PeopleSoft users
				   else if(!fieldvalueEMPLID.equalsIgnoreCase(usr_user_emplid) && !usr_user_type.equalsIgnoreCase("Contractor") && !fieldvalueStatus.equalsIgnoreCase("Active"))
				   {
					   System.out.println("When an Active Employee Record is present and same user is terminated as SPI from PSFT");
					   return "Inactive-PSFT-Spi";
				   }
				   else if (fieldvalueEMPLID.equalsIgnoreCase(usr_user_emplid) && usr_user_type.equalsIgnoreCase("Contractor"))
				   {
					   System.out.println("When an active existing SPI record is updated from PSFT");
					   return "Contractor";
				   }
				   }
				   else if (moResultSetupdated.getRowCount()==0 && fieldvalueStatus.equalsIgnoreCase("Active")){
					   System.out.println("When a New Contractor is onboarded");
				   return "Contractor";
				   }
				   else if(moResultSetupdated.getRowCount()==0 && fieldvalueStatus.equalsIgnoreCase("Disabled")){
					   System.out.println("When an existing sponsored individual is terminated when an Active record exists for them in OIM ");
				   return "Inactive-PSFT-Spi";
				   }
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return "Contractor";
		}
		if (fieldvaluePerOrg != null && !fieldvaluePerOrg.trim().isEmpty() && !fieldvalueCompany.equalsIgnoreCase("SPI")) {
			System.out
					.println("fieldvaluePerOrg: is not null inside Employee Type converter in Job record");
			System.out.println("fieldvaluePerOrg===" + fieldvaluePerOrg);
			strencodeValue=fieldvaluePerOrg+"##"+fieldvalueRegTemp+"##"+fieldvalueFullPartTime;
			System.out.println("strencodeValue:->"+strencodeValue.toString());

			try {
				lookupValue = lookupAPI.getDecodedValueForEncodedValue(
						"Lookup.PSFT.HRMS.WorkForceSync.EmpType", strencodeValue);
			} catch (tcAPIException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}
			
			finally {
				System.out.println("Before closing the OIMapiFactory");
				apiProvider.closeOimApiFactory();
			}

		}
		System.out.println("Before returning" + usr_emp_type + "Employee Type");
		return lookupValue;

	}

}
