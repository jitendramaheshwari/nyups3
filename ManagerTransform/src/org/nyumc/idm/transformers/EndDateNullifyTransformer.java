package org.nyumc.idm.transformers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.iam.platform.Platform;
import oracle.iam.platform.utils.logging.SuperLogger;
import Thor.API.Exceptions.tcAPIException;
import Thor.API.Operations.tcLookupOperationsIntf;

public class EndDateNullifyTransformer {
	/** Variables for logging */
	private final static Logger LOGGER = SuperLogger
			.getLogger(UserManagerTransformer.class.getName());

	/*
	 * Description:Abstract method for transforming the attributes param
	 * hmUserDetails<String,Object> HashMap containing parent data details param
	 * hmEntitlementDetails <String,Object> HashMap containing child data
	 * details
	 */

	public Object transform(HashMap hmUserDetails,
			HashMap hmEntitlementDetails, String sField) throws ParseException {
		/*
		 * You must write code to transform the attributes. Parent data
		 * attribute values can be fetched by using
		 * hmUserDetails.get("Field Name").To fetch child data values, loop
		 * through the ArrayList/Vector fetched by
		 * hmEntitlementDetails.get("Child Table") Return the transformed
		 * attribute.
		 */
		LOGGER.logp(Level.INFO, getClass().getName(), "transform",
				"Entered");
		System.out.println("Transform End Date transformation code enetered"+sField);
		System.out.println("hmUserDetails:"+hmUserDetails.values());
		String fieldvalueCompany = (String) (hmUserDetails.get("Company") == null ? ""
				: hmUserDetails.get("Company"));
		
		System.out.println("hmUserDetails entryset for end date:"+hmUserDetails.entrySet());
		System.out.println("sField for end date:"+sField);
		//System.out.println("hmEntitlementDetails:"+hmEntitlementDetails.values());
		//Date date =null;

		Date date =null;
		String fieldvalue=(String) (hmUserDetails.get(sField)==null?"":hmUserDetails.get(sField));
		System.out.println("fieldvalue=*******=="+fieldvalue); 
		if (fieldvalueCompany != null && !fieldvalueCompany.trim().isEmpty()
				&& fieldvalueCompany.equalsIgnoreCase("SPI")) {
			return fieldvalue;
		}
		
		
		

		
		//String fieldvalue=(String) (hmUserDetails.get(sField)==null?"":hmUserDetails.get(sField));			
		
		//have date here from student file in format dd/mm/yyyy 
		
	
		//Date date = null;
		
		
		
		//return formatter.format(new Date(fieldvalue));
		else return date;
		
	}

}
