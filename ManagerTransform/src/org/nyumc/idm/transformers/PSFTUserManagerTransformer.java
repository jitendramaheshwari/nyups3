package org.nyumc.idm.transformers;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.iam.identity.exception.AccessDeniedException;
import oracle.iam.identity.exception.NoSuchUserException;
import oracle.iam.identity.exception.UserLookupException;
import oracle.iam.identity.exception.UserSearchException;
import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.api.UserManagerConstants;
import oracle.iam.identity.usermgmt.api.UserManagerConstants.AttributeName;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.platform.Platform;
import oracle.iam.platform.entitymgr.vo.SearchCriteria;
import oracle.iam.platform.utils.logging.SuperLogger;

public class PSFTUserManagerTransformer {
	/** Variables for logging */
	private final static Logger LOGGER = SuperLogger
			.getLogger(UserManagerTransformer.class.getName());

	/*
	 * Description:Abstract method for transforming the attributes param
	 * hmUserDetails<String,Object> HashMap containing parent data details param
	 * hmEntitlementDetails <String,Object> HashMap containing child data
	 * details
	 */

	public Object transform(HashMap hmUserDetails,
			HashMap hmEntitlementDetails, String sField) {
		/*
		 * You must write code to transform the attributes. Parent data
		 * attribute values can be fetched by using
		 * hmUserDetails.get("Field Name").To fetch child data values, loop
		 * through the ArrayList/Vector fetched by
		 * hmEntitlementDetails.get("Child Table") Return the transformed
		 * attribute.
		 */
		LOGGER.logp(Level.INFO, getClass().getName(), "transform",
				"Entered");
		System.out.println("hmUserDetails in PSFTUserManagerTransformer :"+hmUserDetails.values());
		System.out.println("hmEntitlementDetails in PSFTUserManagerTransformer:"+hmEntitlementDetails.values());
		System.out.println("hmUserDetails entryset i n PSFTUserManagerTransformer:"+hmUserDetails.entrySet());
		System.out.println("sField:"+sField);

		
		String fieldvalue=(String) (hmUserDetails.get(sField)==null?"":hmUserDetails.get(sField));			
		
		System.out.println("fieldvalue==="+fieldvalue);
		
		
		String userMgr = "";
		String usr_mgr_key = null;
		List<User> users = null;
		if(fieldvalue!=null && !fieldvalue.isEmpty())
		{
			System.out.println("fieldvalue: is not null");
			
			userMgr=fieldvalue;
			System.out.println("userMgr==="+userMgr);
			
			UserManager userService = Platform.getService(UserManager.class);
			
			try {
				
				Set <String>retAttributes = new HashSet<String>();  
		        retAttributes.add(AttributeName.USER_LOGIN.getId());  
		        
		        //SearchCriteria srchCriteria = new SearchCriteria("EmployeeID",fieldvalue, SearchCriteria.Operator.EQUAL);
		        
		        SearchCriteria srchCriteria = new SearchCriteria("PositionNumber",fieldvalue, SearchCriteria.Operator.EQUAL);
		        //AttributeName.USER_KEY.getId()
				//User user = userService.getDetails(userMgr, null, true);
				
				UserManager mgr = Platform.getService(UserManager.class);  
				   
				
			
				users = mgr.search(srchCriteria, retAttributes, null);
				 
				
				if(mgr != null)
				{
					System.out.println("User Manager Login key  : " + fieldvalue + "  --  User Manager Key  : " + users.get(0).getAttribute(AttributeName.USER_KEY.getId()));
					usr_mgr_key = users.get(0).getAttribute(AttributeName.USER_KEY.getId()).toString();
				}
				
				
			} catch (oracle.iam.platform.authz.exception.AccessDeniedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UserSearchException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "";
			}
			
		}	
		else return "";
				
		System.out.println("Transform::usr_mgr_key:" + usr_mgr_key);

		LOGGER.logp(Level.INFO, getClass().getName(), "transform",
				"usr_mgr_key:"+usr_mgr_key);
		LOGGER.logp(Level.INFO, getClass().getName(), "transform",
				"Exited");
		
		return usr_mgr_key;
		
	}
}
