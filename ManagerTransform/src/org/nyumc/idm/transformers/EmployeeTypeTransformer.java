package org.nyumc.idm.transformers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.iam.connectors.psft.common.handler.APIProvider;
import oracle.iam.identity.usermgmt.api.UserManagerConstants.AttributeName;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.platform.Platform;
import oracle.iam.platform.utils.logging.SuperLogger;
import Thor.API.tcResultSet;
import Thor.API.tcUtilityFactory;
import Thor.API.Exceptions.tcAPIException;
import Thor.API.Operations.tcLookupOperationsIntf;
import Thor.API.Operations.tcUserOperationsIntf;

import com.thortech.xl.crypto.*;

public class EmployeeTypeTransformer {
	 private final static Logger LOGGER = SuperLogger
			    .getLogger(UserManagerTransformer.class.getName());
			  private APIProvider apiProvider = null;
			  public tcUtilityFactory apiFactory = null;
			  String userEmplid = "";
			  String usr_mgr_key = null;
			  String usr_emp_type="Full-Time";
			  List<User> users = null;

			private tcUserOperationsIntf userAPI;

			 /*
			   * Description:Abstract method for transforming the attributes param
			   * hmUserDetails<String,Object> HashMap containing parent data details param
			   * hmEntitlementDetails <String,Object> HashMap containing child data
			   * details
			   */

			 public Object transform(HashMap hmUserDetails,
			    HashMap hmEntitlementDetails, String sField) throws ParseException {
			   /*
			    * You must write code to transform the attributes. Parent data
			    * attribute values can be fetched by using
			    * hmUserDetails.get("Field Name").To fetch child data values, loop
			    * through the ArrayList/Vector fetched by
			    * hmEntitlementDetails.get("Child Table") Return the transformed
			    * attribute.
			    */
			   LOGGER.logp(Level.INFO, getClass().getName(), "transform",
			     "Entered");
			   System.out.println("Inside Status Transformer to check for demographics update"+hmUserDetails.values());
			   String fieldvalueEMPLID=(String) (hmUserDetails.get("EMPLID")==null?"":hmUserDetails.get("EMPLID")); 
			   String fieldvalueStatus = (String) (hmUserDetails.get("Status") == null ? "": hmUserDetails.get("Status"));
			   String fieldvalueFN = (String) (hmUserDetails.get("First Name") == null ? "": hmUserDetails.get("First Name"));
			   String fieldvalueLN = (String) (hmUserDetails.get("Last Name") == null ? "": hmUserDetails.get("Last Name"));
			   String fieldvalueMN = (String) (hmUserDetails.get("Middle Name") == null ? "": hmUserDetails.get("Middle Name"));
			   String fieldvalueDOB = (String) (hmUserDetails.get("Birth Date") == null ? "": hmUserDetails.get("Birth Date"));
			   String fieldvalueGender = (String) (hmUserDetails.get("Gender") == null ? "": hmUserDetails.get("Gender"));
			   
			   this.apiProvider = new APIProvider("xelsysadm", 1,5);
			   this.apiProvider.initializeOIMApiFactory();
			   userAPI=apiProvider.getUserAPI();
			   
			   List<User> users = null;
			   List<User> contractorSearchresult =null;
			   if(fieldvalueEMPLID!=null && !fieldvalueEMPLID.trim().isEmpty() )
			   {
			   System.out.println("fieldvalue: is not null inside Employee Type converter");
			   System.out.println("fieldvalueEMPLID==="+fieldvalueEMPLID);
			   try {

			   Hashtable mhSearchCriteria = new Hashtable();
			   Hashtable mhSearchCriteriaupdated = new Hashtable();
			   
			   mhSearchCriteria.put("EMPLID", fieldvalueEMPLID);
			   //Added on 12/08 to change the user type to Contractors for MCIT sponsored individuals
			   // Intention is to fix the issue where termination event of Active SPI who has been re-hired as employee would disable the user
			   if (fieldvalueEMPLID.length()>7 && fieldvalueEMPLID.startsWith("S") ) {
				   mhSearchCriteriaupdated.put("First Name", fieldvalueFN);
				   mhSearchCriteriaupdated.put("Last Name", fieldvalueLN);
				   mhSearchCriteriaupdated.put("DOB", fieldvalueDOB);
				   mhSearchCriteriaupdated.put("Gender", fieldvalueGender);
				   tcResultSet moResultSetupdated = userAPI.findUsers(mhSearchCriteriaupdated);
				   if(moResultSetupdated.getRowCount()>1) 
				   
				      for (int i=0; i<moResultSetupdated.getRowCount(); i++){
				    	  moResultSetupdated.goToRow(i);
				   System.out.println("Employee Type is : "+ String.valueOf(moResultSetupdated.getStringValue("Users.Role")) + " for user id : "+ moResultSetupdated.getStringValue("Users.User ID"));
				   String usr_user_type = moResultSetupdated.getStringValue("Users.Role");
				   String usr_user_emplid = moResultSetupdated.getStringValue("EMPLID");
				   if(!fieldvalueEMPLID.equalsIgnoreCase(usr_user_emplid) && !usr_user_type.equalsIgnoreCase("Contractor") && !fieldvalueStatus.equalsIgnoreCase("Active"))
				   {
					   return "Inactive-PSFT-Spi";
				   }
				   else if (fieldvalueEMPLID.equalsIgnoreCase(usr_user_emplid) && usr_user_type.equalsIgnoreCase("Contractor"))
				   {
					   return "Contractor";
				   }
				   }
				   else if (moResultSetupdated.getRowCount()==0)
				   return "Contractor";//This means this a new user and hence user type should be Contractor
			   }
			   
			   //
			   tcResultSet moResultSet = userAPI.findUsers(mhSearchCriteria);
			   //if(moResultSet.getRowCount()=0){this means two things either user is a new user or }
			   if(moResultSet.getRowCount()>1) return "Full-Time";
			   else
			      for (int i=0; i<moResultSet.getRowCount(); i++){
			          moResultSet.goToRow(i);
			   System.out.println("Employee Type is : "+ String.valueOf(moResultSet.getStringValue("Users.Role")) + " for user id : "+ moResultSet.getStringValue("Users.User ID"));
			   usr_emp_type=moResultSet.getStringValue("Users.Role");
			   }
			 
			   System.out.println("usr_emp_type:"+usr_emp_type); 
			 
			   } catch (Exception e) {
			   // TODO Auto-generated catch block
			   e.printStackTrace();
			   return "Full-Time";
			   }
			   finally{
				   System.out.println("Before closing the OIMapiFactory");
				   apiProvider.closeOimApiFactory(); 
			   }

			   }
			   System.out.println("Before returning" +usr_emp_type+ "Employee Type");
			   return usr_emp_type;
			   
			  }


}



