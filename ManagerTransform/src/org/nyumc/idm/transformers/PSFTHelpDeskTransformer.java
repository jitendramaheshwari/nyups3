package org.nyumc.idm.transformers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.iam.platform.Platform;
import oracle.iam.platform.utils.logging.SuperLogger;
import Thor.API.Exceptions.tcAPIException;
import Thor.API.Operations.tcLookupOperationsIntf;

import com.thortech.xl.crypto.*;

public class PSFTHelpDeskTransformer {
	/** Variables for logging */
	private final static Logger LOGGER = SuperLogger
			.getLogger(UserManagerTransformer.class.getName());

	/*
	 * Description:Abstract method for transforming the attributes param
	 * hmUserDetails<String,Object> HashMap containing parent data details param
	 * hmEntitlementDetails <String,Object> HashMap containing child data
	 * details
	 */

	public Object transform(HashMap hmUserDetails,
			HashMap hmEntitlementDetails, String sField) throws ParseException {
		/*
		 * You must write code to transform the attributes. Parent data
		 * attribute values can be fetched by using
		 * hmUserDetails.get("Field Name").To fetch child data values, loop
		 * through the ArrayList/Vector fetched by
		 * hmEntitlementDetails.get("Child Table") Return the transformed
		 * attribute.
		 */
		LOGGER.logp(Level.INFO, getClass().getName(), "transform",
				"Entered");
		System.out.println("EmployeeTypeTransformer.java:"+hmUserDetails.values());
		//System.out.println("EmployeeTypeTransformer:"+hmEntitlementDetails.values());
		//System.out.println("EmployeeTypeTransformer entryset:"+hmUserDetails.entrySet());
		//System.out.println(" EmployeeTypeTransformer sField:"+sField);
		//Date date =null;

		
		String fieldvalue=(String) (hmUserDetails.get(sField)==null?"":hmUserDetails.get(sField));	
		String fieldvalueCompany = (String) (hmUserDetails.get("Company") == null ? ""
				: hmUserDetails.get("Company"));
		System.out
				.println("fieldvalueCompany: is not null inside Employee Type converter in Job record"
						+ fieldvalueCompany);
		
		//added on 07/17
		
		if (fieldvalueCompany != null && !fieldvalueCompany.trim().isEmpty() && (fieldvalueCompany.equalsIgnoreCase("WUH") || (fieldvalue.equalsIgnoreCase("WUHBUBU")))) {
			return "WUH-EMP";
		}
		
		if (fieldvalueCompany != null && !fieldvalueCompany.trim().isEmpty()
				&& fieldvalueCompany.equalsIgnoreCase("SPI") && !(fieldvalue.equalsIgnoreCase("LMCBU") || fieldvalue.equalsIgnoreCase("AUGBU") || fieldvalue.equalsIgnoreCase("SBCBU") || fieldvalue.equalsIgnoreCase("SHHBU") || fieldvalue.equalsIgnoreCase("SPHBU") || fieldvalue.equalsIgnoreCase("CHABU"))) {
			return "NYU-SPO";
		}
		if (fieldvalueCompany != null && !fieldvalueCompany.trim().isEmpty()
				&& fieldvalueCompany.equalsIgnoreCase("SPI") && (fieldvalue.equalsIgnoreCase("LMCBU") || fieldvalue.equalsIgnoreCase("AUGBU") || fieldvalue.equalsIgnoreCase("SBCBU") || fieldvalue.equalsIgnoreCase("SHHBU") || fieldvalue.equalsIgnoreCase("SPHBU") || fieldvalue.equalsIgnoreCase("CHABU")))
		{
			return "LMC-SPO";
		}
		
		//System.out.println(" EmployeeTypeTransformer Field value"+fieldvalue);
		if(fieldvalue.equalsIgnoreCase("LMCBU") || fieldvalue.equalsIgnoreCase("AUGBU") || fieldvalue.equalsIgnoreCase("SBCBU") || fieldvalue.equalsIgnoreCase("SHHBU") || fieldvalue.equalsIgnoreCase("SPHBU") || fieldvalue.equalsIgnoreCase("CHABU"))
		{
		System.out.println("Before stamping LMC-EMP");
		return "LMC-EMP";
	
	    }
		else return "NYU-EMP";
   }
}

	
		
	
	




