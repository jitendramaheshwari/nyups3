package org.nyumc.idm.transformers;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.iam.identity.exception.AccessDeniedException;
import oracle.iam.identity.exception.NoSuchUserException;
import oracle.iam.identity.exception.UserLookupException;
import oracle.iam.identity.exception.UserSearchException;
import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.api.UserManagerConstants;
import oracle.iam.identity.usermgmt.api.UserManagerConstants.AttributeName;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.platform.Platform;
import oracle.iam.platform.entitymgr.vo.SearchCriteria;
import oracle.iam.platform.utils.logging.SuperLogger;

public class UserManagerTransformer {
	/** Variables for logging */
	private final static Logger LOGGER = SuperLogger
			.getLogger(UserManagerTransformer.class.getName());

	/*
	 * Description:Abstract method for transforming the attributes param
	 * hmUserDetails<String,Object> HashMap containing parent data details param
	 * hmEntitlementDetails <String,Object> HashMap containing child data
	 * details
	 */

	public Object transform(HashMap hmUserDetails,
			HashMap hmEntitlementDetails, String sField) {
		/*
		 * You must write code to transform the attributes. Parent data
		 * attribute values can be fetched by using
		 * hmUserDetails.get("Field Name").To fetch child data values, loop
		 * through the ArrayList/Vector fetched by
		 * hmEntitlementDetails.get("Child Table") Return the transformed
		 * attribute.
		 */
		LOGGER.logp(Level.INFO, getClass().getName(), "transform",
				"Entered");
		System.out.println("hmUserDetails in :"+hmUserDetails.values());
		System.out.println("hmEntitlementDetails:"+hmEntitlementDetails.values());
		System.out.println("hmUserDetails entryset:"+hmUserDetails.entrySet());
		System.out.println("sField:"+sField);

		
		String fieldvalue=(String) (hmUserDetails.get(sField)==null?"":hmUserDetails.get(sField));			
		
		System.out.println("Inside Prehire Manager ID transformation fieldvalue==="+fieldvalue);
		
		
		String userMgr = "";
		String usr_mgr_key = null;
		List<User> users = null;
		if(fieldvalue!=null && !fieldvalue.trim().isEmpty())
		{
			System.out.println("fieldvalue: is not null");
			/*StringTokenizer st = new StringTokenizer(fieldvalue,",");
			while(st.hasMoreTokens())
			{
				String strTk = st.nextToken();
				System.out.println("strTk: "+strTk);
				if(strTk.contains("uid"))
				{
					userMgr = strTk.substring(strTk.indexOf("=")+1);
					break;
				}
			}*/
			userMgr=fieldvalue;
			System.out.println("userMgr==="+userMgr);
			System.out.println("fieldvalue>>>"+fieldvalue);
			UserManager userService = Platform.getService(UserManager.class);
			System.out.println("userService initiated>>>"+userService);
			
			
			try {
				
				Set <String>retAttributes = new HashSet<String>();  
		        retAttributes.add(AttributeName.USER_LOGIN.getId());
		        retAttributes.add(AttributeName.USER_KEY.getId());
		        System.out.println("After setting the retAttributes>>>");
		        
		        //SearchCriteria srchCriteria = new SearchCriteria("EmployeeID",fieldvalue, SearchCriteria.Operator.EQUAL);
		        
		        SearchCriteria srchCriteria = new SearchCriteria("EMPLID",fieldvalue.trim(), SearchCriteria.Operator.EQUAL);
		        System.out.println("After setting the srchCriteria>>>"+srchCriteria);
		        //AttributeName.USER_KEY.getId()
				//User user = userService.getDetails(userMgr, null, true);
				
				UserManager mgr = Platform.getService(UserManager.class);  
				System.out.println("After setting the mgr>>>"+mgr);
				   
				
			
				users = mgr.search(srchCriteria, retAttributes, null);
				System.out.println("user search has been performed"+users.size());
				
				if(users != null && users.size()>0)
				{
					//System.out.println("user search has been performed"+users.size()); 
					System.out.println("User Manager EMPLID  : " + fieldvalue + "  --  User Manager LOGIN  : " + users.get(0).getAttribute(AttributeName.USER_LOGIN.getId()));
					usr_mgr_key = users.get(0).getAttribute(AttributeName.USER_KEY.getId()).toString();
					System.out.println("usr_mgr_key returned from OIM for attaching to Prehire users is "+usr_mgr_key);
				}
				
				
			} catch (oracle.iam.platform.authz.exception.AccessDeniedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UserSearchException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "";
			}
			
		}	
		else return "";
				
		System.out.println("Transform::usr_mgr_key:" + usr_mgr_key);

		LOGGER.logp(Level.INFO, getClass().getName(), "transform",
				"usr_mgr_key:"+usr_mgr_key);
		LOGGER.logp(Level.INFO, getClass().getName(), "transform",
				"Exited");
		
		return usr_mgr_key;
		
	}
}
