package org.nyumc.iam.plugin.scheduledTask;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
















import oracle.iam.identity.exception.NoSuchUserException;
import oracle.iam.identity.exception.SearchKeyNotUniqueException;
import oracle.iam.identity.exception.UserLookupException;
import oracle.iam.identity.exception.UserManagerException;
import oracle.iam.identity.exception.UserModifyException;
import oracle.iam.identity.exception.ValidationFailedException;
import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.api.UserManagerConstants;
import oracle.iam.identity.usermgmt.api.UserManagerConstants.AttributeName;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.identity.vo.Identity;
import oracle.iam.platform.Platform;
import oracle.iam.platform.authz.exception.AccessDeniedException;
import oracle.iam.platform.entitymgr.vo.SearchCriteria;
import oracle.iam.scheduler.vo.StoppableTask;
import oracle.iam.scheduler.vo.TaskSupport;

public class PositionNumberToUsrKeyTask extends TaskSupport implements StoppableTask{

	private static final String C_SUPERVISOR = "Supervisor ID";
	private static final String C_POSITIONNBR = "PositionNumber";
	

	public void execute(HashMap attributes) throws Exception {
		System.out.println("Inside execute method");
		getUsers(C_SUPERVISOR);
			
	}
	


  
	public void getUsers(Object searchField) throws NoSuchUserException, UserLookupException, SearchKeyNotUniqueException, AccessDeniedException, ValidationFailedException, UserModifyException {
		// TODO Auto-generated method stub
		
		System.out.println("Inside getUsers Method");
		String userKey = null;
		String managerKey = null;
		String supervisorID = null;
		String positionNumber = null;
		
		
		
		//String methodName = "getUsers";
		List<User> usersWithSupervisorID = null;
		UserManager userService = Platform.getService(UserManager.class);
		HashMap<String,Object> configParams = new HashMap<String,Object>();
		HashMap<String,String> userAttributes = new HashMap<String,String>();
		
		System.out.println("Before supervisorID search");
		SearchCriteria crit1 = new SearchCriteria(searchField, null, SearchCriteria.Operator.NOT_EQUAL);
		
		Set<String> retAttrs = new HashSet<String>();
		
		
		retAttrs.add(UserManagerConstants.AttributeName.USER_KEY.getId());
		retAttrs.add(UserManagerConstants.AttributeName.MANAGER_KEY.getId());
		retAttrs.add(C_SUPERVISOR);
		retAttrs.add(C_POSITIONNBR);
		
		
		try {
			
			System.out.println("Inside try block in getUsers Method");
			usersWithSupervisorID = userService.search(crit1, retAttrs, configParams);
			System.out.println("Size of the received population: "+usersWithSupervisorID.size());
			
			for(Identity identity : usersWithSupervisorID) {
				if(identity.getEntityId() != null){
					
					UserManager thisUserService = Platform.getService(UserManager.class);
					User usr = thisUserService.getDetails(identity.getEntityId().toString(), retAttrs, false);
					userKey = usr.getId();
					managerKey = usr.getManagerKey();
					supervisorID = (String) usr.getAttribute("Supervisor ID");
					positionNumber = (String) usr.getAttribute("PositionNumber");
					
					System.out.println("userKey: " + userKey);
					System.out.println("supervisorID: " + supervisorID);
					System.out.println("positionNumber: " + positionNumber);
					
					String sIDpN = supervisorID + ";" + positionNumber; //sIDpN stands for supervisorID, posNum
					
					//Placing all retrieved data in a HashMap
					userAttributes.put(userKey, sIDpN);
					
					
					
					
				
				}
			}
			
		} catch (UserManagerException e) {
			
			e.getMessage();
		}
		
		
		String mgrKey = findManagersPositionNumber(userAttributes);
		
	}
	
	@SuppressWarnings("unchecked")
	private String findManagersPositionNumber(HashMap<String, String> userAttrs) throws NoSuchUserException, UserLookupException, AccessDeniedException, SearchKeyNotUniqueException, ValidationFailedException, UserModifyException{
		 //This method needs to use the Supervisor ID number and look for a USER where that supervisor number is 
		// the NEW users Position Number
		
		System.out.println("Inside findManagers method");
	    //List<User> foundUser = null;
	    UserManager userService = Platform.getService(UserManager.class);
	    
		Set testSet = userAttrs.entrySet();
		Iterator i = testSet.iterator();
		//HashMap<String,Object> configParams = new HashMap<String,Object>();
		
		while(i.hasNext()){
			
			Map.Entry<String,String> me = (Entry<String,String>)i.next();
			
			if(me.getValue() != null){
				
				String hashKey = me.getValue().toString();
				System.out.println("hashKey = "+hashKey);
				String userKey = me.getKey();
				int splitAtSemicolon = findSemicolon(hashKey);
 				String supervisorID = hashKey.substring(0,splitAtSemicolon);
 				String positionNumber = hashKey.substring(splitAtSemicolon+1, hashKey.length());
				
				
				System.out.println("userKey: " + userKey);
				System.out.println("supervisorID: " + supervisorID);
				System.out.println("positionNumber: " + positionNumber);
				
				
				
				
				HashSet<String> attrs = new HashSet<String>();
				attrs.add(UserManagerConstants.AttributeName.USER_LOGIN.getId());
				
				try {
					
				
				User supervisorUsr = userService.getDetails(C_POSITIONNBR,supervisorID,attrs);
				String IDtoSetAsMgrKey = supervisorUsr.getId();

				
				long numberID = Long.parseLong(IDtoSetAsMgrKey);
				System.out.println("IDtoSetAsMgrKey: "+numberID);
				
				
				System.out.println("the userKey getting updated: "+ userKey);
				User employeeUser = new User(userKey);
				employeeUser.setManagerKey(numberID);
				
				userService.modify(employeeUser);
				System.out.println("ManagerKey of user: "+employeeUser.getManagerKey());
				
				} catch (NoSuchUserException e) {
					e.getMessage();
					continue;
				} catch (ValidationFailedException e){
					e.getMessage();
					continue;
				} catch (UserLookupException e){
					e.getMessage();
					continue;
				} catch (SearchKeyNotUniqueException e) {
					e.getMessage();
					continue;
				} catch (AccessDeniedException e){
					e.getMessage();
					continue;
				} catch (UserModifyException e){
					e.getMessage();
					continue;
					
					
					
					
				}
				
				
				
				//SearchCriteria lookForUserWithPositionNumber = 
						//new SearchCriteria("PositionNumber", supervisorID, SearchCriteria.Operator.EQUAL);
				
				//try {
					
								
					//foundUser = userService.search(lookForUserWithPositionNumber, retAttrs, configParams);
				    
					//if(foundUser.isEmpty()){
						//System.out.println("FOUND USER LIST IS EMPTY");
					//}
					
					//for(Identity identity : foundUser){
						//if(identity.getEntityId() != null){
						//	String newUserKey = (String) identity.getAttribute(UserManagerConstants.AttributeName.USER_KEY.getId());
							//System.out.println("key found: "+newUserKey);
							//return newUserKey;
						//}
					//}
				
				
				//} catch (UserManagerException e){
					//e.getMessage();
				//}
				
				//}
				//else
					//continue;
				
				
			}}
		return null;
			
		
	
		//return null;	
		
			
		
		
		
	}
	
	//private void setManagerKey(String userKey, String mgrKey){
		
		
		//System.out.println("Inside setManagerKey method");
		//UserManager userService = Platform.getService(UserManager.class);
	//	HashMap<String, Object>  updateAttrs = new HashMap<String, Object>();
		//updateAttrs.put(UserManagerConstants.AttributeName.MANAGER_KEY.getName(), mgrKey);
		//User user = new User(userKey, updateAttrs);
		//try {
		//	userService.modify(UserManagerConstants.AttributeName.MANAGER_KEY.getName(), mgrKey, user);
		//} catch (ValidationFailedException e) {
		//	// TODO Auto-generated catch block
		//	e.printStackTrace();
		//} catch (UserModifyException e) {
		//	// TODO Auto-generated catch block
		//	e.printStackTrace();
		//} catch (NoSuchUserException e) {
		//	// TODO Auto-generated catch block
		//	e.printStackTrace();
		//} catch (SearchKeyNotUniqueException e) {
		//	// TODO Auto-generated catch block
		//	e.printStackTrace();
		//} catch (AccessDeniedException e) {
		//	// TODO Auto-generated catch block
		//	e.printStackTrace();
		//}
		
		
	//}
	
	

	private int findSemicolon(String input){
		
		for(int i = 0; i < input.length(); i++){
			if(input.charAt(i) == ';'){
				return i;
			}
		}
	
	
		return 0;
	 }
	
   
	public void setAttributes() {
		// TODO Auto-generated method stub
		
	}

	public HashMap getAttributes() {
		// TODO Auto-generated method stub
		return null;
	}

}
