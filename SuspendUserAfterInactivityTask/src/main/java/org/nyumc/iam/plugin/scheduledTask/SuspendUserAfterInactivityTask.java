package org.nyumc.iam.plugin.scheduledTask;

//~--- non-JDK imports --------------------------------------------------------

import oracle.iam.identity.exception.NoSuchUserException;
import oracle.iam.identity.exception.SearchKeyNotUniqueException;
import oracle.iam.identity.exception.UserDisableException;
import oracle.iam.identity.exception.UserLookupException;
import oracle.iam.identity.exception.UserManagerException;
import oracle.iam.identity.exception.UserModifyException;
import oracle.iam.identity.exception.ValidationFailedException;
import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.api.UserManagerConstants;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.identity.vo.Identity;
import oracle.iam.platform.Platform;
import oracle.iam.platform.authz.exception.AccessDeniedException;
import oracle.iam.platform.entitymgr.vo.SearchCriteria;
import oracle.iam.scheduler.vo.StoppableTask;
import oracle.iam.scheduler.vo.TaskSupport;

//~--- JDK imports ------------------------------------------------------------




import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.sql.DataSource;

import Thor.API.tcResultSet;
import Thor.API.Exceptions.tcAPIException;
import Thor.API.Exceptions.tcColumnNotFoundException;
import Thor.API.Exceptions.tcITResourceNotFoundException;
import Thor.API.Operations.tcITResourceInstanceOperationsIntf;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;
import java.util.logging.Level;

/**
 * Class description
 *
 *
 * @version        1.0.0, 07/17/2017
 * @author         Shine Thomas, Focal Point LLC
 */
public class SuspendUserAfterInactivityTask extends TaskSupport implements StoppableTask {
    private static final Logger logger = Logger.getLogger(SuspendUserAfterInactivityTask.class.getName());
    private StringBuffer results = new StringBuffer();
    private String crlf = "\r\n";
    List<String> userList = new ArrayList<String>();
    HashMap<Object,Object> userMap = new HashMap<Object,Object>();
    UserManager             usrMgr         = Platform.getService(UserManager.class);
    //Map<String,String> userMap = new HashMap<String,String>();
    
    String resultsTo = "";
	String smtpServer = "";
	String resultsFrom = "";
	String scheduledJobName = "";
	
    /**
     * Method description
     *
     *
     * @param attributes
     *
     * @throws Exception
     */
	public void execute(HashMap attributes) throws Exception {
    	String methodName="execute";
    	logger.logp(Level.FINE, getClass().getName(), methodName,"entering");
    	System.out.println("Inside execute method of SuspendUserAfterInactivityTask");
    	scheduledJobName = getName();
        
        logger.logp(Level.INFO, getClass().getName(), methodName,
            "MIA-DUT-001 Scheduled Job: " + scheduledJobName);
            results.append(crlf + "Results from Scheduled Job: " + scheduledJobName);
        		
        //List getUserswithSuspendFlag 
		System.out.println("Before calling getUsers inside UpdateSuspend Flag method");
		userMap = getUsers();
		System.out.println("After calling getUsers inside UpdateSuspend Flag method");
		HashMap<String, Object>  updateAttrs = new HashMap<String, Object>();
		Iterator it = userMap.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry pair = (Map.Entry)it.next();
	        String UserSuspensionStatusCurrent=pair.getValue().toString();
	        if(UserSuspensionStatusCurrent.equalsIgnoreCase("Y")){
				UserSuspensionStatusCurrent="YES";
			}
			else if(UserSuspensionStatusCurrent.equalsIgnoreCase("N")){
				UserSuspensionStatusCurrent="NO";
			}
			else if(UserSuspensionStatusCurrent.equalsIgnoreCase("T")){
				UserSuspensionStatusCurrent="TER";
			}
	        
			/*updateAttrs.put("UserSuspended",UserSuspensionStatusCurrent);
			User user = new User(pair.getKey().toString(), updateAttrs);
			usrMgr.modify(UserManagerConstants.AttributeName.USER_LOGIN.getId(), pair.getKey().toString(), user);*/
	        
	        HashMap<String, Object>  updatedAttrs = new HashMap<String, Object>();
	        String strUserLogin=pair.getKey().toString();
	        
	        System.out.println("User Login is------->"+strUserLogin+"----> and the flag associated with the user is:"+UserSuspensionStatusCurrent);
	        updatedAttrs.put("UserSuspended",UserSuspensionStatusCurrent);
			User user = new User(strUserLogin.toString(), updatedAttrs);
			try {
				System.out.println("before updating the attribute");
				usrMgr.modify(UserManagerConstants.AttributeName.USER_LOGIN.getId(), strUserLogin, user);
				System.out.println("after updating the attribute");
			} catch (ValidationFailedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UserModifyException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchUserException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SearchKeyNotUniqueException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (oracle.iam.platform.authz.exception.AccessDeniedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	        
			System.out.println("User "+pair.getKey() + " - ***** modified with Value in the User Suspended Flag: " + pair.getValue());
	        
	    }
        

        
    }

    /**
     * Method description
     *
     *
     * @return
     */
    @SuppressWarnings("rawtypes")
	public HashMap getAttributes() {
        return null;
    }

    /**
     * Method description
     *
     */
    public void setAttributes() {}

	private HashMap getUsers() {
    	String methodName="getUsers";
    	System.out.println("Inside getUsers method to find all users whose USR_UDF_USERSUSPENDED flag is set");
    	List<User> userswithsuspendflag     = null;
    	logger.logp(Level.FINE, getClass().getName(), methodName,"entering");
    	usrMgr         = Platform.getService(UserManager.class);
        //List                    userList       = new ArrayList<String>();
        HashMap<String, Object> configParams   = new HashMap<String, Object>();
        HashMap<Object, Object> userMap		   = new HashMap<Object, Object>();	
        int                     startRow       = 1;
        List<String> listOfStatus =  new ArrayList<String>();
        listOfStatus.add("Y");
        listOfStatus.add("N");
        listOfStatus.add("T");
        //SearchCriteria          Criteria = new SearchCriteria("UserSuspended",null,SearchCriteria.Operator.NOT_EQUAL);
        SearchCriteria          Criteria = new SearchCriteria("UserSuspended",listOfStatus,SearchCriteria.Operator.IN);
        //SearchCriteria          Criteria          = new SearchCriteria("UserSuspended", "Y",SearchCriteria.Operator.EQUAL);
        Set<String>            retAttrs        = new HashSet<String>();

        retAttrs.add(UserManagerConstants.AttributeName.USER_LOGIN.getId());
        retAttrs.add("UserSuspended");
       
        try {
            
        	userswithsuspendflag = usrMgr.search(Criteria, retAttrs, configParams);
        	System.out.println("Size of  user population"+userswithsuspendflag.size());
            boolean    hasMore = true;

          
                       

               
                    for (Identity identity : userswithsuspendflag) {
                        if (identity.getEntityId() != null) {
                            
                            String userLogin = (String) identity.getAttribute(UserManagerConstants.AttributeName.USER_LOGIN.getId());
                            System.out.println("user login from search is:"+userLogin);
                            String  suspendFlag = (String) identity.getAttribute("UserSuspended");
                            System.out.println("user suspend flag from search is:"+suspendFlag);
                            //logger.logp(Level.FINEST, getClass().getName(), methodName,
                                        
                            userMap.put(userLogin,suspendFlag);
                        }
                        }
                   
            
                   
                           
        } catch (UserManagerException e) {

        	logger.logp(Level.SEVERE, getClass().getName(), methodName,
        		"MIA-DUT-012 UserManagerException"+e.getMessage());
        	results.append(crlf + "UserManagerException"+e.getMessage());
        }

        return userMap;
    }

	
	
}
